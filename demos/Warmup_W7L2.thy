theory Warmup_W7L2
imports Main "HOL-Library.Monad_Syntax"
begin

  (*
    Last lecture: Monads
    
    \<alpha> M
    return, bind
  *)

  (* Option Monad: propagate errors *)
    
  thm Option.bind.simps

  
  definition "assert P \<equiv> if P then Some () else None"
  
  lemma assert_simps[simp]:
    "assert False = None"
    "assert True = Some ()"
    by (auto simp: assert_def)
  

  (* When is a program 'correct': 
    does not fail, and result satisfies specification
  *)
    
  fun wp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "wp None Q = False"
  | "wp (Some v) Q = Q v"  

  
  term "wp m Q"
  
  lemma wp_cons: 
    assumes "wp c Q'"
    assumes "\<And>r. Q' r \<Longrightarrow> Q r"
    shows "wp c Q"
    using assms by (cases c) auto
  
  
  lemma wp_bind[simp]: 
    "wp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> wp m (\<lambda>x. wp (f x) Q)"
    by (cases m; auto)

  lemma wp_assert[simp]: "wp (assert P) Q \<longleftrightarrow> P \<and> Q ()"
    by (cases P) auto
    
  (* Very simple model of file system: *)
    
  type_synonym fs = "string \<Rightarrow> (string \<times> bool) option"

  definition lookup :: "('k \<rightharpoonup> 'v) \<Rightarrow> 'k \<Rightarrow> 'v option"
    where "lookup fs n \<equiv> fs n"
  
  lemma wp_lookup[simp]:
    "wp (lookup fs n) Q \<longleftrightarrow> n\<in>dom fs \<and> (\<forall>v. fs n = Some v \<longrightarrow> Q v)"
    unfolding lookup_def
    apply (cases "fs n") 
    by auto
  
  definition "create n fs \<equiv> do {
    assert (n\<notin>dom fs);
    Some (fs(n\<mapsto>([],False)))
  }"
    
  definition "open n fs \<equiv> do {
    (d,op) \<leftarrow> lookup fs n;
    assert (\<not>op);
    Some (fs(n\<mapsto>(d,True)))
  }"
  
  definition "close n fs \<equiv> do {
    (d,op) \<leftarrow> lookup fs n;
    assert (op);
    Some (fs(n\<mapsto>(d,False)))
  }"
  
  definition "read n fs \<equiv> do {
    (d,op) \<leftarrow> lookup fs n;
    assert (op);
    Some d
  }"

  definition "write n d fs \<equiv> do {
    (_,op) \<leftarrow> lookup fs n;
    assert (op);
    Some (fs(n\<mapsto>(d,op)))
  }"
    
  
  definition "open_files fs = {n. \<exists>d. fs n = Some (d,True) }"
  definition "data fs = map_option fst o fs"

  
  lemma create_rl: "n \<notin> dom (data fs) \<Longrightarrow> wp 
    (create n fs) 
    (\<lambda>fs'. 
      data fs' = data fs(n\<mapsto>[])
    \<and> open_files fs' = open_files fs  
    )"
    unfolding create_def data_def open_files_def
    by auto
    
  lemma open_rl: "\<lbrakk> n \<in> dom (data fs); n\<notin> open_files fs \<rbrakk> 
    \<Longrightarrow> wp (open n fs) (\<lambda>fs'. 
      data fs' = data fs
    \<and> open_files fs' = insert n (open_files fs)
    )"
    unfolding open_def data_def open_files_def
    by auto
    
  lemma close_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> \<Longrightarrow>
    wp (close n fs) (\<lambda>fs'.
      data fs' = data fs
    \<and> open_files fs' = open_files fs - {n}
    )
  "  
    unfolding close_def data_def open_files_def
    by auto
    
  lemma read_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
    \<Longrightarrow> wp (read n fs) (\<lambda>d. data fs n = Some d)"
    unfolding read_def data_def open_files_def
    by auto
    
  lemma write_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
    \<Longrightarrow> wp (write n d fs) (\<lambda>fs'. 
      data fs' = (data fs)(n\<mapsto>d)
    \<and> open_files fs' = open_files fs  
  )"
    unfolding write_def data_def open_files_def
    by auto

    
  (* Derived operations *)  
    
  (* Read, open/close if not yet open *)
  definition "read' n fs \<equiv> 
    if n\<in>open_files fs then do {
      d \<leftarrow> read n fs;
      Some (d,fs)
    } else do {
      fs \<leftarrow> open n fs;
      d \<leftarrow> read n fs;
      fs \<leftarrow> close n fs;
      Some (d,fs)
    }
  "

  lemma read'_rl: "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
    \<Longrightarrow> wp (read' n fs) (\<lambda>(d,fs'). 
      data fs n = Some d \<and>
      open_files fs' = open_files fs \<and>
      data fs' = data fs
    )"
    unfolding read'_def
    apply auto
    apply (rule wp_cons)
    apply (rule read_rl)
    apply simp
    apply simp
    apply (rule wp_cons, rule open_rl)
    apply auto []
    apply simp
    apply (rule wp_cons, rule read_rl)
    apply simp
    apply (rule wp_cons, rule close_rl)
    apply simp
    apply simp
    done
    

  (* A bit more automation *)    
  lemma "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
    \<Longrightarrow> wp (read' n fs) (\<lambda>(d,fs'). 
      data fs n = Some d \<and>
      open_files fs' = open_files fs \<and>
      data fs' = data fs
    )"
    unfolding read'_def
    by (
      rule wp_cons, rule open_rl read_rl close_rl 
    | (auto)[] )+
          

  (* Now back to set monad *)  


end
