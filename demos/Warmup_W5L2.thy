theory Warmup_W5L2
imports Main
begin
  (* Very simple PDA, just state and stack, no alphabet *)

  type_synonym Q = nat
  type_synonym \<Gamma> = int
  
  type_synonym pda = "(Q \<times> \<Gamma> \<times> Q \<times> \<Gamma> list) set"
  
  type_synonym state = "Q \<times> \<Gamma> list"
  
  context
    fixes \<delta> :: pda
  begin
  
    inductive step :: "state \<Rightarrow> state \<Rightarrow> bool" where
      "(p,\<gamma>,q,w)\<in>\<delta> \<Longrightarrow> step (p,\<gamma>#\<gamma>s) (q,w@\<gamma>s)"
  
    (* Sequence of transitions can be repeated if some stack symbols are added to bottom *)
    lemma "step\<^sup>*\<^sup>* (p,\<gamma>s) (q,\<gamma>s') \<Longrightarrow> step\<^sup>*\<^sup>* (p,\<gamma>s@\<gamma>sb) (q,\<gamma>s'@\<gamma>sb)"
    proof (induction rule: converse_rtranclp_induct2)
      case refl
      then show ?case by auto
    next
      case (step p \<gamma>s p\<^sub>1 \<gamma>s\<^sub>1)

      from step.hyps(1) obtain \<gamma> \<gamma>ss w\<^sub>1 where "\<gamma>s = \<gamma>#\<gamma>ss" "(p,\<gamma>,p\<^sub>1,w\<^sub>1)\<in>\<delta>" "\<gamma>s\<^sub>1 = w\<^sub>1@\<gamma>ss"
        by rule auto (* \<leftarrow> rule inversion! *)
      hence "step (p,\<gamma>s@\<gamma>sb) (p\<^sub>1,\<gamma>s\<^sub>1@\<gamma>sb)"
        by (auto intro: step.intros)
      with step.IH show ?case by simp
    qed

    lemma 
      assumes "step\<^sup>*\<^sup>* (p,\<gamma>s) (q,\<gamma>s')" 
      shows "step\<^sup>*\<^sup>* (p,\<gamma>s@\<gamma>sb) (q,\<gamma>s'@\<gamma>sb)"
      using assms
    proof (induction rule: converse_rtranclp_induct2)
      case refl
      then show ?case by auto
    next
      case (step p \<gamma>s p\<^sub>1 \<gamma>s\<^sub>1)
      
      thm step.prems
      thm step.hyps step.IH
      thm step

      from step.hyps(1) obtain \<gamma> \<gamma>ss w\<^sub>1 where "\<gamma>s = \<gamma>#\<gamma>ss" "(p,\<gamma>,p\<^sub>1,w\<^sub>1)\<in>\<delta>" "\<gamma>s\<^sub>1 = w\<^sub>1@\<gamma>ss"
        by rule auto (* \<leftarrow> rule inversion! *)
      hence "step (p,\<gamma>s@\<gamma>sb) (p\<^sub>1,\<gamma>s\<^sub>1@\<gamma>sb)"
        by (auto intro: step.intros)
      with step.IH show ?case by simp
    qed
    
        
  end
end
