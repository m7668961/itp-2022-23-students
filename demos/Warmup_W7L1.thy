theory Warmup_W7L1
imports Graph_Lib RBTS_Lib While_Lib
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  (*
    Last year:
      Isabelle basics
      
      ADTs 
        \<rightarrow> Separate data-structure (and proof) from algorithm using it
      
      While-Loops and wf-relation for termination
  
      Example: DFS  
      
  *)


  section \<open>Algorithm\<close>
  
  

  text \<open>We implicitly fix a graph, and a start node. 
    This is just to make things more readable.
  \<close>
  context
    fixes gi :: "'v::linorder rbt_graph" and v\<^sub>0 :: 'v
  begin

  definition "dfs_aux \<equiv> 
    let d = rbts_empty in
    let w = [v\<^sub>0] in
    while_option (\<lambda>(d,w). w\<noteq>[]) (\<lambda>(d,w). 
      let (v,w) = (hd w, tl w) in
      if rbts_member v d then (d,w)
      else 
        let d = rbts_insert v d in
        let w = rg_succs gi v @ w in
        (d,w)
    
    ) (d,w)
  "

  definition "dfs \<equiv> fst (the (dfs_aux))"

  
  section \<open>Correctness Proof\<close>
  
    abbreviation (input) "g \<equiv> rg_\<alpha> gi"
  
    text \<open>Correctness can only be shown under the assumption that 
      only finitely many nodes are reachable from the start node!

      Fortunately, red-black trees can only hold finitely many nodes,
      and each successor list can only be finite. Thus, the graph is finite,
      and so is the set reachable from any node.
    \<close>
    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" using rg_finite_ran by simp
      finally (finite_subset) show ?thesis .
    qed        
      
    (*
      Invariant for DFS:
      
      \<^item> v\<^sub>0 is in d \<union> w
      
      \<^item> everything in d and w is reachable
      
      \<^item> making one step from d ends you in d \<union> w
    *)

    definition "dfs_invar \<equiv> \<lambda>(d,w). 
      v\<^sub>0\<in>rbts_\<alpha> d \<union> set w
    \<and> rbts_\<alpha> d \<union> set w \<subseteq> g\<^sup>*``{v\<^sub>0}
    \<and> g``rbts_\<alpha> d \<subseteq> rbts_\<alpha> d \<union> set w
    "

    text \<open>The exact form of the following auxiliary lemmas has been determined 
      during the below proof attempt\<close>
    
    
    lemma invar_init: "dfs_invar (rbts_empty,[v\<^sub>0])"
      unfolding dfs_invar_def
      by (simp)
    
    lemma invar_step1: 
      "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<in>rbts_\<alpha> d \<Longrightarrow> dfs_invar (d,tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply auto
      done
      
    lemma invar_step2: 
      "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<notin>rbts_\<alpha> d 
      \<Longrightarrow> dfs_invar (rbts_insert (hd w) d, rg_succs gi (hd w) @ tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply (auto)
      done
      
    (* Note: if set is closed by step, it's refl-transitively closed *)
    thm Image_closed_trancl
    
    lemma invar_final: "dfs_invar (d,[]) \<Longrightarrow> rbts_\<alpha> d = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_invar_def
      apply (auto)
      thm Image_closed_trancl
      by (metis Image_closed_trancl rev_ImageI)


    (*
      Termination: either the unfinished reachable nodes decrease, or
        they stay the same, and the length of the worklist decreases:
    *)  
    definition "dfs_wfrel \<equiv> 
      inv_image 
        (inv_image finite_psubset (\<lambda>d. g\<^sup>*``{v\<^sub>0} - d) <*lex*> measure length) 
        (apfst rbts_\<alpha>)"
        
    lemma dfs_wfrel_wf: "wf dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto
      
    lemma dfs_wfrel1: "w \<noteq> [] \<Longrightarrow> ((d, tl w), (d, w)) \<in> dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto

    lemma dfs_wfrel2: 
      assumes "dfs_invar (d, w)" "w \<noteq> []" "hd w \<notin> rbts_\<alpha> d"
      shows "((rbts_insert (hd w) d,w'),(d,w)) \<in> dfs_wfrel"
    proof -  
      
      have "hd w\<in>g\<^sup>*``{v\<^sub>0}" 
        using assms(1,2)
        unfolding dfs_invar_def
        by (auto simp: neq_Nil_conv)
      with \<open>hd w \<notin> rbts_\<alpha> d\<close> show ?thesis
        unfolding dfs_wfrel_def
        using finite_reachable
        by auto
        
    qed    
      
    thm while_option_rule'
    
    lemma dfs_aux_correct: "\<exists>d. dfs_aux = Some (d,[]) \<and> rbts_\<alpha> d = g\<^sup>*``{v\<^sub>0}"
      unfolding dfs_aux_def
      apply simp
      apply (rule while_option_rule'[where P=dfs_invar and r=dfs_wfrel])
      subgoal by (rule invar_init)
      subgoal by (auto simp: invar_step1 invar_step2)
      subgoal by (auto simp: invar_final)
      
      subgoal by (rule dfs_wfrel_wf)
      subgoal by (auto simp: dfs_wfrel1 dfs_wfrel2)
      done

    lemma dfs_correct: "rbts_\<alpha> (dfs) = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_def using dfs_aux_correct by force

      
    section \<open>Running the thing\<close>
    value "rbts_to_list (Warmup_W7L1.dfs 
      (rg_from_lg [(0,1),(1,2),(2,1),(2,3),(4,5),(3,4)]) 
      (0::nat))"
      
    value "let n=10000 in \<not>rbts_is_empty (Warmup_W7L1.dfs 
      (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat))"
    value "let n=100000 in \<not>rbts_is_empty (Warmup_W7L1.dfs 
      (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat))"
  

end
