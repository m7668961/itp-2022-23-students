theory DFS_Recursive_Demo
imports Graph_Lib RBTS_Lib While_Lib
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  section \<open>Fold-Rule\<close>
  text \<open>Same as always, needed for inner fold\<close>

  lemma fold_invariant_rule_aux:
    assumes INIT: "I {} s" \<comment> \<open>Initially, no elements are processed\<close>
    assumes STEP: "\<And>x it s. \<lbrakk> it\<subseteq>set xs; x\<in>set xs; I it s \<rbrakk> \<Longrightarrow> I (insert x it) (f x s)"
      \<comment> \<open>Invariant is preserved by processing an element from the list\<close>
    assumes CONS: "I (set xs) (fold f xs s) \<Longrightarrow> Q"
      \<comment> \<open>Subgoal follows from invariant, if all elements of the list have been processed\<close>
    shows "Q"
  proof -
  
    have "I (it\<union>set xs') (fold f xs' s)" if "I it s" "it \<union> set xs' \<subseteq> set xs" for it xs'
      using that
    proof (induction xs' arbitrary: it s)
      case Nil
      then show ?case by simp
    next
      case (Cons x xs')
      
      note IH' = Cons.IH[of "insert x it", simplified]
      
      show ?case
        apply simp
        apply (rule IH')
        apply (rule STEP)
        using Cons.prems
        by auto
      
    qed
    from this[of "{}", OF INIT] CONS show ?thesis by simp
  qed
  
  lemma fold_invariant_rule:
    assumes INIT: "I {} s" \<comment> \<open>Initially, no elements are processed\<close>
    assumes STEP: "\<And>x it s. \<lbrakk> it\<subseteq>set xs; x\<in>set xs; I it s \<rbrakk> \<Longrightarrow> I (insert x it) (f x s)"
      \<comment> \<open>Invariant is preserved by processing an element from the list\<close>
    assumes CONS: "\<And>s'. I (set xs) s' \<Longrightarrow> Q s'"
      \<comment> \<open>Subgoal follows from invariant, if all elements of the list have been processed\<close>
    shows "Q (fold f xs s)"
    using fold_invariant_rule_aux[of I] assms by blast




  context
    fixes gi :: "'v::linorder rbt_graph" and v\<^sub>0 :: 'v
  begin

    abbreviation (input) "g \<equiv> rg_\<alpha> gi"
  
    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" using rg_finite_ran by simp
      finally (finite_subset) show ?thesis .
    qed        
  

    
    (** Recursive formulation: beyond Isabelle's capabilities for (manageable) termination proof! 
      
      We use "fuel" trick here: artificially bound maximum number of iterations
    *)
    
    fun dfsr where
      "dfsr 0 u d = insert u d"
    | "dfsr (Suc fuel) u d = (
        if u\<in>d then d
        else fold (dfsr fuel) (rg_succs gi u) (insert u d)
      )"  

      
    (** Warning: 
      This proof is more intricate and complex than the proof of the iterative algorithm.
      
      After guessing the correct generalization for the induction, 
      and the invariant for the inner loop, I basically forced it through using auto and sledgehammer.
      
      I had to adjust the inner loop invariant a few times because I overlooked corner cases.
      
      Also note that I did not clean up this proof (except for adding comments). 
      It's in the state after proof exploration, and would usually get cleaned up now.
    *)

    
    (*
      The main concept used in the invariant is reachability without using 
      the done-set of nodes. It's phrased as:
    *)
    term "(E\<inter>(-d)\<times>UNIV)\<^sup>*"
    
    
    (* Auxiliary lemmas *)

    (* If v reachable from u, then u needs not be used again after first edge *)          
    lemma path_aux: "(u,v)\<in>E\<^sup>* \<Longrightarrow> v\<noteq>u \<Longrightarrow> (u,v)\<in>E O ((E\<inter>(-{u})\<times>UNIV))\<^sup>*"
      apply (induction rule: rtrancl_induct)
      apply auto
      by (smt (verit, ccfv_threshold) Compl_Times_UNIV2 Compl_iff Int_insert_right_if1 insert_iff insert_not_empty mem_Sigma_iff mk_disjoint_insert relcomp.simps rtrancl.simps)
    
    (* Reachability: make one step, and then don't use start node again. Similar to path_aux, but for set of reachable nodes. *)
    lemma reach_step_unfold: "E\<^sup>*``{u} = insert u ((E\<inter>((-{u}) \<times> UNIV))\<^sup>*``E``{u})"  
      apply auto
      subgoal using path_aux by fastforce
      subgoal by (metis Int_Un_eq(1) converse_rtrancl_into_rtrancl in_rtrancl_UnI)
      done

    (*
      If we can reach v from u, but cannot reach it when only using nodes from d,
      then there is a path from u to v over a node x\<notin>d.
      
      (Note the meaning of d here. 
        Below, we'll always use -d, but that doesn't matter for this lemma,
        so we choose the easier formulation.
      )
    *)
    lemma find_node_aux:
      assumes "(u,v)\<in>E\<^sup>*" "(u,v)\<notin>(E\<inter>(d\<times>UNIV))\<^sup>*"
      obtains x where "(u,x)\<in>E\<^sup>*" "x\<notin>d" "(x,v)\<in>E\<^sup>*"
      using assms
      apply (induction rule: converse_rtrancl_induct)
      apply auto
      by (metis Int_iff SigmaI iso_tuple_UNIV_I rtrancl.rtrancl_refl rtrancl_into_trancl2 trancl_into_rtrancl)
      
    (*
      Hardest proof goal we got stuck with in main proof.
    
      Intuitively: when the inner loop explores all reachable nodes from v,
      it will not use the nodes from d', which is 
        insert u d \<union> \<open>nodes reachable from the successors that we have already iterated over\<close>. 

      We have to show that this is the same set of nodes that can be reached when not using \<open>insert u d\<close>.
      
      Proof sketch: 
        when we ever hit a node reachable from a successor we have already iterated over,
        all its successors are already explored, and we can stop exploring.
          
    *)  
    lemma dfsr_aux:
      assumes "it \<subseteq> E``{u}" "(u,v)\<in>E" "d' = insert u d \<union> (E \<inter> (-insert u d) \<times> UNIV)\<^sup>* `` it" "u\<notin>d"
      shows "
        d' \<union> (E \<inter> (- d') \<times> UNIV)\<^sup>* `` {v} 
        = insert u d \<union> (E \<inter> (-insert u d) \<times> UNIV)\<^sup>* `` insert v it" 
        (is "?L = ?R") 
    proof -
      have "d' \<subseteq> ?R" using assms by auto
      moreover have "(E \<inter> (- d') \<times> UNIV)\<^sup>* `` {v} \<subseteq> ?R"
        using assms
        apply auto
        by (smt (verit, ccfv_SIG) Compl_Int ImageI Int_Un_distrib Sigma_Un_distrib1 Un_Int_eq(1) assms(3) in_rtrancl_UnI insertI1)
      moreover have "insert u d \<subseteq> ?L" using assms by auto
      moreover have "(E \<inter> (- insert u d) \<times> UNIV)\<^sup>* `` insert v it \<subseteq> ?L" 
      proof -
      
        have A: "v'\<in>d'" if "v\<in>d'" "(v,v')\<in>(E \<inter> (- insert u d) \<times> UNIV)\<^sup>*" for v v'
          using that assms apply auto 
          subgoal 
            by (metis Compl_disjoint IntD2 converse_rtranclE insert_disjoint(2) mem_Sigma_iff)
          subgoal
            by (smt (verit, ccfv_SIG) Compl_disjoint Int_lower2 converse_rtranclE disjoint_iff in_mono insert_disjoint(2) mem_Sigma_iff)
          subgoal 
            by (meson Image_iff rtrancl_trans)
          done
      
        have "(v, x) \<in> (E \<inter> (- d') \<times> UNIV)\<^sup>*" if "(v, x) \<in> (E \<inter> (- insert u d) \<times> UNIV)\<^sup>*" "x \<notin> d'" for x
        proof (rule ccontr)
          assume "(v, x) \<notin> (E \<inter> (- d') \<times> UNIV)\<^sup>*"
          then obtain y 
            where 1: "y\<in>d'-insert u d" "(v, y) \<in> (E \<inter> (- insert u d) \<times> UNIV)\<^sup>*" "(y,x)\<in>(E \<inter> (- insert u d) \<times> UNIV)\<^sup>*"
            using find_node_aux[OF \<open>(v, x) \<in> (E \<inter> (- insert u d) \<times> UNIV)\<^sup>*\<close>, of "-((E \<inter> (-insert u d) \<times> UNIV)\<^sup>* `` it)"]
            apply auto
            by (metis A Compl_Un Int_assoc Sigma_Int_distrib1 UnCI \<open>x \<notin> d'\<close> assms(3))
            
          have "x\<in>d'" 
            apply (rule A[of y])
            using 1 by auto
          with \<open>x\<notin>d'\<close> show False ..
        qed    
        then show ?thesis
        using assms(3) by auto
      qed
      ultimately  
      show ?thesis by blast
    qed
      

    (**
      Main lemma:
      
      If we have enough fuel (more than number of reachable nodes),
      we return the correct result.
    *)      
    lemma dfsr_correct:
      assumes "card ((rg_\<alpha> gi)\<^sup>*``{v\<^sub>0}) \<le> fuel"
      shows "dfsr fuel v\<^sub>0 {} = (rg_\<alpha> gi)\<^sup>*``{v\<^sub>0}"  
    proof -

      define reachable where "reachable = (rg_\<alpha> gi)\<^sup>*``{v\<^sub>0}"
    
      have [simp, intro!]: "finite reachable" 
        unfolding reachable_def using finite_reachable .

      (*
        Generalization:
        
          dfsr fuel u d will explore the nodes reachable from u without using nodes in d.
      *)  
              
      have "dfsr fuel u d = d \<union> (rg_\<alpha> gi \<inter> (-d) \<times> UNIV)\<^sup>*``{u}"
        if "d \<subseteq> reachable" "u\<in>reachable" "fuel \<ge> card (reachable - d)"
        for u d
        using that
      proof (induction fuel arbitrary: u d)
        case 0
        then show ?case apply auto 
          by (metis ComplD IntD2 converse_rtranclE mem_Sigma_iff)
      next
        case (Suc fuel)
        show ?case
        proof cases
          assume "u\<in>d" \<comment> \<open>Node already seen\<close>
          thus ?thesis apply auto 
            by (metis Compl_iff IntD2 converse_rtranclE mem_Sigma_iff)
        next
          assume "u\<notin>d" \<comment> \<open>Node not yet seen\<close>
          hence "dfsr (Suc fuel) u d = fold (dfsr fuel) (rg_succs gi u) (insert u d)" by simp
          also have "\<dots> = d \<union> (rg_\<alpha> gi \<inter> (-d) \<times> UNIV)\<^sup>*``{u}" 
          proof -
            have IH: "dfsr fuel u' d' = d' \<union> (rg_\<alpha> gi \<inter> (-d') \<times> UNIV)\<^sup>* `` {u'}"
              if "insert u d \<subseteq> d'" "d' \<subseteq> reachable" "u'\<in>reachable" for d' u'
              apply (rule Suc.IH)
              using that Suc.prems \<open>u\<notin>d\<close>
              apply auto  
              by (smt (verit, best) \<open>finite reachable\<close> card_Diff_subset card_mono card_seteq finite_subset le_diff_iff' not_less_eq_eq order_trans)

            (*
              At this point, we start the inner loop, iterating over the successors, and
              incrementing the set of explored nodes.
              
              We show that we have explored the nodes reachable from anything we have already iterated over.
              
              The only complication is that we have to show that not using nodes explored by previously
              iterated-over successors for the next successor is ok. (see aux-lemma dfsr_aux)
            *)
            
                            
            show ?thesis  
              apply (rule fold_invariant_rule[where I="\<lambda>it d'. d' = insert u d \<union> (rg_\<alpha> gi \<inter> (-insert u d) \<times> UNIV)\<^sup>*``it"])
              subgoal by simp
              subgoal for v it d'
                apply (subst IH)
                subgoal by auto
                subgoal
                  using Suc.prems
                  unfolding reachable_def
                  apply auto
                  by (metis Image_singleton_iff Int_Un_eq(1) converse_rtrancl_into_rtrancl in_mono in_rtrancl_UnI rtrancl_trans)
                subgoal
                  using Suc.prems
                  unfolding reachable_def
                  by auto 
                subgoal 
                  using dfsr_aux[of it "rg_\<alpha> gi" u v d' d] \<open>u \<notin> d\<close>
                  by simp
                done  
                  
              subgoal for s'
                apply (subst reach_step_unfold)
                apply auto 
                subgoal 
                  by (smt (verit, best) ComplI ImageI Image_singleton_iff IntI SigmaI Sigma_Int_distrib1 UNIV_I \<open>u \<notin> d\<close> boolean_algebra.de_Morgan_disj inf_aci(1) inf_aci(3) insert_def singleton_conv)
                subgoal 
                  by (metis Compl_Un Sigma_Int_distrib1 Un_empty_right Un_insert_right inf_assoc rev_ImageI singleton_iff)
                done  
              done      
          
          qed
          finally show ?thesis by simp
        qed  
      qed  
      from this[of "{}" v\<^sub>0] assms show ?thesis 
        unfolding reachable_def by simp
      
    qed    
  
  end


end
