theory Warmup_W8L2
imports 
  Main "HOL-Library.Monad_Syntax"
begin

  text \<open>Let's define the \<open>3n+1\<close> sequence (also known as Collatz sequence). 
  
      \<open>s n = if even n then s (n div 2) else s (3*n+1)\<close>

    It's an open problem whether this sequence always ends in the cycle \<open>1\<rightarrow>4\<rightarrow>1\<rightarrow>4\<rightarrow>\<dots>\<close>, or
    if there are start values for which it reaches other cycles, or increases without a bound.
    
    \<^url>\<open>https://en.wikipedia.org/wiki/Collatz_conjecture\<close>  
  \<close>

  (* First attempt: fuel *)
  
  definition collatz_step :: "nat\<Rightarrow>nat" 
    where "collatz_step n \<equiv> if even n then (n div 2) else (3*n+1)"
 
  (* f x = f x + 1 *)
    
  function collatz0 where
    "collatz0 n = (if n=1 then [n] else n#collatz0 (collatz_step n))"
    by pat_completeness auto
  termination
    apply (relation r)
    oops  
    
  fun collatz1 where
    "collatz1 0 n = []"
  | "collatz1 (Suc fuel) n = (if n=1 then [n] else n#collatz1 fuel (collatz_step n))"
    

  (* OK, this is somehow usable. But we have to provide enough fuel. *)
  value "collatz1 50 23"
  value "collatz1 112 27"


  (* Second attempt: partial_function + option-monad: None means nontermination *)
  
  partial_function (option) collatz2 :: "nat \<Rightarrow> nat list option" where
    [code]: "collatz2 n = (
      if n=1 then Some [n] 
      else do { 
        seq\<leftarrow>collatz2 (collatz_step n); 
        Some (n#seq) 
      })"

  thm collatz2.simps      
    
  value "collatz2 23"
  value "collatz2 27"

  (*
    Recall: while-option, returned None if iteration does not terminate!
    
    partial_function is more general!
      \<^item> recursion is in monad itself
      \<^item> recursion needs not be tail-recursive
  
  *)

  
  (* How to prove facts about partial_function? *)
  
  find_theorems collatz2
  
  thm collatz2.simps \<comment> \<open>Unfold lemma (matches recursion equation we specified)\<close>
  thm collatz2.fixp_induct \<comment> \<open>Induction lemma. 
    The lemma looks a bit strange, but can be read as:
  
    We can prove properties for terminating arguments by induction!
  \<close>
  
  (* First step: rename the lemma, to make it more readable. 
    (The heuristics of partial_function for variable naming did a particularly bad job here ;) ) 
  *)
  
  print_statement collatz2.fixp_induct
  
  theorem collatz2_induct:
    fixes P :: "(nat \<Rightarrow> nat list option) \<Rightarrow> bool"
    assumes "option.admissible P"
      and "P (\<lambda>_. None)"
      and "\<And>rec_invocation. P rec_invocation \<Longrightarrow> P (\<lambda>n. if n = 1 then Some [n] else do {
          seq \<leftarrow> rec_invocation (collatz_step n);
          Some (n # seq)
        })"
    shows "P collatz2"  
    using assms
    by (rule collatz2.fixp_induct)

  (* Second step: rewrite to precondition form *)  
       
  (* Weakest liberal precondition: If program terminates, then postcondition holds.
  
    cf. wp m Q - Program terminates and postcondition holds
  *)
  fun wlp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "wlp None _ \<longleftrightarrow> True"
  | "wlp (Some x) Q \<longleftrightarrow> Q x"  

  lemma wlp_bind[simp]: "wlp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> wlp m (\<lambda>x. wlp (f x) Q)"
    by (cases m) auto
  
  lemma wlp_cons: 
    assumes "wlp m Q'"
    assumes "\<And>x. Q' x \<Longrightarrow> Q x"
    shows "wlp m Q"  
    using assms by (cases m) auto
    
  
  lemma wlp_arg_iff: "(\<forall>x. wlp (f x) (P x)) = (\<forall>x y. f x = Some y \<longrightarrow> P x y)"
    by (metis wlp.elims(3) wlp.simps(2))

     
  lemma collatz2_wlp_induct[case_names rec_step]:
    assumes step: "\<And>rec_invocation n. 
      (\<And>n. wlp (rec_invocation n) (P n))   \<comment> \<open>Assuming recursive invocation is correct\<close>
      \<Longrightarrow> wlp (                            \<comment> \<open>We have to show that function body is correct\<close>
            if n=1 then Some [n] 
            else do { 
              seq\<leftarrow>rec_invocation (collatz_step n); 
              Some (n#seq) 
            }
    ) (P n)"
    shows "wlp (collatz2 n) (P n)"
    
    thm collatz2_induct[where P="\<lambda>collatz2. \<forall>n. wlp (collatz2 n) (P n)"]
    thm collatz2_induct[where P="\<lambda>collatz2. \<forall>n. wlp (collatz2 n) (P n)", rule_format]
    
    apply (rule collatz2_induct[where P="\<lambda>collatz2. \<forall>n. wlp (collatz2 n) (P n)", rule_format])
    subgoal 
      thm Partial_Function.option_admissible
      by (simp add: wlp_arg_iff Partial_Function.option_admissible)
    subgoal by simp
    subgoal by (rule step)
    done

  (* There's not much useful we can prove (it's an open conjecture ;) )
  
    but let's prove that it computes the same as our definition with fuel!
  *)
  lemma "wlp (collatz2 n) (\<lambda>r. \<exists>fuel. r = collatz1 fuel n)"
  proof (induction rule: collatz2_wlp_induct)
    case (rec_step rec_invocation n)
    
    thm rec_step.IH
    
    show ?case
      apply (simp)
      apply safe
      subgoal
        apply (rule exI[where x="1"])
        by simp
      subgoal
        apply (rule wlp_cons[OF rec_step.IH])
        apply clarify
        subgoal for x fuel
          apply (rule exI[where x="Suc fuel"])
          by simp
        done
      done  
    
  qed
  
  
  (*
    What if we know that function terminates? At least within precondition?
    
    Use wf-induction + unfold. No fixed point induction required!
  *)

  partial_function (option) power2_countdown :: "int \<Rightarrow> int option" where
    "power2_countdown n = (if (n=0) then Some 1 else do { r\<leftarrow>power2_countdown (n-1); Some (2*r)})"
    
  (* Note: we should put these definitions into a library file. But they're important, so I repeat them again ;) *)  
  fun wp where
    "wp None Q \<longleftrightarrow> False"  
  | "wp (Some x) Q \<longleftrightarrow> Q x"  
    
  lemma wp_bind[simp]: "wp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> wp m (\<lambda>x. wp (f x) Q)"
    by (cases m) auto

  lemma wp_cons: 
    assumes "wp m Q'"
    assumes "\<And>x. Q' x \<Longrightarrow> Q x"
    shows "wp m Q"  
    using assms by (cases m) auto
    
    
      
  lemma 
    assumes "i\<ge>0" 
    shows "wp (power2_countdown i) (\<lambda>r. r = 2^nat i)"
  proof -
    have "wf (measure nat)" by simp
    then show ?thesis using \<open>i\<ge>0\<close>
    proof (induction i rule: wf_induct_rule)
      case (less x)
      
      (* The actual VCG to be proved. *)
      have [simp]: "2 * 2 ^ nat (x - 1) = 2 ^ nat x" if "x\<noteq>0"
        using less.prems that
        by (simp add: nat_diff_distrib' power_eq_if)
      
      from less.prems
      show ?case
        apply (subst power2_countdown.simps) (* Partial-Function does not declare simp-lemma by default! *)      
        apply (clarsimp)
        apply (rule wp_cons, rule less.IH)
        by (auto)
        
    qed
  qed
  

  (*
    Rest of this lecture:
    
    * Defining recursion as fixed-point of function body (what we just did)
    
    * Nres Monad (and defining recursion there)
    
    * Some examples \<leftarrow> I will most likely run out of time here
    
    * Refinement for nres monad
    
    * Some examples
    
    * Lecture summary! \<leftarrow> will do that!
  
  *)
  
  
  
  
    

end
