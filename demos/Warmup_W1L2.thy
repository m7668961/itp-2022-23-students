theory Warmup_W1L2
imports Main
begin

  section \<open>Last Lecture\<close>

  (*
    datatypes
  *)
  datatype 'a tree = Leaf | Node "'a" "'a tree" "'a tree"

  (*
    functions
  *)

  fun numnodes :: "'a tree \<Rightarrow> nat" where
    "numnodes Leaf = 0"
  | "numnodes (Node _ l r) = 1 + numnodes l + numnodes r"  
      
  term "numnodes (Node a Leaf (Node b Leaf Leaf))"
  value "numnodes (Node a Leaf (Node b Leaf Leaf))"

  (*
    types and type annotations
  *)
  value "5 + 3"
  value "(5::nat) + 3"
  
  (*
    Isabelle's standard types and functions
  *)
  typ bool term True term False
  typ nat term Suc term "0::nat"
  typ int term "-42::int" term "a+b::int"
  typ "'a list" term "[]" term "[a,b,c]" term "a#b#c#Nil" term "xs@ys"
  typ "'a\<times>'b" term "(a,b)" term fst term snd term "(a,b,c)" term "(a,(b,c))"
  

  section \<open>Warming up\<close>
  (* Let's define together *)
  
  (* A function nth_odd :: nat \<Rightarrow> nat, that returns the nth odd number.
    We start counting at 0!
    By recursion over the argument!
  *)
  fun nth_odd :: "nat \<Rightarrow> nat" where
    "nth_odd 0 = 1"
  | "nth_odd (Suc n) = 2 + nth_odd (n)"   
  thm nth_odd.simps
  
  
  (*
    A function square :: nat \<Rightarrow> nat that computes the square by adding up the 
    first n odd numbers.
    
    Note: n\<^sup>2 = nth_odd 0 + \<dots> + nth_odd (n-1)
  *)  
  fun square :: "nat \<Rightarrow> nat" where
    "square 0 = 0"
  | "square (Suc n) = nth_odd n + square n"  
  
  (* A function sum :: nat list \<Rightarrow> nat that sums up the elements of a list.
    E.g. sum [3,4,5] = 12 *)

  fun listsum :: "nat list \<Rightarrow> nat" where
    "listsum [] = 0"
  | "listsum (x#xs) = x + listsum xs"  
        
  value "listsum [3,4,5]"
  

  (* The Fibonacchi sequence: fib :: "nat \<Rightarrow> nat" where
    fib 0 = 0, fib 1 = 1, fib n = fib (n-2) + fib (n-1)
  *)  
  fun fib :: "nat \<Rightarrow> nat" where
    "fib 0 = 0"
  | "fib (Suc 0) = 1"
  | "fib (Suc (Suc n)) = fib n + fib (Suc n)"  

  value "map fib [1..<10]"
  
    
  

  (* A function rep_list :: "nat \<Rightarrow> 'a list \<Rightarrow> 'a list"
    such that "rep_list n l = l @ \<dots> @ l  (n times)"
  *)  


end
