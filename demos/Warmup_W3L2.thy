theory Warmup_W3L2
imports Main
begin

section\<open>Set comprehension\<close>

term "{x::int. x>0}"  
  
term "{x+y | x y. x>0 \<and> y>0}"
  
term "{(a,hd). List.hd a = hd}"

term "{(a,a). a>5}"

term "{(a,a) | a. a>5}"



lemma "{x+y :: int | x y. x>0 \<and> y>0} = {foo. foo>1}"  
  apply auto
  subgoal for xy
    by presburger 
  done
    
  
section\<open>Logic and sets\<close>

lemma "\<forall>x. \<exists>y. x=y"
  by auto
    
term "()"

lemma "(x::unit) = ()" by simp
    
    
lemma "\<exists>x::'a. \<forall>y. x=y" oops

lemma "f x y z = ()" for f :: "_ \<Rightarrow> _ \<Rightarrow> _ \<Rightarrow> unit"
  by simp 

  
lemma "\<exists>x. True" by blast
    
lemma "\<And>x. \<lbrakk>x \<in> A; x \<notin> C; A \<subseteq> B; A \<subseteq> C\<rbrakk> \<Longrightarrow> False"
  by blast

lemma "A \<subseteq> B \<inter> C \<Longrightarrow> A \<subseteq> B \<union> C"
  apply clarsimp
  by auto

text\<open>Note the bounded quantification notation:\<close>

term "\<forall>xs. xs\<in>A \<longrightarrow> xxx"

thm Ball_def Bex_def

lemma "\<lbrakk> \<forall>xs \<in> A. \<exists>ys. xs = ys @ ys;  us \<in> A \<rbrakk> \<Longrightarrow> \<exists>n. length us = n+n"
  apply auto
  by fastforce

lemma "\<lbrakk> \<forall>xs \<in> A. \<exists>ys. xs = ys @ ys;  us \<in> A \<rbrakk> \<Longrightarrow> \<exists>n. length us = n+n"
  by fastforce

  
  
text\<open>
 Most simple proofs in FOL and set theory are automatic.
 Example: if T is total, A is antisymmetric
 and T is a subset of A, then A is a subset of T.
\<close>

lemma AT:
  "\<lbrakk> \<forall>x y. T x y \<or> T y x;
     \<forall>x y. A x y \<and> A y x \<longrightarrow> x = y;
     \<forall>x y. T x y \<longrightarrow> A x y \<rbrakk>
   \<Longrightarrow> \<forall>x y. A x y \<longrightarrow> T x y"
by blast


section\<open>Sledgehammer\<close>


thm rtrancl_mono
lemma "R\<^sup>* \<subseteq> (R \<union> S)\<^sup>*"
  by (simp add: rtrancl_mono)

(* Find a suitable P and try sledgehammer: *)
lemma lemma1: "a # xs = ys @ [a] \<Longrightarrow> xs=[] \<and> ys=[] \<or> (\<exists>zs. xs = zs@[a] \<and> ys=a#zs)"
  by (metis append_eq_Cons_conv list.inject)
    
(* Can you also show equivalence ? *)    
lemma "a # xs = ys @ [a] \<longleftrightarrow> (xs=[] \<and> ys=[] \<or> (\<exists>zs. xs = zs@[a] \<and> ys=a#zs))"
  apply auto
  subgoal sl edgehammer sorry
  subgoal sle dgehammer sorry
  oops  
    

section\<open>Arithmetic\<close>

lemma "\<lbrakk> (a::int) \<le> f x + b; 2 * f x < c \<rbrakk> \<Longrightarrow> 2*a + 1 \<le> 2*b + c"
try0
by arith

lemma "\<forall> (k::nat) \<ge> 8. \<exists> i j. k = 3*i + 5*j"
by arith

lemma "(n::int) mod 2 = 1 \<Longrightarrow> (n+n) mod 2 = 0"
by arith

lemma "(i + j) * (i - j) \<le> i*i + j*(j::int)"
by (simp add: algebra_simps)

lemma "(5::int) ^ 2 = 20+5"
by simp


section \<open>Path and renaming \<close>

  (* 
    Graph as set of edges (u,v).
  
    path v\<^sub>0 [v\<^sub>1,\<dots>,v\<^sub>n] v\<^sub>n  \<longleftrightarrow>  v\<^sub>0\<rightarrow>v\<^sub>1\<rightarrow>\<dots>\<rightarrow>v\<^sub>n   \<longleftrightarrow> (v\<^sub>i,v\<^sub>i\<^sub>+\<^sub>1)\<in>E 
  *)

  fun path where
    "path E u [] v \<longleftrightarrow> u=v"
  | "path E u (v#vs) v' \<longleftrightarrow> (u,v)\<in>E \<and> path E v vs v'"  

  (* Append-lemma: paths can be easily concatenated *)
  lemma path_append[simp]: "path E u (vs@vs') v \<longleftrightarrow> (\<exists>v'. path E u vs v' \<and> path E v' vs' v)"
    apply (induction vs arbitrary: u)
    apply auto
    done

  (* Last element is on (non-empty) path*)  
  lemma "vs\<noteq>[] \<Longrightarrow> path E u vs v \<Longrightarrow> v=last vs"
    apply (induction vs arbitrary: u)
    apply auto
    done
    
  lemma "f`S = {f s | s. s\<in>S}" by auto  
    
  (* Path only contains nodes with incoming edges of graph *)
  lemma path_set_ss: "path E u vs v \<Longrightarrow> set vs \<subseteq> snd`E"
    apply (induction vs arbitrary: u)
    apply auto
    by force

  (* Rename all nodes of graph by applying function f *)  
    
  definition "rename f E \<equiv> { (f u, f v) | u v. (u,v)\<in>E  }"

  (* Show: path implies corresponding path in renamed graph *)
  lemma "path E u vs v \<Longrightarrow> path (rename f E) (f u) (map f vs) (f v)"
    apply (induction vs arbitrary: u)
    apply (auto simp: rename_def)
    done
    
  
  (* Show: path in renamed graph \<longleftrightarrow> \<exists>path in original graph.
  
    what do we need to know about renaming?
  *)  
    
  thm inj_eq
  
  lemma "inj f \<Longrightarrow> path (rename f E) (f u) us' v' \<longleftrightarrow> (\<exists>us v. us'=map f us \<and> v' = f v \<and> path E u us v)"  
    apply (induction us' arbitrary: u)
    by (auto simp: rename_def Cons_eq_map_conv inj_eq)
    
  
    thm conjI impI
  lemma "b=a \<Longrightarrow> a \<longrightarrow> (b \<and> True)"
    apply (intro conjI impI)
      oops
      
  notepad begin    
    fix A B C
    assume foo: "A\<Longrightarrow>B\<Longrightarrow>C"
    note t = conjI[of C]
  
    thm conjI[OF _ foo]
    
    
    
  thm  conjI[OF refl[of a]]  
    
      
  lemma A: "A\<Longrightarrow>B\<Longrightarrow>A\<and>B" by simp
  
  
  
  
  
  lemma "True\<Longrightarrow>True\<Longrightarrow>True\<and>True"
    
    
    

end
