theory Warmup_W8L1
imports 
  Main 
  "HOL-Library.Multiset" 
  "HOL-Library.Monad_Syntax" 
  "HOL-Library.Word"
  "HOL-Library.While_Combinator"
begin

  section \<open>Auxiliary Lemmas\<close>
  
  (*
    Auxiliary lemmas on slicing concatenations of words:
  *)

  
  (*
    The slice is completely in the left word:

    |  a       | b       |
      |######|  
             
    same as
    
    | a        |
      |######|
      
  *)
  lemma slice_cat_left:
    fixes a :: "'a::len word"
    fixes b :: "'b::len word"
    assumes "LENGTH('b)\<le>n"
    assumes "LENGTH('a)+LENGTH('b) \<le> LENGTH('c::len)"
    shows "slice n (word_cat a b :: 'c::len word) = slice (n-LENGTH('b)) a"  
    apply (rule bit_word_eqI)
    using assms
    apply (clarsimp simp: bit_simps)
    using bit_imp_le_length by fastforce

  (*
    The slice is completely in the right word:

    |  a       | b         |
                  |######|

    same as
    | b         |
       |######|
                               
  *)
  lemma slice_cat_right:
    fixes a :: "'a::len word"
    fixes b :: "'b::len word"
    assumes "n+LENGTH('d) \<le> LENGTH('b)"
    assumes "LENGTH('a)+LENGTH('b) \<le> LENGTH('c::len)"
    shows "(slice n (word_cat a b :: 'c::len word) :: 'd::len word) = slice n b"
    apply (rule bit_word_eqI)
    using assms
    by (simp add: bit_simps)

  
  lemma bit_word_eq_iff: "a=b \<longleftrightarrow> (\<forall>i. bit a i = bit b i)" for a b :: "'a::len word" 
    by (auto intro: bit_word_eqI)
      
  lemma word_cat_inj_iff[simp]:
    fixes a a' :: "'a::len word"
    fixes b b' :: "'b::len word"
    assumes "LENGTH('a)+LENGTH('b) \<le> LENGTH('c::len)"
    shows "(word_cat a b :: 'c word) = word_cat a' b' \<longleftrightarrow> a=a' \<and> b=b'"
    apply (rule)
    subgoal 
      apply (simp add: bit_word_eq_iff) 
      apply safe
      subgoal for i
        apply (frule bit_imp_le_length)
        apply (drule spec[where x="i+LENGTH('b)"])
        using assms
        by (simp add: bit_simps)
      subgoal for i
        apply (frule bit_imp_le_length)
        apply (drule spec[where x="i+LENGTH('b)"])
        using assms
        by (simp add: bit_simps)
      subgoal for i
        apply (frule bit_imp_le_length)
        apply (drule spec[where x="i"])
        using assms
        by (simp add: bit_simps)
      subgoal for i
        apply (frule bit_imp_le_length)
        apply (drule spec[where x="i"])
        using assms
        by (simp add: bit_simps)
      done  
    subgoal by simp
    done
    
    
        

  section \<open>Introduction\<close>  

  (*
    Let's model a naive and minimalistic CPU

      Memory: 32 bit addresses, 32 bit word size (yep, no bytes ;) )
      
      Instruction set:
    
      \<^item> 32-bit registers
        IP
        R1..Rx

      \<^item> Operations
        LOADI reg imm         reg := imm
        LOAD reg1 reg2        reg1 := mem[reg2]
        STORE reg1 reg2       mem[reg1] := reg2
                              
        ADD reg1 reg2 reg3    reg1 := reg2 + reg3
        SUB reg1 reg2 reg3    reg1 := reg2 - reg3
        MOV reg1 reg2         reg1 := reg2
                              
        JLT reg1 reg2 imm     if reg1<reg2 then IP=imm
        
        HLT                   indicate that program has finished
        
        JMP = LOADI IP imm   
  
  
      \<^item> Instruction encoding
      
        Number of registers: 2^r
        Number of opcodes: 2^o
        To encode instructions with 3 register operands into 32 bit word: 3*r+o = 32
          Our choice: r=9, o=5
      
        [opcode][reg1][reg2][reg3] | optional immediate

      \<^item> State
        Register bank: reg \<Rightarrow> val
        Memory: val \<Rightarrow> val
        Halted flag: bool
             
      \<^item> What can go wrong:
        Invalid opcode
        null-pointer
  
  *)

  section \<open>State-Error Monad\<close>
  
  (* Designing that beast *)
  
  (* State-Error Monad. 
  
    For debugging, we'll name the errors and include the state into the error!
  *)
  
  (*
    We can build the monad by 'stacking' a state monad onto a sum-monad:
  *)

  subsection \<open>Sum-Monad\<close>
  typ "'a+'b"
  term Inl \<comment> \<open>Error (with message)\<close>
  term Inr \<comment> \<open>Normal result\<close>
  
  definition "bind_sum m f \<equiv> case m of Inl e \<Rightarrow> Inl e | Inr x \<Rightarrow> f x"
  
  definition "wp_sum m Q \<equiv> case m of Inl _ \<Rightarrow> False | Inr x \<Rightarrow> Q x"
  
  context
    notes [simp] = bind_sum_def wp_sum_def
    notes [split] = sum.splits
  begin    
    lemma return_bind_sum[simp]: "bind_sum (Inr x) f = f x" by simp
    lemma bind_return_sum[simp]: "bind_sum m Inr = m" by simp
    lemma fail_bind_sum[simp]: "bind_sum (Inl e) f = Inl e" by simp
    lemma bind_assoc_sum[simp]: "bind_sum (bind_sum m f) g = bind_sum m (\<lambda>x. bind_sum (f x) g)" by simp
    
    lemma wp_sum_return[simp]: "wp_sum (Inr x) Q = Q x" by simp
    lemma wp_sum_fail[simp]: "\<not>wp_sum (Inl e) Q" by simp
    lemma wp_sum_bind[simp]: "wp_sum (bind_sum m f) Q = wp_sum m (\<lambda>x. wp_sum (f x) Q)" by simp
  end

  subsection \<open>Adding State\<close>
  (*
    Given Monad M, we can construct a state monad S on top of M by:
      'a S = 's \<Rightarrow> ('a \<times> 's) M
  
      return\<^sub>S x = \<lambda>s. return\<^sub>M (x,s)
      bind\<^sub>S m f = \<lambda>s. bind\<^sub>M (m s) (\<lambda>(x,s). f x s)
  
      (^^ pretty informal. In Isabelle, we cannot elegantly formalize this, due to type-system restrictions)
  
    But we can, if monad M is fixed. Let's stack a state monad on top of the sum monad:
  *)

  datatype ('a,'e,'s) ssM = COMP (run: "'s \<Rightarrow> 'e+'a\<times>'s")
  (* We wrap the function \<Rightarrow> intro our own type. This gives us a better abstraction barrier *)
  
  definition return :: "'a \<Rightarrow> ('a,'e,'s) ssM" where
    "return x = COMP (\<lambda>s. Inr (x,s))"
  
  definition fail :: "'e \<Rightarrow> ('a,'e,'s) ssM" where 
    "fail msg = COMP (\<lambda>s. Inl msg)"
    
  definition bind_ssM where 
    "bind_ssM m f = COMP (\<lambda>s. bind_sum (run m s) (\<lambda>(x,s). run (f x) s))"

  adhoc_overloading bind bind_ssM
    
  definition "get = COMP (\<lambda>s. Inr (s,s))"
  definition "put s = COMP (\<lambda>_. Inr ((),s))"
  
  
  
  subsubsection \<open>Monad Laws\<close>
  context
    notes [simp] = bind_ssM_def return_def fail_def
    notes [split] = sum.splits
  begin
  
    lemma return_bind[simp]: "do {x\<leftarrow>return v; f x} = f v" by (auto)
    lemma bind_return[simp]: "do {x\<leftarrow>m; return x } = m" by (cases m; auto)
  
    lemma bind_assoc[simp]: "do { y\<leftarrow>do {x\<leftarrow>m; f x}; g y } = do { x\<leftarrow>m; y\<leftarrow>f x; g y}" for m :: "(_,_,_) ssM" 
      by (auto simp: fun_eq_iff arg_cong[where f="bind_sum _"])
  
    lemma bind_fail[simp]: "do {x\<leftarrow>fail msg; f x} = fail msg" by auto
  
    subsubsection \<open>Weakest Precondition\<close>
    definition "wp m Q s = wp_sum (run m s) (\<lambda>(x,s). Q x s)"
    
    context
      notes [simp] = wp_def get_def put_def
    begin
      
      lemma wp_return[simp]: "wp (return x) Q s \<longleftrightarrow> Q x s" by auto
      lemma wp_fail[simp]: "\<not>wp (fail msg) Q s" by auto
      
      lemma wp_bind[simp]: "wp (do {x\<leftarrow>m; f x}) Q s \<longleftrightarrow> wp m (\<lambda>x. wp (f x) Q) s" 
        by (auto simp: fun_eq_iff arg_cong[where f="wp_sum _"])
    
      lemma wp_gut[simp]: "wp (get) Q s \<longleftrightarrow> Q s s" by simp
      lemma wp_put[simp]: "wp (put s') Q s \<longleftrightarrow> Q () s'" by simp
    
    end  

  end

  
  
      
  (* Derived Operations *)
  
  definition "assert P msg \<equiv> if P then return () else fail msg"
  
  lemma wp_assert[simp]: "wp (assert P msg) Q s \<longleftrightarrow> P \<and> (P\<longrightarrow>Q () s)"
    unfolding assert_def by auto
  
  definition "maps f \<equiv> do { s\<leftarrow>get; put (f s) }"
  lemma wp_maps[simp]: "wp (maps f) Q s \<longleftrightarrow> Q () (f s)"
    unfolding maps_def by auto
  

  section \<open>Formalizing Simple CPU\<close>  
  
  subsection \<open>Machine State\<close>
  
  type_synonym val = "32 word"    \<comment> \<open>32 bit values and addresses\<close>
  type_synonym rname = "9 word"   \<comment> \<open>9 bit wide register bank\<close>
  type_synonym opcode = "5 word"  \<comment> \<open>5 bit for opcodes\<close>

    
  datatype state = STATE 
    (regs: "rname \<Rightarrow> val") \<comment> \<open>Register values\<close>
    (mem: "val \<Rightarrow> val")    \<comment> \<open>Memory content\<close>
    (halted: bool)         \<comment> \<open>Halted flag\<close>

  text \<open>Functions to map the components of the state. 
    These make the design easier to change, e.g., add more components to state later.
  \<close>
  fun map_regs where "map_regs f (STATE r m h) = STATE (f r) m h"
  fun map_mem where "map_mem f (STATE r m h) = STATE r (f m) h"
  fun set_halted where "set_halted (STATE r m h) = STATE r m True"
  
  (* Note: Lenses can make the below lemmas more systematic *)

  lemma map_map_regs[simp]: "map_regs f\<^sub>1 (map_regs f\<^sub>2 s) = map_regs (f\<^sub>1 o f\<^sub>2) s" by (cases s) auto
  lemma map_map_mem[simp]: "map_mem f\<^sub>1 (map_mem f\<^sub>2 s) = map_mem (f\<^sub>1 o f\<^sub>2) s" by (cases s) auto
  lemma set_set_halted[simp]: "set_halted (set_halted s) = set_halted s" by (cases s) auto
  
  lemma regs_map_regs[simp]: "regs (map_regs f s) = f (regs s)" by (cases s) auto
  lemma regs_map_mem[simp]: "regs (map_mem f s) = regs s" by (cases s) auto
  lemma regs_set_halted[simp]: "regs (set_halted s) = regs s" by (cases s) auto
  
  lemma mem_map_mem[simp]: "mem (map_mem f s) = f (mem s)" by (cases s) auto
  lemma mem_map_regs[simp]: "mem (map_regs f s) = mem s" by (cases s) auto
  lemma mem_set_halted[simp]: "mem (set_halted s) = mem s" by (cases s) auto
  
  lemma halted_set_halted[simp]: "halted (set_halted s)" by (cases s) auto
  lemma halted_map_regs[simp]: "halted (map_regs f s) = halted s" by (cases s) auto
  lemma halted_map_mem[simp]: "halted (map_mem f s) = halted s" by (cases s) auto
  
  text \<open>Maximum representable value\<close>
  abbreviation max_val::val where "max_val \<equiv> 0xFFFFFFFF"

  subsection \<open>Symbolic Register Names\<close>
  abbreviation (input) rIP :: rname where "rIP \<equiv> 0"
  
  subsection \<open>Instruction Encoding\<close>
  
  (* Instruction format:
  
   | 31 ... 27 | 26 ... 18 | 17 ...  9 | 8  ...  0 |
   |   opcode  |  reg1     |  reg2     |  reg3     |
   |
  *)

  subsubsection \<open>Decoder\<close>
    
  term slice
  value "slice 3 (0b01001111::8 word) :: 4 word"
  
  definition deci_opcode :: "val \<Rightarrow> opcode" where "deci_opcode c = slice 27 c"
  definition deci_reg1 :: "val \<Rightarrow> rname" where "deci_reg1 c = slice 18 c"
  definition deci_reg2 :: "val \<Rightarrow> rname" where "deci_reg2 c = slice 9 c"
  definition deci_reg3 :: "val \<Rightarrow> rname" where "deci_reg3 c = slice 0 c"

  subsubsection \<open>Encoder and sanity checks\<close>
  
  term word_cat
  
  definition enci :: "opcode \<Rightarrow> rname \<Rightarrow> rname \<Rightarrow> rname \<Rightarrow> val" where 
    "enci op r1 r2 r3 = 
      word_cat op (word_cat r1 (word_cat r2 r3 :: 18 word) :: 27 word)"
  
  lemma dec_enc_opcode[simp]: "deci_opcode (enci op r1 r2 r3) = op"
    unfolding deci_opcode_def enci_def
    by (simp add: slice_cat_left slice_id)
    
  lemma dec_enc_reg1[simp]: "deci_reg1 (enci op r1 r2 r3) = r1"
    unfolding deci_reg1_def enci_def
    by (simp add: slice_cat_left slice_cat_right slice_id)
    
  lemma dec_enc_reg2[simp]: "deci_reg2 (enci op r1 r2 r3) = r2"
    unfolding deci_reg2_def enci_def
    by (simp add: slice_cat_left slice_cat_right slice_id)
  
  lemma dec_enc_reg3[simp]: "deci_reg3 (enci op r1 r2 r3) = r3"
    unfolding deci_reg3_def enci_def
    by (simp add: slice_cat_left slice_cat_right slice_id)
  
  lemma enci_inj_iff[simp]: "enci op r1 r2 r3 = enci op' r1' r2' r3' 
    \<longleftrightarrow> op=op' \<and> r1=r1' \<and> r2=r2' \<and> r3=r3'"
    unfolding enci_def
    by auto
    
  subsubsection \<open>Op-Codes for instructions\<close>
    
  abbreviation opc_LOADI :: opcode where "opc_LOADI \<equiv> 0x01"
  abbreviation opc_LOAD :: opcode  where "opc_LOAD  \<equiv> 0x02"
  abbreviation opc_STORE :: opcode where "opc_STORE \<equiv> 0x03"
  
  abbreviation opc_ADD :: opcode   where "opc_ADD   \<equiv> 0x04"
  abbreviation opc_SUB :: opcode   where "opc_SUB   \<equiv> 0x05"
  abbreviation opc_MOV :: opcode   where "opc_MOV   \<equiv> 0x06"
  
  abbreviation opc_JLT :: opcode   where "opc_JLT   \<equiv> 0x07"
  abbreviation opc_HLT :: opcode   where "opc_HLT   \<equiv> 0x08"
    
  
  subsection \<open>Monad for instruction semantics\<close>
  (* Think of writing a (straightforward) interpreter *)

  subsubsection \<open>Monad Type\<close>
      
  type_synonym error_msg = string
  
  type_synonym 'a mM = "('a,error_msg,state) ssM"
  translations (type) "'a mM" \<leftharpoondown> (type) "('a,char list,state) ssM" (* Fold on pretty-printing type *)
  
  subsubsection \<open>Operations on State\<close>
  
  (* Note: Lenses can make those operations look simpler (but are, sometimes, overkill) *)
  
  (* Note: Type annotations are important here, as the type is not fully determined, in general! *)
  definition "\<mu>c_get_reg r :: _ mM \<equiv> do { s\<leftarrow>get; return (regs s r) }"
  definition "\<mu>c_set_reg r v :: _ mM \<equiv> do { s\<leftarrow>get; put (map_regs (\<lambda>rs. rs(r:=v)) s) }"

  definition "\<mu>c_get_mem a :: _ mM \<equiv> do { s\<leftarrow>get; return (mem s a) }"
  definition "\<mu>c_set_mem a v :: _ mM \<equiv> do { s\<leftarrow>get; put (map_mem (\<lambda>m. m(a:=v)) s) }"

  definition "\<mu>c_get_halted :: _ mM \<equiv> do { s\<leftarrow>get; return (halted s) }"
  definition "\<mu>c_set_halted :: _ mM \<equiv> do { s\<leftarrow>get; put (set_halted s) }"
    
  
  lemma wp_\<mu>c_get_reg[simp]: "wp (\<mu>c_get_reg r) Q s \<longleftrightarrow> Q (regs s r) s"
    unfolding \<mu>c_get_reg_def by simp
  
  lemma wp_\<mu>c_set_reg[simp]: "wp (\<mu>c_set_reg r v) Q s \<longleftrightarrow> Q () ((map_regs (\<lambda>rs. rs(r:=v)) s))"
    unfolding \<mu>c_set_reg_def by simp

  lemma wp_\<mu>c_get_mem[simp]: "wp (\<mu>c_get_mem a) Q s \<longleftrightarrow> Q (mem s a) s"
    unfolding \<mu>c_get_mem_def by simp
  
  lemma wp_\<mu>c_set_mem[simp]: "wp (\<mu>c_set_mem a v) Q s \<longleftrightarrow> Q () (map_mem (\<lambda>m. m(a:=v)) s)"
    unfolding \<mu>c_set_mem_def by simp

  lemma wp_\<mu>c_get_halted[simp]: "wp \<mu>c_get_halted Q s \<longleftrightarrow> Q (halted s) s"
    unfolding \<mu>c_get_halted_def by simp
    
  lemma wp_\<mu>c_set_halted[simp]: "wp \<mu>c_set_halted Q s \<longleftrightarrow> Q () (set_halted s)"
    unfolding \<mu>c_set_halted_def by simp
      
  
  subsection \<open>Executing One Instruction\<close>
  
  (*
    Fetch instruction word
    decode
    execute (and fetch additional immediates)
  *)

  (*
    Note: we did not add type annotations here, as the types are fully determined.
    BUT: if in doubt, better add a type annotation too much, than one to less!
  *)
    
  
  (* Fetch instruction word *)
  definition "\<mu>c_fetch_instr_word \<equiv> do {
    ip \<leftarrow> \<mu>c_get_reg rIP;
    assert (ip<max_val) ''IP overflow'';
    w \<leftarrow> \<mu>c_get_mem ip;
    \<mu>c_set_reg rIP (ip+1);
  
    return w
  }"

  (* Instruction semantics *)
  definition "\<mu>c_LOADI r \<equiv> do {
    i \<leftarrow> \<mu>c_fetch_instr_word; \<comment> \<open>Fetch additional immediate operand\<close>
    \<mu>c_set_reg r i
  }"

  definition "\<mu>c_LOAD rtgt ra \<equiv> do {
    a \<leftarrow> \<mu>c_get_reg ra;
    
    assert (a\<noteq>0) ''Null pointer access'';
    v \<leftarrow> \<mu>c_get_mem a;
    
    \<mu>c_set_reg rtgt v
  }"  

  definition "\<mu>c_STORE ra rv \<equiv> do {
    a \<leftarrow> \<mu>c_get_reg ra;
    v \<leftarrow> \<mu>c_get_reg rv;
    
    assert (a\<noteq>0) ''Null pointer access'';
    
    \<mu>c_set_mem a v
  }"  

  definition "\<mu>c_binop f rtgt ra rb \<equiv> do {
    a \<leftarrow> \<mu>c_get_reg ra;
    b \<leftarrow> \<mu>c_get_reg rb;
  
    \<mu>c_set_reg rtgt (f a b)
  }"

  definition "\<mu>c_MOV rtgt rsrc \<equiv> do {
    x \<leftarrow> \<mu>c_get_reg rsrc;
    \<mu>c_set_reg rtgt x
  }"      
    
  definition "\<mu>c_JLT ra rb \<equiv> do {
    j_tgt \<leftarrow> \<mu>c_fetch_instr_word; \<comment> \<open>Fetch additional immediate operand\<close>
    
    a \<leftarrow> \<mu>c_get_reg ra;
    b \<leftarrow> \<mu>c_get_reg rb;
    
    if (a<b) then \<mu>c_set_reg rIP j_tgt
    else return ()
  }"
  
  definition "\<mu>c_HLT \<equiv> \<mu>c_set_halted"
  

  definition "assert_not_halted \<equiv> do {
    hlt \<leftarrow> \<mu>c_get_halted;
    assert (\<not>hlt) ''Tried to make next step from halted state''
  }"
        
  (* Fetch, decode, execute *)  
  definition "\<mu>c_step \<equiv> do {
    assert_not_halted;
  
    w \<leftarrow> \<mu>c_fetch_instr_word;
    let opc = deci_opcode w;
    let reg1 = deci_reg1 w;
    let reg2 = deci_reg2 w;
    let reg3 = deci_reg3 w;

    if opc = opc_LOADI then \<mu>c_LOADI reg1
    else if opc = opc_LOAD then \<mu>c_LOAD reg1 reg2
    else if opc = opc_STORE then \<mu>c_STORE reg1 reg2

    else if opc = opc_ADD then \<mu>c_binop (+) reg1 reg2 reg3
    else if opc = opc_SUB then \<mu>c_binop (-) reg1 reg2 reg3
    else if opc = opc_MOV then \<mu>c_MOV reg1 reg2

    else if opc = opc_JLT then \<mu>c_JLT reg1 reg2
    else if opc = opc_HLT then \<mu>c_HLT
            
    else fail ''Invalid opcode''
  }"

  subsection \<open>Repeating Execution\<close>
  (* Running *)
  
  (* We could define iteration here, using whileOption. 
    However, there's a simple trick to make any function terminating, and thus exploit
    the capabilities of tools for defining terminating functions, like Isabelle's \<open>fun\<close>:

    We simply add an extra parameter that limits the number of recursions. 
    This parameter is usually called fuel. If we chose it big enough, the function terminates normally.
    Otherwise, it may run out of fuel.
  *)
  
  fun run_cpu where
    "run_cpu 0 = return ()"
  | "run_cpu (Suc fuel) = do {
      hlt\<leftarrow>\<mu>c_get_halted;
      if hlt then return ()
      else do {
        \<mu>c_step;
        run_cpu fuel
      }
    }"
  
  lemmas [simp del] = run_cpu.simps  
    
  subsection \<open>Example Program\<close>
  
  (* We can actually execute programs now.
  
    Let's compile 'from hand':
  
    
    LOADI r1 21
    LOADI r2 21
    ADD r3 r1 r2
    HLT
    
  *)

  definition lst2mem :: "val list \<Rightarrow> val \<Rightarrow> val" 
    where "lst2mem xs i \<equiv> if unat i<length xs then xs!unat i else 0"  
  
  definition "my_mem\<^sub>0 \<equiv> lst2mem [
    enci opc_LOADI 1 0 0, 21,
    enci opc_LOADI 2 0 0, 21,
    enci opc_ADD 3 1 2,
    enci opc_HLT 0 0 0
  ]"
      
  definition "my_state\<^sub>0 \<equiv> STATE (\<lambda>_. 0) (my_mem\<^sub>0) False"
  
  definition "inspect_result :: _ mM \<equiv> do {
    s\<leftarrow>get;
    return (regs s 3)
  }"
  
  
  value "run (run_cpu 30 \<then> inspect_result) my_state\<^sub>0"
  
  
  subsubsection \<open>Example 2\<close>
  
  (*    
    LOADI r1 21
    LOADI r2 0x1000
    LOAD r2 r2
    ADD r3 r1 r2
    HLT
    
  *)
  
  definition "my_mem\<^sub>2 \<equiv> lst2mem [
    enci opc_LOADI 1 0 0, 21,
    enci opc_LOADI 2 0 0, 0x1000,
    enci opc_LOAD 2 2 0,
    enci opc_ADD 3 1 2,
    enci opc_HLT 0 0 0
  ]"
      
  definition "my_state\<^sub>2 x \<equiv> STATE (\<lambda>_. 0) (my_mem\<^sub>2(0x1000:=x)) False"
  
  value "run (run_cpu 30 \<then> inspect_result) (my_state\<^sub>2 21)"
  
  
  
  subsection \<open>Proving Programs Correct\<close>
  (* Let's prove something about our program! *)
  

  (* Let the simplifier unfold wp (run_cpu _) *)
  lemma wp_run_cpu[simp]: "n\<noteq>0 \<Longrightarrow> wp (run_cpu n) = wp (do {
      hlt\<leftarrow>\<mu>c_get_halted;
      if hlt then return ()
      else do {
        \<mu>c_step;
        run_cpu (n-1)
      }
    })"
    by (cases n; simp add: run_cpu.simps cong: if_cong)  
    

  (* Let the simplifier resolve indexes into our memory *)
  lemma lst2mem_inbounds[simp]: "unat i<length xs \<Longrightarrow> lst2mem xs i = xs!unat i"
    by (simp add: lst2mem_def)
  
  (* Let's prove that our program needs less than 30 steps to return 42 *)
  lemma "wp (run_cpu 30 \<then> inspect_result) (\<lambda>r s. r=42) my_state\<^sub>0"
    unfolding my_state\<^sub>0_def my_mem\<^sub>0_def
    apply (simp)
  
    apply (subst \<mu>c_step_def; simp add: Let_def)
    apply (subst assert_not_halted_def; simp)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
    apply (subst \<mu>c_LOADI_def; simp)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
  
    apply (subst \<mu>c_step_def; simp add: Let_def)
    apply (subst assert_not_halted_def; simp)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
    apply (subst \<mu>c_LOADI_def; simp)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
  
    apply (subst \<mu>c_step_def; simp add: Let_def)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
    apply (subst assert_not_halted_def; simp)
    apply (subst \<mu>c_binop_def; simp)
  
    apply (subst \<mu>c_step_def; simp add: Let_def)
    apply (subst \<mu>c_fetch_instr_word_def; simp add: )
    apply (subst assert_not_halted_def; simp)
    apply (subst \<mu>c_HLT_def; simp)
  
    apply (subst inspect_result_def; simp)
    done
  
  (* Engineering task: 
    automate the above, and extends to loops, etc!
  *)  
  
  (* Let's start with fetch_instr_word *)
  lemma wp_\<mu>c_fetch_instr_word[simp]: "wp (\<mu>c_fetch_instr_word) Q s \<longleftrightarrow> (
    let ip = regs s rIP in
    let w = mem s ip in
    let s' = map_regs (\<lambda>rs. rs(rIP := ip+1)) s in
      ip<max_val 
    \<and> Q w s'
    )"
    unfolding \<mu>c_fetch_instr_word_def
    by (auto simp: Let_def)

  lemma wp_assert_not_halted[simp]: "wp assert_not_halted Q s \<longleftrightarrow> \<not>halted s \<and> Q () s"
    by (auto simp: assert_not_halted_def)
      
  (* And lemmas for instructions *)  
  
  lemma wp_\<mu>c_LOADI[simp]: "wp (\<mu>c_LOADI r) Q s \<longleftrightarrow> 
    (let ip = regs s 0 in 
        ip < max_val 
      \<and> Q () (map_regs (\<lambda>rs. rs(rIP := ip+1, r := mem s ip)) s))"
    by (auto simp: \<mu>c_LOADI_def Let_def comp_def)

  lemma wp_\<mu>c_LOAD[simp]: "wp (\<mu>c_LOAD r1 r2) Q s \<longleftrightarrow> 
    (let addr=regs s r2 in 
        addr \<noteq> 0 
      \<and> Q () (map_regs (\<lambda>rs. rs(r1 := mem s addr)) s))"
    unfolding \<mu>c_LOAD_def
    by (auto simp add: Let_def)
      
  lemma wp_\<mu>c_binop[simp]: "wp (\<mu>c_binop f rtgt ra rb) Q s \<longleftrightarrow> 
    (let a = regs s ra; b=regs s rb in Q () (map_regs (\<lambda>rs. rs(rtgt:=f a b)) s))
  "  
    by (simp add: \<mu>c_binop_def)
    
  lemma wp_\<mu>c_HLT[simp]: "wp \<mu>c_HLT Q s = Q () (set_halted s)"  
    by (auto simp: \<mu>c_HLT_def)
    

  (* Finally, simplifier can do the 'computation' *)  
  lemma "wp (run_cpu 30 \<then> inspect_result) (\<lambda>r s. r=42) my_state\<^sub>0"
    unfolding my_state\<^sub>0_def my_mem\<^sub>0_def
    apply (simp add: inspect_result_def \<mu>c_step_def)
    done

  (* And also reason symbolically *)
  lemma "wp (run_cpu 30 \<then> inspect_result) (\<lambda>r s. r=21+x) (my_state\<^sub>2 x)"
    unfolding my_state\<^sub>2_def my_mem\<^sub>2_def
    apply (simp add: inspect_result_def \<mu>c_step_def)
    done
    

    
  (* And now? 

    Reasoning about looping programs. Floyd's method.
    
    More engineering to make both, interpretation + VC-generation faster
      \<^item> but we do not want to change original semantics for that! This should be as easy as possible!
        \<Longrightarrow> Refinement techniques
  
  *)  

end    
