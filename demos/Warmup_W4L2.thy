theory Warmup_W4L2
imports Main
begin

(** Inductive:

  define smallest predicate for that given rules hold.

  smallest: whenever predicate holds, it can be derived by rules
*)

inductive my_odd :: "nat\<Rightarrow>bool" where
  "my_odd (Suc 0)"
| "my_odd n \<Longrightarrow> my_odd (Suc (Suc n))"

thm my_odd.intros (* The specified intro rules *)

thm my_odd.cases (* Rule inversion: one-step back in derivation *)

thm my_odd.induct (* Rule induction: induction over derivation *)

(* Proving that inductive predicate holds: rule, intro *)
lemma "my_odd (2*k+1)"
  apply (induction k)
  apply (auto intro: my_odd.intros)
  done

(* Proving that property holds for range of inductive predicate: rule induction *)  
lemma "my_odd n \<Longrightarrow> (\<exists>k. n=2*k+1)"  
  apply (induction rule: my_odd.induct)
  apply auto
  try0
  by presburger

(* Careful how you define your rules! *)  
inductive my_odd' :: "nat\<Rightarrow>bool" where
  "my_odd' 1"
| "my_odd' n \<Longrightarrow> my_odd' (n+2)"
  
lemma "my_odd' (2*k+1)"
  apply (induction k)
  apply (auto simp: my_odd'.intros)
  
  apply (rule my_odd'.intros)
  
  apply (simp add: my_odd'.intros)
using my_odd'.intros(1) apply simp
  subgoal for k  
    using my_odd'.intros(2)[of "2*k+1"] (** Does not syntactically match! *)
  apply auto
  
  (* Usually the simplifier 'simplifies' the goal to a form that does not
    match the rule any more!
    
    Makes proofs more complicated!
  *)

  thm my_odd'.intros[simplified] (** Sometimes helps *)
    
  by auto
oops  
  (** But best: be aware of what simplifier will do when defining your rules! *)
  

(*
  Inductive can be used to formalize systems of rules, like:
*)

(* Semantics, e.g. of iteration *)

context
  fixes cond :: "'s\<Rightarrow>bool"
  fixes step :: "'s \<Rightarrow> 's"
begin

  (**
    wcomp s s' \<equiv> while cond step s terminates in state s'
  *)
  inductive wcomp :: "'s \<Rightarrow> 's \<Rightarrow> bool" where
    wcomp_finish: "\<not>cond s \<Longrightarrow> wcomp s s"
  | wcomp_step: "\<lbrakk> cond s; wcomp (step s) s' \<rbrakk> \<Longrightarrow> wcomp s s'"  
  
  lemma invar_rule:
    "\<lbrakk>
      I s\<^sub>0;
      \<And>s. \<lbrakk>I s; cond s\<rbrakk> \<Longrightarrow> I (step s);
      wcomp s\<^sub>0 s'
    \<rbrakk> \<Longrightarrow> I s' \<and> \<not>cond s'"
    apply (rotate_tac 2)
    apply (induction rule: wcomp.induct)
    by (auto)
    
    
  lemma wcomp_det: "wcomp s s' \<Longrightarrow> wcomp s s'' \<Longrightarrow> s'=s''"
    apply (induction rule: wcomp.induct)
    (* elim: rule inversion*)
    subgoal for s
      thm wcomp.cases (** How has the 'other' wcomp been derived? *)
      apply (erule wcomp.cases)
      apply simp
      apply simp
      done
      
    apply (auto elim: wcomp.cases)
    done
  
    
end




(* Context free grammars 

  S = <num> | S + S | (S)

*)
  
datatype token = NUMBER nat | PLUS | LPAREN | RPAREN

inductive expr :: "token list \<Rightarrow> bool" where
  num: "expr ([NUMBER n])"
| plus: "expr e\<^sub>1 \<Longrightarrow> expr e\<^sub>2 \<Longrightarrow> expr (e\<^sub>1@PLUS#e\<^sub>2)"  
| par: "expr e \<Longrightarrow> expr (LPAREN#e@[RPAREN])"

(* 1+2+(3) *)
lemma "expr [NUMBER 1,PLUS,NUMBER 2,PLUS,LPAREN,NUMBER 3,RPAREN]"
  (** Parsing (automatically) is difficult ;) 
  
    but we can compose the rules manually!
  *)

  using plus[OF plus[OF num num] par[OF num]]
  by simp
  
  (* our grammar is ambiguous!
  
  using plus[OF num plus[OF num par[OF num]]]
  by simp
  *)
  
(* Context free grammars with effects

  S = <num n> \<rightarrow> n 
    | S:n\<^sub>1 + S:n\<^sub>2 \<rightarrow> n\<^sub>1+n\<^sub>2 
    | (S:n) \<rightarrow> n

*)
  

inductive expr' where
  num': "expr' ([NUMBER n]) n"
| plus': "expr' e\<^sub>1 n\<^sub>1 \<Longrightarrow> expr' e\<^sub>2 n\<^sub>2 \<Longrightarrow> expr' (e\<^sub>1@PLUS#e\<^sub>2) (n\<^sub>1+n\<^sub>2)"  
| par': "expr' e n \<Longrightarrow> expr' (LPAREN#e@[RPAREN]) n"


lemma "expr' [NUMBER 1,PLUS,NUMBER 2,PLUS,LPAREN,NUMBER 3,RPAREN] 6"
  (** Parsing (automatically) is difficult ;) 
  
    but we can compose the rules manually!
  *)
  using plus'[OF plus'[OF num'[of 1] num'[of 2]] par'[OF num'[of 3]]]
  by simp
  
lemma "expr e \<longleftrightarrow> (\<exists>n. expr' e n)" oops
  
lemma aux1: "expr e \<Longrightarrow> (\<exists>n. expr' e n)"
  apply (induction rule: expr.induct)
  apply (auto intro: expr'.intros)
  done
  
lemma aux2: "expr' e n \<Longrightarrow> expr e"
  apply (induction rule: expr'.induct)
  apply (auto intro: expr.intros)
  done

lemma "expr e \<longleftrightarrow> (\<exists>n. expr' e n)" 
  using aux1 aux2 by blast
  
(** Hmm. Two globally visible aux-lemmas? 

  Would you like a bit more control and structure in your proofs?
*)

lemma "expr e \<longleftrightarrow> (\<exists>n. expr' e n)"
proof safe
  assume "expr e" then show "\<exists>n. expr' e n" 
    by induction (auto intro: expr'.intros)
next
  fix n
  assume "expr' e n" then show "expr e"
    by induction (auto intro: expr.intros)
qed

(**
  \<longrightarrow> Isar proof language!
*)  
  
  
  
  
(** How ambiguous? *)  
lemma "expr' e n \<Longrightarrow> expr' e n' \<Longrightarrow> n'=n"
  (** \<rightarrow> difficult homework?! ISAR! *)    
  oops


  
    

end
