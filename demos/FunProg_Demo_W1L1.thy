(* Header of theory always looks like this: *)
theory FunProg_Demo_W1L1 (* Theory name, must coincide with filename *)
imports Main (* List of imported theories. Typically only Main. *)
begin

  section \<open>Datatype Examples\<close> (* Section commands have no semantic meaning, only for source structuring. *)
    (* Hint: Use sidekick-panel! *)
    
  (* First Example: Nat  *)
  (* Data represented as algebraic datatypes: *)
  datatype my_nat = Z | S my_nat 
  
  (* Intuitive reading: A natural number is either *Z*ero, or the *S*uccessor of a natural number *)
    
  (* Note: Unary representation. 
    Not efficient for computation. But very efficient for proving!
  *)

  (* Use the term command to display a term and its type in the Output panel *)
  term "Z" (* In theory text, terms and types always enclosed in "quotes". *)
  term "''This is a string'' @ ''bar''" (* Do not confuse with strings *)
  term "CHR ''a''" (* Syntax for single character *) 
    
  term "S Z" (* Function application is written: f x\<^sub>1 ... x\<^sub>n *)
  term "S (S (S Z))" (* Note the parentheses! *)
  
  term "g (f (x+1) y) (n+1)"
  
  term "Z"  
 
  
  term \<open>S (S Z)\<close> (* You may also use cartouches \<open>...\<close>  instead of quotes. 
      Type  quote " and wait for completion menu. 
      Hint: Set Plugins>Plugin Options>Isabelle>General>Completion Delay to 0 for smoother typing.
    *)
  (* Hint: Use the Symbols-panel to explore available 
    symbols, and methods how to enter them! *)
    
  datatype 'a my_list = NIL | CONS 'a "'a my_list"
  (* \<open>\<open>'a\<close> is type parameter. May be filled with any type, e.g.: *)
  typ "my_nat my_list" typ "bool my_list"
    
  datatype bintree = Leaf | Node bintree bintree
    
  datatype 'a bt = Leaf' | Node' 'a "'a bt" "'a bt" 
    
  section \<open>Functions\<close>
  
  \<comment> \<open>Comment\<close>
    
  (* Functions: Recursive functions. For example: *)
  fun add where
    "add Z m = m"
  | "add (S n) m = S (add n m)"

  (* Note: This definition is much simpler than it would be for binary numbers! *)
  (* Also simpler than what you would write in C++ or Java! *)

  term "add (S Z) (S (S Z))"
      
  value "add (S Z) (S (S Z))" (* Evaluate a term *)
  value "add (S n) (S (S (Z)))" (* Partial evaluation also possible! *)
  
  fun appnd  
    where  
    "appnd NIL l = l"
  | "appnd (CONS x l) ll = CONS x (appnd l ll)"  

  
  value "appnd (CONS a (CONS b NIL)) (CONS c (CONS d NIL))"  
    
  (*
      appnd (CONS a (CONS b NIL)) (CONS c (CONS d NIL)) 
    = CONS a (appnd (CONS b NIL) (CONS c (CONS d NIL)))
    = CONS a (CONS b (appnd NIL (CONS c (CONS d NIL))))
    = CONS a (CONS b (CONS c (CONS d NIL)))

    More examples how evaluation works: 

      add (S (S Z)) (S Z)    -- Match with equation add (S n) m = S (add n m)
    = S (add (S Z) (S Z))    -- Match with equation add (S n) m = S (add n m)
    = S (S (add Z (S Z)))    -- Match with equation add Z m = m
    = S (S (S Z))            -- No more equations to match with


  *)  
    
    
    
    
  section \<open>Types\<close>
  (* Every term in Isabelle must be typeable, e.g., it has a type. *)
  term "Z" 
  term "S Z"  
    
  
  term "S" (* Function type indicated by \<Rightarrow>  *)
  term "add" (* Function with many arguments. Note: \<Rightarrow> is right associative, i.e. "a\<Rightarrow>b\<Rightarrow>c" is same as "a\<Rightarrow>(b\<Rightarrow>c)" *)
  
  term "f a b"
  term "(f a) b"
  
  typ "'a \<Rightarrow> ('a \<Rightarrow> 'a)"
  
  term "add (S Z) (S Z)"
  term "add (S Z)" (* Partial application *)
    
  (*
    Hint: press Ctrl (Mac: Cmd) and move the mouse over (almost) any item in the editor window.
      A tooltip will show further information. Left-click will move to the definition of the item.
  *)  
    
    
  (* We may specify type with function definition. If no type specified,
    Isabelle infers most generic one (type inference). *)
  fun numnodes :: "bintree \<Rightarrow> my_nat" where
    "numnodes Leaf = Z"
  | "numnodes (Node t1 t2) = S (add (numnodes t1) (numnodes t2))"  
    
  value "numnodes (Node (Node Leaf Leaf) (Node Leaf (Node Leaf Leaf)))"

  fun numnodes' :: "bintree \<Rightarrow> nat" where
    "numnodes' Leaf = 0"
  | "numnodes' (Node t1 t2) = (numnodes' t1) + (numnodes' t2) + 1"  
  
  value "1 + (1::nat)"    
    
  (* Type annotations may be added everywhere in term. 
    To influence (restrict) inferred type *)
  term "CONS x xs"
  term "(CONS :: my_nat \<Rightarrow> my_nat my_list \<Rightarrow> my_nat my_list) (x::my_nat) xs"
  term "(CONS x xs)::my_nat my_list"  
    
    
  (* Note: Variables and constants are displayed in different color! Useful to find typos! *)  
    
  term "CONS x xs"  
  term "C0NS x xs"
    
  (* Also, bound variables get different color. Bound variable: 
    Occurs in pattern on left hand side of function equation
  *)  
  fun double :: "my_nat \<Rightarrow> my_nat" where
    "double Z = Z"
  | "double (S n) = S (S (double n))"
    
  (* Don't get confused: The function that is actually being defined is rendered
    as a free variable!
  *)  
  term double  (* It only becomes a constant after definition *)
    
  section \<open>Standard Library\<close>  

  (* Of course, Isabelle has data types for natural numbers and lists in
        its standard library. Included by default! With many functions! *)
    
  typ nat
  term "42::nat"  
  term "0::nat" 
  term Suc 

  typ "int"
  term "42::int"
  term "-42::int"
    
  term Suc  
  (* Note: For numerals, you always have to specify the type *)  
  term "Suc 41" (* Unless clear from type inference! *)
  term "Suc 10"
  term "(10::int) - 7"  
    
  value "2+3 :: nat"  
    
  typ bool  
  term True term False

  typ "'a\<times>'b"
  typ "'a * 'b"  
  term "(True, False)"  
  term "(10::nat, True, 5::int, 6::int)"  
  term "(10::nat, (True, (5::int, 6::int)))"  
    
  term fst term snd 
  value "fst (snd (10::nat, True, 5::int, 6::int))"    
      
    
  typ "'a list"  
  term Nil term Cons  
  term "[]" (* Nil *)
  term "x#l" (* Cons *)
  term "[a,b,c]" (* Syntax sugar for a#b#c#Nil *)
  term "l1@l2" (* append *)
    
  term "l1@x1#x1#l2@l3"  
    
  term "l1@[x1,x2]@l2@l3"
  
  
  (* Arithmetic operations overloaded for int and nat *)
  value "(3::int) + 7"  
  value "(3::nat) + 7"  
    
  value "(1::int)*3 - 4*5 + 2*6^2" (* Priority and associativity of standard operators is as expected *)

  lemma "x\<ge>y \<Longrightarrow> (x::nat) - y + y = x"  oops
    
  (* BEWARE: Subtraction on nat saturates at 0 *)
  value "(5::nat) - 10"  

  (* BEWARE: Division on int rounds down. In many PLs (such as C), it rounds towards zero! *)
  value "(-3::int) div 2"  
    
  (* Numbers in Isabelle are arbitrary precision! *)
  term "3871637126732613123526352635726456213527813658125817512332323232323::nat"  

  value "(3871637126732613123526352635726456213527813658125817512332323232323::int) > 0" 

  lemma "(3871637126732613123526352635726456213527813658125817512332323232323::nat) > 0" 
    by simp
     
end
