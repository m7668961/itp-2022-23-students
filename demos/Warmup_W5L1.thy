theory Warmup_W5L1
imports Main "HOL-Computational_Algebra.Primes"
begin

find_consts name: prime

(* Isar *)

lemma "x\<in>{2..<9} \<Longrightarrow> odd x \<Longrightarrow> prime x" for x :: nat
proof -
  assume "x\<in>{2..<9}"  
  from this have 1: "x\<in>{2,3,4,5,6,7,8}" by fastforce
  
  assume "odd x" 
  from this 1 have "x\<in>{3,5,7}" by auto
  from this show "prime x" by auto
qed  


(** Basic principles *)

lemma "\<And>a b. \<lbrakk>P a b; Q a \<or> Q b\<rbrakk> \<Longrightarrow> R a b"
proof (erule disjE) (* method to start proof with. Typically rule, intro, elim, or just - *)
  (* for each subgoal *)
  fix a b (* fix \<And> - variables *)
  assume "P a b" "Q a" (* assume premises *)
  show "R a b" sorry (* Show conclusion *)
next (* Next subgoal *)  
  fix a b
  assume "P a b" "Q b"
  show "R a b" sorry
qed


(*
  https://en.wikipedia.org/wiki/Euclid%27s_theorem#Euclid's_proof

  There are infinitely many primes:
    Consider any finite list of prime numbers p1, p2, ..., pn. It will be 
    shown that at least one additional prime number not in this list exists. 
    Let P be the product of all the prime numbers in the list: 
    P = p1p2...pn. Let q = P + 1. Then q is either prime or not:
    
    \<^item> If q is prime, then there is at least one more prime that is not in the list, 
      namely, q itself.
    \<^item> If q is not prime, then some prime factor p divides q. If this factor p were in our 
      list, then it would divide P (since P is the product of every number in the list); 
      but p also divides P + 1 = q, as just stated. If p divides P and also q, then p 
      must also divide the difference[3] of the two numbers, which is (P + 1) − P or just 1. 
      Since no prime number divides 1, p cannot be in the list. This means that at least one 
      more prime number exists beyond those in the list.  
  
*)

(* Let's see how this infinity proof works *)

lemma infinite_nextI:
  assumes "\<And>s. \<lbrakk>finite s; \<forall>x\<in>s. P x\<rbrakk> \<Longrightarrow> \<exists>x. x\<notin>s \<and> P x"
  shows "infinite {x. P x}"
  using assms by blast

lemma prod_ge_elems:
  fixes s :: "nat set"
  assumes "finite s" "0\<notin>s" "x\<in>s" 
  shows "x\<le>\<Prod>s"  
proof -
  from \<open>x\<in>s\<close> obtain s' where [simp]: "s=insert x s'" "x\<notin>s'"
    by (rule Set.set_insert)

  have "\<Prod>s = x * \<Prod>s'" using \<open>finite s\<close> by auto
  moreover have "0\<noteq>\<Prod>s'" using \<open>0\<notin>s\<close> \<open>finite s\<close> by auto
  ultimately show ?thesis by auto
qed    
  

lemma "infinite {x::nat. prime x}"
proof (rule infinite_nextI)  
  fix s :: "nat set"
  assume [simp, intro!]: "finite s"
  let ?P="\<Prod>s"
  let ?q="?P+1"

  assume "\<forall>x\<in>s. prime x"
  hence "0\<notin>s" "1\<notin>s" by auto
  
  show "\<exists>x. x\<notin>s \<and> prime x"
  proof cases
    assume "prime ?q"
    moreover have "?q\<notin>s" using prod_ge_elems \<open>0\<notin>s\<close> by fastforce
    ultimately show ?thesis by blast
  next
    assume "\<not>prime ?q"  
    with prime_factor_nat[of ?q]
    obtain p where "prime p" "p dvd ?q" using \<open>0\<notin>s\<close> by auto

    have "p\<notin>s" proof
      assume "p\<in>s"
      hence "p dvd ?P" by blast
      with \<open>p dvd ?q\<close> have "p dvd (?q-?P)" by fastforce
      hence "p=1" by simp
      with \<open>prime p\<close> show False by simp
    qed
    with \<open>prime p\<close> show ?thesis by blast
  qed
qed  


(** Variation with factorial *)

(*
The factorial n! of a positive integer n is divisible by every integer from 2 to n, 
as it is the product of all of them. Hence, n! + 1 is not divisible by any of the 
integers from 2 to n, inclusive (it gives a remainder of 1 when divided by each). 
Hence n! + 1 is either prime or divisible by a prime larger than n. In either case, 
for every positive integer n, there is at least one prime bigger than n. 
The conclusion is that the number of primes is infinite.
*)

lemma infinite_biggerI: "\<lbrakk>\<And>n. \<exists>m>n. P m\<rbrakk> \<Longrightarrow> infinite {p. P p}" for P :: "nat \<Rightarrow> bool"
  by (meson finite_nat_set_iff_bounded mem_Collect_eq order_less_imp_not_less)


lemma "infinite {p::nat. prime p}"
proof -
  {
    fix n :: nat
    have no_div: "\<forall>i\<in>{2..n}. \<not> i dvd (fact n + 1)"
      by (metis Suc_eq_plus1 Suc_eq_plus1_left atLeastAtMost_iff bot_nat_0.extremum dvd_add_left_iff dvd_fact leI nat_1_add_1 nat_dvd_not_less not0_implies_Suc not_less_eq_eq plus_nat.add_0)
    
    have "\<exists>p>n. prime p"
    proof (cases "prime (fact n + 1 :: nat)")
      case True
      have "fact n + 1 > n" 
        using fact_ge_self leI not_less_eq_eq by fastforce
      with True show ?thesis by blast
    next
      case False
      then obtain p' :: nat where "prime p'" "p' dvd fact n + 1"
        by (metis add.commute add_0_iff fact_nonzero prime_factor_nat)
      with no_div have "p'\<notin>{2..n}" by blast
      with \<open>prime p'\<close> have "p'>n"
        by (cases "p'=1"; cases "p'=0"; simp)
      then show ?thesis using \<open>prime p'\<close> by blast
    qed
  }
  thus ?thesis by (rule infinite_biggerI)
qed

end
