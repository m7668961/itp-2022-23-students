theory RBTS_Lib
imports Main "HOL-Library.RBT"
begin

(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_\<alpha> t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

definition "rbts_is_empty t \<equiv> RBT.is_empty t"

definition "rbts_to_list t \<equiv> RBT.keys t"


lemma rbts_empty_correct: "rbts_\<alpha> rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_\<alpha>_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_\<alpha> t" by (auto simp: rbts_member_def rbts_\<alpha>_def)
lemma rbts_insert_correct:
  "rbts_\<alpha> (rbts_insert x t) =  insert x (rbts_\<alpha> t)" by (auto simp: rbts_member_def rbts_insert_def rbts_\<alpha>_def)
lemma rbts_delete_correct:
  "rbts_\<alpha> (rbts_delete x t) = rbts_\<alpha> t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_\<alpha>_def)

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_\<alpha> t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_\<alpha>_def)
qed  

lemma rbts_to_list_sorted: "sorted (rbts_to_list t)" unfolding rbts_to_list_def by auto
lemma rbts_to_list_set: "set (rbts_to_list t) = rbts_\<alpha> t" unfolding rbts_to_list_def rbts_\<alpha>_def rbts_member_def  
  by (auto simp flip: lookup_keys)

  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_\<alpha>_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct
  rbts_to_list_sorted rbts_to_list_set

(* Add operation *)  
  
(* Note: This is a generic operation. 
  It's not specific to RBTs, but the pattern works for all 
  set implementations that support insert and empty  
*)
definition "rbts_from_list xs \<equiv> fold rbts_insert xs rbts_empty"

lemma rbts_from_list_correct[simp]: "rbts_\<alpha> (rbts_from_list xs) = set xs"  
proof -
  have "rbts_\<alpha> (fold rbts_insert xs s) = rbts_\<alpha> s \<union> set xs" for s
    apply (induction xs arbitrary: s)
    by auto
  thus ?thesis unfolding rbts_from_list_def by auto
qed  
  
(* Generic operation to subtract elements from list *)  

definition "rbts_diff_list s xs \<equiv> fold rbts_delete xs s"

lemma rbts_diff_list_correct[simp]: "rbts_\<alpha> (rbts_diff_list s xs) = rbts_\<alpha> s - set xs"
  unfolding rbts_diff_list_def
  apply (induction xs arbitrary: s)
  by auto

end
