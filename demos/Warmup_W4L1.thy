theory Warmup_W4L1
imports Main
begin

  section \<open>Single-step proofs\<close>

  subsection \<open>Instantiation\<close>
  
  (* Positional *)
  thm conjI
  thm conjI[of True False]
  thm conjI[of "_=_"]  (** Wildcards *)
  thm conjI[of "_=_" "_=_"]
  thm conjI[of "_=y" "y=z" for y z] (** Named variables *)

  (* By name *)    
  
  thm conjI[where P="_=_"]
  thm conjI[where P="x=x" and Q="x=_" for x]
  
  subsection \<open>Forward reasoning\<close>
  
  (** 
    Modify a \<^bold>t\<^bold>h\<^bold>e\<^bold>o\<^bold>r\<^bold>e\<^bold>m
  *)
  
  (* OF: resolve premises by given theorems:
  
    A\<Longrightarrow>B OF C\<Longrightarrow>A yields C\<Longrightarrow>B
  *)
  
  notepad begin
    fix A B C D E F G :: bool
  
    assume 1: "E" and a1: "D"
    
    thm conjI 1 conjI[OF 1]
    thm conjI[OF _ 1]
    thm conjI 1 a1 conjI[OF 1 a1]

    assume 2: "A\<Longrightarrow>B\<Longrightarrow>C"
    assume 3: "B\<Longrightarrow>C\<Longrightarrow>D"
        
    (* If theorem has preconds, these are inserted instead of original premise *)
    thm conjI 2 conjI[OF 2]
    thm conjI[OF _ 2]
    thm conjI[OF 3 2]
    
    thm conjI[OF 3, OF _ 2] (** Break down complicated patterns to better understand what's going on *)
    thm conjI[OF 3] 2 conjI[OF 3, OF _ 2]

    (* Unification: theorems are unified *)
    thm conjI[of "_\<or>_"] refl conjI[of "_\<or>_",OF refl]
  end

  (** THEN: chain theorems together
  
    A\<Longrightarrow>B THEN B\<Longrightarrow>C  yields A\<Longrightarrow>C
  
  *)
  
  notepad begin
    fix A B C D E F G :: bool
  
    assume 1: "A\<Longrightarrow>B"
    assume 2: "B\<Longrightarrow>C"
    
    thm 1[THEN 2]
    
    assume 3: "B\<Longrightarrow>D\<Longrightarrow>C"
    
    thm 1 3 1[THEN 3]
    thm 3[OF 1]
    
    assume 4: "D\<Longrightarrow>B\<Longrightarrow>C"
    thm 1 4 1[THEN 4]
    thm 4[OF _ 1]
    
    assume 5: "A\<Longrightarrow>E\<Longrightarrow>B"
    
    thm 5 3 5[THEN 3]

  end    

  (** Useful examples *)
    
  thm iffD1 iffD2
  
  thm takeWhile_eq_all_conv
  
  thm 
    takeWhile_eq_all_conv[THEN iffD1]
    takeWhile_eq_all_conv[THEN iffD2]

  thm ballI
  
  thm takeWhile_eq_all_conv[THEN iffD2, OF ballI]
        
  thm order_trans
  thm order_trans[THEN order_trans]
  

  subsection \<open>Backwards reasoning\<close>
  
  (**
  
    Modify a \<^bold>s\<^bold>u\<^bold>b\<^bold>g\<^bold>o\<^bold>a\<^bold>l
  *)

  
  notepad begin
    fix A B C D E F G :: bool
  
    (**
      rule: resolve conclusion with rule:
  
      1. \<lbrakk>\<dots>\<rbrakk> \<Longrightarrow> C
      
        rule \<lbrakk>A\<^sub>1;\<dots>;A\<^sub>n\<rbrakk> \<Longrightarrow> C
        
      1. \<lbrakk>\<dots>\<rbrakk> \<Longrightarrow> A\<^sub>1
         ...
      n. \<lbrakk>\<dots>\<rbrakk> \<Longrightarrow> A\<^sub>n
    *)
      

    assume 1: "\<lbrakk>A;B;C;D\<rbrakk> \<Longrightarrow> E"
    
    have "\<lbrakk>F;G\<rbrakk> \<Longrightarrow> E"
      apply (rule 1)
      sorry
      
    (**
      drule: resolve matching premise with rule
    
      1. \<lbrakk> \<dots> A \<dots> \<rbrakk> \<Longrightarrow> C
      
        drule A \<Longrightarrow> B
      
      1. \<lbrakk> \<dots> \<dots> B \<rbrakk> \<Longrightarrow> C  (* New premise moves to end *)
    *)

    assume 2: "F\<Longrightarrow>B"
    have "\<lbrakk> F; A; G \<rbrakk> \<Longrightarrow> E"
      apply (drule 2)
      sorry    

    (**
      erule: simultaneosuly resolve premise \<^bold>a\<^bold>n\<^bold>d conclusion
    
      1. \<lbrakk> \<dots> A \<dots> \<rbrakk> \<Longrightarrow> C

          erule \<lbrakk> A; P\<^sub>1; \<dots>; P\<^sub>n \<rbrakk> \<Longrightarrow> C
          
      1. P\<^sub>1
      \<dots>
      n. P\<^sub>n      
    *)

    assume 3: "\<lbrakk> A;  B;C \<rbrakk> \<Longrightarrow> D"
              
    have "\<lbrakk> E;A;F \<rbrakk> \<Longrightarrow> D"
      apply (erule 3)
      sorry
    
      
  end  
  
  (* Erule: very often with single ?var as conclusion. ex-elim, case-rules *)
  
  thm exE
  
  (** Note: 
    exists x. Q x 
    \<equiv>
    every P can be proved, 
      by proving it under the assumption Q x 
      for some arbitrary but fixed x
  
  *)
  lemma "(\<forall>P. (\<forall>x. Q x \<longrightarrow> P) \<longrightarrow> P) \<longleftrightarrow> (\<exists>x. Q x)" 
    by blast
  
  
  thm bool.exhaust
  thm nat.exhaust
  thm list.exhaust
  
  thm nat.induct
  
  thm sorted_wrt.cases
  
  
  
  
  
  subsection \<open>Useful Examples\<close>
  
  lemma "\<exists>x. \<forall>y. P x y \<Longrightarrow> \<forall>y. \<exists>x. P x y"
    apply (rule allI)
    apply (erule exE)
    apply (rule exI)
    apply (drule spec)
    apply assumption
    done
  
  thm exI
  lemma "\<exists>a b c::nat. a>b \<and> b>c \<and> c>0 \<and> a+b^2+c^3=8"
    apply (rule exI[where x="3"])
    apply (rule exI[where x="2"])
    apply (rule exI[where x="1"])
    by simp
      
  thm  equalityI subsetI
  lemma "A = B" for A B :: "_ set"
    apply (intro equalityI subsetI)  (** intro: apply rules exhaustively, recursively *)
    oops
    
  lemma "(\<exists>a b c. P a b c) \<Longrightarrow> Q"  
    apply (elim exE)
    oops
    
  (** There's no dest - method! but you can use 
    elim and [elim_format] to convert dest to elim rule *)  
  thm spec spec[elim_format]  
    
  lemma "\<lbrakk> \<forall>x y z. P x y z \<rbrakk> \<Longrightarrow> P 1 1 1"
    apply (elim spec[of _ "1", elim_format])
    .
  

  subsection \<open>Unification\<close>
  
  lemma "\<exists>x. \<forall>y. P x y \<Longrightarrow> \<forall>y. \<exists>x. P x y"
    apply (rule allI)
    apply (rule exI)
    apply (erule exE)
    apply (drule spec)
    apply assumption

    oops
  
  (** To unify t and ?x, all bound variables in t must also be parameters of ?x ! 
  
    This is necessary for soundness:
  *)  
  lemma "\<forall>y. \<exists>x. P x y \<Longrightarrow> \<exists>x. \<forall>y. P x y"
    apply (rule exI)
    apply (rule allI)
    apply (drule spec)
    apply (erule exE)
    oops
    
  subsection \<open>Debugging\<close>

  (* Why doesn't my rule apply? *)
  
  lemma "x \<le> x" for x :: int
    apply (rule refl)
    supply [[unify_trace_failure]]
    apply (rule refl)
    apply (rule order_refl)
    supply [[show_sorts]]
    apply (rule order_refl)
    oops
    
    
end
