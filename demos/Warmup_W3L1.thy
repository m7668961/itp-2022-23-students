theory Warmup_W3L1
imports Main
begin


  (* Case splitting *)
  
  lemma "(case xs of [] \<Rightarrow> 0 | _ \<Rightarrow> hd xs) > 0 \<Longrightarrow> xs=[] \<or> hd xs > 0"
    by (simp split: list.splits)

  lemma "(case xs of [] \<Rightarrow> 0 | [Suc 0] \<Rightarrow> 1 | _ \<Rightarrow> hd xs) > 0 \<Longrightarrow> xs=[] \<or> hd xs > 0"
    apply (simp split: list.splits nat.splits)
    done
  
(*    apply (split list.splits)
    apply simp
    apply simp
    done *)
    
  (* for each datatype T, there's a T.splits lemma *)  
    
  (* Idiosyncra{s/z}ies / exceptions *)
  
  (* If (in conclusion) is split by default *)
  
  lemma "(if P then 2 else Suc 0) \<ge> 1" by simp
   
  lemma "(if P then 2 else Suc 0) \<ge> 1" 
    apply (simp split del: if_split)
    oops

  (* But not in assumptions *)  
  lemma "(if P then x else x+1) > 2 \<Longrightarrow> x>1" for x :: nat
    apply (simp)
    apply (simp split: if_splits)
    done

  (* auto and clarsimp split *bound* variables of product type *)  

  lemma "\<And>(a :: _ \<times>_) (b :: _ \<times>_). Q a b \<Longrightarrow> P a b"    
    apply simp
    apply clarsimp
    done
  
      
  lemma "\<And>a b. fst a = fst b \<Longrightarrow> snd a = snd b \<Longrightarrow> a=b"    
    apply simp
    apply clarsimp
    done

  lemma "fst a = fst b \<Longrightarrow> snd a = snd b \<Longrightarrow> a=b"    
    apply simp
    apply clarsimp
    apply auto
    done

  lemma "\<And>a b. fst a = fst b \<Longrightarrow> snd a = snd b \<Longrightarrow> a=b"    
    subgoal for a b
      apply simp
      apply clarsimp
      apply auto
    oops

  (* You'll encounter that situation during proofs \<dots> frequently! *)  
    

  (* You can decide to only split in conclusion, or only in assumption *)
  
  lemma "(case P of True \<Rightarrow> x>3 | _ \<Rightarrow> x>2) \<Longrightarrow> (case Q of True \<Rightarrow> x | _ \<Rightarrow> x+1) > 1" for x :: nat
    apply simp
    apply (simp split: bool.splits)  (* splits = split +  split_asm *)

    apply (simp split: bool.split_asm)
    apply (simp split: bool.split)
    apply (simp split: bool.split)
    
    apply (simp split: bool.splits)  (* splits = split +  split_asm *)
    oops
  
  (* Rarely used! Usually: splits! *)  
    
  
  
  (* Lemma collection: algebra_simps *)
  
  lemma "(a+b)*(a-b) = a*a - b*(b::int)"
    apply simp
    apply(simp add: algebra_simps) (* More arithmetic rules *)
    done

  

  
  (** Let's prove something useful *)

  text \<open>From \<^url>\<open>https://hackage.haskell.org/package/base-4.17.0.0/docs/src/GHC.List.html#span\<close>\<close>
  (** 
    -- | 'span', applied to a predicate @p@ and a list @xs@, returns a tuple where
    -- first element is longest prefix (possibly empty) of @xs@ of elements that
    -- satisfy @p@ and second element is the remainder of the list:
    --
    -- >>> span (< 3) [1,2,3,4,1,2,3,4]
    -- ([1,2],[3,4,1,2,3,4])
    -- >>> span (< 9) [1,2,3]
    -- ([1,2,3],[])
    -- >>> span (< 0) [1,2,3]
    -- ([],[1,2,3])
    --
    -- 'span' @p xs@ is equivalent to @('takeWhile' p xs, 'dropWhile' p xs)@
    span                    :: (a -> Bool) -> [a] -> ([a],[a])
    span _ xs@[]            =  (xs, xs)
    span p xs@(x:xs')
             | p x          =  let (ys,zs) = span p xs' in (x:ys,zs)
             | otherwise    =  ([],xs)
  
  *)
  
  fun span :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> 'a list \<times> 'a list" where
    "span p [] = ([],[])"
  | "span p (x#xs') = (if p x then let (ys,zs) = span p xs' in (x#ys,zs) else ([],x#xs'))"  
  
  lemma span_eq_take_drop: "span p xs = (takeWhile p xs, dropWhile p xs)"
    apply (induction xs)
    apply auto
    done
    
  lemma "span p xs = (ys,zs) \<Longrightarrow> xs=ys@zs"
    apply (induction xs arbitrary: ys)
    apply (auto split: if_splits prod.splits)
    done   

  lemma "span p xs = (ys,zs) \<Longrightarrow> xs=ys@zs"
    apply (auto simp add: span_eq_take_drop)  
    (*
    apply clarify
    apply simp
    find_theorems "takeWhile _ _ @ dropWhile _ _"
    *)
    done
    
  (**
    define function
    show eq takeWhile , dropWhile
  
    show span p xs = (ys,zs) \<Longrightarrow> xs = ys @ zs
     alt: fst (span p xs) @ snd (span p xs) = xs
     and via: span_eq_take_drop
    
  *)

  
  text \<open>From \<^url>\<open>https://hackage.haskell.org/package/base-4.17.0.0/docs/src/Data.OldList.html#groupBy\<close>\<close>  
  
  (*
    -- | The 'group' function takes a list and returns a list of lists such
    -- that the concatenation of the result is equal to the argument.  Moreover,
    -- each sublist in the result contains only equal elements.  For example,
    --
    -- >>> group "Mississippi"
    -- ["M","i","ss","i","ss","i","pp","i"]
    --
    -- It is a special case of 'groupBy', which allows the programmer to supply
    -- their own equality test.
    group                   :: Eq a => [a] -> [[a]]
    group                   =  groupBy (==)
    
    -- | The 'groupBy' function is the non-overloaded version of 'group'.
    groupBy                 :: (a -> a -> Bool) -> [a] -> [[a]]
    groupBy _  []           =  []
    groupBy eq (x:xs)       =  (x:ys) : groupBy eq zs
                               where (ys,zs) = span (eq x) xs
  *)
  
  lemma aux: "(xb, y) = span p xs \<Longrightarrow> length y < Suc (length xs)"
    apply (induction xs arbitrary: xb)
    apply (auto split: if_splits prod.splits)
    done

  context
    (*notes [simp] = aux*)
  begin    
  
  fun groupBy :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> 'a list list" where
    "groupBy _ [] = []"
  | "groupBy eq (x#xs) = (let (ys,zs) = span (eq x) xs in (x#ys) # groupBy eq zs)"  
  
  end
  
  (**
  
    function definition
    aux-lemma for termination
    
    prove no empty groups, no elements dropped
  
  *)
  
  
  
  
  
  
  
  
  
  
  (* Binary trees. We stopped at delete! *)  
  
  
  

end
