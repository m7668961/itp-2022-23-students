theory DFS_Demo
imports Graph_Lib RBTS_Lib While_Lib
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  section \<open>Prerequisites\<close>

  subsection \<open>Overview of Used ADTs\<close>

  (**
    Sets (by red-black trees)
  *)

  term rbts_\<alpha>
  
  term rbts_empty thm rbts_empty_correct

  term rbts_is_empty thm rbts_is_empty_correct
  
  term rbts_member thm rbts_member_correct
    
  term rbts_insert thm rbts_insert_correct
  
  term rbts_delete thm rbts_delete_correct
  
  term rbts_from_list thm rbts_from_list_correct
  
  term rbts_diff_list thm rbts_diff_list_correct

  term rbts_to_list thm rbts_to_list_sorted rbts_to_list_set
    

  (**
    Graphs (as adjacency list, by red-black trees)
  *)

  term rg_\<alpha>
  
  term rg_empty thm rg_empty_correct
  
  term rg_add_edge thm rg_add_edge_correct

  term rg_succs thm rg_succs_correct


  subsection \<open>Overview of While-Loop Rules\<close>
  
  term while_option
  thm while_option_rule'
  
  section \<open>Algorithm\<close>
  
  

  text \<open>We implicitly fix a graph, and a start node. 
    This is just to make things more readable.
  \<close>
  context
    fixes gi :: "'v::linorder rbt_graph" and v\<^sub>0 :: 'v
  begin

    
  definition "dfs_aux \<equiv> 
    while_option (\<lambda>(d,w). w\<noteq>[]) (\<lambda>(d,w). 
      let (v,w) = (hd w, tl w) in
      if rbts_member v d then (d,w)
      else 
        let d = rbts_insert v d in
        let w = rg_succs gi v @ w in
        (d,w)
    
    ) (rbts_empty,[v\<^sub>0]) 
  "

  definition "dfs \<equiv> fst (the (dfs_aux))"

  
  section \<open>Correctness Proof\<close>
  
    abbreviation (input) "g \<equiv> rg_\<alpha> gi"
  
    text \<open>Correctness can only be shown under the assumption that 
      only finitely many nodes are reachable from the start node!

      Fortunately, red-black trees can only hold finitely many nodes,
      and each successor list can only be finite. Thus, the graph is finite,
      and so is the set reachable from any node.
    \<close>
    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" using rg_finite_ran by simp
      finally (finite_subset) show ?thesis .
    qed        
      
    (*
      Invariant for DFS:
      
      \<^item> v\<^sub>0 is in d \<union> w
      
      \<^item> everything in d and w is reachable
      
      \<^item> making one step from d ends you in d \<union> w
    *)

    definition "dfs_invar \<equiv> \<lambda>(d,w). 
      v\<^sub>0\<in>rbts_\<alpha> d \<union> set w
    \<and> rbts_\<alpha> d \<union> set w \<subseteq> g\<^sup>*``{v\<^sub>0}
    \<and> g``rbts_\<alpha> d \<subseteq> rbts_\<alpha> d \<union> set w
    "

    text \<open>The exact form of the following auxiliary lemmas has been determined 
      during the below proof attempt\<close>
    
    
    lemma invar_init: "dfs_invar (rbts_empty,[v\<^sub>0])"
      unfolding dfs_invar_def
      by (simp)
    
    lemma invar_step1: 
      "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<in>rbts_\<alpha> d \<Longrightarrow> dfs_invar (d,tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply auto
      done
      
    lemma invar_step2: 
      "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<notin>rbts_\<alpha> d 
      \<Longrightarrow> dfs_invar (rbts_insert (hd w) d, rg_succs gi (hd w) @ tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply (auto)
      done
      
    (* Note: if set is closed by step, it's refl-transitively closed *)
    thm Image_closed_trancl
    
    lemma invar_final: "dfs_invar (d,[]) \<Longrightarrow> rbts_\<alpha> d = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_invar_def
      apply (auto)
      thm Image_closed_trancl
      by (metis Image_closed_trancl rev_ImageI)


    (*
      Termination: either the unfinished reachable nodes decrease, or
        they stay the same, and the length of the worklist decreases:
    *)  
    definition "dfs_wfrel \<equiv> 
      inv_image 
        (inv_image finite_psubset (\<lambda>d. g\<^sup>*``{v\<^sub>0} - d) <*lex*> measure length) 
        (apfst rbts_\<alpha>)"
        
    lemma dfs_wfrel_wf: "wf dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto
      
    lemma dfs_wfrel1: "w \<noteq> [] \<Longrightarrow> ((d, tl w), (d, w)) \<in> dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto

    lemma dfs_wfrel2: 
      assumes "dfs_invar (d, w)" "w \<noteq> []" "hd w \<notin> rbts_\<alpha> d"
      shows "((rbts_insert (hd w) d,w'),(d,w)) \<in> dfs_wfrel"
    proof -  
      
      have "hd w\<in>g\<^sup>*``{v\<^sub>0}" 
        using assms(1,2)
        unfolding dfs_invar_def
        by (auto simp: neq_Nil_conv)
      with \<open>hd w \<notin> rbts_\<alpha> d\<close> show ?thesis
        unfolding dfs_wfrel_def
        using finite_reachable
        by auto
        
    qed    
      
    lemma dfs_aux_correct: "\<exists>rbts. dfs_aux = Some (rbts,[]) \<and> rbts_\<alpha> rbts = g\<^sup>*``{v\<^sub>0}"
      unfolding dfs_aux_def
      apply (rule while_option_rule'[where P=dfs_invar and r=dfs_wfrel])
      subgoal by (auto simp: invar_init)
      subgoal by (auto simp: invar_step1 invar_step2)
      subgoal by (auto simp: invar_final)
      
      subgoal by (rule dfs_wfrel_wf)
      subgoal by (auto simp: dfs_wfrel1 dfs_wfrel2)
      done

    lemma dfs_correct: "rbts_\<alpha> (dfs) = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_def using dfs_aux_correct by force

      
    section \<open>Running the thing\<close>
    value "rbts_to_list (DFS_Demo.dfs 
      (rg_from_lg [(0,1),(1,2),(2,1),(2,3),(4,5)]) 
      (0::nat))"
      
    value "let n=10000 in \<not>rbts_is_empty (DFS_Demo.dfs 
      (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat))"
    value "let n=100000 in \<not>rbts_is_empty (DFS_Demo.dfs 
      (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat))"
      

    section \<open>Searching a Node and Returning a Path\<close>

    text \<open>
      We now search for a node that satisfies a predicate P. 
      
      Also, in the worklist, we keep track of a path to each node. 
      Thus, when we find a node where P holds, we can return a path to that node.
    \<close>
    
    (* Some magic to define a datatype with a linorder type constraint.
      
      Alternative: just use a fixed type, e.g. int, for nodes.
    *)
    context
      notes [[typedef_overloaded]]
    begin
    
      text \<open>In our loop, we now have two states: 
        \<^enum> we are still searching, and have a done-set and a worklist
        \<^enum> we have found a node and a path to it
      \<close>
    
      datatype 'vv::linorder state = 
        SEARCHING "'vv::linorder rbts" "('vv \<times> 'vv list) list" 
      | FOUND 'vv "'vv list"
    end  

    (** Contexts can be nested *)
    context
      fixes P :: "'v \<Rightarrow> bool"  \<comment> \<open>The property we are searching for\<close>
    begin    
      
      definition "dfs'_aux \<equiv> 
        while_option (\<lambda>SEARCHING d w \<Rightarrow> w\<noteq>[] | FOUND _ _ \<Rightarrow> False) (\<lambda>SEARCHING d w \<Rightarrow> 
          let ((v,p),w) = (hd w, tl w) in
          if rbts_member v d then SEARCHING d w
          else if P v then FOUND v (rev p)
          else 
            let d = rbts_insert v d in
            let w = map (\<lambda>v'. (v',v'#p)) (rg_succs gi v) @ w in
            SEARCHING d w
        
        ) (SEARCHING rbts_empty [(v\<^sub>0,[])]) 
      "
      
      definition "dfs' 
        = (case the dfs'_aux of SEARCHING _ _ \<Rightarrow> None | FOUND v p \<Rightarrow> Some (v,p))"
      
      fun path where
        "path u [] v \<longleftrightarrow> u=v"
      | "path u (v#vs) v' \<longleftrightarrow> (u,v)\<in>g \<and> path v vs v'"  

      (* Append-lemma: paths can be easily concatenated *)
      lemma path_append[simp]: "path u (vs@vs') v \<longleftrightarrow> (\<exists>v'. path u vs v' \<and> path v' vs' v)"
        apply (induction vs arbitrary: u)
        apply auto
        done
    
      text \<open>We define when the result of \<open>dfs'_aux\<close> is correct.
        \<^item> if the loop stops still searching, there is no reachable node that satisfies \<open>P\<close>
        \<^item> if we found a node, the path leads to that node and \<open>P\<close> holds for that node
      \<close>  
      fun correct_res_aux where
        "correct_res_aux (SEARCHING _ _) \<longleftrightarrow> (\<forall>v\<in>g\<^sup>*``{v\<^sub>0}. \<not>P v)"
      | "correct_res_aux (FOUND v p) \<longleftrightarrow> path v\<^sub>0 p v \<and> P v"

      text \<open>For the invariant, we re-use the original invariant, and add the following:
        \<^item> the paths recorded in the worklist are actually paths to their corresponding nodes
        \<^item> nodes in the done set do not satisfy \<open>P\<close>
        \<^item> if we are in found-state, we have a valid path to a node that satisfies \<open>P\<close>
      \<close>    
      fun dfs'_invar where
        "dfs'_invar (SEARCHING d w) \<longleftrightarrow> 
            dfs_invar (d,map fst w) 
          \<and> (\<forall>(v,p)\<in>set w. path v\<^sub>0 (rev p) v)
          \<and> (\<forall>v\<in>rbts_\<alpha> d. \<not>P v)
          "
      | "dfs'_invar (FOUND v p) \<longleftrightarrow> path v\<^sub>0 p v \<and> P v"
      
      
      text \<open>Termination: either we switch from searching to found, 
        or the dfs-state gets smaller according to the basic DFS's relation.
        
        We encode that as follows:
      \<close>
      fun state_m where
        "state_m (SEARCHING d w) = (1::nat,(d,map fst w))"
      | "state_m (FOUND _ _) = (0,undefined)"

      definition "dfs'_wfrel \<equiv> inv_image (less_than <*lex*> dfs_wfrel) state_m"

      text \<open>The following auxiliary lemma have been discovered during the below proof attempt\<close>
              
      lemma dfs'_invar_dnP: "\<lbrakk>dfs'_invar (SEARCHING d w); P v\<rbrakk> \<Longrightarrow> v\<notin>rbts_\<alpha> d" by auto
      
      lemma dfs'_invar_found: "\<lbrakk>dfs'_invar (SEARCHING d ((v, p) # w)); P v\<rbrakk>
         \<Longrightarrow> dfs'_invar (FOUND v (rev p))" by auto  
      
      lemma dfs'_invar_step1: "\<lbrakk>dfs'_invar (SEARCHING d ((v, p) # w)); v \<in> rbts_\<alpha> d\<rbrakk>
         \<Longrightarrow> dfs'_invar (SEARCHING d w)"
        apply simp
        using invar_step1[of d "v#map fst w"]
        by simp
    
      lemma dfs'_invar_step2: "\<lbrakk>dfs'_invar (SEARCHING d ((v, p) # w)); \<not> P v; v \<notin> rbts_\<alpha> d\<rbrakk>
        \<Longrightarrow> dfs'_invar (SEARCHING (rbts_insert v d) (map (\<lambda>v'. (v', v' # p)) (rg_succs gi v) @ w))"
        apply simp
        using invar_step2[of d "v#map fst w"]
        apply (simp add: comp_def)
        apply auto
        done
      
      lemma dfs'_invar_final1: "\<lbrakk>dfs'_invar (SEARCHING d []) \<rbrakk>
        \<Longrightarrow> rbts_\<alpha> d = (rg_\<alpha> gi)\<^sup>* `` {v\<^sub>0} \<and> (\<forall>v\<in>(rg_\<alpha> gi)\<^sup>* `` {v\<^sub>0}. \<not> P v)"
        using invar_final
        by simp
        
      lemma dfs'_invar_final2: "dfs'_invar (FOUND d w) \<Longrightarrow> path v\<^sub>0 w d \<and> P d" by simp
        
      lemma dfs'_aux_correct: "\<exists>res. dfs'_aux = Some res \<and> correct_res_aux res"
        unfolding dfs'_aux_def
        apply (rule while_option_rule'[where P=dfs'_invar and r="dfs'_wfrel"])
        
        subgoal by (auto simp: invar_init)
        subgoal for s
          text \<open>We deliberately do not unfold dfs'_invar, 
            such that we get more readable subgoals which we then convert into auxiliary lemmas\<close>
          supply [simp del] = dfs'_invar.simps
          text \<open>We use auto for proof exploration here. 
            Afterwards, when we have found the auxiliary lemmas,
            we would clean up the proof, such that auto only occurs as last method of the proof.
            \<close>
          apply (auto simp: Let_def neq_Nil_conv split: state.split prod.split)
          
          subgoal for d v p w by (simp add: dfs'_invar_dnP)
          subgoal for d v p w by (simp add: dfs'_invar_found)
          subgoal for d v p w by (erule dfs'_invar_step1)
          subgoal for d v p w by (erule dfs'_invar_step2)
          done
        subgoal for s
          supply [simp del] = dfs'_invar.simps
          apply (cases s; simp)
          subgoal for d w using dfs'_invar_final1 by simp
          subgoal for d w using dfs'_invar_final2 by simp 
          done
        subgoal 
          by (auto simp: dfs_wfrel_wf dfs'_wfrel_def)
        subgoal for s
          apply (cases s; simp add: dfs'_wfrel_def)
          by (auto split: prod.split simp: map_tl neq_Nil_conv dfs_wfrel1 comp_def dest: dfs_wfrel2)
        done  
     
      lemma dfs'_correct: "case dfs' of 
          None \<Rightarrow> \<forall>v\<in>g\<^sup>*``{v\<^sub>0}. \<not>P v 
        | Some (v,p) \<Rightarrow> path v\<^sub>0 p v \<and> P v"
        unfolding dfs'_def using dfs'_aux_correct
        by (auto split: state.split)
                     
    end    
  end
  
  term dfs'
  
  value "let n=1000 in (dfs' (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat) (\<lambda>v. v>200))"
            
end
