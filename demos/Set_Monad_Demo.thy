theory Set_Monad_Demo
imports Main "HOL-Library.Monad_Syntax" "HOL-Library.Multiset"
begin

section \<open>Definition\<close>

(* Return x is {x}.*)

definition "return x \<equiv> {x}"

thm Set.bind_def

lemma "Set.bind m f = (\<Union>(f`m))" by auto

lemma "r\<in>Set.bind m f \<longleftrightarrow> (\<exists>x\<in>m. r\<in>f x)" by auto



(* Take any element x from s, then run f x *)
lemma 
  "do { x\<leftarrow>s; f x } = { r. \<exists>x. x\<in>s \<and> r\<in>f x }"
  by auto

(* Shorter *)  
lemma 
  "do { x\<leftarrow>s; f x } = \<Union>(f`s)"
  by auto

(* Intuition: possible results of nondeterministic computation. *)
  
section \<open>Monad Laws\<close>

(** Monad laws *)  
context
  notes [simp] = return_def
begin
  lemma "do { x \<leftarrow> return a; f x } = f a" by auto
  lemma "do { x \<leftarrow> m; return x } = m" by auto
  lemma "do { y \<leftarrow> do { x \<leftarrow> m; f x }; g y } = do { x\<leftarrow>m; y \<leftarrow> f x; g y }"
    for m :: "_ set"
    by (rule Set.bind_bind)

end  
  
(* Empty set: no possible results at all! *)  
lemma "do { x\<leftarrow>{}; f x } = {}" by simp
lemma "do { x\<leftarrow>m; {} } = {}" by auto

section \<open>Specification\<close>

definition "wp f Q \<longleftrightarrow> (\<forall>x\<in>f. Q x)"

subsection \<open>Proof Rules\<close>

context
  notes [simp] = return_def wp_def
begin

lemma wp_return: "wp (return a) Q \<longleftrightarrow> Q a" by auto
lemma wp_bind: "wp (do { x\<leftarrow>m; f x}) Q \<longleftrightarrow> wp m (\<lambda>x. wp (f x) Q)"
  by (auto simp: Set.bind_def)

lemma wp_spec: "wp {x. P x} Q \<longleftrightarrow> (\<forall>x. P x \<longrightarrow> Q x)" by auto

(* Empty program: satisfies any specification! *)
lemma wp_empty: "wp {} Q" by auto

lemmas wp_simps[simp] = wp_return wp_bind wp_spec wp_empty
  
(* Consequence rule: weaken postcondition*)
lemma wp_cons: 
  assumes "wp c Q"  
  assumes "\<And>r. Q r \<Longrightarrow> Q' r"  
  shows "wp c Q'" 
  using assms
  by auto

end


subsection \<open>Quicksort Spec\<close>

subsubsection \<open>Specification of Sorting\<close>
(* Top-Down: 1st step: specify sorting *)
definition "sort_spec xs xs' \<longleftrightarrow> mset xs' = mset xs \<and> sorted xs'"


subsubsection \<open>Specification of Partitioning\<close>

(* Example: partition a list. Any partition does the job. 
  Not caring about order or where elements equal to pivot go *)
definition partition_spec :: "'a::linorder \<Rightarrow> 'a list \<Rightarrow> 'a list \<times> 'a list \<Rightarrow> bool"
where "partition_spec p xs \<equiv> \<lambda>(ls,rs). 
    mset xs = mset ls + mset rs   \<comment> \<open>Elements preserved\<close>
  \<and> (\<forall>x\<in>set ls. x\<le>p) \<comment> \<open>Left partition: \<open>\<le> p\<close>\<close>
  \<and> (\<forall>x\<in>set rs. x\<ge>p) \<comment> \<open>Right partition: \<open>\<ge> p\<close>\<close>
"


subsubsection \<open>Specification of Quicksort\<close>

(* We leave the actual partitioning open for now, and just use the specification *)

(* Tell function package that two binds are equal, already if second arguments 
  are equal only for possible first arguments.
  
  The termination prover uses it to create extra assumptions, that help with proving!
*)
lemma bind_cong[fundef_cong]: 
  "s=s' \<Longrightarrow> (\<And>x. x\<in>s' \<Longrightarrow> f x = f' x) \<Longrightarrow> do { x\<leftarrow>s; f x } = do { x\<leftarrow>s'; f' x }" by auto
 

function (sequential) quicksort :: "'a::linorder list \<Rightarrow> 'a list set" where
  "quicksort [] = return []" 
| "quicksort (p#xs) = do {
    (ls,rs) \<leftarrow> {x. partition_spec p xs x};
    ls \<leftarrow> quicksort ls;
    rs \<leftarrow> quicksort rs;
    return (ls@p#rs) 
  }"  
  by pat_completeness auto
  
termination
  apply (relation \<open>measure length\<close>)
  apply (auto simp: partition_spec_def)
  apply (metis le_add1 le_imp_less_Suc size_mset size_union)
  by (metis less_add_Suc2 size_mset size_union)

(* Note: generated induction rule is hard to use, as second IH depends on 
  "xb \<in> quicksort xa". We prove a stronger and easier to use rule: *)  
thm quicksort.induct
  
lemma quicksort_induct'[case_names empty rec]:
  assumes "P []"
  assumes "\<And>p xs.\<lbrakk>
    \<And>ls rs. \<lbrakk> partition_spec p xs (ls,rs) \<rbrakk> \<Longrightarrow> P ls;
    \<And>ls rs. \<lbrakk> partition_spec p xs (ls,rs) \<rbrakk> \<Longrightarrow> P rs
    \<rbrakk> 
    \<Longrightarrow> P (p#xs)"
  shows "P xs"
proof -
  have "wf (measure length)" by simp
  thus ?thesis proof (induction)
    case (less xs)
    
    show "P xs" 
    proof (cases xs)
      case Nil
      then show ?thesis using assms(1) by simp
    next
      case [simp]: (Cons p xs')
      show ?thesis proof (simp, rule assms(2))
        fix ls rs
        assume "partition_spec p xs' (ls, rs)"
        hence "length ls < Suc (length xs')" "length rs < Suc (length xs')"
          unfolding partition_spec_def
          apply -
          apply (clarsimp, metis less_iff_Suc_add size_mset size_union)
          apply (clarsimp, metis less_add_Suc2 size_mset size_union)
          done
        from this[THEN less.IH[simplified]] show "P ls" "P rs" .
      qed
    qed
  qed
qed  


(* The essential idea of quicksort *)
lemma quicksort_lemma:
  assumes "partition_spec p xs (ls,rs)"
  assumes "sort_spec ls ls'"
  assumes "sort_spec rs rs'"
  shows "sort_spec (p#xs) (ls'@p#rs')"
  using assms
  unfolding partition_spec_def sort_spec_def
  by (fastforce simp: sorted_append mset_eq_setD[of ls'] mset_eq_setD[of rs'])
  
  
lemma quicksort_correct: "wp (quicksort xs) (\<lambda>xs'. sort_spec xs xs')"
proof (induction xs rule: quicksort_induct')
  case empty
  then show ?case by (simp add: sort_spec_def)
next
  case (rec p xs)
  show ?case   
  proof (simp, intro allI impI)
    fix ls rs
    assume PART: "partition_spec p xs (ls,rs)"
    
    from rec.IH[OF PART] have 
      LREC: "wp (quicksort ls) (sort_spec ls)"
      and RREC: "wp (quicksort rs) (sort_spec rs)"
      .
    
    show "wp (quicksort ls) (\<lambda>ls'. 
          wp (quicksort rs) (\<lambda>rs'. 
          sort_spec (p # xs) (ls' @ p # rs')
        ))"
      apply (rule wp_cons)
      apply (rule LREC)
      apply (rule wp_cons)
      apply (rule RREC)
      using quicksort_lemma[OF PART] by simp
  qed
qed

section \<open>Refinement\<close>

(* Monotonicity and Refinement *)  

  
(* Refinement: s \<subseteq> s': all possible results of s, are also possible results of s'.

  In particular: if s' is correct, so is s
*)
  
(*
  Monotonicity: bind is monotone
*)    

lemma bind_mono:
  assumes "s\<subseteq>s'"  
  assumes "(\<And>x. x\<in>s \<Longrightarrow> x\<in>s' \<Longrightarrow> f x \<subseteq> f' x)"
  shows "do { x\<leftarrow>s; f x } \<subseteq> do {x\<leftarrow>s'; f' x}"
  using assms by force

lemma wp_mono:
  assumes "s\<subseteq>s'"
  assumes "wp s' Q"
  shows "wp s Q"
  using assms unfolding wp_def 
  by auto
  
  
(*
  Refining a specification by its implementation
*)  
lemma refine_spec:
  assumes "wp m P"
  shows "m \<subseteq> {x. P x}"
  using assms unfolding wp_def by auto
  
  
subsection \<open>Implementation of Partitioning\<close>

(* Possible implementation: filter, pivots to the right *)
definition "partition_filter p xs \<equiv> return (filter (\<lambda>x. x<p) xs, filter (\<lambda>x. x\<ge>p) xs)"

lemma partition_filter_correct: "wp (partition_filter p xs) (partition_spec p xs)"
  unfolding partition_filter_def
  apply simp (* spec-simps *)
  unfolding partition_spec_def
  by (auto simp: not_le)

(* Another implementation: filter, pivots to the left *)
definition "partition_filter_left p xs \<equiv> return (filter (\<lambda>x. x\<le>p) xs, filter (\<lambda>x. x>p) xs)"

lemma partition_filter_left_correct: "wp (partition_filter_left p xs) (partition_spec p xs)"
  unfolding partition_filter_left_def
  apply simp (* spec-simps *)
  unfolding partition_spec_def
  by (auto simp: not_less) 
  
  
subsection \<open>Refining Quicksort\<close>
    
(*
  Now we can implement quicksort, using partition-filter
*)  
function (sequential) quicksort_impl :: "'a::linorder list \<Rightarrow> 'a list set" where
  "quicksort_impl [] = return []" 
| "quicksort_impl (p#xs) = do {
    (ls,rs) \<leftarrow> partition_filter_left p xs;
    ls \<leftarrow> quicksort_impl ls;
    rs \<leftarrow> quicksort_impl rs;
    return (ls@p#rs) 
  }"  
  by pat_completeness auto
  
termination
  apply (relation \<open>measure length\<close>)
  apply (auto simp: partition_filter_left_def return_def)
  subgoal by (meson le_imp_less_Suc length_filter_le)
  subgoal using length_filter_le less_Suc_eq_le by blast
  done

(*
  And show that it refines the original quicksort
*)
lemma quicksort_impl_refine: "quicksort_impl xs \<subseteq> quicksort xs"  
  apply (induction xs rule: quicksort_induct')
  subgoal by simp
  subgoal
    apply simp
    apply (rule bind_mono)
    apply (rule refine_spec)
    apply (rule partition_filter_left_correct)
    apply (simp split: prod.split, intro impI allI)
    apply (rule bind_mono) 
    subgoal by (auto)
    apply (rule bind_mono) 
    subgoal by (auto)
    by simp
  done

(*
  By transitivity/wp_mono, it refines the specification
*)
lemma quicksort_impl_correct: "wp (quicksort_impl xs) (sort_spec xs)"
  thm wp_mono quicksort_impl_refine quicksort_correct
  using wp_mono[OF quicksort_impl_refine quicksort_correct] .

    
(* Note: we can also express wp as refinement *)

lemma "wp m Q \<longleftrightarrow> m \<subseteq> {x. Q x}" by (auto simp: wp_def)

(*
  Conclusions:

    nondeterminism: allow multiple possible solutions
    proof rules: decompose correctness statement syntactically over program
    refinement: narrow solutions (e.g. by implementing spec)
  
    set-monad: monad for nondeterminism
    disadvantages:
      what if function does not always terminate? But only if precondition is met?
      even if function always terminate: computation induction gets ugly!
          
    Now: set-monad + option-monad = nondeterminism-error monad
      + general mechanism for recursive function

*)

  
end
