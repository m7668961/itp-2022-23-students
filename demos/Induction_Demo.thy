theory Induction_Demo
imports Main
begin

subsection \<open> Generalization \<close>
  
fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"itrev [] ys = ys" |
"itrev (x#xs) ys = itrev xs (x#ys)"


lemma "itrev xs [] = rev xs" 
  apply (induction xs)
  apply auto
  oops
  
  
lemma itrev_eq_rev_aux: "itrev xs ys = rev xs @ ys" 
  apply (induction xs arbitrary: ys)
  apply auto
  done
  
lemma "itrev xs [] = rev xs" by (simp add: itrev_eq_rev_aux)  
  
lemmas foo = itrev_eq_rev_aux[where ys="[]", simplified]

  
  
  (*
    will need to generalize.
  *)




subsection\<open> Computation Induction \<close>

fun f_test where 
  "f_test [True] = False"
| "f_test x = True"  

thm f_test.simps




fun sep :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"sep a [] = []" |
"sep a [x] = [x]" |
"sep a (x#y#zs) = x # a # sep a (y#zs)"

lemma "map f (sep a xs) = sep (f a) (map f xs)"
  apply (induction a xs rule: sep.induct)
  apply auto
  done

subsection \<open> Auxiliary Lemmas \<close>
  
hide_const rev  (* Get predefined rev of standard lib out of way! *)  
fun rev where 
  "rev [] = []"  
| "rev (x#xs) = rev xs @ [x]"  
  
thm rev.induct list.induct

lemma aux: "rev (xs@ys) = rev ys @ rev xs" 
  apply (induction xs)
  apply auto
  done


lemma "rev (rev xs) = xs"  
  apply (induction xs)
  apply (auto simp: aux)
  done
  (* We get stuck here! Auxiliary lemma needed! *)
  
notepad
begin

  fix f :: "nat \<Rightarrow> nat"
  assume [simp]: "f n = f (n+1)" for n

  have "f x + x = f (2*x)"
    apply si mp
  


end
  
  
  
  
end
