theory Warmup_W2L2
imports Main
begin


  (* Option data type *)
  term "None" term "Some 42"

  (* Lookup in association list *)
  fun al_lookup :: "('k \<times> 'v) list \<Rightarrow> 'k \<Rightarrow> 'v option" where
    "al_lookup [] _ = None"
  | "al_lookup ((k,v)#kvs) k' = (if k=k' then Some v else al_lookup kvs k')"  
  

  (* Non-recursive definitions *)
  definition "al_empty = []" (* Empty association list *)
  
  
  (* Unfold using unfolding (or apply (unfold \<dots>), or simp add: \<dots> ) *)
  lemma "al_lookup al_empty k = None"
    unfolding al_empty_def by auto
    (*
    apply (unfold al_empty_def) by auto
    by (auto simp: al_empty_def)
    *)

    
  (* Termination: recursive functions must terminate *)  
    
  fun f :: "nat \<Rightarrow> nat" where "f x = f x + 1" (* Error if function package cannot prove termination automatically.
    The heuristics works in most cases!
  *)

  (* Fun: patterns and disambiguation *)
  fun XOR where
    "XOR True False = True"
  | "XOR False True = True"
  | "XOR _ _ = False"

  thm XOR.simps

  fun OR where
    "OR True b = True"
  | "OR a True = True"  
  | "OR a b = False"
    
  thm OR.simps
  
  (* Note: this equation is missing \<dots> though it obviously holds. \<longrightarrow> manually add later! *)
  lemma OR_add_simps[simp]: "OR a True"
    apply (cases a)
    apply auto
    done
  
  
  (* Induction heuristics *)  
  (* Generalization (of statement, or variables) *)
   
  fun lsum :: "nat list \<Rightarrow> nat" where
    "lsum [] = 0"
  | "lsum (x#xs) = x + lsum xs"  
  
  fun itsum :: "nat \<Rightarrow> nat list \<Rightarrow> nat" where
    "itsum s [] = s"
  | "itsum s (x#xs) = itsum (s+x) xs"  
    
  lemma "itsum s xs = lsum xs + s"
    apply (induction xs arbitrary: s)
    apply auto
    done
  

  (* Computation induction (along the terminating recursion of a function)
  
    Heuristics: to prove property of f, use f.induct.
  
      for primitive recursion. f.induct = datatype.induct
  *)  

  thm lsum.induct list.induct
    
  
  fun pair where
    "pair [] = []"
  | "pair [_] = []"  
  | "pair (a#b#xs) = (a,b)#pair xs"
    
  definition "unpair xs \<equiv> concat (map (\<lambda>(a,b). [a,b]) xs)"
    
  lemma "even (length xs) \<Longrightarrow> unpair (pair xs) = xs"
    apply (induction xs rule: pair.induct)
    apply (auto simp: unpair_def)
    done
  
  (* Auxiliary Lemmas *)
    
  lemma [simp]: "lsum (xs@ys) = lsum xs + lsum ys"
    apply (induction xs)
    apply auto
    done
  
  lemma "lsum (rev xs) = lsum xs"
    apply (induction xs)
    apply auto
    done
    
  (* Simplification: 
  
    apply (simp add: eqs)
  
    uses fun-equations, thms by datatype, everything declared [simp], eqs, assumptions of subgoal
  *)  
    
      
    
    
    
    


end
