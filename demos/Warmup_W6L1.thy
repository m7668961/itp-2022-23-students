theory Warmup_W6L1
imports Main
  "HOL-Library.RBT"
  (* The next two activate dictionary order on strings, 
     such that we can use them as keys in RBTs
  *)
  "HOL-Library.Char_ord" 
  "HOL-Library.List_Lexorder"  
  "HOL-Library.Multiset"  
begin

  (*
    Anagrams: words that have the same multiset of letters
  
    e.g. race, care, acre
  *)
  
  definition "is_anagram xs ys \<longleftrightarrow> mset xs = mset ys"

  (*   
    By sorting the letters of a word, we can identify all anagrams
  *)
  lemma is_anagram_iff_sort_eq: "is_anagram xs ys \<longleftrightarrow> sort xs = sort ys"
    unfolding is_anagram_def
    by (metis mset_sort sorted_list_of_multiset_mset)
  
  (*
    Build a database that maps sorted lists of letters to words with those letters.
    
    The ADT of this database is \<open>'k \<Rightarrow> 'v set\<close>
  *)

  (* Abstract characterization of our anagram database *)  
  definition "is_anagram_db words db \<longleftrightarrow> 
      (\<forall>k. db k \<noteq> {} \<longrightarrow> sorted k) \<comment> \<open>Keys are sorted\<close>
    \<and> (\<forall>k w. w\<in>db k \<longrightarrow> k=sort w) \<comment> \<open>Words stored at correct keys\<close> 
    \<and> (words = \<Union>(range db))" \<comment> \<open>All words in database\<close>

  (* And the abstract idea: *)
  lemma lookup_anagram_correct: 
    "is_anagram_db words db \<Longrightarrow> db (sort w) = {w' \<in> words. is_anagram w w'}"
    unfolding is_anagram_db_def is_anagram_iff_sort_eq
    by (auto)
    
  
  (*
    We implement that database by a RBT (or any other key-value structure).
  *)  

  term \<open>RBT.lookup\<close> (* Used as abstraction function, too *)
  
  term RBT.empty thm RBT.lookup_empty
  term RBT.insert thm RBT.lookup_insert
  term RBT.delete thm RBT.lookup_delete
  
      
  definition "kvs_\<alpha> t k 
    = (case RBT.lookup t k of None \<Rightarrow> {} | Some vs \<Rightarrow> set vs)"
  
  definition "kvs_empty = RBT.empty"
  
  lemma kvs_empty_refine[simp]: "kvs_\<alpha> kvs_empty = (\<lambda>_. {})" 
    unfolding kvs_\<alpha>_def kvs_empty_def by auto

  definition "kvs_lookup t k 
    = (case RBT.lookup t k of None \<Rightarrow> [] | Some vs \<Rightarrow> vs)"
  
  lemma kvs_lookup_correct[simp]: "set (kvs_lookup t k) = kvs_\<alpha> t k"
    unfolding kvs_\<alpha>_def kvs_lookup_def
    by (auto split: option.splits)
      
  definition "kvs_insert k v t = (case RBT.lookup t k of
    None \<Rightarrow> RBT.insert k [v] t
  | Some vs \<Rightarrow> RBT.insert k (v#vs) t
  )"

  lemma kvs_insert_refine[simp]: "kvs_\<alpha> (kvs_insert k v t) = (kvs_\<alpha> t)( k := (insert v (kvs_\<alpha> t k)))"  
    unfolding kvs_\<alpha>_def kvs_insert_def
    (* Note: without wrapping this is ADT, we'd get 
      this subgoal's complexity \<times> complexity of algorithm we are proving! *)
    by (auto simp: fun_eq_iff split: option.splits)
  
    

  (* The concrete algorithms: *)  
  definition "add_word w db = kvs_insert (sort w) w db"
      
  definition "build_db words = fold add_word words kvs_empty"

  definition "get_anagrams db w = kvs_lookup db (sort w)"
    
  definition "anagrams words = get_anagrams (build_db words)" \<comment> \<open>Combining creation and query\<close>
  
      
  (* Concrete characterization of database *)
  definition "is_anagram_dbi words db \<longleftrightarrow> is_anagram_db (set words) (kvs_\<alpha> db)"

  (* Empty map characterizes empty set of words *)  
  lemma empty_anagram_dbi_correct[simp]: "is_anagram_dbi [] kvs_empty"
    unfolding is_anagram_dbi_def is_anagram_db_def
    by auto
  
  lemma add_word_correct[simp]: 
    "\<lbrakk>is_anagram_dbi words db; set words' = insert w (set words)\<rbrakk> 
    \<Longrightarrow> is_anagram_dbi words' (add_word w db)"
    unfolding is_anagram_dbi_def is_anagram_db_def add_word_def
    apply auto
    by metis

      
  lemma build_db_correct[simp]: 
    "is_anagram_dbi words (build_db words)" 
    for words :: "'a::linorder list list"
  proof -
    (* Fold \<rightarrow> always needs generalization *)
    have "is_anagram_dbi ws' (fold add_word ws\<^sub>1 db\<^sub>0)"
      if "is_anagram_dbi ws\<^sub>0 db\<^sub>0"
     and "set ws' = set ws\<^sub>0 \<union> set ws\<^sub>1" (* We do not care about the order in the word database, so we just use sets *)
      for ws\<^sub>0 ws\<^sub>1 ws' :: "'a list list" and db\<^sub>0
      using that
    proof (induction ws\<^sub>1 arbitrary: ws\<^sub>0 db\<^sub>0)
      case Nil
      then show ?case by (auto simp: is_anagram_dbi_def)
    next
      case (Cons w ws\<^sub>1)
      
      from \<open>is_anagram_dbi ws\<^sub>0 db\<^sub>0\<close>
      have "is_anagram_dbi (w#ws\<^sub>0) (add_word w db\<^sub>0)" by simp
      from Cons.IH[OF this] \<open>set ws' = set ws\<^sub>0 \<union> set (w # ws\<^sub>1)\<close> 
      show ?case by simp
    qed
    from this[OF empty_anagram_dbi_correct, of words words]
    show ?thesis
      unfolding build_db_def
      by simp
      
  qed  
      
  
  
  lemma get_anagrams_correct[simp]: 
    "is_anagram_dbi words db \<Longrightarrow> set (get_anagrams db w) = {w'\<in>set words. is_anagram w w'}"
    unfolding is_anagram_dbi_def get_anagrams_def
    by (auto simp: lookup_anagram_correct)
    
  lemma anagrams_correct[simp]: "set (anagrams words w) = {w'\<in>set words. is_anagram w w'}"
    unfolding anagrams_def 
    by simp

  (*
    We can actually use our functional programs!

    (A bit of boilerplate required to link Isabelle with target language)
  *)  
    
    
  definition anagrams_strings :: "string list \<Rightarrow> _" 
    where "anagrams_strings = anagrams"
    
  ML \<open>
    local
      val anagrams = @{code anagrams_strings}  
    
      val lines = File.read_lines (Path.explode "/etc/dictionaries-common/words")
        |> map_filter (try @{code String.explode}) (* This filters words containing non-ASCII characters *)

    in      
      val anagramsi = map (@{code String.implode}) o anagrams lines o @{code String.explode}
    end
  \<close>  
    
  ML_val \<open>anagramsi "hello"\<close>  
  ML_val \<open>anagramsi "notaword"\<close>  
  ML_val \<open>anagramsi "hlleo"\<close>
  
  ML_val \<open>anagramsi "race"\<close>  
  ML_val \<open>anagramsi "part"\<close>  
  ML_val \<open>anagramsi "heart"\<close>
  ML_val \<open>anagramsi "words"\<close>
    
    
end
