theory Homework5sol
  imports Main Homework5_Aux1
begin

text \<open>
  Make sure you download Aux1.thy and put it in the same folder as this file!
\<close>

  text \<open>
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  
  
    HOMEWORK #5
    RELEASED: Wed, Dec 14 2022
    DUE:      Wed, Dec 21 2022, 23:59
  
    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  \<close>
  
  section \<open>General Information\<close>  
  
  text \<open>
    This sheet has two bonus exercises! The regular bonus exercise is more difficult as always. The
    extra bonus exercise is not as hard. It's a good way to catch up with points. It's the very last 
    exercise on the sheet, make sure to give it a go!

    The best way to work on this homework is to fill in the missing gaps in this file.
  
    All solutions are a few lines only, and do, unless indicated, not require 
    to define any auxiliary functions. So if you end up with 
    lengthy and complicated function definitions, you are probably just 
    missing an easier solution.
  
    Do not hesitate to show me your problems with your solutions, 
    eg, if Isabelle throws some cryptic error messages at you 
      that you cannot decipher ...
  
  
    This exercise will focus mainly on the definition of datatypes.
    We will define some custom datatypes and show that correctness by
    proving invariant preservations. For this, we use their abstract representation.
  \<close>



  section \<open>Ex 1: Leftist Heaps (25 points)\<close>

  text \<open>
    You have likely heard about heaps before. Heaps are trees with the (min) heap property.
    \<^url>\<open>https://en.wikipedia.org/wiki/Heap_(data_structure)\<close>

    We are going to look at a leftist heap. This is a heap whose minimal depth of the left subheap
    is always greater than or equal to the depth of the right subheap at every node.

    The abstract datatype for heaps is a multiset over linearly ordered elements,
    with the operations empty, is_empty, insert, get_min, and del_min
  \<close>


  text \<open>The abstraction function that transforms the leftist heap (lheap) into a multiset.\<close>
  term lh_mset

  text \<open>The empty lheap.\<close>
  term lh_empty

  text \<open>lh_is_empty h tests whether the lheap is empty.\<close>
  term lh_is_empty

  text \<open>lh_insert a h inserts an element into the right place on the heap.\<close>
  term lh_insert

  text \<open>lh_get_min h gets the lowest value in the heap\<close>
  term lh_get_min

  text \<open>lh_del_min h deletes the lowest value from the heap\<close>
  term lh_del_min

  text \<open>The refinement lemmas for the operations on the concrete datatype try to understand what 
    they do, you will need them in your proofs.\<close>
  thm lh_empty_refine

  thm lh_is_empty_refine

  thm lh_insert_refine

  thm lh_get_min_refine

  thm lh_del_min_refine
    

  subsection \<open>a) Define a function \<open>build_heap\<close> and prove correctness. (6p)\<close>


  text \<open>
    Define a function that creates an lheap from a list. Remember that all operations are already 
    available and just need to be slotted together. Then, prove that it preserves the multiset of 
    elements in the list!
  \<close>

  (*
    Alternative solution: adding the refinement lemmas to the simp set will make your life a lot easier
    lemmas [simp] = lh_empty_refine lh_is_empty_refine lh_insert_refine lh_get_min_refine lh_del_min_refine
  *)

  definition build_heap :: "'a::linorder list \<Rightarrow> 'a lheap" where 
    "build_heap xs \<equiv> fold lh_insert xs lh_empty"
    
  lemma build_heap_mset[simp]: "lh_mset (build_heap xs) = mset xs"
  proof -
    have "lh_mset (fold lh_insert xs h) = lh_mset h + mset xs" for h
      apply (induction xs arbitrary: h) 
      by (auto simp: lh_insert_refine)
    thus ?thesis unfolding build_heap_def by (auto simp: lh_empty_refine)  
  qed  



  text \<open>
    The following function converts an lheap back to a sorted list. We also need to prove 
    termination in order to do induction. We have already done this for you.
  \<close>
  fun heap_to_list where
    "heap_to_list h = (if lh_is_empty h then [] else lh_get_min h # heap_to_list (lh_del_min h))"

  subsection \<open>b) Show that heap_to_list preserves the elements (6p)\<close>

  lemmas [simp del] = heap_to_list.simps 

  text \<open>
    NOTE: heap_to_list.simps is prone to loop forever if used with the simplifier (try to find out 
    why). We deleted it from the simp set for this reason. To unfold the function definition for 
    heap_to_list in a controlled way use subst heap_to_list.simps or Isar
    Also remember that computation induction is the best way to reason about functions with complex
    recursion patterns like heap_to_list\<close>
  
  lemma mset_heap_to_list[simp]: "mset (heap_to_list h) = lh_mset h"
    apply (induction h rule: heap_to_list.induct)
    apply (subst heap_to_list.simps )
    by (auto simp: lh_del_min_refine lh_is_empty_refine lh_get_min_refine)


  subsection \<open>c) Show that heap_to_list produces a sorted list. (6p)\<close>
  
  text \<open>  Note: looks easy, but some careful reasoning is actually required here!
    Isar + sledgehammer are your friends!
    
    Hint: Use computation induction over heap-to-list!
  \<close>

  lemma sorted_heap_to_list[simp]: "sorted (heap_to_list h)" for h :: "'a::linorder lheap"
  proof(induction h rule: heap_to_list.induct)
    case (1 h)
    then show ?case
    proof (cases "lh_is_empty h")
      case True
      then show ?thesis 
        apply (subst heap_to_list.simps) 
        by simp
    next
      case False
      from 1[OF False] have SORT: "sorted (heap_to_list (lh_del_min h))"  .
      from False have "\<forall>x \<in># mset (heap_to_list h). lh_get_min h \<le> x" 
        by (auto simp: lh_is_empty_refine lh_get_min_refine lh_del_min_refine) 
      with SORT show ?thesis 
        by (simp add: False heap_to_list.simps)
    qed
  qed

  
  subsection \<open>d) Assemble everything: define lheapsort and prove it correct! (7p)\<close>

  text \<open>
    HINTL Everything that you have used so far can be used to create heapsort. Don't overcomplicate 
    anything, the simplest solution fits well within one line!
  \<close>
      
  definition lheapsort :: "'a::linorder list \<Rightarrow> 'a list" 
    where "lheapsort xs = heap_to_list (build_heap xs)"
      
  lemma lheapsort_correct: "mset (lheapsort xs) = mset xs" "sorted (lheapsort xs)"
    unfolding lheapsort_def
    by auto



  section \<open>Ex 2: Prime Factorization (15 points)\<close>

  text \<open>
    Every integer greater than 1 can be represented uniquely as a product of prime numbers, 
    up to the order of the factors.
  
    \<^url>\<open>https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic#Proof\<close>
    
    Existence
    It must be shown that every integer greater than 1 is either prime or a product of primes. 
    First, 2 is prime. Then, by strong induction, assume this is true for all numbers greater 
    than 1 and less than n. If n is prime, there is nothing more to prove. Otherwise, there 
    are integers a and b, where n = a b, and 1 < a \<le> b < n. By the induction hypothesis, 
    a = p1 p2 \<cdot>\<cdot>\<cdot> pj and b = q1 q2 \<cdot>\<cdot>\<cdot> qk are products of primes. 
    But then n = a b = p1 p2 \<cdot>\<cdot>\<cdot> pj q1 q2 \<cdot>\<cdot>\<cdot> qk is a product of primes.
    
    Uniqueness
    Suppose, to the contrary, there is an integer that has two distinct prime factorizations. 
    Let n be the least such integer and write n = p1 p2 ... pj = q1 q2 ... qk, 
    where each pi and qi is prime. We see that p1 divides q1 q2 ... qk, so p1 divides some qi 
    by Euclid's lemma. Without loss of generality, say p1 divides q1. 
    Since p1 and q1 are both prime, it follows that p1 = q1. 
    Returning to our factorizations of n, we may cancel these two factors to conclude 
    that p2 ... pj = q2 ... qk. We now have two distinct prime factorizations of some integer 
    strictly smaller than n, which contradicts the minimality of n.  
  \<close>
  

  subsection \<open>a) formalize the above existence proof in Isar! (15p)\<close>

  text \<open>
    NOTE: Be careful what lemmas sledgehammer uses to prove your goal!
      It might find the fundamental theorem of arithmetic or derived theorems in the library,
      and use them to discharge your current subgoal! These 'shortcuts' are obviously not allowed. 
      Please follow the given proof outline, and make sure that the proofs of each step do not use
      weird detours, in particular the ones found by sledgehammer!
  \<close>

  (* Excluding a few 'obvious' shortcut lemmas. *)
  lemmas [no_atp] = 
    prime_factorization_prod_mset_primes prime_factorization_eqI_strong
    prod_mset_prime_factorization_nat
    prime_factorization_exists_nat
  
  

  thm less_induct (* That's what the proof refers to a "strong induction" *)

  text \<open>
    HINTS:
    Use multisets, instead of enumerating elements! 
    With multisets, you go slightly beyond the 'comfort zone' of auto, but sledgehammer will
    usually find the relevant proofs! If not: insert intermediate steps, and use find_theorems!
  
    Be careful with types: the constant prime is defined over a more generic type than nat,
    but some of the lemmas you need only hold for nat. Check your types and add annotations when
    you run into unexpectedly unprovable statements.
  \<close>
  
  lemma exists_prime_factorization: "1 < n \<Longrightarrow> \<exists>s. n = \<Prod>\<^sub># s \<and> (\<forall>x\<in>#s. prime x)" for n :: nat
  proof (induction n rule: less_induct)
    case (less n)
    show ?case proof (cases)
      assume "prime n"
      thus ?thesis by (auto intro: exI[where x="{#n#}"])
    next
      assume "\<not>prime n"  
      then obtain a b where "1<a" "1<b" "a<n" "b<n" and "n=a*b" 
        using less.prems nat_neq_iff prime_nat_iff by fastforce
      with less.IH[of a] less.IH[of b] obtain sa sb where 
        "a = \<Prod>\<^sub># sa \<and> (\<forall>a\<in>#sa. prime a)"
        "b = \<Prod>\<^sub># sb \<and> (\<forall>a\<in>#sb. prime a)"
        by auto
      hence "n = \<Prod>\<^sub># (sa+sb) \<and> (\<forall>a\<in>#sa+sb. prime a)" using \<open>n=a*b\<close> by auto
      thus ?thesis ..
    qed
  qed


  section \<open>Bonus: Uniqueness of prime factorizations (5 points)\<close>

  text \<open>WARNING: There is a an easier extra bonus exercise after this one. This one is very 
    difficult though! You may want to try the next one first!\<close>

  text \<open>
    Complete the proof of the Fundamental Theorem of Arithmetic, 
    following the proof sketch on Wikipedia.
  
    Hint: the Wikipedia proof sketch skips about quite some details here, 
    that you will have to amend! You are welcome to make slight changes to the proof structure,
    if that helps! (e.g., in our sample solution, we used the number of factors, 
    rather than the prime number itself, for the "get-a-smaller" argument)
  \<close>
  
  lemma prime_eq_prod_msetD:
    fixes p :: nat
    assumes "prime p"
    assumes "\<forall>x\<in>#s. prime x"
    assumes "p = \<Prod>\<^sub>#s"
    shows "s={#p#}"
    using assms  
    apply (cases s)
    subgoal by auto
    subgoal for x s'
      apply (cases s')
      by (auto dest: prime_product)
    done
    
  lemma prod_mset_primes_gt_1: "\<forall>x\<in>#s. prime x \<Longrightarrow> s\<noteq>{#} \<Longrightarrow> \<Prod>\<^sub>#s > 1" for s :: "nat multiset"
    by (metis dvd_refl is_unit_prod_mset_primes_iff less_one not_less_iff_gr_or_eq not_prime_0 prod_mset_zero_iff)
  
  
  lemma prime_factorization_unique:
    fixes s s' :: "nat multiset"
    assumes "1 < \<Prod>\<^sub># s" "\<Prod>\<^sub># s = \<Prod>\<^sub># s'" "\<forall>x\<in>#s. prime x" "\<forall>x\<in>#s'. prime x"
    shows "s=s'"
    using assms
  proof (induction "size s" arbitrary: s s' rule: less_induct) 
    case less
    
    note PRIM = \<open>\<forall>x\<in>#s. prime x\<close> \<open>\<forall>x\<in>#s'. prime x\<close>
  
    note euclids_lemma = prime_dvd_prod_mset_iff
    
        
    from \<open>1 < \<Prod>\<^sub># s\<close> obtain p\<^sub>1 where "p\<^sub>1\<in>#s" "prime p\<^sub>1"
      using PRIM by fastforce
    hence "p\<^sub>1 dvd \<Prod>\<^sub># s" by (simp add: dvd_prod_mset)
    with \<open>\<Prod>\<^sub># s = \<Prod>\<^sub># s'\<close> have "p\<^sub>1 dvd \<Prod>\<^sub># s'" by simp
    with euclids_lemma[OF \<open>prime p\<^sub>1\<close>] obtain q\<^sub>1 where "q\<^sub>1\<in>#s'" "p\<^sub>1 dvd q\<^sub>1" "prime q\<^sub>1" 
      using PRIM by auto
    with \<open>prime p\<^sub>1\<close> have "q\<^sub>1=p\<^sub>1" using primes_dvd_imp_eq by blast
    
    from \<open>\<Prod>\<^sub># s = \<Prod>\<^sub># s'\<close> \<open>p\<^sub>1\<in>#s\<close> \<open>q\<^sub>1\<in>#s'\<close> \<open>q\<^sub>1=p\<^sub>1\<close> have EQ': "\<Prod>\<^sub>#(s-{#p\<^sub>1#}) = \<Prod>\<^sub>#(s'-{#q\<^sub>1#})"  
      by (metis PRIM(1) not_prime_0 prod_mset_minus)
    have SIZE_LESS: "size (s-{#p\<^sub>1#}) < size s" using \<open>p\<^sub>1\<in>#s\<close>
      by (simp add: size_Diff1_less)
    
    show "s=s'" proof cases
      assume "s-{#p\<^sub>1#} = {#}"
      with \<open>p\<^sub>1 \<in># s\<close> have "s={#p\<^sub>1#}" by (metis insert_DiffM)
      hence \<open>p\<^sub>1 = \<Prod>\<^sub>#s'\<close> using \<open>\<Prod>\<^sub># s = \<Prod>\<^sub># s'\<close> by force
      thus ?thesis using PRIM(2) \<open>prime p\<^sub>1\<close> \<open>s = {#p\<^sub>1#}\<close> prime_eq_prod_msetD by simp
    next
      assume "s-{#p\<^sub>1#} \<noteq> {#}"
      
      from PRIM have LT_PP: "1 < \<Prod>\<^sub># (s - {#p\<^sub>1#})"
        by (meson \<open>s - {#p\<^sub>1#} \<noteq> {#}\<close> in_diffD prod_mset_primes_gt_1)
      
      have "s - {#p\<^sub>1#} = s' - {#q\<^sub>1#}"
        apply (rule less.hyps[OF SIZE_LESS LT_PP EQ'])
        using PRIM by (meson in_diffD)+
      thus ?thesis using \<open>p\<^sub>1\<in>#s\<close> \<open>q\<^sub>1\<in>#s'\<close> 
        by (metis \<open>q\<^sub>1 = p\<^sub>1\<close> insert_DiffM2)
    qed
  qed
  
  lemma fundamental_thm_arith: "n>1 \<Longrightarrow> \<exists>!s. n=\<Prod>\<^sub># s \<and> (\<forall>x\<in>#s. prime x)" for n :: nat
    using exists_prime_factorization prime_factorization_unique by blast




  section \<open>Extra Bonus: Stable quicksort (5 points)\<close>

  text \<open>This is an additional bonus exercise to catch up with some points. It's easier than the
    previous one, try to give it a go!\<close>

  text \<open>
    Recall that usual in-place implementations of quicksort are not stable.
    \<^url>\<open>https://en.wikipedia.org/wiki/Quicksort\<close>
    
    This is because of the partitioning algorithm.
    When we implement quicksort functionally, it's not in place, and can easily 
    be implemented in a stable way.
    
    Hint:
    This exercise can be solved with simple definitions and simple proofs!
    When your definitions (or proofs) get too complicated, you may be on the wrong track.
    
    It only depends on very basic concepts that have been introduced in the first weeks of the lecture.
    If you think you are getting too confused with the typeclass \<open>'b::linorder\<close>, you are welcome to
    restrict your key function to \<open>'a \<Rightarrow> nat\<close> for -1 point. 
    (Don't forget to at least try to change it back after you have done all the proofs with nat!)
  \<close>

  subsection \<open>a) Sorted by key (1p)\<close>
  
  text \<open>
    Given a function that maps the elements into a linearly ordered type,
    we can define when a list of keys is sorted wrt. their value. This means that we don't sort the 
    list itself but we sort the entries by the value of the mapping function. E.g. suppose we have 
    a function \<open>f: a \<mapsto> 2, b \<mapsto>5, c \<mapsto> 1\<close>, then \<open>sorted_key f [c,a,b]\<close> holds.
  \<close>
  fun sorted_key :: "('a \<Rightarrow> 'b::linorder) \<Rightarrow> 'a list \<Rightarrow> bool" where
    "sorted_key k [] = True"
  | "sorted_key k (x # xs) = ((\<forall>y\<in>set xs. k x \<le> k y) \<and> sorted_key k xs)"

  
  text \<open>Prove an alternative characterization of the form: sorted_key k xs <--> sorted_wrt XXX xs\<close>  
  lemma "sorted_key k xs \<longleftrightarrow> sorted_wrt (\<lambda>x y. k x \<le> k y) xs"
    by (induction xs) auto

    
  subsection \<open>b) Quicksort Implementation (1p)\<close>      
  
  text \<open>There is a function @{term partition} which, given a predicate and a list, computes a pair of
        lists such that one of them has the members of the given list satisfying the given predicate,
        and the second has members which do not.\<close>
  
  thm partition.simps
  
  text \<open>Implement quicksort using @{term partition}\<close>
  
  fun quicksort :: "('a \<Rightarrow> nat) \<Rightarrow> 'a list \<Rightarrow> 'a list" where
    "quicksort k []     = []"
  | "quicksort k (x#xs) = (let (xs1, xs2) = partition (\<lambda>x'. k x' < k x) xs in quicksort k xs1 @ [x] @ quicksort k xs2)"


  subsection \<open>c) Element preservation (1p)\<close>

  text \<open>Prove that quicksort preserves the elements of the list\<close>
      
  lemma quicksort_preserves_mset: "mset (quicksort k xs) = mset xs"
    apply (induction xs rule: quicksort.induct)
    by (auto simp add: o_def)



  subsection \<open>d) Sortedness (1p)\<close>
  text \<open>Prove that quicksort sorts\<close>
    
  lemma quicksort_preserves_set: "set (quicksort k xs) = set xs"
    apply(intro mset_eq_setD)
    by (simp add: quicksort_preserves_mset)

  
  lemma sorted_key_append:
    "sorted_key k (xs@ys) = (sorted_key k xs & sorted_key k ys & (\<forall>x \<in> set xs. \<forall>y \<in> set ys. k x\<le>k y))"
    by (induct xs) (auto)

  
  lemma quicksort_sorts: "sorted_key k (quicksort k xs)"
    apply (induction xs rule: quicksort.induct)
    apply (auto simp: sorted_key_append quicksort_preserves_set)
    done

  subsection \<open>e) Stability (1p)\<close>
  
  text \<open>
    We are going to prove that the implementation here is stable. A sorting algorithm is stable if
    elements with the same value occur in the same order as they occur in the original list.
    For example, given \<open>g: a \<mapsto> 2, b \<mapsto>5, c \<mapsto> 1, d \<mapsto> 2\<close>, we observe that \<open>a\<close> and \<open>d\<close> map to the 
    same value. If \<open>a\<close> occurs before \<open>d\<close> in the original list, then \<open>a\<close> occurs before \<open>d\<close> in the 
    sorted list, e.g. \<open>quicksort g [a,b,c,d] = [c,a,d,b]\<close>
    However, if \<open>d\<close> would occur before \<open>a\<close> in the original list, then \<open>d\<close> occurs before \<open>a\<close> in the
    sorted list as well, e.g. \<open>quicksort g [d,c,b,a] = [c,d,a,b]\<close>

    \<^url>\<open>https://en.wikipedia.org/wiki/Sorting_algorithm#Stability\<close>
  
    Specify stability formally in Isabelle, 
    explain why your specification makes sense in a few sentences,
    and prove that your quicksort algorithm is stable.

    Warning/Hint: there are many overly complicated ways to specify stability in HOL, 
      but at least one very simple way that comfortably fits into a single line without 
      being unreadable.
  \<close>
  
  (*<*) (* Alternatively, this can be done using filter. Actually, this is exactly the same thing
  as we are doing here with syntactic sugaring. Just select the theorem ans see what the output
  window says.*)
  lemma quicksort_stable:
    "[x\<leftarrow>quicksort k xs. k x = (a::nat)] = [x\<leftarrow>xs. k x = a]"
  proof -
    have [simp]: "[x\<leftarrow>xs . k x < k p \<and> k x = a] = (if a<k p then [x\<leftarrow>xs. k x = a] else [])" for k :: "'a \<Rightarrow> nat" and xs p   
      by (induction xs) auto
    have [simp]: "[x\<leftarrow>xs . \<not>k x < k p \<and> k x = a] = (if k p \<le> a then [x\<leftarrow>xs. k x = a] else [])" for k :: "'a \<Rightarrow> nat" and xs p   
      by (induction xs) auto
        
    show ?thesis
      apply (induction k xs rule: quicksort.induct)
      apply (auto)
      done
  qed      
  (*>*)

end
