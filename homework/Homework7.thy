  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #7
    RELEASED: Thu, Jan 12, 2023
    DUE:      Thu, Jan 19, 2023, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)


  (*******************
    FINAL EXAM: EXAMPLE EXAM.
    
    This is an example exam, similar to what you might expect in the main exam.
    As homework, each question Q1..Q4 gives you 10 points, and Q5 is for 5 bonus points.
    In the real exam, the score per question would be similar.
  
    Theories will work in Isabelle2022
    
    Note, you can use the sidekick-panel to get a quick overview of the contents
    of this exam.
      
    You have three hours to work on this exam. 
    
    You solve this exam on your own machines. Before you start working on the exam,
    please make sure your internet connection is disabled (we will check that!)

    When the time is up, we will announce that you can switch on your internet again, and
    send the exam by email to p.lammich@utwente.nl, using [ITP-EXAM] in the subject.
    
    Afterwards, please wait until I have confirmed that I have received your solution!

    If you want to submit earlier, raise your hand and ask for early submission
      

    
    
    
  *******************)


theory Homework7
imports 
  Main 
  "HOL-Library.While_Combinator" 
  "../Demos/While_Lib"
  "HOL-Library.Monad_Syntax"
begin



























(**

  SWITCH OFF YOUR INTERNET CONNECTION NOW!


**)



















section \<open>Q1: Constant-time append lists (10 Points)\<close>

  (* We can implement lists as the empty list, 
     a singleton list, or the concatenation of two lists:  
  *)
    
  datatype 'a clist = CNIL | CSNG 'a | CAPPEND "'a clist" "'a clist" 

  (* Note that this representation allows a constant-time append operation *)
  
  subsection \<open>a) Definition (easy) (2p)\<close>

  (*
    Define the abstraction function from clists to lists:
  *) 
  fun cl_\<alpha> :: "'a clist \<Rightarrow> 'a list" where
  (*<*)
    "cl_\<alpha> _ = undefined"
  (*>*)

  (* Examples to check you definition *)
  
  value "cl_\<alpha> (CAPPEND CNIL (CAPPEND (CSNG 1) (CSNG (2::nat)))) = [1,2]"
  value "cl_\<alpha> (CAPPEND (CSNG 3) (CAPPEND (CSNG 1) (CSNG (2::nat)))) = [3,1,2]"
  
  subsection \<open>b) Fold over clists (medium) (3p)\<close>  

  (* Define a fold function over clist, and show that it is correct
  
    (Use recursion over the structure of clist, definitions that fall back to
      cl_\<alpha>, e.g. \<open>clfold f xs s = fold f (cl_\<alpha> xs) s\<close> do not count!
    )
  
  *)
  
  fun clfold :: "('a \<Rightarrow> 's \<Rightarrow> 's) \<Rightarrow> 'a clist \<Rightarrow> 's \<Rightarrow> 's" where
    "clfold _ _ _ = undefined"
  
  lemma clfold_correct: "clfold f xs s = fold f (cl_\<alpha> xs) s" 
    sorry


  subsection \<open>c) Summing clists (easy+) (2p)\<close>
  
  (* Show that we can use clfold to sum up lists *)
    
  lemma "clfold (+) xs 0 = sum_list (cl_\<alpha> xs)" for xs :: "int clist" 
    sorry

    
  subsection \<open>d) Normal-form (medium) (3p)\<close>

  (*
    The only reason to use CNIL is to represent an empty list. Using CNIL as argument to append will 
    not change the contents of the list. E.g.
  *)

  value "cl_\<alpha> (CAPPEND CNIL (CAPPEND (CSNG 1) (CSNG (2::nat)))) = cl_\<alpha> (CAPPEND (CSNG 1) (CSNG (2::nat)))"

  (*
    Implement a function \<open>is_normal xs\<close> that checks whether xs is in this normal form.
    I.e. \<open>is_normal xs\<close> holds when \<open>xs = CNIL\<close> or \<open>xs = CSNG x\<close> or when \<open>xs = CAPPEND x1 x2\<close> where
    x1 and x2 don't contain any occurrence of CNIL. Also define a function that converts xs into
    normal form and show that this indeed works correctly (i.e. \<open>is_normal (normalize xs)\<close>
    and \<open>cl_\<alpha> (normalize xs) = cl_\<alpha> xs\<close>
  *)
  
  fun is_normal where
    "is_normal _ = undefined"

  
  fun normalize where
    "normalize _ = undefined"

  lemma "cl_\<alpha> (normalize xs) = cl_\<alpha> xs" oops
  lemma "is_normal (normalize xs)" oops
  
      
    
section \<open>Q2: Simple Balanced Words (10 Points)\<close>    
  (* Old question from 2018 exam *)

  (*
    We consider 'simple' words, a restricted form of balanced words, namely those 
    of the form (\<dots>()\<dots>), or, equivalently L\<^sup>nR\<^sup>n.
  *)    
  datatype A = L | R
  type_synonym word = "A list"

  (*
    These are created by the Empty and Surround rule. There is no Concatenate rule.
  *)    
  inductive simple :: "word \<Rightarrow> bool" where
    E: "simple []"    
  | S: "simple w \<Longrightarrow> simple (L#w@[R])"

  subsection \<open>a) Even Length (easy) (3p)\<close>
  (*
    Show: Every simple word has even length
    
    (easy)
  *)    
  lemma "simple w \<Longrightarrow> even (length w)"
    oops

  subsection \<open>b) dup function (easy) (3p)\<close>  
  (*
    Define a recursive function \<open>dup n x\<close> to create a list of n \<open>x\<close>s, i.e.,
    [x,x,x,\<dots>x] (n times)

    Do not use functions from the Isabelle library in your definition.
        
    (easy)
  *)  
  fun dup :: "nat \<Rightarrow> 'a \<Rightarrow> 'a list" where
    "dup _ _ = undefined"
    
  subsection \<open>c) Simple words have form \<open>L\<^sup>nR\<^sup>n\<close> (medium+) (4p)\<close>
  (*
    Show that every simple word can actually be expressed as "dup n L @ dup n R"
    
    Hint: You might want to use an Isar proof, to instantiate the \<exists> in a controlled way!
    Hint: You might want to prove an auxiliary lemma of the form
          \<open>dup n x @ [x] = \<dots>\<close> 
    
    NO SLEDGEHAMMER ALLOWED! I.e., you must not use metis or smt methods for this proof!
    
    (medium+)
  *)

  lemma "simple w \<Longrightarrow> \<exists>n. w = dup n L @ dup n R"
    oops


section \<open>Q3: Finding an Invariant and Termination (10 Points)\<close>  
  (*
    Modified question from 2018 exam. Arrays where used a lot in this lecture,
    so the reasoning about indexing into lists was easier in this context than
    a more functional implementation using take, drop, and fold.
    
    In the actual exam, you'd expect a while-loop + ADT here!
  *)

  (*
    The following program sums up the elements of the range l..<h in list a, 
    by iterating from h down to l, and indexing into the list.
    
    It implements the following 'imperative' program:

    r=0
    i=h
    
    while (i>l) {
      i= i - 1
      r= r + xs[i]
    }
        
  *)  
  
  definition "list_sum l h xs \<equiv> while_option 
    (\<lambda>(i,r). i>l)
    (\<lambda>(i,r). let i=i-1 in (i,r + xs!i))
    (h,0)
    "
  
  subsection \<open>a) Invariant (medium) (6p)\<close>    
  (*
    We want to prove that this program actually sums up the given interval:
  *)  
    
  (*
    Note that "sum xs {l..<h}", which can also be written as "\<Sum>j=l..<h. xs j",
    sums up the elements of the list xs in range l..<h, i.e., l INCLUSIVE and h EXCLUSIVE.
  *)  
  term "sum xs {l..<h}" term "\<Sum>j=l..<h. xs j"
    
  definition list_sum_invar :: "int list \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat \<times> int \<Rightarrow> bool" 
    where "list_sum_invar xs l h \<equiv> undefined"
    
  (* Hint: the following lemma may be useful! *)
  lemma sum_split:
    fixes f :: "nat \<Rightarrow> int"
    assumes "i>0" "i\<le>h" 
    shows "(\<Sum>j=i-Suc 0..<h. f j) = f (i-Suc 0) + (\<Sum>j=i..<h. f j)"  
  proof -
    have "{i - Suc 0..<h} = insert (i - Suc 0) {i..<h}"
      using assms apply (cases i) by auto
    thus ?thesis using \<open>i>0\<close> 
      by fastforce
  qed  
    
  (*
    Hint: use quickcheck on the VCxx-lemmas to identify a wrong invariant early
  *)
  
  (* Invariant preserved by loop body *)  
  lemma VC1: "\<lbrakk>l < i; list_sum_invar a l h (i,r)\<rbrakk> \<Longrightarrow> list_sum_invar a l h (i - Suc 0, r + a!(i - Suc 0))"
    (* Hint: The lemma sum_split (see above) may be useful here *)
    unfolding list_sum_invar_def
    sorry
    
  (* Invariant implies postcondition: I \<and> \<not>b \<Longrightarrow> Q *)  
  lemma VC2: "\<lbrakk>\<not> l < i; list_sum_invar xs l h (i,r)\<rbrakk> \<Longrightarrow> r = (\<Sum>j=l..<h. xs!j)"
    unfolding list_sum_invar_def
    sorry
    
  (* Invariant holds initially: P \<Longrightarrow> I *)  
  lemma VC3: "l \<le> h \<Longrightarrow> list_sum_invar xs l h (h,0)"
    unfolding list_sum_invar_def
    sorry
    
  term list_sum  

  definition \<open>termination_measure \<equiv> undefined\<close>  
      
  lemma fact'_correct: "l\<le>h \<Longrightarrow> \<exists>l'. list_sum l h xs = Some (l',\<Sum>j=l..<h. xs!j)" for xs :: "int list"
    unfolding list_sum_def  
    apply (rule while_option_rule'[where P="list_sum_invar xs l h" and r="measure termination_measure"])
    apply (simp_all add: VC1 VC2 VC3 Let_def split: prod.splits)
    
  subsection \<open>b) Termination (easy) (4p)\<close>
    
    apply -
  
    (* Implement termination_measure by finding a meaningful measure function, 
        and complete the proof (if you haven't done so already).
    
      Note: You can also solve this if you did not manage to find a working invariant:
        For this loop pattern, there is a measure expression that works without
        a loop invariant, and will be proved automatically once you get it right!

      (easy)
    
    *)
    
    apply (auto simp: termination_measure_def)
    
    sorry (* Don't forget to close the proof ;) *)
  
  
section \<open>Q4: Monads ;) (10p) \<close>  

  (*
    The writer monad is a monad with an additional "log" operator. This operator can be used to
    log certain actions or other data of interest. It's implemented as a value and a string. The
    log is updated by appending a new line to the string.
  
    Return carries over a value and an empty log. Bind applies the value component of the monad to
    a given function and then appends the log that results from the function to the log of the input
    monad. Additionally, the log function returns the unit value and a specified log message.
  *)
  
  type_synonym 'a wM = "'a \<times> string"
  
  definition return :: "'a \<Rightarrow> 'a wM" where "return x = (x,[])"
  
  definition log :: "string \<Rightarrow> unit wM" where "log msg \<equiv> ((),msg)"
  
  definition bind_w :: "'a wM \<Rightarrow> ('a \<Rightarrow> 'b wM) \<Rightarrow> 'b wM" 
    where "bind_w m f = (let (x\<^sub>1,msg\<^sub>1) = m; (x\<^sub>2,msg\<^sub>2) = f x\<^sub>1 in (x\<^sub>2, msg\<^sub>1 @ msg\<^sub>2))"
  
  (*
    Adhoc_overloading allows us to use the monad syntax (do {..}, \<leftarrow>).
  *)
  
  adhoc_overloading bind bind_w  
  
  (*
    Next up, we prove the monad laws, define the weakest precondition and define some basic lemmas 
    that we need for proofs.
  *)
  
  context
    notes [simp] = return_def log_def bind_w_def
  begin
    lemma "bind_w (return x) f = f x" by auto
  
    lemma "bind_w m (return) = m" by auto
    
    lemma "bind_w (bind_w m f) g = bind_w m (\<lambda>x. bind_w (f x) g)"
      by (auto split: prod.splits)
      
  end
  
  (** log messages are ignored for the correctness proof (they're only debug output anyway) *)
  definition wp :: "'a wM \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where "wp m Q \<equiv> Q (fst m)"
  
  subsection \<open>a) Weakest precondition simplifications (easy) (3p)\<close>

  context
    notes [simp] = return_def log_def bind_w_def wp_def
  begin

    (* Complete the following lemmas and prove them! *)
    
    lemma wp_return: "wp (return x) Q = undefined" oops
  
    lemma wp_bind: "wp (bind m f) Q = undefined" oops
      
    lemma wp_log: "wp (log msg) Q = undefined" oops
    
  end
  
  subsection \<open>b) Logfilter (medium) (5p)\<close>
    
  (*
    Using the monad, we define a filter that logs every element that was filtered out of the 
    original list. Below the definition we provided an example of what the output should look like.
    If the sanity check passes, you can prove the correctness of your logfilter.
  *)
  
  fun logfilter :: "(string \<Rightarrow> bool) \<Rightarrow> string list \<Rightarrow> string list wM" where
    "logfilter _ _ = undefined"  
  
  value "logfilter (\<lambda>s. length s < 5) [''foo'',''bar-foo'',''blob'',''too long'']"
  (** Should produce something like: 
    (exact format of log messages not important, as long as they contain the information 
      which elements have been filtered out!)
  
    ([''foo'', ''blob''], ''Filtering out: bar-foo; Filtering out: too long;'')
  *)

  subsection \<open>c) Correctness of Logfilter (medium) (2p)\<close>
  (*
    If you did exercises 4a and 4b correctly, this may be easy. If the proof won't work, you maybe
    have made some mistakes there.
  *)
  
  lemma "wp (logfilter P xs) (\<lambda>xs'. xs' = filter P xs)"
    (*
      You must use the rules for wp here, that you proved above.
      Do not unfold the definitions of wp,bind,etc!
    *)
  
    oops

  
section \<open>Q5: Push/Pop (5 Bonus Points)\<close>
  (* Original question from 2018 exam. 
    This was the 'hard question' at the end, to challenge the good students.
  *)

  (* The following is an abstraction of a stack machine.
    Only the height of the stack is considered, the operations 
    are push and pop.
  *)  
  datatype instr = PUSH | POP  

  (*
    The following function computes the new height after executing a list of instructions,
    or returns None on stack underflow.
  *)    
  fun height :: "instr list \<Rightarrow> nat \<Rightarrow> nat option" where
    "height [] n = Some n" 
  | "height (PUSH#xs) n = height xs (Suc n)" 
  | "height (POP#xs) (Suc n) = height xs n" 
  | "height _ _ = None"

  subsection \<open>a) Same-Level Sequences (medium+) (2p)\<close>
  (*
    A same-level sequence is an instruction sequence that can run from an empty stack, 
    and yields an empty stack again.
    
    Show that any same-level sequence, when started in height n, 
      will yield height n again.
    
    (medium+)
  *)

  lemma lift_sl: "height is 0 = Some 0 \<Longrightarrow> height is n = Some n"  
    oops


  
  subsection \<open>b) Elimination \<open>PUSH@sl#POP\<close>  (hard) (3p)\<close>
  (*
    Prove that eliminating a subsequence of the form \<open>PUSH sl POP\<close>,
    where sl is a same-level path, does not change the semantics of 
    the instruction sequence.
    
    Hints:
      Start your proof with a case split on successful execution and failing execution.
      You might want to develop some auxiliary lemmas about concatenating/splitting of executions.
        
    (hard)
  *)


  lemma "height xs2 0 = Some 0 
    \<Longrightarrow> height (xs1@PUSH#xs2@POP#xs3) m = res \<longleftrightarrow> height (xs1@xs3) m = res"
    oops
  
  
  
  
  
end
