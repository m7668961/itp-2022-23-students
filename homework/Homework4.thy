theory Homework4
  imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin


text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #4
  RELEASED: Wed, Dec 7 2022
  DUE:      Wed, Dec 14 2022, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
\<close>

section \<open>General Information\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...


  This exercise will focus mainly on the definition of datatypes.
  We will define some custom datatypes and show that correctness by
  proving invariant preservations. For this, we use their abstract representation.
\<close>



section \<open>Ex 1: Let's attempt some Isar proofs (15 points) + (2 bonus points for style)\<close>

text \<open>NOTE: Only Isar proofs will be accepted, don't use sledgehammer for these proofs!

  Note: It's hard to phrase precise guidelines where an "Isar" proof ends and one found 
  with sledgehammer starts. However, we will subtract points for:
    \<^item> Typical 'sledgehammer artifacts', like
      \<^item> "metis/smt <long list of theorems>"
      \<^item> using <long list of theorems> apply auto/force/simp etc
    \<^item> Proofs that are hard to read, have a clear lack of structure, or are a failed 
      attempt of a structured proof that has then be 'forced through' with sledgehammer.
    
  On the other hand, we will give up to two bonus points for clear, on-the-point proofs,
  that find a nice balance between automation and explaining the essential steps of the proof.

\<close>


subsection \<open>a) Structured proof (4p)\<close>

text \<open>
  We consider two binary predicates \<open>T\<close> and \<open>A\<close> and assume that \<open>T\<close> is total,
  \<open>A\<close> is antisymmetric and \<open>T\<close> is a subset of \<open>A\<close>.

  We have given a skeleton on an Isar proof, try to understand what's going on and complete the 
  proof.

  Show with a structured, Isar-style proof that then \<open>A\<close> is also a subset of \<open>T\<close>:
\<close>

lemma structured_proof:
assumes TOT: "\<forall> x y. T x y \<or> T y x"
    and AS: "\<forall> x y. A x y \<and> A y x \<longrightarrow> x = y"
    and IMP: "\<forall> x y. T x y \<longrightarrow> A x y"
shows "A x y \<longrightarrow> T x y"
proof
  fix x y assume "A x y"
  from TOT have "T x y \<or> T y x" by simp
  then show "T x y"
  proof
    assume "T x y"
    (*<*)

    (*>*) 
    show ?thesis
      oops
  next
    assume "T y x"
    (*<*)

    (*>*) 
    show ?thesis
      oops
  qed
qed



subsection \<open>b) Factorials grow quickly (4p)\<close>

text \<open>We have given you a skeleton for an Isar proof stating that \<open>2 ^ n < fact n\<close> if \<open>n > 3\<close>.
  Try to understand how the skeleton works and fill in the gaps.\<close>


lemma exp_fact_estimate: "n>3 \<Longrightarrow> (2::nat)^n < fact n"
proof (induction n)
  case 0 then show ?case by auto
next
  case (Suc n)

  show "(2::nat) ^ Suc n < fact (Suc n)"
  proof (cases "n=3")
    case True
    then show ?thesis by (auto simp: numeral_eq_Suc)
  next
    case False
    with Suc.prems have A: "n>3" by auto
    note IH = Suc.IH[OF this]
    (*<*)

    (*>*) 
    show ?thesis
      oops
  qed
qed


subsection \<open>c) Irrationality of sin (arctan 2) (7p)\<close>

text \<open>In the Tutorial you have seen how to show that the square root of 2 is irrational. We step
  it up a notch now by showing that \<open>sin (arctan 2)\<close> is irrational. In essence, the proof will
  be very similar to the one from the tutorial. This is caused by the fact that we can convert
  \<open>sin (arctan ?x)\<close> into a representation containing a square root. The proof works as follows:
  We assume that \<open>sin (arctan 2)\<close> is rational, hence this alternative expression is rational. 
  We then obtain natural numbers m and n such that our alternative representation equals m / n
  and finish the proof as closely as possible to the proof of irrationality of \<open>sqrt 2\<close>

  HINT: find_theorems is your friend\<close>


lemma aux1:
  fixes m n :: nat
  assumes N0: "n > 0"
  assumes "\<bar>2/sqrt 5\<bar> = m / n" 
  shows "5 * m ^ 2 = 4 * n ^ 2"
proof -
  from assms have "(2/sqrt 5) ^ 2 = (m / n) ^ 2"
    by (metis power2_abs)
  hence "4 / 5 = m ^ 2 / n ^ 2" 
    by(auto simp: power2_eq_square)
  thus ?thesis 
    find_theorems "?a / ?b = ?c / ?d \<longleftrightarrow> ?a * ?d = ?c * ?b"
    apply(subst(asm) frac_eq_eq)
    apply simp
     apply (simp add: N0)
    by linarith 
qed



lemma "sin (arctan 2) \<notin> \<rat>" 
proof
  assume "sin (arctan 2) \<in> \<rat>"
  hence "some_expression_with_sqrt \<in> \<rat>" oops
  then obtain m n :: nat where
    expr_sqrt_rat: "\<bar>some_expression_with_sqrt\<bar> = m / n" and lowest_terms: "coprime m n" and "n \<noteq> 0"
    by (rule Rats_abs_nat_div_natE)
    (*<*)

    (*>*) 
  thus False using lowest_terms by auto
qed (*Proof as text*)




section \<open>Ex 2. Comma separated numbers (25 Points)\<close>

text \<open>
  Consider the following grammar, which accepts comma separated numbers
  
  S \<rightarrow> <num> | <num> "," S 

  It can be formalized in Isabelle as follows (producing lists of numbers as AST).
\<close>

datatype token = NUM int | COMMA

inductive csep :: "token list \<Rightarrow> int list \<Rightarrow> bool" where
  single: "csep [NUM a] [a]"
| comma: "csep tks as \<Longrightarrow> csep (NUM a#COMMA#tks) (a#as)"



text \<open>Additionally, this definition evaluates whether the input tokens match a list of integers.
  \<open>csep [NUM 1, COMMA, NUM -2, COMMA, NUM 4] as\<close> evaluates to True if and only if \<open>as = [1,-2,4]\<close>\<close>


subsection \<open>a) Empty list not accepted (5p)\<close>

text \<open>We can easily show that our grammar does not parse the empty string:\<close>


lemma "\<not>csep [] xs"
  by (auto elim: csep.cases)

text \<open>
    However, this proof is not really satisfactory, 
    as we find it hard to understand what happened here!
    
    Rephrase this proof (in Isar or apply-style), and annotate it with comments explaining the
    essential steps!
    
    (Guideline: the proof will have about 4..6 lines/steps)
  \<close>

lemma "\<not>csep [] xs"
proof 
oops

  
subsection \<open>b) Unambiguity (5p)\<close>

text \<open>
  We want to show that our grammar is not ambiguous, 
  i.e., there's at most one parse tree for each input.

  Note: in apply style, you will quickly run into a problem:
  the induction hypothesis of the form "\<dots> \<Longrightarrow> xs=xs'" will be used as a simp-rule, 
  making the simplifier loop.
  While sledgehammer might bail you out on this one, the rules for this question are:
  \<open>\<^bold>U\<^bold>s\<^bold>e \<^bold>I\<^bold>s\<^bold>a\<^bold>r to control which assumptions you use, \<^bold>d\<^bold>o \<^bold>n\<^bold>o\<^bold>t \<^bold>u\<^bold>s\<^bold>e \<^bold>s\<^bold>l\<^bold>e\<^bold>d\<^bold>g\<^bold>e\<^bold>h\<^bold>a\<^bold>m\<^bold>m\<^bold>e\<^bold>r!\<close>
\<close>
  
lemma "csep tks xs \<Longrightarrow> csep tks xs' \<Longrightarrow> xs=xs'"
oops

subsection \<open>c) Uniqueness (5p)\<close>

text \<open>
  Prove that, in csep, there is at most one input for each parse tree.
\<close>


lemma "csep tks xs \<Longrightarrow> csep tks' xs \<Longrightarrow> tks=tks'"
oops

subsection \<open>d) Completeness (5p)\<close>

text \<open>
  Prove that there is an input for any possible (non-empty) list
\<close>

lemma "xs\<noteq>[] \<Longrightarrow> \<exists>tks. csep tks xs"
oops

subsection \<open>e) Parser (5p)\<close>

text \<open>
  The following is a parser for comma-separated lists.
  It will return None on malformed input.
\<close>
  
fun parse_commas :: "token list \<Rightarrow> int list option" where
  "parse_commas [] = None"
| "parse_commas [NUM i] = Some [i]"  
| "parse_commas (NUM i#COMMA#tks) = map_option ((#)i) (parse_commas tks)"
| "parse_commas _ = None"

text \<open> Show that the parser is correct! \<close>
  
lemma "(parse_commas tks = Some xs) \<longleftrightarrow> csep tks xs"  
oops




section \<open>Bonus: The essence of the pumping lemma (5 Points)\<close>

text \<open>A finite LTS can recognize only limited sets of words (also called languages). These are 
  called 'regular languages. There is a very famous lemma that can be used to contradict that
  a given language is regular. This is called the Pumping lemma 
  \<^url>\<open>https://en.wikipedia.org/wiki/Pumping_lemma_for_regular_languages\<close>

  The idea is that a regular language is recognized by a finite automaton, which means that words
  that have more symbols that the number of states in the automaton must visit some state twice.
  Therefore, if we take a sufficiently long word, there is a loop which we can 'pump' (which means 
  we repeat the loop multiple times). This is usually used to show that a language is not regular.

  We are going to prove a weaker version using \<open>path \<delta> p ws q\<close>, which states that, if the list 
  of states of ws is not distinct, then there is a loop that we can 'pump'.
\<close>

subsection \<open>a: Show the essence of the pumping lemma (5p)\<close>

text \<open>We define path, which is a function that checks whether a list of states builds a path 
  starting in state p and ending in state q (the last element of a non-empty list must therefore 
  be q).\<close>

fun path :: "('a \<times> 'a) set \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a \<Rightarrow> bool" where
  "path \<delta> p [] q \<longleftrightarrow> p=q"
| "path \<delta> p (p'#ps) q \<longleftrightarrow> ((p,p')\<in>\<delta> \<and> path \<delta> p' ps q)"  


text \<open>For the pumping lemma to work, we need to be able to 'pump' our word. We do this using the
  repeat definition given below. We use a definition with concat and replicate because Isabelle
  already knows a lot about these functions and has a lot of simp lemmas on them. So auto will
  be able to help you a lot here.\<close>

definition "repeat n xs = concat (replicate n xs)"    
  


text \<open>Using this, we are now ready to show the essence of the pumping lemma. The lemma essentially
  says that every path that is not distinct must contain a loop and is therefore separable into 3 
  segments: The part before the loop, the loop itself and the part after the loop. The proof works
  by finding these segments and showing that the loop can be repeated indefinitely. You can do the 
  proof however you like, but we suggest that you follow our instructions by doing one proof step 
  after each text block.\<close>

     
lemma 
  assumes W: "path \<delta> p ps q"
  assumes LEN: "\<not>distinct ps"
  shows "\<exists>ps\<^sub>1 ps\<^sub>2 ps\<^sub>3. ps = ps\<^sub>1@ps\<^sub>2@ps\<^sub>3 \<and> (\<forall>n. path \<delta> p (ps\<^sub>1@(repeat n ps\<^sub>2)@ps\<^sub>3) q)"
proof -

(*
  Given that we visit a state twice, we can use the following theorem to find a decomposition of our
  list of states.
  Remember you can use [OF ...] to instantiate assumptions.
*)

  thm not_distinct_decomp

(*
  You have now obtained 5 segments of the path.
  Our solution will be three lists ?p1, ?p2, ?p3 where \<open>ps = ?ps1 @ ?ps2 @ ?ps3\<close> in which the final 
  state of ?p1 equals the final state of ?ps2. In other words, ?ps1 has the form "xs @ [p']" 
  and ?ps2 has the form "ys @ [p']". This means that ?ps1 will end in state p' and ?ps2 will 
  begin and end in p'. This means we can loop this part of the word indefinitely. ?ps3 will then
  build a path from p' to q.
*)

  (* change the variable names to match the ones in your proof! *)
  let ?ps1 = "xs @ [p']"
  let ?ps2 = "ys @ [p']"
  let ?ps3 = "zs"
  (* Note: let simply defines shortcuts, that are expanded during parsing.*)
  
(*
  This means that ?as1, ?as2 and ?as3 are also words in themselves (starting and ending in the 
  according states).
*)

(*
  If we can show the following, we are done. We may need an auxiliary lemma. Under which conditions 
  can we repeat a word?
*)

  hence "path \<delta> p (?ps1@(repeat n ?ps2)@?ps3) q" for n
    oops
  moreover 
  have "ps = ?ps1@?ps2@?ps3" oops
  ultimately show ?thesis oops
qed


end