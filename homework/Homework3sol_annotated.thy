theory Homework3sol_annotated
  imports Main
begin

(**
  Annotated version of hw3 solutions.

  General remarks:
  
    \<^item> most of you did ex1 correctly
    \<^item> most of you did \<^bold>n\<^bold>o\<^bold>t manage to define the powerset automaton correctly
  
    We were a bit surprised by that, as we expected that to be not that hard.
    We were even more surprised how many of you didn't even realize that their definition was wrong,
    and attempted the proof, even if quickcheck indicated a counterexample!

*)


text\<open>
    Assume we model one step of a program by a function \<open>f :: 's \<Rightarrow> 's\<close>, 
    such that for state s, f s gives you the next state. 
   
    At this point, it is not important how exactly the state (or the function f) looks like.
    
    We now start with an initial state \<open>s\<^sub>0\<close>, and apply steps until nothing changes any more.
  
   \<open>s\<^sub>0 \<rightarrow> s\<^sub>1 \<rightarrow> s\<^sub>2 \<rightarrow> s\<^sub>3 \<rightarrow> \<dots> \<rightarrow> s\<^sub>n, such that 
      \<^item> s\<^sub>i\<^sub>+\<^sub>1 = f s\<^sub>i    (the s\<^sub>i are a sequence of steps)
      \<^item> and f s\<^sub>n = s\<^sub>n  (nothing changes any more after reaching s\<^sub>n)\<close>

    We call any x with f x = x a fixed point of f (it is fixed under application of f).

    Thus, we want to repeatedly apply f to \<open>s\<^sub>0\<close> until we reach a fixed point.
        
    The following predicate states that s' is a fixed point that can be reached in this way:
  \<close>
  
definition fixedp :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "fixedp f s\<^sub>0 s' \<equiv> \<exists>n. s' = (f^^n) s\<^sub>0 \<and> f s' = s'"
  
text \<open> Intuitively: there is an n, such that applying f n times to \<open>s\<^sub>0\<close> reaches s' and s' is a fixed 
  point. Obviously, if we continue iteration beyond the fixed point, nothing changes any more 
  recall, we have reached a fixed point!). Show that iterating any number of times beyond the fixed
  point does not change the outcome.\<close>


subsection \<open>a: Show that iterating beyond the fixpoint does not change the outcome. (3p)\<close>

  
lemma funpow_fix_add: "f ((f^^n) s) = (f^^n) s \<Longrightarrow> (f^^(n+n')) s = (f^^n) s"
  (*<*)
  by (induction n') auto
  (*>*)

(**
  Most got that one. Though, I saw a few overly-complicated solutions here, like
  first proving an auxiliary lemma, and then using that to prove the main 
  lemma by induction on n (which is not productive towards the proof at all)
*)  
  
  
text \<open> That means, that we can reach (at most) one fixed point from the same start state \<open>s\<^sub>0\<close>. \<close>

subsection \<open>b: Show that at most one fixpoint exists. (3p)\<close>

lemma fixedp_determ: "fixedp f s\<^sub>0 s' \<Longrightarrow> fixedp f s\<^sub>0 s'' \<Longrightarrow> s'=s''"
  (* While sledgehammer might find proofs here, we ask you to adhere to the following proof scheme: *)
  (* Unfold definition and clarify goal *)
  unfolding fixedp_def
  apply clarify
  (* Name the number of iterations *)
  subgoal for n\<^sub>1 n\<^sub>2
    apply (cases "n\<^sub>1\<le>n\<^sub>2") (* Case distinction: which number is smaller? *)
    subgoal
      (* Find a suitable instantiation of funpow_fix_add *)
      using funpow_fix_add[of (*<*)f "n\<^sub>1" s\<^sub>0 "n\<^sub>2-n\<^sub>1"(*>*)] 
      by simp
    subgoal
      (* Find a suitable instantiation of funpow_fix_add *)
      using funpow_fix_add[of (*<*)f "n\<^sub>2" s\<^sub>0 "n\<^sub>1-n\<^sub>2"(*>*)] 
      by simp
    done
  done

  (** Most found the correct instantiation, and easily got this proof
  *)  
  

subsection \<open>c: Show invariant preservation. (2p)\<close>
    
lemma funpow_invar_rule: "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s) \<rbrakk> \<Longrightarrow> I ((f^^n) s)"  
  (*<*)
  by (induction n) auto
  (*>*)
  
(**

  s \<rightarrow> f s \<rightarrow> f(f(s)) \<rightarrow> \<dots>

  I s
  \<And>s. I s \<Longrightarrow> I (f s)
  


*)  
  

(** No problems here *)
  
subsection \<open>d: Show that the invariant holds for the fixed point (3p)\<close>
     
lemma fixedp_invar_rule:
  "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s); fixedp f s s' \<rbrakk> \<Longrightarrow> I s'"  
  (*<*)
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done
  (*>*)

(** No problems here *)
  
  
text \<open> Note that the following rule is easier to use in the following (equivalent) form.

  applied as an erule, this allows us to show invariant preservation, 
  and then derive our actual goal (Q) from the fact that the invariant holds 
  for s' and that s' is a fixed point.
\<close>

subsection \<open>e: Show the elimination rule (3p)\<close>


lemma fixedp_invar_ruleE:
  "\<lbrakk> fixedp f s s'; I s; \<And>s. I s \<Longrightarrow> I (f s); I s' \<and> f s' = s' \<Longrightarrow> Q\<rbrakk> \<Longrightarrow> Q"  
  (*<*)
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done
  (*>*)

(** No problems here. (sledgehammer finds the proof, too) *)
  
  
text \<open>
  A simple version of Euclid's algorithm \<^url>\<open>https://en.wikipedia.org/wiki/Euclidean_algorithm\<close> finds
  the greatest common divisor of two positive numbers by repeatedly replacing the greater number 
  by the difference of the two numbers. If both numbers are equal, they are equal to the gcd of 
  the original numbers.

  Thus, the state of the algorithm is a pair of numbers (a,b).
  When not equal, a step replaces the greater number by the difference. 
  When the numbers are already equal, a step does not change them (thus we will reach a fixed point).

  For example, given a state (10,8), the algorithm will go through the following steps:
  (10,8) \<rightarrow> (2,8) \<rightarrow> (2,6) \<rightarrow> (2,4) \<rightarrow> (2,2) \<rightarrow> (2,2) \<rightarrow> ....
  So the state (2,2) is the fixed point.
\<close>  

subsection \<open>f: Specify one step of Euclid's algorithm (2p)\<close>

text \<open>HINT: Don't specify the whole procedure, just one step, i.e. \<open>euclid_step (10,8) = (2,8)\<close>\<close>
      
definition euclid_step :: "nat \<times> nat \<Rightarrow> nat \<times> nat" where
(*<*)
  "euclid_step \<equiv> \<lambda>(a,b). if a<b then (a,b-a) else if b<a then (a-b,b) else (a,b)"
(*>*)

(** No problems here *)


text \<open>
  A few tests: we just apply the step function a few hundred times, should be enough for small numbers!

  Before you continue, check that all of these yield True!\<close>

value "(euclid_step^^100) (20,10) = (10,10)" 
value "(euclid_step^^100) (7,9) = (1,1)"
value "(euclid_step^^100) (17,34) = (17,17)"
value "(euclid_step^^100) (128,256) = (128,128)"
value "(euclid_step^^200) (2*2*2*3*3*5*7*13,2*2*5*13) = (2*2*5*13,2*2*5*13)"


text \<open>
  Now that we have a fixpoint algorithm, we can use the proofs and definitions that we have readily
  available to show the correctness of the algorithm. The only thing we need for that is an 
  invariant.

  We specify it in text here:
  
  the gcd of the current pair is equal to the gcd of the original pair, and both values of 
  the current pair are positive.
\<close>

subsection \<open>g: Specify the invariant formally (2p)\<close>

definition euclid_invar :: "nat \<Rightarrow> nat \<Rightarrow> nat\<times>nat \<Rightarrow> bool" where 
  (*<*)
  "euclid_invar a\<^sub>0 b\<^sub>0 \<equiv> \<lambda>(a,b). gcd a b = gcd a\<^sub>0 b\<^sub>0 \<and> a\<noteq>0 \<and> b\<noteq>0"
  (*>*)

(** No problems here *)
  
  
text \<open> To show that the invariant is correct, we must show that it is preserved over an iteration.\<close>

subsection \<open>h: Show that it is preserved by a step. (3p)\<close>

text \<open>HINT: (sledgehammer and find_theorems are your friends to discover the relevant lemmas about gcd! )\<close>

lemma euclid_invar_pres: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_invar a\<^sub>0 b\<^sub>0 (euclid_step (a,b))"
  (*<*)
  apply (auto simp: euclid_invar_def euclid_step_def)
  subgoal
    using gcd_diff1_nat less_or_eq_imp_le by presburger
  subgoal
    by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
  done
  (*>*)

(** Most of you got that!  *)

(**
  However, I saw one interesting attempt. By defining an odd invariant, 
  they found themselves in the position that they had to prove
    \<lbrakk>0 < a\<^sub>0; 0 < b\<^sub>0\<rbrakk> \<Longrightarrow> fixedp euclid_step (a\<^sub>0, b\<^sub>0) (gcd a\<^sub>0 b\<^sub>0, gcd a\<^sub>0 b\<^sub>0)

    
  note that this is a stronger property than the original one, 
  called total correctness!

  See theory Homework3_sol_FIXP_total_correct for an example.
*)

  
  
text \<open>A fixed point implies that both elements of the pair are equal to the gcd of the original pair\<close>

subsection \<open>i: Show this (3p)\<close>

lemma euclid_invar_final: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_step (a,b) = (a,b) \<Longrightarrow> a=b \<and> b=gcd a\<^sub>0 b\<^sub>0"
  (*<*)
  apply (auto simp: euclid_invar_def euclid_step_def split: if_splits)
  done
  (*>*)
  

subsection \<open>j: Assemble everything (3p)\<close>

text \<open>We have already given you the skeleton for the proof. You just need to collect the relevant 
  lemmas. However, try to find our for yourself why we chose this structure, why we used erule and 
  try to reason about how the structure of lemma \<open>fixedp_invar_ruleE\<close> benefits us here.\<close>

lemma euclid_nat_correct: "fixedp euclid_step (a\<^sub>0,b\<^sub>0) (a,b) \<Longrightarrow> a\<^sub>0\<noteq>0 \<Longrightarrow> b\<^sub>0\<noteq>0 \<Longrightarrow> a=b \<and> b=gcd a\<^sub>0 b\<^sub>0"
  apply (erule fixedp_invar_ruleE[where I="euclid_invar a\<^sub>0 b\<^sub>0"]; clarify)
  subgoal (* Invar holds initially *)
    (*<*) by (auto simp: euclid_invar_def) (*>*)
  subgoal for a b (* Invar preserved by step *)
    (*<*) by (blast intro: euclid_invar_pres) (*>*)
  subgoal (* Invar at fixed point implies gcd *) 
    (*<*) by (blast dest: euclid_invar_final) (*>*)
  done  





section \<open>Ex2: Labelled Transition System (13 Points)\<close>

text \<open>A labelled transition system (LTS) is a transition system in which each edge is labelled with 
  a symbol. By following a path on the LTS, we can construct a word out of the symbols in the same 
  order in which we passed over them. For example, we may have an LTS like the one below.
  LTS \<delta>:
  \<open>
  S\<^sub>1 \<comment>c\<rightarrow> S\<^sub>2 \<comment>b \<rightarrow> S\<^sub>3
           |        |
           a        a
           \<down>        \<down>
           S\<^sub>4 \<comment>c\<rightarrow> S\<^sub>5
  \<close>
  In the tutorial, we have defined a word as follows:
\<close>

fun word :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> 'q \<Rightarrow> 'a list \<Rightarrow> 'q \<Rightarrow> bool" where
  "word \<delta> p [] q \<longleftrightarrow> p=q"
| "word \<delta> p (a#as) q \<longleftrightarrow> (\<exists>p'. (p,a,p')\<in>\<delta> \<and> word \<delta> p' as q)"  

text \<open>This means that \<open>word \<delta> S\<^sub>1 [c,b,a] S\<^sub>5\<close> evaluates to True for example.
  However, this method causes exponential behaviour, because our search branches whenever there are
  multiple outgoing edges with the same symbol from a state. We can counter this by adapting our
  word function. Insted of a list of symbols, we can input a list of tuples containing a symbol and
  the successor state (the one that we transition to). This is what we do in \<open>word_dir\<close>.
  For example \<open>word_dir \<delta> S\<^sub>1 ws S\<^sub>5\<close> evaluates to True for \<open>ws = [(c,S\<^sub>2),(b,S\<^sub>3),(a,S\<^sub>5)]\<close> or 
  \<open>ws = [(c,S\<^sub>2),(a,S\<^sub>4),(c,S\<^sub>5)]\<close> but not for any other word. Also not for 
  \<open>ws = [(c,S\<^sub>2),(b,S\<^sub>4),(a,S\<^sub>5)]\<close> which has a combination of symbols that we can find between our 
  start and end state but not via state \<open>S\<^sub>4\<close>\<close>

subsection \<open>a: Specify word_dir (2p)\<close>

fun word_dir where
  "word_dir \<delta> p [] q \<longleftrightarrow> p=q"
| "word_dir \<delta> p ((a,p')#as) q \<longleftrightarrow> ((p,a,p')\<in>\<delta> \<and> word_dir \<delta> p' as q)"  

(** Most of you got the definition right *)


text \<open>word_dir should do exactly the same as word with the exception that one needs to provide the 
  states. We can conclude the following: If \<open>word\<close> accepts a list of symbols, then there must be a
  sequence of states such that, if combined with this list of symbols, \<open>word_dir\<close> accepts it. This
  should also hold in the other direction. We use the function \<open>zip\<close>, which takes two lists and
  converts them into a list of tuples. We use this to combine a list of symbols with a list of 
  successor states.\<close>


subsection \<open>b: Show this relation between word and word_dir (3p)\<close>

text \<open>HINT: Take a look at the following theorem\<close>
thm length_Suc_conv 

lemma "word \<delta> p as q \<longleftrightarrow> (\<exists>qs. length qs = length as \<and> word_dir \<delta> p (zip as qs) q)"
  apply (induction as arbitrary: p)
  subgoal by simp
  subgoal for a as p by (force simp: length_Suc_conv)
  done
  
(** Many got that somehow \<dots> but I saw a few very desperate attempts, basically blindly trying to
  somehow 'use' length_Suc_conv, without understanding what happened, and then 
  either succeeding or not in forcing the proof through.
  
  One proof attempt contained an interesting thing:
*)  

lemma "word \<delta> p as q \<longleftrightarrow> (\<exists>qs. length qs = length as \<and> word_dir \<delta> p (zip as qs) q)"
  apply(induction as) (** You'll need arbitrary p at the very least! *)
  apply auto[]
  apply simp_all
  using length_Suc_conv[of "qs" "length as"] (** Note the yellow-background on qs! That means that this name is not in scope here! *)
  apply(auto)
  sorry
  
  


  


text \<open>Explicitly telling an LTS which states to visit is not a very flexible method. Usually, the 
  LTS is a black box for the word. We want to check if an LTS recognizes a word without attaching 
  any information about the LTS to the word. To eliminate branching, we can make the LTS 
  deterministic. This means that, in every state, every symbol occures at most once on all of its
  outgoing edges. We achieve this using the so-called powerset construction:
  \<^url>\<open>https://en.wikipedia.org/wiki/Powerset_construction\<close>

  It works as follows: Given an LTS \<delta>, we define its powerset construction as \<delta>d \<delta>. A state in \<delta>d \<delta> is
  a set of states of \<delta>. We have \<open>(ps,a,qs) \<in> \<delta>d \<delta>\<close> if and only if qs is a subset of states \<open>q \<in> qs\<close> 
  of \<delta> for which there exists a \<open>p \<in> ps\<close> with \<open>(p,a,q) \<in> \<delta>\<close>.
  For example given the following \<delta>:
  \<open>
  S\<^sub>1 \<comment>a\<rightarrow> S\<^sub>2 \<comment>b\<rightarrow> S\<^sub>3
  |        
  a        
  \<down>        
  S\<^sub>4 \<comment>c\<rightarrow> S\<^sub>5
  \<close>
  Then \<delta>d \<delta> looks as follows:
  \<open>
  {S\<^sub>1} \<comment>a\<rightarrow> {S\<^sub>2,S\<^sub>4} \<comment>b\<rightarrow> {S\<^sub>3}
               |        
               c        
               \<down> 
              {S\<^sub>5}
  \<close>
  \<close>

subsection \<open>c: Define \<open>\<delta>d\<close> according to the powerset construction using set comprehension. (3p)\<close>

text \<open>NOTE: It's okay if your determinization produces 'ghost edges'. In the example above, there
  is also the edge \<open>({S\<^sub>1,S\<^sub>2},a,{S\<^sub>2,S\<^sub>4})\<close>. This is caused by the fact that we don't construct the 
  state space exploratively, but through set comprehension. However, by only starting in singleton
  states (like \<open>S\<^sub>1\<close>) we can never reach those ghost transitions and states. So if your 
  implementation contains these, it's not wrong.

  Eternal fame will come to those who fix this problem and still manage to solve the proofs.\<close>

definition \<delta>d :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> ('q set \<times> 'a \<times> 'q set) set" where 
  "\<delta>d \<delta> \<equiv> { (ps,a,qs). qs = {q. \<exists>p\<in>ps. (p,a,q) \<in> \<delta>}}"

lemma "(\<forall>x. x\<in>s \<and> P x) \<longleftrightarrow> (s=UNIV \<and> (\<forall>x. P x))" by blast 
  
lemma "(\<exists>x. x\<in>s \<longrightarrow> P x) \<longleftrightarrow> (s\<noteq>UNIV \<or> (\<exists>x. P x))" by blast

(**
  We were very surprised that almost no one got that right.
  You definitely have to work on:
    \<^item> formalizing a problem from a textual description
    \<^item> testing your formalization when you suspect it might not be what you intended

  Noteable things I saw here:

  \<^item>
      \<exists>p. p\<in>ps \<longrightarrow> (p,a,q) \<in> \<delta>
      \<forall>p. p\<in>ps \<and> \<dots>
    
      
    \<forall>x. x\<in>s \<longrightarrow> P x wrong
    \<forall>x. x\<in>s \<and> P x  

    \<exists>x. x\<in>s \<longrightarrow> P x wrong
    
          
    Note that there is a rule of thumb: \<forall> goes with \<longrightarrow>, \<exists> goes with \<and>.
    
    \<forall>x. x\<in>s \<and> \<dots> simply implies that s contains all elements of the type (UNIV)
    
    \<exists>p. p\<in>ps \<longrightarrow> \<dots> is trivially true for every set ps but UNIV
    
  \<^item> Overly complicated (and wrong) definitions:
    Note, sometimes the simplifier can help you to understand your definitions.
    Here's an example:
      
*)  
  
definition \<delta>d' :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> ('q set \<times> 'a \<times> 'q set) set" where 
  "\<delta>d' \<delta> = {(q, x, p) | q x p e. 
      e \<in> \<delta> 
    \<and> x=fst (snd e) 
    \<and> q = {q' | q' a. a \<in> \<delta> \<and> fst (snd a) = x \<and> q' = fst a} 
    \<and> p = {p' | p' b. b \<in> \<delta> \<and> fst (snd b) = x \<and> p' = snd (snd b)}}"
(**
  that looks complicated! Why not use a tuple for e, and inline a bit:
*)
(** HW DISCUSSION *)
lemma "\<delta>d' \<delta> = {(q, x, p) | q x p e. 
      e \<in> \<delta> 
    \<and> x=fst (snd e) 
    \<and> q = {q' | q' a. a \<in> \<delta> \<and> fst (snd a) = x \<and> q' = fst a} 
    \<and> p = {p' | p' b. b \<in> \<delta> \<and> fst (snd b) = x \<and> p' = snd (snd b)}}"
  apply simp  
  (** Note the subgoal here \<dots> *)
  oops

(** Already more readable. *)    
lemma "\<delta>d' \<delta> = {({p. \<exists>q. (p, x, q) \<in> \<delta>}, x, {q. \<exists>p. (p, x, q) \<in> \<delta>}) |x. \<exists>p q. (p, x, q) \<in> \<delta>}"  
  unfolding \<delta>d'_def by auto
  
(** And now you can also easily see that it's wrong. 
  Your \<delta>d does not really depend on the state, it actually only contains one state per 
  letter in the alphabet. *)  
  




text \<open>We have seen a predicate that checks whether an LTS is deterministic in the tutorial. We
  can use it to check whether our implementation is correct.\<close>

definition det :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> bool" where 
  "det \<delta> \<equiv> \<forall>q a q1 q2. (q, a, q1) \<in> \<delta> \<and> (q, a, q2) \<in> \<delta> \<longrightarrow> q1=q2"

subsection \<open>d: Use this predicate as a sanity check (1p)\<close>

lemma "det (\<delta>d \<delta>)"
  unfolding det_def \<delta>d_def
  apply auto
  done
  
(** Also surprisingly, most of your wrong definitions were still deterministic, and
    most of you managed to prove that. (We should have defined another sanity check)
*)  
  

text \<open>Given that we start from a singleton state, our determinization should recognize the same 
  words as the original non-deterministic automaton. (We have to start in a singleton state due
  to the ghost state problem described earlier). Show that this indeed holds.\<close>

subsection \<open>e: Show that determinization recognizes the same words. (4p)\<close>

text \<open>HINT: You may want to break the problem down into multiple subproblems.\<close>

(**
  Even after we gave the extra hint on how to split and generalize this lemma, 
  not all of you attempted the split.
  
  However, those that did usually managed to prove one direction (got them 2p) 
  \<dots> the other direction was usually unprovable (the \<delta>d was wrong)

  And, some of you are still ignoring quickcheck \<dots> and attempting proofs that have a clear counterexample
  
*)

definition \<delta>d'' :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> ('q set \<times> 'a \<times> 'q set) set" where 
  "\<delta>d'' \<delta> = {(ps, a, qs) | p ps a qs. \<forall>q. (p, a, q) \<in> \<delta> \<and> p \<in> ps \<and> q \<in> qs}"


lemma aux1: "word \<delta> p as q \<Longrightarrow> (\<forall>ps. p\<in>ps \<longrightarrow> (\<exists>qs. q\<in>qs \<and> word (\<delta>d'' \<delta>) ps as qs))"
  (** Do enable auto-quickcheck. And do not ignore it! *)
  oops

  
lemma "(word (\<delta>d' \<delta>) ps as qs) \<Longrightarrow> (\<forall>q\<in>qs. \<exists>p\<in>ps. word \<delta> p as q)"
  quickcheck (** \<leftarrow> 
    sometimes, quickcheck finds potentially spurious counterexamples. 
    The lemma may still be provable! 
    
    auto-quickcheck does not display those!
  *)
  apply (induction as arbitrary: ps)
  apply (auto simp: \<delta>d'_def)
  by blast
  

lemma aux2: "(word (\<delta>d \<delta>) ps as qs) \<Longrightarrow> (\<forall>q\<in>qs. \<exists>p\<in>ps. word \<delta> p as q)"
  apply (induction as arbitrary: ps)
  apply (auto simp: \<delta>d_def)
  by force
  
lemma aux1: "word \<delta> p as q \<Longrightarrow> (\<forall>ps. p\<in>ps \<longrightarrow> (\<exists>qs. q\<in>qs \<and> word (\<delta>d \<delta>) ps as qs))"
  apply (induction as arbitrary: p)
  apply (auto simp: \<delta>d_def)
  by (metis (mono_tags, lifting) mem_Collect_eq)
  

lemma "word \<delta> p as q \<longleftrightarrow> (\<exists>qs. word (\<delta>d \<delta>) {p} as qs \<and> q\<in>qs)"
  apply rule
  subgoal using aux1 by fast
  subgoal using aux2 by blast
  done

  
(** And one last thing:

  If you sorry a lemma, AND quickcheck shows you a counterexample 
  \<dots> better do not add it to the simpset!

*)  

lemma [simp]: "2+x<0" sorry
  
(* Subsequent proofs may start to magically work out very easily \<dots> ;) *)

lemma "3*x^2 + 4*x + 2 < 0" for x::int 
  apply (auto simp: algebra_simps)
  done
  

section \<open>Bonus: On the fly powerset (5 Points)\<close>
  
text \<open>
  Note: the proofs in this question are easy, but we give almost no hints on the formalization. 
  You have to come up with the functions and correctness statement yourself.
  Please add sufficient explanation such that we can follow your formalization! 
  
  Wikipedia lists a few ways how to 'implement' an NFA. \<^url>\<open>https://en.wikipedia.org/wiki/Nondeterministic_finite_automaton#Implementation\<close>
  
  We have already explored the conversion to a DFA.
  
  Another option is to do the powerset construction 'on the fly', 
  i.e., keep track of the set of states that the automaton can be in while reading the word.
  If you start with the set singleton set that only contains the start state, you can then check if 
  an accepting state is contained in the set at the end.
  
  Your task:
  
  \<^item> define a function that computes, for a start state and a word, the states that the 
    automaton may be in after reading that word. Do not compute more sets than necessary, e.g.
    solutions along the lines \<^term>\<open>SOME qs. word (\<delta>d \<delta>) ps as qs\<close> do not count!
  
  \<^item> show that your function is correct (sound and complete) wrt. \<^const>\<open>word\<close>
  
  Note: no need for any fancy set data structure! \<^typ>\<open>'q set\<close> will do for this question.
\<close>

(*<*)  
fun of_pwr where
  "of_pwr \<delta> qs [] = qs"
| "of_pwr \<delta> qs (a#as) = of_pwr \<delta> ({ q'. \<exists>q\<in>qs. (q,a,q')\<in>\<delta> }) as"  


lemma of_pwr_correct_aux: "q' \<in> of_pwr \<delta> qs as \<longleftrightarrow> (\<exists>q\<in>qs. word \<delta> q as q')"
  apply (induction as arbitrary: qs)
  apply auto
  done
  
lemma of_pwr_correct: "q' \<in> of_pwr \<delta> {q} as \<longleftrightarrow> word \<delta> q as q'"
  by (simp add: of_pwr_correct_aux)


(* Alternative solution *)

fun word_det :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> 'q set \<Rightarrow> 'a list \<Rightarrow> 'q set \<Rightarrow> bool" where
  "word_det \<delta> ps [] qs \<longleftrightarrow> ps=qs"
| "word_det \<delta> ps (a#as) qs \<longleftrightarrow> word_det \<delta> {q. \<exists>p\<in>ps. ((p,a,q) \<in> \<delta>)} as qs"  


lemma "word (\<delta>d \<delta>) ps as qs = word_det \<delta> ps as qs"
  apply(induction as arbitrary: ps)
   apply (auto simp: \<delta>d_def)
  done

(*>*)  

  



