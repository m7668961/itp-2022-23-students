theory Homework2sol
  imports Main
begin

text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #2
  RELEASED: Wed, Nov 23 2022
  DUE:      Wed, Nov 30, 2022, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
\<close>
  

section \<open>General Information\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...


  This exercise will focus mainly on the definition of datatypes.
  We will define some custom datatypes and show that correctness by
  proving invariant preservations. For this, we use their abstract representation.
\<close>


section \<open>Ex1: Slist (15 Points)\<close>

text \<open>Slists (sorted lists) are lists in whose elements are sorted in ascending order (with the index).
  e.g. [1,2,3] is an slist. Moreover, elements can only occur once. So [1,2,2,3] is not an slist. 
  The abstraction function of slists is \<open>set\<close>\<close>

subsection \<open>a: Specify the insert function \<open>ins_slist\<close> (3p)\<close>

text \<open>E.g. \<open>ins_slist 5 [2,4,6,8] = [2,4,5,6,8]\<close>\<close>

fun ins_slist :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  "ins_slist x [] = [x]" |
  "ins_slist x (y#ys) = (if x = y then y#ys else if x < y then (x#y#ys) else y#ins_slist x ys)"


subsection \<open>b: Specify the invariant for slists (3p)\<close>

text \<open>As specified before, slists are sorted in ascending order and cannot have duplicates.
  HINT: take a look at \<open>sorted_wrt\<close>\<close>

definition "slist_invar xs = (sorted_wrt (<) xs)"



subsection \<open>c: Specify and prove invariant preservation (3p)\<close>

text \<open>If the invariant holds before inserting an element to an s_list, it should also hold
  afterwards. Specify this as a lemma and show correctness. Use the invariant definition from 1b.
  HINT: you need an auxiliary lemma, remember that we defined \<open>set\<close> to be our abstraction function.\<close>

lemma [simp]: "set(ins_slist x xs) = insert x (set xs)"
  apply(induction xs)
  apply auto
  done

lemma "slist_invar xs \<Longrightarrow> slist_invar (ins_slist x xs)"
  unfolding slist_invar_def
  apply(induction xs)
  apply auto
  done


subsection \<open>d: Specify and prove deletion as well as invariant preservation and abstract operation
  (6p)\<close>

text \<open>Follow the same procedure\<close>

fun del_slist :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  "del_slist _ [] = []" |
  "del_slist i (x#xs) = (if i = x then xs else if i < x then x#xs else x#(del_slist i xs))"

lemma [simp]: "slist_invar xs \<Longrightarrow> set (del_slist i xs) = set xs - {i}"
  unfolding slist_invar_def
  apply(induction xs)
   apply auto
  done

lemma "slist_invar xs \<Longrightarrow> slist_invar (del_slist i xs)"
  apply(induction xs)
   apply (auto simp: slist_invar_def)
  done



section \<open>Ex 2: BST (13 Points)\<close>

text \<open>We have seen binary search trees in the lecture and we have defined basic operations on
  them. One of them is the delete operation. In the lecture, we have deleted a node by replacing
  the value that we want to delete with the largest value in its left subtree and then deleting that
  value. Now, we will look at a different method: Once we have found the node that we want to delete
  we 'rotate' the tree until the value can be removed directly.

  Rotation works as follows (in this case we rotate to the right):
  
        A               B
       / \             / \
      B   \<triangle>\<^sub>3  \<Longrightarrow>     \<triangle>\<^sub>1  A
     / \                 / \
    \<triangle>\<^sub>1  \<triangle>\<^sub>2              \<triangle>\<^sub>2 \<triangle>\<^sub>3
  
  Where \<triangle>\<^sub>i is an arbitrary subtree.

  After rotation, we can apply our delete procedure to the right subtree (node A as a root).
  We can terminate the process once we apply delete to a node with at least one Leaf by replacing 
  the current tree with the only available child node or with a Leaf if no child nodes are 
  available.

  We provide the data structure from the lecture.
\<close>


datatype BST = Node BST int BST | Leaf

fun bst_set :: "BST \<Rightarrow> int set" where
  "bst_set Leaf = {}"
| "bst_set (Node l x r) = bst_set l \<union> {x} \<union> bst_set r"  

fun bst_invar :: "BST \<Rightarrow> bool" where
  "bst_invar Leaf \<longleftrightarrow> True"
| "bst_invar (Node l x r) \<longleftrightarrow> 
      bst_invar l 
    \<and> bst_invar r 
    \<and> (\<forall>y\<in>bst_set l. y<x) 
    \<and> (\<forall>y\<in>bst_set r. y>x)"


section \<open>a: Specify bst_delete_root (4p)\<close>

text \<open>Specify the delete function which removes a node if it is available. Otherwise, nothing 
  changes. The best procedure for this may be to specify a function first which deletes the root
  node of a given tree using rotation. Specify such function.
  HINT: it may be beneficial to prove correctness using the abstraction function \<open>bst_set\<close> and
  showing invariant preservation\<close>

fun bst_root_value :: "BST \<Rightarrow> int" where
  "bst_root_value Leaf = undefined" |
  "bst_root_value (Node _ x _) = x"

fun bst_delete_root :: "BST \<Rightarrow> BST" where
  "bst_delete_root Leaf = Leaf" |
  "bst_delete_root (Node Leaf x rt) = rt" |
  "bst_delete_root (Node (Node lst y rst) x rt) = Node lst y (bst_delete_root (Node rst x rt))"

lemma bst_delete_root_\<alpha>: "bst_invar b \<Longrightarrow> bst_set (bst_delete_root b) = bst_set b - {bst_root_value b}"
  apply(induction b rule: bst_delete_root.induct)
    apply auto
  done

lemma bst_delete_root_invar: "bst_invar b \<Longrightarrow> bst_invar (bst_delete_root b)"
  apply(induction b rule: bst_delete_root.induct)
    apply (auto simp: bst_delete_root_\<alpha>)
  done


subsection \<open>b: Specify the full delete function (4p)\<close>

text \<open>Now that we have a function to delete the root, we can simply find the node that
  we want to remove in our tree. This will be the root of a subtree and we already have a function 
  that can remove the root node.\<close>

fun bst_delete_rotate :: "BST \<Rightarrow> int \<Rightarrow> BST" where
  "bst_delete_rotate Leaf _ = Leaf" |
  "bst_delete_rotate (Node lt x rt) y = (if x = y then bst_delete_root (Node lt x rt)
    else if y < x then (Node (bst_delete_rotate lt y) x rt) 
    else (Node lt x (bst_delete_rotate rt y)))"


subsection \<open>c: Show invariant preservation (5p)\<close>

text \<open>HINT: Remember the procedure from previous exercises and the lectures.\<close>
                                           
lemma bst_delete_rotate_\<alpha>: "bst_invar b \<Longrightarrow> bst_set (bst_delete_rotate b i) = bst_set b - {i}"
  apply(induction b i rule: bst_delete_rotate.induct)
   apply simp_all
  apply (auto simp: bst_delete_root_\<alpha>)
  done

lemma bst_delete_rotate_invar: "bst_invar b \<Longrightarrow> bst_invar (bst_delete_rotate b i)"
  apply(induction b)
  apply (auto simp: bst_delete_rotate_\<alpha> bst_delete_root_invar)
  done





section \<open>Ex 3: Run-length encoded lists (12 points)\<close>


text \<open>A simple compression techniques is called run-length encoding, in which subsequent duplicates
  are compressed into a list that specifies the value and how often it occurs. E.g. 
  \<open>encode [1,1,1,1,3,4,4] = [(4,1),(1,3),(2,4)]\<close>. An element (e.g. \<open>(4,1)\<close>) of this list is called 
  a 'run'. The first value in each tuple specifies how often the second value is repeated in the 
  decoded list. We give the run-length decoded list as a \<open>'a rl\<close> below.\<close>
    
type_synonym 'a rl = "(nat \<times> 'a) list"


subsection \<open>a: Specify the abstraction function and the invariant. (3p)\<close>

text \<open>The abstraction function transforms and rl into a list, 
  e.g. \<open>rl_\<alpha> [(4,1),(1,3),(2,4)] = [1,1,1,1,3,4,4]\<close>
  The invariant makes sure that there are no 0 repetitions (so (0,x) does not occur in the rl) and 
  that no adjacent entries exist for the same element (so (n,x) is never followed by (m,x).\<close>


fun rl_\<alpha> :: "'a rl \<Rightarrow> 'a list" where
  "rl_\<alpha> [] = []"
| "rl_\<alpha> ((n,x) # xs) = replicate n x @ rl_\<alpha> xs"  

  
fun rl_invar :: "'a rl \<Rightarrow> bool" where
  "rl_invar [] = True"
| "rl_invar [(n,x)] \<longleftrightarrow> n\<noteq>0"
| "rl_invar ((n\<^sub>1,x\<^sub>1)#(n\<^sub>2,x\<^sub>2)#xs) \<longleftrightarrow> n\<^sub>1\<noteq>0 \<and> x\<^sub>1\<noteq>x\<^sub>2 \<and> rl_invar ((n\<^sub>2,x\<^sub>2)#xs)"


subsection \<open>b: Specify rl_Nil and the refinement and invariant preservation lemmas and prove them. 
  (3p)\<close>

text \<open>rl_Nil is the rl that encodes the empty list \<open>[]\<close>\<close>

definition "rl_Nil = []"
  
lemma rl_Nil_refine[simp]: "rl_\<alpha> rl_Nil = []" by (auto simp: rl_Nil_def)
lemma rl_Nil_invar[simp]: "rl_invar rl_Nil"
  unfolding rl_Nil_def
  by auto


subsection \<open>c: Specify rl_Cons and the refinement and invariant preservation lemmas and prove them. 
  (3p)\<close>

text \<open>Similarly to Cons, rl_Cons prepends an element in front of the list. If the element is the
  same as the first element in the rl, the counter is incremented, e.g. 
  \<open>rl_Cons 2 [(3,2)(2,5)(6,2)] = [(4,2),(2,5),(6,2)]\<close>. Otherwise, a new run is prepended, e.g.
  \<open>rl_Cons 2 [(2,5),(6,2)] = [(1,2),(2,5),(6,2)]\<close>\<close>

fun rl_Cons :: "'a \<Rightarrow> (nat \<times> 'a) list \<Rightarrow> (nat \<times> 'a) list" where 
  "rl_Cons x [] = [(1,x)]" |
  "rl_Cons x ((n,x')#xs') = (if x=x' then (Suc n,x')#xs' else (1,x)#(n,x')#xs')"  

lemma rl_Cons_refine[simp]: "rl_\<alpha> (rl_Cons x xs) = x # rl_\<alpha> xs"
  apply (cases xs)
   apply auto
  done

  (* Aux lemma *)
lemma rl_invar_Suc: "rl_invar ((n,x)#xs) \<Longrightarrow> rl_invar ((Suc n,x)#xs)"  
  apply (cases xs) by auto
    
lemma rl_Cons_Invar[simp]: "rl_invar xs \<Longrightarrow> rl_invar (rl_Cons x xs)"  
  apply (cases xs) 
   apply (auto simp: rl_invar_Suc)
  done


subsection \<open>d: Specify rl_nth and the refinement lemma and prove it (3p)\<close>

text \<open>Similarly to nth for regular lists (where nth xs n or xs ! n gets the element at index n), 
  we can define nth for an rl. Attetion, we don't want to return a run here, but the nth element 
  over all runs if we were using the decoded list. E.g. \<open>rl_nth [(3,5),(4,2)] 2 = 5\<close> and 
  \<open>rl_nth [(3,5),(4,2)] 3 = 2\<close> 
  HINT: invariant preservation does not make sense as we are merely reading from the list.\<close>

fun rl_nth :: "'a rl \<Rightarrow> nat \<Rightarrow> 'a" where
  "rl_nth ((n,x)#xs) i = (if i<n then x else rl_nth xs (i-n))"
    
lemma rl_nth_correct: "i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i"  
  apply (induction xs i rule: rl_nth.induct)
  by (auto simp: nth_append)


section \<open>Bonus: Updating the run-length encoded list (5 bonus points)\<close>

text \<open>Define an update function \<open>rl_upd xs i y\<close> which updates the value of the element at index i
  (as per rl_nth) to a value y. E.g. \<open>rl_upd [(4,2),(3,5),(6,2)] 4 7 = [(4,2),(1,7),(2,5),(6,2)]\<close>
  Some edge cases, if the update happens in the middle of a run, it needs to be split, e.g. 
  \<open>rl_upd [(4,2),(3,5),(6,2)] 5 7 = [(4,2),(1,5),(1,7),(1,5),(6,2)]\<close>. If a value is updated, we
  should also check whether we can merge runs, e.g. 
  \<open>rl_upd [(3,2),(1,5),(3,2)] 3 2 = [(7,2)]\<close>

  WARNING: this is a rather difficult exercise with a lot of missing gaps, make sure that you don't
    sink too much time into this.\<close>


  fun rl_Cons' where
    "rl_Cons' (n,x) [] = (if n=0 then [] else [(n,x)])"
  | "rl_Cons' (n,x) ((n',x')#xs) = (if n=0 then (n',x')#xs else if x=x' then (n+n',x')#xs else (n,x)#(n',x')#xs)"
  
  lemma rl_Cons'_refine[simp]: "rl_\<alpha> (rl_Cons' (n,x) xs) = replicate n x @ rl_\<alpha> xs"
    apply (cases xs)
    apply (auto simp: replicate_add)
    done
    
  lemma rl_Cons'_invar[simp]: "rl_invar xs \<Longrightarrow> rl_invar (rl_Cons' (n,x) xs)"
    apply (cases xs rule: rl_invar.cases)
    by auto
    
  (* Now, updating a list is a generalization of standard list_update *)
    
      
  thm list_update.simps  
    
  fun rl_upd where
    "rl_upd [] i x = []"
  | "rl_upd ((n,y)#ys) i x = (
      if i<n then rl_Cons' (i,y) (rl_Cons x (rl_Cons' (n-i-1,y) ys))
      else rl_Cons' (n,y) (rl_upd ys (i-n) x)
     )"  
  (* aux lemma *)   
  lemma replicate_upd: "i<n \<Longrightarrow> (replicate n x)[i:=x'] = (replicate i x)@x'#replicate (n-i-1) x"  
    apply (induction n arbitrary: i)
    by (auto split: nat.split)
    
  lemma rl_upd_refine[simp]: "rl_\<alpha> (rl_upd xs i y) = ((rl_\<alpha> xs)[i:=y])"
    apply (induction xs arbitrary: i) 
    apply (auto simp: list_update_append replicate_upd)
    done

  (* Aux-lemma: not a good simp-rule, though. *)  
  lemma rl_invar_ConsD: "rl_invar (x#xs) \<Longrightarrow> rl_invar xs"  
    apply (cases x; cases xs)
    by auto

  lemma rl_upd_invar[simp]: "\<lbrakk>rl_invar xs\<rbrakk> \<Longrightarrow> rl_invar (rl_upd xs i y)"  
    apply (induction xs arbitrary: i) 
    (* Possibility 1: use dest: *)
    by (auto dest: rl_invar_ConsD)
            
  lemma "\<lbrakk>rl_invar xs\<rbrakk> \<Longrightarrow> rl_invar (rl_upd xs i y)"  
    apply (induction xs arbitrary: i) 
    subgoal by auto
    apply clarify (* split bound product type var, before it gets fixed *)
    subgoal for a b xs i 
      (* Possibility 2: fix variable, to avoid looping simplifier *)
      by (auto simp: rl_invar_ConsD[where xs=xs])
    done


end

