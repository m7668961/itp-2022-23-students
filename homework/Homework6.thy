chapter \<open>Homework 6\<close>
theory Homework6
imports Main
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #6
    RELEASED: Wed, Dec 21 2022
    DUE:      Wed, Jan 18, 2022, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)

section \<open>1: Be creative (80 points + bonus points for creative/elegant/ingenious solutions)\<close>

(*  
  Develop your own piece of interesting theory. 
  This can be correctness of an algorithm, a mathematical theorem, etc.
  
  Requirement: something must be proved, i.e., 
  just a functional program in Isabelle without correctness proof is not enough!
                
  Ideally, come up with your own idea. Do not copy from existing repositories, 
    we fill find out about this!
  
  You are welcome to send Peter or Bram your idea for discussion/feasibility check.
          
  DEADLINE: 18.01.2022 (you have two full (non-holiday) weeks). 
    But there will be another (small) homework on 11.01, with deadline 18.01, too.
    (We trust in your time-management capabilities ;) )
  
  If you need inspiration what to do, here are a few suggestions:
        
  E.g. some graph/automata problem:
    \<^item> Add outer loop (fold) to DFS, to explore states from all roots of non-connected graph
    \<^item> Topological sorting
    \<^item> Make: build recipe
    \<^item> Critical path in DAG
    \<^item> Shortest path from s to t (use BFS)
    
  Or some functional algorithms:
    \<^item> natural merge sort (e.g. implemented in Haskell \<^url>\<open>https://hackage.haskell.org/package/base-4.17.0.0/docs/src/Data.OldList.html#sortBy\<close>)
    \<^item> a (simple) algorithm to solve linear equations (e.g. Gauss)
    \<^item> prime number sieves
    \<^item> grammar (inductive) and parser (fun) for arithmetic expressions/Simple XML/\<dots>, including lexer!
    
  Additionally, try to think about some interesting mathematical/logic problem that you encountered
  in a hackathon (or other contest), internet video or during a lecture
    
    
  Please document your proofs well. Add as many comments as possible so we can
    understand what you are proving. Good documentation will affect the grading
    in a positive way.

  Hint: keep it simple! If you have an ambitious idea, first try to prove the minimal viable 
    product


  Grading:
    Your grades will be determined by a number of things:
    - Use of sledgehammer:
        We don't appreciate 'wild' proof attempts that are somehow rescued with sledgehammer, 
        and show that you didn't really understand how the proof works. 
        Still, we prefer a somehow completed proof over a "sorry".
        
        Using sledgehammer to bring forward a clear, targeted proof structure is fine, though.
    - Use of sorry: We think that "sorry" is a good way to test whether an auxiliary lemma can be
        used to prove your statement. If you run out of time, we prefer a finished proof that has
        a few sorries in it over a proof that never came to its most important statement. If you 
        sorry a proof, please give us a comment as to why you think that your lemma should hold. 
        If you do so, we will show more mercy in the grading. However, if you sorry a lemma that 
        is clearly False, we will be strict. So be careful!
        tldr: IF YOU USE SORRY, ADD COMMENTS SAYING WHY YOU THINK YOUR SORRIED LEMMA HOLDS
    - Use of comments: use plenty of comments to explain us your thought process and the structure 
        of your proof. Also add comments what your lemmas intuitively mean, where appropriate. 
        Ideally, your comments read like a story that will guide us through your proof.
    - Ingenuity/elegance: Clever and elegant proofs of non-trivial theorems will get you bonus points.
        If you have proven a lemma and want to make it more elegant because you had a clever insight, 
        please keep the old lemma (as well as the new lemma of course) and explain the differences 
        and the insight (you don't have to, but it increases the chances that we know how to 
        appreciate your proof).
    - Creativity: We really like it if you come up with your own idea or give your own twist to 
        one of our suggestions. Let your mind run free!

    We understand that the last three points are a bit fluffy. If you are not sure whether your
    proof meets our standards you can always ask us for feedback on your idea/proof We may also 
    give you some additional ideas that you can work on. Please be on time with this!

    Disclaimer: If you did not ask for our feedback in time, please don't complain afterwards.

    You can contact Peter (p.lammich@utwente.nl) and/or Bram (b.kohlen@utwente.nl) for any questions.


    For now, merry christmas, a happy new year and good luck proving your theorems!

*)

end

