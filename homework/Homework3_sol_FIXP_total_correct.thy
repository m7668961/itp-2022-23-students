theory Homework3_sol_FIXP_total_correct
imports Main
begin

(** Proving termination

  There are two definitions of correctness of a program:
  
    partial correctness: if the program terminates, then it does so with a correct result
    total correctness: the program terminates with a correct result
  
    Note that, for partial correctness, the program might also loop forever, and is still 'correct'.
    
  In Ex1, we only asked for partial correctness, i.e., you could assume that the program terminates:
    fixedp euclid_step (a\<^sub>0,b\<^sub>0) (a,b)
    
      
*)  


definition fixedp :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "fixedp f s\<^sub>0 s' \<equiv> \<exists>n. s' = (f^^n) s\<^sub>0 \<and> f s' = s'"
  
lemma funpow_fix_add: "f ((f^^n) s) = (f^^n) s \<Longrightarrow> (f^^(n+n')) s = (f^^n) s"
  by (induction n') auto


lemma fixedp_determ: "fixedp f s\<^sub>0 s' \<Longrightarrow> fixedp f s\<^sub>0 s'' \<Longrightarrow> s'=s''"
  unfolding fixedp_def
  apply clarify
  subgoal for n\<^sub>1 n\<^sub>2
    apply (cases "n\<^sub>1\<le>n\<^sub>2") (* Case distinction: which number is smaller? *)
    subgoal
      using funpow_fix_add[of (*<*)f "n\<^sub>1" s\<^sub>0 "n\<^sub>2-n\<^sub>1"(*>*)] 
      by simp
    subgoal
      using funpow_fix_add[of (*<*)f "n\<^sub>2" s\<^sub>0 "n\<^sub>1-n\<^sub>2"(*>*)] 
      by simp
    done
  done
  
lemma funpow_invar_rule: "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s) \<rbrakk> \<Longrightarrow> I ((f^^n) s)"  
  by (induction n) auto

     
lemma fixedp_invar_rule:
  "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s); fixedp f s s' \<rbrakk> \<Longrightarrow> I s'"  
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done

lemma fixedp_invar_ruleE:
  "\<lbrakk> fixedp f s s'; I s; \<And>s. I s \<Longrightarrow> I (f s); I s' \<and> f s' = s' \<Longrightarrow> Q\<rbrakk> \<Longrightarrow> Q"  
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done


(** To more aux-lemmas of useful fixedp properties *)  
lemma fixedp_step: "fixedp f (f s) s' = fixedp f s s'"
  unfolding fixedp_def
  apply rule
  subgoal by (metis comp_apply funpow.simps(2) funpow_swap1)
  subgoal by (metis funpow_swap1)
  done
  
lemma fixedp_fixedp: "fixedp f s s' \<Longrightarrow> f s' = s'"
  unfolding fixedp_def
  by simp
  
(** Total correctness is proved by showing that something decreases, 
  similar to termination arguments for recursive functions.
  
  While the most general rules will use a well-founded relation, we show a rule here
  that uses a measure-function, i.e., a function state\<Rightarrow>nat, which must decrease 
  in every step.
*)  
  
lemma fixedp_invar_rule_total: 
  fixes s\<^sub>0::'s and v :: "'s \<Rightarrow> nat"
  assumes I0: "I s\<^sub>0" (** Invar holds initially *)
  assumes STEP: "\<And>s. \<lbrakk>I s; f s \<noteq> s\<rbrakk> \<Longrightarrow> I (f s) \<and> v (f s) < v s"
    (** If fixp not yet reached, invar holds in next step, and measure decreases *)
    
  shows "\<exists>s'. fixedp f s\<^sub>0 s' \<and> I s'"
    (** Then, the iteration terminates in some state s', for which the invar holds *)
  using I0
  (** The relevant induction rule is less_induct  *)
  thm less_induct
proof (induction "v s\<^sub>0" arbitrary: s\<^sub>0 rule: less_induct)
  case (less s)
  
  show ?case 
  proof cases
    assume "f s = s" \<comment> \<open>fixed point reached\<close>
    hence "fixedp f s s" by (metis fixedp_def funpow_0)
    with \<open>I s\<close> show ?thesis by blast
  next
    assume "f s \<noteq> s" \<comment> \<open>fixed point not yet reached\<close>
    with \<open>I s\<close> STEP have "I (f s)" "v (f s) < v s" by auto \<comment> \<open>STEP tells us that invar still holds, and measure gets smaller\<close>
    with less.hyps[of "f s"] obtain s' where "fixedp f (f s) s' \<and> I s'" by blast \<comment> \<open>IH applicable for smaller measures\<close>
    also have "fixedp f (f s) s' = fixedp f s s'" using fixedp_step . \<comment> \<open>Prepend first step\<close>
    finally show ?thesis ..
  qed
qed
  
lemma fixedp_invar_rule_totalE: 
  fixes s\<^sub>0::'s and v :: "'s \<Rightarrow> nat"
  assumes I0: "I s\<^sub>0"
  assumes STEP: "\<And>s. \<lbrakk>I s; f s \<noteq> s\<rbrakk> \<Longrightarrow> I (f s) \<and> v (f s) < v s"
  assumes FINAL: "\<And>s'. \<lbrakk>fixedp f s\<^sub>0 s'; I s'; f s' = s'\<rbrakk> \<Longrightarrow> Q"
  shows "Q"
  using fixedp_invar_rule_total[where I=I and v=v,OF assms(1,2)] assms(3) fixedp_fixedp[of f] 
  (** Note: we instantiate all the schematic variables that are in predicate position (?I), or
    on LHS of equation ?f ?s' = ?s'. If we don't, Isabelle's default unification heuristics gets confused.
  *)
  by blast
  
  
  
  
      
definition euclid_step :: "nat \<times> nat \<Rightarrow> nat \<times> nat" where
  "euclid_step \<equiv> \<lambda>(a,b). if a<b then (a,b-a) else if b<a then (a-b,b) else (a,b)"

definition euclid_invar :: "nat \<Rightarrow> nat \<Rightarrow> nat\<times>nat \<Rightarrow> bool" where 
  "euclid_invar a\<^sub>0 b\<^sub>0 \<equiv> \<lambda>(a,b). gcd a b = gcd a\<^sub>0 b\<^sub>0 \<and> a\<noteq>0 \<and> b\<noteq>0"

lemma euclid_invar_initial: "\<lbrakk>0 < a\<^sub>0; 0 < b\<^sub>0\<rbrakk> \<Longrightarrow> euclid_invar a\<^sub>0 b\<^sub>0 (a\<^sub>0, b\<^sub>0)"  
  by (auto simp: euclid_invar_def)
  
  
lemma euclid_invar_pres: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_invar a\<^sub>0 b\<^sub>0 (euclid_step (a,b))"
  apply (auto simp: euclid_invar_def euclid_step_def)
  subgoal
    using gcd_diff1_nat less_or_eq_imp_le by presburger
  subgoal
    by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
  done

lemma euclid_step_decr: "\<lbrakk>euclid_invar a\<^sub>0 b\<^sub>0 (a,b); euclid_step (a,b) = (a',b'); (a',b')\<noteq>(a,b)\<rbrakk> 
  \<Longrightarrow> a'+b' < a+b"  
  by (auto simp: euclid_invar_def euclid_step_def split: if_splits)
  

lemma euclid_invar_final: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_step (a,b) = (a,b) \<Longrightarrow> a=b \<and> b=gcd a\<^sub>0 b\<^sub>0"
  (*<*)
  apply (auto simp: euclid_invar_def euclid_step_def split: if_splits)
  done
  (*>*)

lemma euclid_nat_total_correct: "\<lbrakk>0 < a\<^sub>0; 0 < b\<^sub>0\<rbrakk> \<Longrightarrow> fixedp euclid_step (a\<^sub>0, b\<^sub>0) (gcd a\<^sub>0 b\<^sub>0, gcd a\<^sub>0 b\<^sub>0)"
  apply (rule fixedp_invar_rule_totalE[where 
    I="euclid_invar a\<^sub>0 b\<^sub>0"
    and f=euclid_step
    and v="\<lambda>(a,b). a+b" \<comment> \<open>The sum of the values decreases in every step\<close>
    and s\<^sub>0="(a\<^sub>0,b\<^sub>0)"])
  subgoal by (auto simp: euclid_invar_initial)
  subgoal for s by (cases s) (auto simp: euclid_invar_pres euclid_step_decr split: prod.split)
  subgoal for s by (cases s) (auto dest: euclid_invar_final)
  done  



end
