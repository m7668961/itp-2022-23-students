  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #7
    RELEASED: Wed, Jan 12, 2023
    DUE:      Wed, Jan 19, 2023, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)


  (*******************
    FINAL EXAM: EXAMPLE EXAM.
    
    This is an example exam, similar to what you might expect in the main exam.
    As homework, each question gives you 10 points.
    
  
    Theories will work in Isabelle2022
    
    Note, you can use the sidekick-panel to get a quick overview of the contents
    of this exam.
      
    You have three hours to work on this exam. 
    
    You solve this exam on your own machines. Before you start working on the exam,
    please make sure your internet connection is disabled (we will check that!)

    When the time is up, we will announce that you can switch on your internet again, and
    send the exam by email to p.lammich@utwente.nl, using [ITP-EXAM] in the subject.
    
    Afterwards, please wait until I have confirmed that I have received your solution!

    If you want to submit earlier, raise your hand and ask for early submission
      

    
    
    
  *******************)


theory Homework7sol
imports 
  Main 
  "HOL-Library.While_Combinator" 
  "../Demos/While_Lib"
  "HOL-Library.Monad_Syntax"
begin



























(**

  SWITCH OFF YOUR INTERNET CONNECTION NOW!


**)




















section \<open>Q1: Constant-time append lists (10 Points)\<close>

  (* We can implement lists as the empty list, 
     a singleton list, or the concatenation of two lists:  
  *)
    
  datatype 'a clist = CNIL | CSNG 'a | CAPPEND "'a clist" "'a clist" 

  (* Note that this representation allows a constant-time append operation *)
  
  subsection \<open>a) Definition (easy) (2p)\<close>

  (*
    Define the abstraction function from clists to lists:
  *) 
  fun cl_\<alpha> :: "'a clist \<Rightarrow> 'a list" where
  (*<*)
    "cl_\<alpha> CNIL = []"
  | "cl_\<alpha> (CSNG x) = [x]"
  | "cl_\<alpha> (CAPPEND xs ys) = cl_\<alpha> xs @ cl_\<alpha> ys"
  (*>*)

  (* Examples to check you definition *)
  
  value "cl_\<alpha> (CAPPEND CNIL (CAPPEND (CSNG 1) (CSNG (2::nat)))) = [1,2]"
  value "cl_\<alpha> (CAPPEND (CSNG 3) (CAPPEND (CSNG 1) (CSNG (2::nat)))) = [3,1,2]"
  
  subsection \<open>b) Fold over clists (medium) (3p)\<close>  

  (* Define a fold function over clist, and show that it is correct
  
    (Use recursion over the structure of clist, definitions that fall back to
      cl_\<alpha>, e.g. \<open>clfold f xs s = fold f (cl_\<alpha> xs) s\<close> do not count!
    )
  
  *)
  
  fun clfold :: "('a \<Rightarrow> 's \<Rightarrow> 's) \<Rightarrow> 'a clist \<Rightarrow> 's \<Rightarrow> 's" where
  (*<*)  
    "clfold f CNIL s = s"
  | "clfold f (CSNG x) s = f x s"  
  | "clfold f (CAPPEND xs ys) s = clfold f ys (clfold f xs s)"
  (*>*)
  
  lemma clfold_correct: "clfold f xs s = fold f (cl_\<alpha> xs) s"  
  (*<*)
    apply (induction xs arbitrary: s)
    by auto
  (*>*)

  subsection \<open>c) Summing clists (easy+) (2p)\<close>
  
  (* Show that we can use clfold to sum up lists *)
    
  lemma "clfold (+) xs 0 = sum_list (cl_\<alpha> xs)" for xs :: "int clist" 
    apply (simp add: clfold_correct)
    by (simp add: fold_plus_sum_list_rev)
    
  lemma "clfold (+) xs 0 = sum_list (cl_\<alpha> xs)" for xs :: "int clist"
  proof -
    have "clfold (+) xs acc = acc+sum_list (cl_\<alpha> xs)" for acc
      apply (induction xs arbitrary: acc)
      by auto
    thus ?thesis by simp
  qed  
    
  subsection \<open>d) Normal-form (medium) (3p)\<close>

  (*
    The only reason to use CNIL is to represent an empty list. Using CNIL in an append setting will 
    not change the contents of the list. E.g.
  *)

  value "cl_\<alpha> (CAPPEND CNIL (CAPPEND (CSNG 1) (CSNG (2::nat)))) = cl_\<alpha> (CAPPEND (CSNG 1) (CSNG (2::nat)))"

  (*
    We now implement a function \<open>is_normal xs\<close> that checks whether xs is in this normal form.
    I.e. \<open>is_normal xs\<close> holds when \<open>xs = CNIL\<close> or \<open>xs = CSNG x\<close> or when \<open>xs = CAPPEND x1 x2\<close> where
    x1 and x2 don't contain any occurrence of CNIL. We also define a function that converts xs into
    a normalized function and show that this indeed works correctly (i.e. \<open>is_normal (normalize xs)\<close>
    and \<open>cl_\<alpha> (normalize xs) = cl_\<alpha> xs\<close>
  *)
  
  fun is_normal where
    "is_normal CNIL \<longleftrightarrow> True"
  | "is_normal (CSNG _) \<longleftrightarrow> True"  
  | "is_normal (CAPPEND CNIL _) \<longleftrightarrow> False"
  | "is_normal (CAPPEND _ CNIL) \<longleftrightarrow> False"
  | "is_normal (CAPPEND xs ys) \<longleftrightarrow> is_normal xs \<and> is_normal ys"
  
  fun smart_append where
    "smart_append CNIL xs = xs"
  | "smart_append xs CNIL = xs"  
  | "smart_append xs ys = CAPPEND xs ys"
  
  lemma [simp]: "cl_\<alpha> (smart_append xs ys) = cl_\<alpha> xs @ cl_\<alpha> ys"
    by (cases xs; cases ys) auto

  lemma [simp]: "is_normal xs \<Longrightarrow> is_normal ys \<Longrightarrow> is_normal (smart_append xs ys)"  
    by (cases xs; cases ys) auto
      
  fun normalize where
    "normalize CNIL = CNIL"
  | "normalize (CSNG x) = CSNG x"  
  | "normalize (CAPPEND xs ys) = smart_append (normalize xs) (normalize ys)"

  lemma "cl_\<alpha> (normalize xs) = cl_\<alpha> xs" by (induction xs) auto
  lemma "is_normal (normalize xs)" by (induction xs) auto
  
      
    
section \<open>Q2: Simple Balanced Words (10 Point)\<close>    
  (* Old question from 2018 exam *)

  (*
    We consider 'simple' words, a restricted form of balanced words, namely those 
    of the form (\<dots>()\<dots>), or, equivalently L\<^sup>nR\<^sup>n.
  *)    
  datatype A = L | R
  type_synonym word = "A list"

  (*
    These are created by the Empty and Surround rule. There is no Concatenate rule.
  *)    
  inductive simple :: "word \<Rightarrow> bool" where
    E: "simple []"    
  | S: "simple w \<Longrightarrow> simple (L#w@[R])"

  subsection \<open>a) Even Length (easy) (3p)\<close>
  (*
    Show: Every simple word has even length
    
    (easy)
  *)    
  lemma "simple w \<Longrightarrow> even (length w)"
    by(induction w rule: simple.induct) auto

  subsection \<open>b) dup function (easy) (3p)\<close>  
  (*
    Define a recursive function \<open>dup n x\<close> to create a list of n \<open>x\<close>s, i.e.,
    [x,x,x,\<dots>x] (n times)

    Do not use functions from the Isabelle library in your definition.
        
    (easy)
  *)  
  fun dup :: "nat \<Rightarrow> 'a \<Rightarrow> 'a list" where
    "dup 0 _ = []"
  | "dup (Suc n) x = x # dup n x"
    
  subsection \<open>c) Simple words have form \<open>L\<^sup>nR\<^sup>n\<close> (medium+) (4p)\<close>
  (*
    Show that every simple word can actually be expressed as "dup n L @ dup n R"
    
    Hint: You might want to use an Isar proof, to instantiate the \<exists> in a controlled way!
    Hint: You might want to prove an auxiliary lemma of the form
          \<open>dup n x @ [x] = \<dots>\<close> 
    
    NO SLEDGEHAMMER ALLOWED! I.e., you must not use metis or smt methods for this proof!
    
    (medium+)
  *)  
  lemma dup_append_x: "dup n x @ [x] = dup (Suc n) x"
    apply(induction n)
    apply auto
    done

  lemma "simple w \<Longrightarrow> \<exists>n. w = dup n L @ dup n R"
  proof(induction rule: simple.induct)
    case E
    have "[] = dup 0 L @ dup 0 R" by auto
    then show ?case by blast
  next
    case (S w)
    then obtain n where "w = dup n L @ dup n R" by blast
    moreover have "L # dup n L = dup (Suc n) L" by simp
    moreover have "dup n R @ [R] = dup (Suc n) R" using dup_append_x by fast
    ultimately have "L # w @ [R] = dup (Suc n) L @ dup (Suc n) R" by auto
    then show ?case by blast
  qed


section \<open>Q3: Finding an Invariant and Termination (10 Points)\<close>  
  (*
    Modified question from 2018 exam. Arrays where used a lot in this lecture,
    so the reasoning about indexing into lists was easier in this context than
    a more functional implementation using take, drop, and fold.
    
    In the actual exam, you'd expect a while-loop + ADT here!
  *)

  (*
    The following program sums up the elements of the range l..<h in list a, 
    by iterating from h down to l, and indexing into the list.
    
    It implements the following 'imperative' program:

    r=0
    i=h
    
    while (i>l) {
      i= i - 1
      r= r + xs[i]
    }
        
  *)  
  
  definition "list_sum l h xs \<equiv> while_option 
    (\<lambda>(i,r). i>l)
    (\<lambda>(i,r). let i=i-1 in (i,r + xs!i))
    (h,0)
    "
  
  subsection \<open>a) Invariant (medium) (6p)\<close>    
  (*
    We want to prove that this program actually sums up the given interval:
  *)  
    
  (*
    Note that "sum (nth xs) {l..<h}", which can also be written as "\<Sum>j=l..<h. xs!j",
    sums up the elements of the list xs in range l..<h, i.e., l INCLUSIVE and h EXCLUSIVE.
  *)  
  term "sum (nth xs) {l..<h}" term "\<Sum>j=l..<h. xs!j"
  
  (* It works for lists with elements 
    that form a commutative monoid, in particular, for integers! *)
  term "\<Sum>j=l..<h. (xs :: int list)!j"
    
  definition list_sum_invar :: "int list \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat \<times> int \<Rightarrow> bool" 
    where "list_sum_invar xs l h \<equiv> \<lambda>(i,r). i\<in>{l..h} \<and> r = (\<Sum>j=i..<h. xs!j)"
    
  lemma sum_split:
    fixes f :: "nat \<Rightarrow> int"
    assumes "i>0" "i\<le>h" 
    shows "(\<Sum>j=i-Suc 0..<h. f j) = f (i-Suc 0) + (\<Sum>j=i..<h. f j)"  
  proof -
    have "{i - Suc 0..<h} = insert (i - Suc 0) {i..<h}"
      using assms apply (cases i) by auto
    thus ?thesis using \<open>i>0\<close> 
      by fastforce
  qed  
    
  (*
    Hint: use quickcheck on the VCxx-lemmas to identify a wrong invariant early
  *)
  
  (* Invariant preserved by loop body *)  
  lemma VC1: "\<lbrakk>l < i; list_sum_invar a l h (i,r)\<rbrakk> \<Longrightarrow> list_sum_invar a l h (i - Suc 0, r + a!(i - Suc 0))"
    (* Hint: The lemma sum_split (see above) may be useful here *)
    unfolding list_sum_invar_def
    by (auto simp: sum_split)
    
  (* Invariant implies postcondition: I \<and> \<not>b \<Longrightarrow> Q *)  
  lemma VC2: "\<lbrakk>\<not> l < i; list_sum_invar xs l h (i,r)\<rbrakk> \<Longrightarrow> r = (\<Sum>j=l..<h. xs!j)"
    unfolding list_sum_invar_def
    by auto
    
  (* Invariant holds initially: P \<Longrightarrow> I *)  
  lemma VC3: "l \<le> h \<Longrightarrow> list_sum_invar xs l h (h,0)"
    unfolding list_sum_invar_def
    by auto
    
  term list_sum  

  definition \<open>termination_measure \<equiv> \<lambda>(i,r). i\<close>  
      
  lemma fact'_correct: "l\<le>h \<Longrightarrow> \<exists>l'. list_sum l h xs = Some (l',\<Sum>j=l..<h. xs!j)" for xs :: "int list"
    unfolding list_sum_def  
    apply (rule while_option_rule'[where P="list_sum_invar xs l h" and r="measure termination_measure"])
    apply (simp_all add: VC1 VC2 VC3 Let_def split: prod.splits)
    
  subsection \<open>b) Termination (easy) (4p)\<close>
    
    apply -
  
    (* Implement termination_measure by finding a meaningful measure function, 
        and complete the proof (if you haven't done so already).
    
      Note: You can also solve this if you did not manage to find a working invariant:
        For this loop pattern, there is a measure expression that works without
        a loop invariant, and will be proved automatically once you get it right!

      (easy)
    
    *)
    
    apply (auto simp: termination_measure_def)
    
    done
  
  
section \<open>Q4: Monads ;) (10p) \<close>  

  (*
    The writer monad is a monad with an additional "log" operator. This operator can be used to
    log certain actions or other data of interest. It's implemented as a value and a string. The
    log is updated by appending a new line to the string.
  
    Return carries over a value and an empty log. Bind applies the value component of the monad to
    a given function and then appends the log that results from the function to the log of the input
    monad. Additionally, the log function returns the unit value and a specified log message.
  *)
  
  type_synonym 'a wM = "'a \<times> string"
  
  definition return :: "'a \<Rightarrow> 'a wM" where "return x = (x,[])"
  
  definition log :: "string \<Rightarrow> unit wM" where "log msg \<equiv> ((),msg)"
  
  definition bind_w :: "'a wM \<Rightarrow> ('a \<Rightarrow> 'b wM) \<Rightarrow> 'b wM" 
    where "bind_w m f = (let (x\<^sub>1,msg\<^sub>1) = m; (x\<^sub>2,msg\<^sub>2) = f x\<^sub>1 in (x\<^sub>2, msg\<^sub>1 @ msg\<^sub>2))"
  
  (*
    Adhoc_overloading allows us to use the monad syntax (do {..}, \<leftarrow>).
  *)
  
  adhoc_overloading bind bind_w  
  
  (*
    Next up, we prove the monadic laws, define the weakest precondition and define some basic lemmas 
    that we need for proofs.
  *)
  
  context
    notes [simp] = return_def log_def bind_w_def
  begin
    lemma "bind_w (return x) f = f x" by auto
  
    lemma "bind_w m (return) = m" by auto
    
    lemma "bind_w (bind_w m f) g = bind_w m (\<lambda>x. bind_w (f x) g)"
      by (auto split: prod.splits)
      
  end
  
  (** log messages are ignored for the correctness proof (they're only debug output anyway) *)
  definition wp :: "'a wM \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where "wp m Q \<equiv> Q (fst m)"
  
  subsection \<open>a) Weakest precondition simplifications (easy) (3p)\<close>

  context
    notes [simp] = return_def log_def bind_w_def wp_def
  begin
  
    lemma wp_return: "wp (return x) Q = Q x" by auto
  
    lemma wp_bind: "wp (bind m f) Q = wp m (\<lambda>x\<^sub>1. wp (f x\<^sub>1) Q)" by (auto split: prod.splits)
      
    lemma wp_log: "wp (log msg) Q = Q ()" by auto
    
  end
  
  subsection \<open>b) Logfilter (medium) (5p)\<close>
    
  (*
    Using the monad, we define a filter that logs every element that was filtered out of the 
    original list. Below the definition we provided an example of what the output should look like.
    If the sanity check passes, you can prove the correctness of your logfilter.
  *)
  
  fun logfilter :: "(string \<Rightarrow> bool) \<Rightarrow> string list \<Rightarrow> string list wM" where
    "logfilter P [] = return []"
  | "logfilter P (x#xs) = (
      if P x then do {
        xs'\<leftarrow>logfilter P xs;
        return (x#xs')
      } else do {
        log (''Filtering out: '' @ x @ '';'');
        xs'\<leftarrow>logfilter P xs;
        return xs'
      })"  
  
  value "logfilter (\<lambda>s. length s < 5) [''foo'',''bar-foo'',''blob'',''too long'']"
  (** Should produce something like: 
    (exact format of log messages not important, as long as they contain the information 
      which elements have been filtered out!)
  
    ([''foo'', ''blob''], ''Filtering out: bar-foo; Filtering out: too long;'')
  *)

  subsection \<open>c) Correctness of Logfilter (medium) (2p)\<close>
  (*
    If you did exercises 4a and 4b correctly, this may be easy. If te proof won't work, you maybe
    have made some mistakes there.
  *)
  
  lemma "wp (logfilter P xs) (\<lambda>xs'. xs' = filter P xs)"
    apply (induction xs)
    apply (auto simp: wp_return wp_bind wp_log)
    done

  
section \<open>Q5: Push/Pop (5 Bonus Points)\<close>
  (* Original question from 2018 exam. 
    This was the 'hard question' at the end, 
    to let the top 10% show off their skills.
  *)

  (* The following is an abstraction of a stack machine.
    Only the height of the stack is considered, the operations 
    are push and pop.
  *)  
  datatype instr = PUSH | POP  

  (*
    The following function computes the new height after executing a list of instructions,
    or returns None on stack underflow.
  *)    
  fun height :: "instr list \<Rightarrow> nat \<Rightarrow> nat option" where
    "height [] n = Some n" 
  | "height (PUSH#xs) n = height xs (Suc n)" 
  | "height (POP#xs) (Suc n) = height xs n" 
  | "height _ _ = None"

  subsection \<open>a) Same-Level Sequences (medium+) (2p)\<close>
  (*
    A same-level sequence is an instruction sequence that can run from an empty stack, 
    and yields an empty stack again.
    
    Show that any same-level sequence, when started in height n, 
      will yield height n again.
    
    (medium+)
  *)

  lemma lift_height: "height is n = Some m \<Longrightarrow> height is (n+b) = Some (m+b)"  
    by (induction "is" n rule: height.induct) auto

  lemma lift_sl: "height is 0 = Some 0 \<Longrightarrow> height is n = Some n"  
    by (auto dest: lift_height)


  
  subsection \<open>b) Elimination \<open>PUSH@sl#POP\<close>  (hard) (3p)\<close>
  (*
    Prove that eliminating a subsequence of the form \<open>PUSH sl POP\<close>,
    where sl is a same-level path, does not change the semantics of 
    the instruction sequence.
    
    Hints:
      Start your proof with a case split on successful execution and failing execution.
      You might want to develop some auxiliary lemmas about concatenating/splitting of executions.
        
    (hard)
  *)

  lemma height_append_conv: 
    "height (is1@is2) n1 = Some n3 \<longleftrightarrow> (\<exists>n2. height is1 n1 = Some n2 \<and> height is2 n2 = Some n3)"
    by (induction is1 n1 rule: height.induct) auto
  lemma height_POP_conv: "height (POP # xs) n = Some m \<longleftrightarrow> (\<exists>n'. n=Suc n' \<and> height xs n' = Some m)"  
    by (cases n) (auto)
  (*>*)
        
  lemma height_Some: "height xs2 0 = Some 0 
    \<Longrightarrow> height (xs1@PUSH#xs2@POP#xs3) m = Some n \<longleftrightarrow> height (xs1@xs3) m = Some n"  
    apply (auto simp: height_append_conv height_POP_conv dest: lift_sl)
    by (simp add: lift_sl)  

  (*<*)
  lemma height_append_None_conv: 
    "height (xs1@xs2) m = None \<longleftrightarrow> (height xs1 m = None \<or> (\<exists>n. height xs1 m = Some n \<and> height xs2 n = None))"
    by (induction xs1 m rule: height.induct) auto

  lemma height_POP_None_conv: "height (POP # xs) n = None \<longleftrightarrow> n=0 \<or> (\<exists>n'. n=Suc n' \<and> height xs n' = None)"  
    by (cases n) (auto)
  (*>*)

  (* Prove the same theorem for failing executions! *)
  lemma height_None: "height xs2 0 = Some 0 
    \<Longrightarrow> height (xs1@PUSH#xs2@POP#xs3) m = None \<longleftrightarrow> height (xs1@xs3) m = None"  
    apply (auto simp: height_append_None_conv height_POP_None_conv dest: lift_sl)
    apply (simp add: lift_sl)
    apply (simp add: lift_sl)
    apply (simp add: lift_sl)
    done


  lemma "height xs2 0 = Some 0 
    \<Longrightarrow> height (xs1@PUSH#xs2@POP#xs3) m = res \<longleftrightarrow> height (xs1@xs3) m = res"
    apply(cases res)
    apply (simp add: height_None)
    by(auto simp add: height_Some)




  
end
