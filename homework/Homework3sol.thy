theory Homework3sol
  imports Main
begin

text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #3
  RELEASED: Wed, Nov 30 2022
  DUE:      Wed, Dec 7 2022, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
\<close>

section \<open>General Information\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...


  This exercise will focus mainly on the definition of datatypes.
  We will define some custom datatypes and show that correctness by
  proving invariant preservations. For this, we use their abstract representation.
\<close>

section \<open>Ex1: Fixpoints (27 Points)\<close>

text\<open>
    Assume we model one step of a program by a function \<open>f :: 's \<Rightarrow> 's\<close>, 
    such that for state s, f s gives you the next state. 
   
    At this point, it is not important how exactly the state (or the function f) looks like.
    
    We now start with an initial state \<open>s\<^sub>0\<close>, and apply steps until nothing changes any more.
  
   \<open>s\<^sub>0 \<rightarrow> s\<^sub>1 \<rightarrow> s\<^sub>2 \<rightarrow> s\<^sub>3 \<rightarrow> \<dots> \<rightarrow> s\<^sub>n, such that 
      \<^item> s\<^sub>i\<^sub>+\<^sub>1 = f s\<^sub>i    (the s\<^sub>i are a sequence of steps)
      \<^item> and f s\<^sub>n = s\<^sub>n  (nothing changes any more after reaching s\<^sub>n)\<close>

    We call any x with f x = x a fixed point of f (it is fixed under application of f).

    Thus, we want to repeatedly apply f to \<open>s\<^sub>0\<close> until we reach a fixed point.
        
    The following predicate states that s' is a fixed point that can be reached in this way:
  \<close>
  
definition fixedp :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "fixedp f s\<^sub>0 s' \<equiv> \<exists>n. s' = (f^^n) s\<^sub>0 \<and> f s' = s'"
  
text \<open> Intuitively: there is an n, such that applying f n times to \<open>s\<^sub>0\<close> reaches s' and s' is a fixed 
  point. Obviously, if we continue iteration beyond the fixed point, nothing changes any more 
  recall, we have reached a fixed point!). Show that iterating any number of times beyond the fixed
  point does not change the outcome.\<close>


subsection \<open>a: Show that iterating beyond the fixpoint does not change the outcome. (3p)\<close>

  
lemma funpow_fix_add: "f ((f^^n) s) = (f^^n) s \<Longrightarrow> (f^^(n+n')) s = (f^^n) s"
  (*<*)
  by (induction n') auto
  (*>*)

text \<open> That means, that we can reach (at most) one fixed point from the same start state \<open>s\<^sub>0\<close>. \<close>

subsection \<open>b: Show that at most one fixpoint exists. (3p)\<close>

text \<open>HINT: This proof is a bit more involved. We have already filled in some gaps, but you need
  to instantiate the variables in some theorems to make the proof work.\<close>

lemma fixedp_determ: "fixedp f s\<^sub>0 s' \<Longrightarrow> fixedp f s\<^sub>0 s'' \<Longrightarrow> s'=s''"
  (* While sledgehammer might find proofs here, we ask you to adhere to the following proof scheme: *)
  (* Unfold definition and clarify goal *)
  unfolding fixedp_def
  apply clarify
  (* Name the number of iterations *)
  subgoal for n\<^sub>1 n\<^sub>2
    apply (cases "n\<^sub>1\<le>n\<^sub>2") (* Case distinction: which number is smaller? *)
    subgoal
      (* Find a suitable instantiation of funpow_fix_add *)
      using funpow_fix_add[of (*<*)f "n\<^sub>1" s\<^sub>0 "n\<^sub>2-n\<^sub>1"(*>*)] 
      by simp
    subgoal
      (* Find a suitable instantiation of funpow_fix_add *)
      using funpow_fix_add[of (*<*)f "n\<^sub>2" s\<^sub>0 "n\<^sub>1-n\<^sub>2"(*>*)] 
      by simp
    done
  done

text \<open>In order to prove properties of iterative algorithms, we can use the invariant method: 

  We find an invariant I, that holds for the initial state, and is preserved by steps.
  Then, by induction, we know that it holds in any state of the iteration:
\<close>

subsection \<open>c: Show invariant preservation. (2p)\<close>
    
lemma funpow_invar_rule: "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s) \<rbrakk> \<Longrightarrow> I ((f^^n) s)"  
  (*<*)
  by (induction n) auto
  (*>*)


subsection \<open>d: Show that the invariant holds for the fixed point (3p)\<close>
     
lemma fixedp_invar_rule:
  "\<lbrakk> I s; \<And>s. I s \<Longrightarrow> I (f s); fixedp f s s' \<rbrakk> \<Longrightarrow> I s'"  
  (*<*)
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done
  (*>*)

text \<open> Note that the following rule is easier to use in the following (equivalent) form.

  applied as an erule, this allows us to show invariant preservation, 
  and then derive our actual goal (Q) from the fact that the invariant holds 
  for s' and that s' is a fixed point.
\<close>

subsection \<open>e: Show the elimination rule (3p)\<close>


lemma fixedp_invar_ruleE:
  "\<lbrakk> fixedp f s s'; I s; \<And>s. I s \<Longrightarrow> I (f s); I s' \<and> f s' = s' \<Longrightarrow> Q\<rbrakk> \<Longrightarrow> Q"  
  (*<*)
  apply (auto simp: fixedp_def intro: funpow_invar_rule)
  done
  (*>*)

text \<open>
  A simple version of Euclid's algorithm \<^url>\<open>https://en.wikipedia.org/wiki/Euclidean_algorithm\<close> finds
  the greatest common divisor of two positive numbers by repeatedly replacing the greater number 
  by the difference of the two numbers. If both numbers are equal, they are equal to the gcd of 
  the original numbers.

  Thus, the state of the algorithm is a pair of numbers (a,b).
  When not equal, a step replaces the greater number by the difference. 
  When the numbers are already equal, a step does not change them (thus we will reach a fixed point).

  For example, given a state (10,8), the algorithm will go through the following steps:
  (10,8) \<rightarrow> (2,8) \<rightarrow> (2,6) \<rightarrow> (2,4) \<rightarrow> (2,2) \<rightarrow> (2,2) \<rightarrow> ....
  So the state (2,2) is the fixed point.
\<close>  

subsection \<open>f: Specify one step of Euclid's algorithm (2p)\<close>

text \<open>HINT: Don't specify the whole procedure, just one step, i.e. \<open>euclid_step (10,8) = (2,8)\<close>\<close>
      
definition euclid_step :: "nat \<times> nat \<Rightarrow> nat \<times> nat" where
(*<*)
  "euclid_step \<equiv> \<lambda>(a,b). if a<b then (a,b-a) else if b<a then (a-b,b) else (a,b)"
(*>*)

text \<open>
  A few tests: we just apply the step function a few hundred times, should be enough for small numbers!

  Before you continue, check that all of these yield True!\<close>

value "(euclid_step^^100) (20,10) = (10,10)" 
value "(euclid_step^^100) (7,9) = (1,1)"
value "(euclid_step^^100) (17,34) = (17,17)"
value "(euclid_step^^100) (128,256) = (128,128)"
value "(euclid_step^^200) (2*2*2*3*3*5*7*13,2*2*5*13) = (2*2*5*13,2*2*5*13)"


text \<open>
  Now that we have a fixpoint algorithm, we can use the proofs and definitions that we have readily
  available to show the correctness of the algorithm. The only thing we need for that is an 
  invariant.

  We specify it in text here:
  
  the gcd of the current pair is equal to the gcd of the original pair, and both values of 
  the current pair are positive.
\<close>

subsection \<open>g: Specify the invariant formally (2p)\<close>

definition euclid_invar :: "nat \<Rightarrow> nat \<Rightarrow> nat\<times>nat \<Rightarrow> bool" where 
  (*<*)
  "euclid_invar a\<^sub>0 b\<^sub>0 \<equiv> \<lambda>(a,b). gcd a b = gcd a\<^sub>0 b\<^sub>0 \<and> a\<noteq>0 \<and> b\<noteq>0"
  (*>*)

text \<open> To show that the invariant is correct, we must show that it is preserved over an iteration.\<close>

subsection \<open>h: Show that it is preserved by a step. (3p)\<close>

text \<open>HINT: (sledgehammer and find_theorems are your friends to discover the relevant lemmas about gcd! )\<close>

lemma euclid_invar_pres: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_invar a\<^sub>0 b\<^sub>0 (euclid_step (a,b))"
  (*<*)
  apply (auto simp: euclid_invar_def euclid_step_def)
  subgoal
    using gcd_diff1_nat less_or_eq_imp_le by presburger
  subgoal
    by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
  done
  (*>*)

text \<open>A fixed point implies that both elements of the pair are equal to the gcd of the original pair\<close>

subsection \<open>i: Show this (3p)\<close>

lemma euclid_invar_final: "euclid_invar a\<^sub>0 b\<^sub>0 (a,b) \<Longrightarrow> euclid_step (a,b) = (a,b) \<Longrightarrow> a=b \<and> b=gcd a\<^sub>0 b\<^sub>0"
  (*<*)
  apply (auto simp: euclid_invar_def euclid_step_def split: if_splits)
  done
  (*>*)
  

subsection \<open>j: Assemble everything (3p)\<close>

text \<open>We have already given you the skeleton for the proof. You just need to collect the relevant 
  lemmas. However, try to find our for yourself why we chose this structure, why we used erule and 
  try to reason about how the structure of lemma \<open>fixedp_invar_ruleE\<close> benefits us here.\<close>

lemma euclid_nat_correct: "fixedp euclid_step (a\<^sub>0,b\<^sub>0) (a,b) \<Longrightarrow> a\<^sub>0\<noteq>0 \<Longrightarrow> b\<^sub>0\<noteq>0 \<Longrightarrow> a=b \<and> b=gcd a\<^sub>0 b\<^sub>0"
  apply (erule fixedp_invar_ruleE[where I="euclid_invar a\<^sub>0 b\<^sub>0"]; clarify)
  subgoal (* Invar holds initially *)
    (*<*) by (auto simp: euclid_invar_def) (*>*)
  subgoal for a b (* Invar preserved by step *)
    (*<*) by (blast intro: euclid_invar_pres) (*>*)
  subgoal (* Invar at fixed point implies gcd *) 
    (*<*) by (blast dest: euclid_invar_final) (*>*)
  done  





section \<open>Ex2: Labelled Transition System (13 Points)\<close>

text \<open>A labelled transition system (LTS) is a transition system in which each edge is labelled with 
  a symbol. By following a path on the LTS, we can construct a word out of the symbols in the same 
  order in which we passed over them. For example, we may have an LTS like the one below.
  LTS \<delta>:
  \<open>
  S\<^sub>1 \<comment>c\<rightarrow> S\<^sub>2 \<comment>b \<rightarrow> S\<^sub>3
           |        |
           a        a
           \<down>        \<down>
           S\<^sub>4 \<comment>c\<rightarrow> S\<^sub>5
  \<close>
  In the tutorial, we have defined a word as follows:
\<close>

fun word :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> 'q \<Rightarrow> 'a list \<Rightarrow> 'q \<Rightarrow> bool" where
  "word \<delta> p [] q \<longleftrightarrow> p=q"
| "word \<delta> p (a#as) q \<longleftrightarrow> (\<exists>p'. (p,a,p')\<in>\<delta> \<and> word \<delta> p' as q)"  

text \<open>This means that \<open>word \<delta> S\<^sub>1 [c,b,a] S\<^sub>5\<close> evaluates to True for example.
  However, this method causes exponential behaviour, because our search branches whenever there are
  multiple outgoing edges with the same symbol from a state. We can counter this by adapting our
  word function. Insted of a list of symbols, we can input a list of tuples containing a symbol and
  the successor state (the one that we transition to). This is what we do in \<open>word_dir\<close>.
  For example \<open>word_dir \<delta> S\<^sub>1 ws S\<^sub>5\<close> evaluates to True for \<open>ws = [(c,S\<^sub>2),(b,S\<^sub>3),(a,S\<^sub>5)]\<close> or 
  \<open>ws = [(c,S\<^sub>2),(a,S\<^sub>4),(c,S\<^sub>5)]\<close> but not for any other word. Also not for 
  \<open>ws = [(c,S\<^sub>2),(b,S\<^sub>4),(a,S\<^sub>5)]\<close> which has a combination of symbols that we can find between our 
  start and end state but not via state \<open>S\<^sub>4\<close>\<close>

subsection \<open>a: Specify word_dir (2p)\<close>

fun word_dir where
  "word_dir \<delta> p [] q \<longleftrightarrow> p=q"
| "word_dir \<delta> p ((a,p')#as) q \<longleftrightarrow> ((p,a,p')\<in>\<delta> \<and> word_dir \<delta> p' as q)"  


text \<open>word_dir should do exactly the same as word with the exception that one needs to provide the 
  states. We can conclude the following: If \<open>word\<close> accepts a list of symbols, then there must be a
  sequence of states such that, if combined with this list of symbols, \<open>word_dir\<close> accepts it. This
  should also hold in the other direction. We use the function \<open>zip\<close>, which takes two lists and
  converts them into a list of tuples. We use this to combine a list of symbols with a list of 
  successor states.\<close>


subsection \<open>b: Show this relation between word and word_dir (3p)\<close>

text \<open>HINT: Take a look at the following theorem\<close>
thm length_Suc_conv 

lemma "word \<delta> p as q \<longleftrightarrow> (\<exists>qs. length qs = length as \<and> word_dir \<delta> p (zip as qs) q)"
  apply (induction as arbitrary: p)
  apply (auto simp: length_Suc_conv)
  apply force
  by blast


text \<open>Explicitly telling an LTS which states to visit is not a very flexible method. Usually, the 
  LTS is a black box for the word. We want to check if an LTS recognizes a word without attaching 
  any information about the LTS to the word. To eliminate branching, we can make the LTS 
  deterministic. This means that, in every state, every symbol occures at most once on all of its
  outgoing edges. We achieve this using the so-called powerset construction:
  \<^url>\<open>https://en.wikipedia.org/wiki/Powerset_construction\<close>

  It works as follows: Given an LTS \<delta>, we define its powerset construction as \<delta>d \<delta>. A state in \<delta>d \<delta> is
  a set of states of \<delta>. We have \<open>(ps,a,qs) \<in> \<delta>d \<delta>\<close> if and only if qs is a subset of states \<open>q \<in> qs\<close> 
  of \<delta> for which there exists a \<open>p \<in> ps\<close> with \<open>(p,a,q) \<in> \<delta>\<close>.
  For example given the following \<delta>:
  \<open>
  S\<^sub>1 \<comment>a\<rightarrow> S\<^sub>2 \<comment>b\<rightarrow> S\<^sub>3
  |        
  a        
  \<down>        
  S\<^sub>4 \<comment>c\<rightarrow> S\<^sub>5
  \<close>
  Then \<delta>d \<delta> looks as follows:
  \<open>
  {S\<^sub>1} \<comment>a\<rightarrow> {S\<^sub>2,S\<^sub>4} \<comment>b\<rightarrow> {S\<^sub>3}
               |        
               c        
               \<down> 
              {S\<^sub>5}
  \<close>
  \<close>

subsection \<open>c: Define \<open>\<delta>d\<close> according to the powerset construction using set comprehension. (3p)\<close>

text \<open>NOTE: It's okay if your determinization produces 'ghost edges'. In the example above, there
  is also the edge \<open>({S\<^sub>1,S\<^sub>2},a,{S\<^sub>2,S\<^sub>4})\<close>. This is caused by the fact that we don't construct the 
  state space exploratively, but through set comprehension. However, by only starting in singleton
  states (like \<open>S\<^sub>1\<close>) we can never reach those ghost transitions and states. So if your 
  implementation contains these, it's not wrong.

  Eternal fame will come to those who fix this problem and still manage to solve the proofs.\<close>

definition \<delta>d :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> ('q set \<times> 'a \<times> 'q set) set" where 
  "\<delta>d \<delta> \<equiv> { (ps,a,qs). qs = {q. \<exists>p\<in>ps. (p,a,q) \<in> \<delta>}}"


text \<open>We have seen a predicate that checks whether an LTS is deterministic in the tutorial. We
  can use it to check whether our implementation is correct.\<close>

definition det :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> bool" where 
  "det \<delta> \<equiv> \<forall>q a q1 q2. (q, a, q1) \<in> \<delta> \<and> (q, a, q2) \<in> \<delta> \<longrightarrow> q1=q2"

subsection \<open>d: Use this predicate as a sanity check (1p)\<close>

lemma "det (\<delta>d \<delta>)"
  unfolding det_def \<delta>d_def
  apply auto
  done

text \<open>Given that we start from a singleton state, our determinization should recognize the same 
  words as the original non-deterministic automaton. (We have to start in a singleton state due
  to the ghost state problem described earlier). Show that this indeed holds.\<close>

subsection \<open>e: Show that determinization recognizes the same words. (4p)\<close>

text \<open>HINT: You may want to break the problem down into multiple subproblems.\<close>


lemma aux2: "(word (\<delta>d \<delta>) ps as qs) \<Longrightarrow> (\<forall>q\<in>qs. \<exists>p\<in>ps. word \<delta> p as q)"
  apply (induction as arbitrary: ps)
  apply (auto simp: \<delta>d_def)
  by force
  
lemma aux1: "word \<delta> p as q \<Longrightarrow> (\<forall>ps. p\<in>ps \<longrightarrow> (\<exists>qs. q\<in>qs \<and> word (\<delta>d \<delta>) ps as qs))"
  apply (induction as arbitrary: p)
  apply (auto simp: \<delta>d_def)
  by (metis (mono_tags, lifting) mem_Collect_eq)
  

lemma "word \<delta> p as q \<longleftrightarrow> (\<exists>qs. word (\<delta>d \<delta>) {p} as qs \<and> q\<in>qs)"
  apply rule
  subgoal using aux1 by fast
  subgoal using aux2 by blast
  done


section \<open>Bonus: On the fly powerset (5 Points)\<close>
  
text \<open>
  Note: the proofs in this question are easy, but we give almost no hints on the formalization. 
  You have to come up with the functions and correctness statement yourself.
  Please add sufficient explanation such that we can follow your formalization! 
  
  Wikipedia lists a few ways how to 'implement' an NFA. \<^url>\<open>https://en.wikipedia.org/wiki/Nondeterministic_finite_automaton#Implementation\<close>
  
  We have already explored the conversion to a DFA.
  
  Another option is to do the powerset construction 'on the fly', 
  i.e., keep track of the set of states that the automaton can be in while reading the word.
  If you start with the set singleton set that only contains the start state, you can then check if 
  an accepting state is contained in the set at the end.
  
  Your task:
  
  \<^item> define a function that computes, for a start state and a word, the states that the 
    automaton may be in after reading that word. Do not compute more sets than necessary, e.g.
    solutions along the lines \<^term>\<open>SOME qs. word (\<delta>d \<delta>) ps as qs\<close> do not count!
  
  \<^item> show that your function is correct (sound and complete) wrt. \<^const>\<open>word\<close>
  
  Note: no need for any fancy set data structure! \<^typ>\<open>'q set\<close> will do for this question.
\<close>

(*<*)  
fun of_pwr where
  "of_pwr \<delta> qs [] = qs"
| "of_pwr \<delta> qs (a#as) = of_pwr \<delta> ({ q'. \<exists>q\<in>qs. (q,a,q')\<in>\<delta> }) as"  


lemma of_pwr_correct_aux: "q' \<in> of_pwr \<delta> qs as \<longleftrightarrow> (\<exists>q\<in>qs. word \<delta> q as q')"
  apply (induction as arbitrary: qs)
  apply auto
  done
  
lemma of_pwr_correct: "q' \<in> of_pwr \<delta> {q} as \<longleftrightarrow> word \<delta> q as q'"
  by (simp add: of_pwr_correct_aux)


(* Alternative solution *)

fun word_det :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> 'q set \<Rightarrow> 'a list \<Rightarrow> 'q set \<Rightarrow> bool" where
  "word_det \<delta> ps [] qs \<longleftrightarrow> ps=qs"
| "word_det \<delta> ps (a#as) qs \<longleftrightarrow> word_det \<delta> {q. \<exists>p\<in>ps. ((p,a,q) \<in> \<delta>)} as qs"  


lemma "word (\<delta>d \<delta>) ps as qs = word_det \<delta> ps as qs"
  apply(induction as arbitrary: ps)
   apply (auto simp: \<delta>d_def)
  done

(*>*)  

  



