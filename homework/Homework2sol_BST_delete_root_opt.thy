theory Homework2sol_BST_delete_root_opt
imports Main
begin

datatype BST = Node BST int BST | Leaf

fun bst_set :: "BST \<Rightarrow> int set" where
  "bst_set Leaf = {}"
| "bst_set (Node l x r) = bst_set l \<union> {x} \<union> bst_set r"  

fun bst_invar :: "BST \<Rightarrow> bool" where
  "bst_invar Leaf \<longleftrightarrow> True"
| "bst_invar (Node l x r) \<longleftrightarrow> 
      bst_invar l 
    \<and> bst_invar r 
    \<and> (\<forall>y\<in>bst_set l. y<x) 
    \<and> (\<forall>y\<in>bst_set r. y>x)"


fun bst_root_value :: "BST \<Rightarrow> int" where
  "bst_root_value Leaf = undefined" (* No meaningful root value for leafs *)
| "bst_root_value (Node l x r) = x" (* Specify the other patterns *)

fun bst_delete_root :: "BST \<Rightarrow> BST" where
  "bst_delete_root Leaf = Leaf" (* We define this to be Leaf! *)
| "bst_delete_root (Node Leaf _ r) = r"
| "bst_delete_root (Node l _ Leaf) = l" (** We added this 'optimization' *)
| "bst_delete_root (Node (Node l2 y2 r2) x r1) = Node l2 y2 (bst_delete_root (Node r2 x r1))"


lemma bst_delete_root_\<alpha>: "bst_invar b \<Longrightarrow> bst_set (bst_delete_root b) = bst_set b - {bst_root_value b}"
  apply(induction b rule: bst_delete_root.induct)
    apply auto
  done

lemma bst_delete_root_invar: "bst_invar b \<Longrightarrow> bst_invar (bst_delete_root b)"
  apply(induction b rule: bst_delete_root.induct)
  apply (auto simp: bst_delete_root_\<alpha> dest: order_less_trans)
  (** The argumentation now requires transitivity. auto can't do that automatically. 
      Sledgehammer will also find a proof after the auto without the dest: returns. *)
    
  done

(** Here's an annotated attempt of mine to analyze the goal, and understand what happened 
  (and why I have to come up with order_less_trans in first place) *)

lemma "bst_invar b \<Longrightarrow> bst_invar (bst_delete_root b)"
  apply (induction b rule: bst_delete_root.induct)
  (** First step: find out where exactly we get stuck *)
  subgoal by (auto simp: bst_delete_root_\<alpha>)
  subgoal by (auto simp: bst_delete_root_\<alpha>)
  subgoal by (auto simp: bst_delete_root_\<alpha>)
  (** here! last subgoal of the induction. I.e. if you rotate and then recursively delete the root.*)
  
  (** Second step: give meaningful names to automatically generated variables. 
    That helps understanding the goal a lot *)
  subgoal for l2 y2 r2 x2 l1 v1 r1
    (** apply (auto simp: bst_delete_root_\<alpha>) takes long, and returns unreadable goal *)
  
    (** 3rd: use less aggressive methods, to understand how the proof works. 
      Also split it up, i.e., apply some simp-lemmas only later 
    *)
    apply (clarsimp)
    
    (** We have to show that y2 (the root of the left subtree) is less than anything
      in bst_delete_root (Node r2 x2 (Node l1 v1 r1))
    *)
    
    apply (clarsimp simp: bst_delete_root_\<alpha>)
    (** Apparently, we have 4 cases here. Note the disjunction in the premises. *)
    
    apply (elim disjE) (** we split them, and, again, try to find the problematic one *)
    subgoal by auto
    subgoal by auto
    subgoal for x (** Here we go \<dots> x\<in>bst_set l1 seems to be the problematic one.
      Now that we know what is going on, we check if we could prove that case (e.g. on paper).
      Can we argue why this should hold?
      
      Note that we started with: bst_invar (Node (Node l2 y2 r2) x2 (Node l1 v1 r1)), which
      has been simplified by now. Apparently, we know, by the invariant, that y2 < x2 < l1.
      So the argument is by transitivity.
      
      Let's try to find the relevant assumptions:
      
      we have \<open>y2<x2\<close>. and also \<forall>x\<in>bst_set l1 \<union> bst_set r1. x2 < x.
      
      so, this goal is obvious by transitivity here. Unfortunately, try0 does not find a 
      proof (sledgehmmer finds a proof). But let's for now assume we don't use sledgehammer.
      A common situation would be, that we have omitted some type annotations, and are trying to
      use transitivity on an arbitrary type 'a (which does not hold there). So sledgehammer would not
      find a proof, and we need to go more fine-grained to actually figure out what happened.
        
      We can apply the transitivity lemma manually!
      *)
  
      find_theorems "_<_ \<Longrightarrow> _<_ \<Longrightarrow> _<_" (* I forgot how it's called for < \<dots> find_theorems helps *)
      thm order.strict_trans
      thm order.strict_trans[of y2 x2]
      
      (** using order.strict_trans[of y2 x2]  this will display the theorem alongside the subgoal. 
        Useful for proof exploration!   *)
        
      apply (rule order.strict_trans[of y2 x2])
      apply assumption
          
      try0
      by blast
    subgoal by auto
    done
  done
  (** At this point, you would now try to clean up the proof to make it look nicer. 
    That's why you hardly ever see proofs as detailed as this. But knowing how to do proof 
    exploration is essential to find the easy looking proofs in first place.
  *)




end

