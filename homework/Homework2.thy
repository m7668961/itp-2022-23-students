theory Homework2
  imports Main
begin

text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #2
  RELEASED: Wed, Nov 23 2022
  DUE:      Wed, Nov 30, 2022, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
\<close>
  

section \<open>General Information\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...


  This exercise will focus mainly on the definition of datatypes.
  We will define some custom datatypes and show that correctness by
  proving invariant preservations. For this, we use their abstract representation.
\<close>


section \<open>Ex1: Slist (15 Points)\<close>

text \<open>Slists (sorted lists) are lists whose elements are sorted in ascending order.
  e.g. [1,2,3] is an slist. Moreover, elements can only occur once. So [1,2,2,3] is not an slist. 
  The abstraction function of slists is \<^const>\<open>set\<close>\<close>

subsection \<open>a: Specify the insert function \<open>ins_slist\<close> (3p)\<close>

text \<open>E.g. \<open>ins_slist 5 [2,4,6,8] = [2,4,5,6,8]\<close>\<close>

fun ins_slist :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  "ins_slist _ _ = undefined"


subsection \<open>b: Specify the invariant for slists (3p)\<close>

text \<open>As specified before, slists are sorted in ascending order and cannot have duplicates.
  HINT: take a look at \<open>sorted_wrt\<close>\<close>

definition "slist_invar xs = undefined"



subsection \<open>c: Prove correctness (3p)\<close>

text \<open>The first part of correctness relates the concrete operation \<open>ins_slist\<close> with the abstract 
  operation \<^const>\<open>insert\<close> on sets. Specify and prove that lemma: \<close>
  
lemma ins_slist_refine: "undefined" oops 
  

text \<open>
  The second part of correctness specifies that the invariant is preserved by the operation.
  Prove the following lemma!
\<close>  

lemma ins_slist_invar: "slist_invar xs \<Longrightarrow> slist_invar (ins_slist x xs)"
  oops
 
text \<open> 
  HINT: you will need an auxiliary lemma. Remember what you just proved as \<open>ins_slist_refine\<close>.
\<close>


subsection \<open>d: Specify and prove correct a delete operation (6p)\<close>

text \<open>Follow the same procedure, you probably need an abstraction refinement here as well.\<close>

fun del_slist :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  "del_slist _ _ = undefined"

(* Relate concrete operation to abstract deletion operation (\<open>\<lambda>x s. s - {x}\<close>) *)
lemma del_slist_refine: "undefined" 
  oops

(* Invariant preservation *)    
lemma del_slist_invar: "slist_invar xs \<Longrightarrow> slist_invar (del_slist i xs)"
  oops



section \<open>Ex 2: BST (13 Points)\<close>

text \<open>We have seen binary search trees in the lecture and we have defined basic operations on
  them. One of them is the delete operation. In the lecture, we have deleted a node by replacing
  the value that we want to delete with the largest value in its left subtree and then deleting that
  value. Now, we will look at a different method: Once we have found the node that we want to delete
  we 'rotate' the tree until the value can be removed directly.

  Rotation works as follows (in this case we rotate to the right):
  
        A               B
       / \             / \
      B   \<triangle>\<^sub>3  \<Longrightarrow>     \<triangle>\<^sub>1  A
     / \                 / \
    \<triangle>\<^sub>1  \<triangle>\<^sub>2              \<triangle>\<^sub>2 \<triangle>\<^sub>3
  
  Where \<triangle>\<^sub>i is an arbitrary subtree.

  After rotation, we can apply our delete procedure to the right subtree (node A as a root).
  We can terminate the process once we apply delete to a node with at least one Leaf by replacing 
  the current tree with the only available child node or with a Leaf if no child nodes are 
  available.

  We provide the data structure from the lecture.
\<close>


datatype BST = Node BST int BST | Leaf

fun bst_set :: "BST \<Rightarrow> int set" where
  "bst_set Leaf = {}"
| "bst_set (Node l x r) = bst_set l \<union> {x} \<union> bst_set r"  

fun bst_invar :: "BST \<Rightarrow> bool" where
  "bst_invar Leaf \<longleftrightarrow> True"
| "bst_invar (Node l x r) \<longleftrightarrow> 
      bst_invar l 
    \<and> bst_invar r 
    \<and> (\<forall>y\<in>bst_set l. y<x) 
    \<and> (\<forall>y\<in>bst_set r. y>x)"


section \<open>a: Specify bst_delete_root (4p)\<close>

text \<open>Specify the delete function which removes a node if it is in the tree. Otherwise, nothing 
  changes. The best approach to this may be to first specify a function which deletes the root
  node of a given tree using rotation. Specify such a function.
  HINT: it may be beneficial to prove correctness using the abstraction function \<open>bst_set\<close> and
  showing invariant preservation\<close>

fun bst_root_value :: "BST \<Rightarrow> int" where
  "bst_root_value Leaf = undefined" (* No meaningful root value for leafs *)
| "bst_root_value _ = undefined" (* Specify the other patterns *)

fun bst_delete_root :: "BST \<Rightarrow> BST" where
  "bst_delete_root Leaf = Leaf" (* We define this to be Leaf! *)
| "bst_delete_root _ = undefined" (* Specify the other patterns! *)

lemma bst_delete_root_\<alpha>: "bst_invar b \<Longrightarrow> bst_set (bst_delete_root b) = bst_set b - {bst_root_value b}"
  oops
(* Note: technically, you could assume that b\<noteq>Leaf here. 
  However, in the b=Leaf case, the lemma trivially holds "{} - {undefined} = {}", such
  that this precondition is not needed.
*)  
  

lemma bst_delete_root_invar: "bst_invar b \<Longrightarrow> bst_invar (bst_delete_root b)"
  oops
(*
  Note: we have defined "bst_delete_root Leaf = Leaf", such that this lemma does not need
  the precondition "b\<noteq>Leaf".
*)

subsection \<open>b: Specify the full delete function (4p)\<close>

text \<open>Now that we have a function to delete the root, we can simply find the node that
  we want to remove in our tree. This will be the root of a subtree and we already have a function 
  that can remove the root node.\<close>

fun bst_delete_rotate :: "BST \<Rightarrow> int \<Rightarrow> BST" where
  "bst_delete_rotate _ _ = undefined"


subsection \<open>c: Show correctness and invariant preservation (5p)\<close>               


lemma bst_delete_rotate_refine: "undefined"
  oops

lemma bst_delete_rotate_invar: "bst_invar b \<Longrightarrow> bst_invar (bst_delete_rotate b i)"
  oops





section \<open>Ex 3: Run-length encoded lists (12 points)\<close>


text \<open>A simple compression techniques is called run-length encoding, in which subsequent duplicates
  are compressed into a list that specifies the value and how often it occurs. E.g. 
  \<open>encode [1,1,1,1,3,4,4] = [(4,1),(1,3),(2,4)]\<close>. An element (e.g. \<open>(4,1)\<close>) of this list is called 
  a 'run'. The first value in each tuple specifies how often the second value is repeated in the 
  decoded list. We give the run-length encoded list as a \<open>'a rl\<close> below.\<close>
    
type_synonym 'a rl = "(nat \<times> 'a) list"

text \<open>
  The invariant makes sure that there are no 0 repetitions (so (0,x) does not occur in the rl) and 
  that no adjacent entries exist for the same element (so (n,x) is never followed by (m,x).
\<close>
fun rl_invar :: "'a rl \<Rightarrow> bool" where
  "rl_invar [] = True"
| "rl_invar [(n,x)] \<longleftrightarrow> n\<noteq>0"
| "rl_invar ((n\<^sub>1,x\<^sub>1)#(n\<^sub>2,x\<^sub>2)#xs) \<longleftrightarrow> n\<^sub>1\<noteq>0 \<and> x\<^sub>1\<noteq>x\<^sub>2 \<and> rl_invar ((n\<^sub>2,x\<^sub>2)#xs)"


subsection \<open>a: Specify the abstraction function. (3p)\<close>

text \<open>The abstraction function transforms an rl into a list, 
  e.g. \<open>rl_\<alpha> [(4,1),(1,3),(2,4)] = [1,1,1,1,3,4,4]\<close>
\<close>

fun rl_\<alpha> :: "'a rl \<Rightarrow> 'a list" where
  "rl_\<alpha> _ = undefined"  


subsection \<open>b: Specify rl_Nil and the refinement and invariant preservation lemmas and prove them. 
  (3p)\<close>

text \<open>rl_Nil is the rl that encodes the empty list \<open>[]\<close>
  HINT: you may need an abstraction refinement lemma.\<close>

definition "rl_Nil = undefined"
  
lemma rl_Nil_refine[simp]: specify oops
lemma rl_Nil_invar[simp]: specify oops


subsection \<open>c: Specify rl_Cons and prove its correctness.
  (3p)\<close>

text \<open>Similarly to Cons, rl_Cons prepends an element in front of the list. If the element is the
  same as the first element in the rl, the counter is incremented, e.g. 
  \<open>rl_Cons 2 [(3,2)(2,5)(6,2)] = [(4,2),(2,5),(6,2)]\<close>. Otherwise, a new run is prepended, e.g.
  \<open>rl_Cons 2 [(2,5),(6,2)] = [(1,2),(2,5),(6,2)]\<close>
  Hint: you may need an abstraction refinement lemma\<close>

fun rl_Cons :: "'a \<Rightarrow> (nat \<times> 'a) list \<Rightarrow> (nat \<times> 'a) list" where 
  "rl_Cons _ _ = undefined"  


lemma rl_Cons_refine: specify oops     
lemma rl_Cons_invar: specify oops


subsection \<open>d: Specify rl_nth and prove it correct (3p)\<close>

text \<open>Similarly to nth for regular lists (where nth xs n or xs ! n gets the element at index n), 
  we can define nth for an rl. \<^bold>A\<^bold>t\<^bold>t\<^bold>e\<^bold>n\<^bold>t\<^bold>i\<^bold>o\<^bold>n, we don't want to return a run here, but the nth element 
  over all runs if we were using the decoded list. E.g. \<open>rl_nth [(3,5),(4,2)] 2 = 5\<close> and 
  \<open>rl_nth [(3,5),(4,2)] 3 = 2\<close> 
\<close>

fun rl_nth :: "'a rl \<Rightarrow> nat \<Rightarrow> 'a" where
  "rl_nth _ _ = undefined"

text \<open>Hint: Have a look at the definition of \<^const>\<open>List.nth\<close> to get inspiration on how to define this 
  function:\<close>
thm List.nth.simps  
  
      
lemma rl_nth_refine: "i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i"  oops

text \<open>NOTE: invariant preservation does not make sense as we are merely reading from the list.\<close>


section \<open>Bonus: Updating the run-length encoded list (5 bonus points)\<close>

text \<open>Define an update function \<open>rl_upd xs i y\<close> which updates the value of the element at index i
  (as per rl_nth) to a value y. E.g. \<open>rl_upd [(4,2),(3,5),(6,2)] 4 7 = [(4,2),(1,7),(2,5),(6,2)]\<close>
  Some edge cases, if the update happens in the middle of a run, it needs to be split, e.g. 
  \<open>rl_upd [(4,2),(3,5),(6,2)] 5 7 = [(4,2),(1,5),(1,7),(1,5),(6,2)]\<close>. If a value is updated, we
  should also check whether we can merge runs, e.g. 
  \<open>rl_upd [(3,2),(1,5),(3,2)] 3 2 = [(7,2)]\<close>

  \<^bold>W\<^bold>A\<^bold>R\<^bold>N\<^bold>I\<^bold>N\<^bold>G: this is a rather difficult exercise with a lot of missing gaps, make sure that you don't
    sink too much time into this.\<close>


text \<open>It could be beneficial to have a so-called 'smart constructor' here which prepends an entire run to an rl\<close>

fun rl_Cons' :: "(nat \<times> 'a) \<Rightarrow> 'a rl \<Rightarrow> 'a rl" where
  "rl_Cons' _ _ = undefined"
  
text \<open>Define the update function and prove refinement wrt. Isabelle's \<^const>\<open>list_update\<close> function,
  as well as invariant preservation
\<close>
term "xs[i:=v]"
term "list_update xs i v"


fun rl_upd :: "'a rl \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a rl" where
  "rl_upd _ _ _ = undefined"  

lemma rl_upd_refine: specify oops
  oops
  
  
text \<open>You may need this, you can use it in auto as a dest rule:
  apply (auto dest: rl_invar_ConsD)\<close>
lemma rl_invar_ConsD: "rl_invar (x#xs) \<Longrightarrow> rl_invar xs"  
  apply (cases x; cases xs)
  by auto

lemma rl_upd_invar: "rl_invar xs \<Longrightarrow> rl_invar (rl_upd xs i y)"
  oops



end

