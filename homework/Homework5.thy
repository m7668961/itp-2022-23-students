theory Homework5
  imports Main Homework5_Aux1
begin

text \<open>
  Make sure you download Homework5_Aux1.thy and put it in the same folder as this file!
\<close>

  text \<open>
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  
  
    HOMEWORK #5
    RELEASED: Wed, Dec 14 2022
    DUE:      Wed, Dec 21 2022, 23:59
  
    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  \<close>
  
  section \<open>General Information\<close>  
  
  text \<open>
    This sheet has two bonus exercises! The regular bonus exercise is more difficult as always. The
    extra bonus exercise is not as hard. It's a good way to catch up with points. It's the very last 
    exercise on the sheet, make sure to give it a go!

    The best way to work on this homework is to fill in the missing gaps in this file.
  
    All solutions are a few lines only, and do, unless indicated, not require 
    to define any auxiliary functions. So if you end up with 
    lengthy and complicated function definitions, you are probably just 
    missing an easier solution.
  
    Do not hesitate to show me your problems with your solutions, 
    eg, if Isabelle throws some cryptic error messages at you 
      that you cannot decipher ...
  
  
    This exercise will focus mainly on the definition of datatypes.
    We will define some custom datatypes and show that correctness by
    proving invariant preservations. For this, we use their abstract representation.
  \<close>



  section \<open>Ex 1: Leftist Heaps (25 points)\<close>

  text \<open>
    You have likely heard about heaps before. Heaps are trees with the (min) heap property.
    \<^url>\<open>https://en.wikipedia.org/wiki/Heap_(data_structure)\<close>

    We are going to look at a leftist heap. This is a heap whose minimal depth of the left subheap
    is always greater than or equal to the depth of the right subheap at every node.
    However, the exact details of the data structure are not relevant for this homework, as you will
    only use it via its ADT interface.

    The abstract datatype for heaps is a multiset over linearly ordered elements,
    with the operations empty, is_empty, insert, get_min, and del_min
  \<close>


  
  text \<open>The abstraction function that transforms the leftist heap (lheap) into a multiset.\<close>
  term lh_mset

  text \<open>In the following, we summarize the operations and their correctness statements\<close>
  
  text \<open>The empty lheap implements the empty multiset\<close>
  term lh_empty
  thm lh_empty_refine

  text \<open>lh_is_empty h tests whether the lheap h is empty.\<close>
  term lh_is_empty
  thm lh_is_empty_refine

  text \<open>lh_insert a h inserts an element into the right place on the lheap h.\<close>
  term lh_insert
  thm lh_insert_refine

  text \<open>lh_get_min h gets the lowest value in the lheap h\<close>
  term lh_get_min
  thm lh_get_min_refine

  text \<open>lh_del_min h deletes the lowest value from the lheap h\<close>
  term lh_del_min
  thm lh_del_min_refine

  text \<open>
    The above constants and lemmas is all you will need to know about leftist heaps in your proof.
    It's important that you understand them.
  \<close>




    

  subsection \<open>a) Define a function \<open>build_heap\<close> and prove correctness. (6p)\<close>


  text \<open>
    Define a function that creates an lheap from a list. Remember that all operations are already 
    available and just need to be slotted together. Then, prove that it preserves the multiset of 
    elements in the list!
  \<close>

  definition build_heap :: "'a::linorder list \<Rightarrow> 'a lheap" where
    "build_heap xs \<equiv> undefined"
    
  lemma build_heap_mset: "lh_mset (build_heap xs) = mset xs"
    oops



  text \<open>
    The following function converts an lheap back to a sorted list. 
  \<close>
  
  fun heap_to_list where
    "heap_to_list h = (if lh_is_empty h then [] else lh_get_min h # heap_to_list (lh_del_min h))"

  subsection \<open>b) Show that heap_to_list preserves the elements (6p)\<close>

  lemmas [simp del] = heap_to_list.simps 

  text \<open>
    NOTE: heap_to_list.simps is prone to loop forever if used with the simplifier (try to find out 
    for yourself why). We deleted it from the simpset for this reason. To unfold the function 
    definition for heap_to_list in a controlled way use subst heap_to_list.simps or Isar.
    Also remember that computation induction is the best way to reason about functions with complex
    recursion patterns like heap_to_list\<close>
  
  lemma mset_heap_to_list: "mset (heap_to_list h) = lh_mset h"
    oops


  subsection \<open>c) Show that heap_to_list produces a sorted list. (6p)\<close>
  
  text \<open> 
    Note: looks easy, but some careful reasoning is actually required here!
    Isar + sledgehammer are your friends!
    
    Hint: Use computation induction over heap-to-list!
  \<close>

  lemma sorted_heap_to_list: "sorted (heap_to_list h)" for h :: "'a::linorder lheap"
    oops

  
  subsection \<open>d) Assemble everything: define lheapsort and prove it correct! (7p)\<close>

  text \<open>
    HINT: Everything that you have used so far can be used to create heapsort. Don't overcomplicate 
    anything, the simplest solution fits well within one line!
  \<close>
      
  definition lheapsort :: "'a::linorder list \<Rightarrow> 'a list" 
    where "lheapsort xs = undefined"
      
  lemma lheapsort_correct: "mset (lheapsort xs) = mset xs" "sorted (lheapsort xs)"
    oops



  section \<open>Ex 2: Prime Factorization (15 points)\<close>

  text \<open>
    Every integer greater than 1 can be represented uniquely as a product of prime numbers, 
    up to the order of the factors.
  
    \<^url>\<open>https://en.wikipedia.org/wiki/Fundamental_theorem_of_arithmetic#Proof\<close>
    
    Existence
    It must be shown that every integer greater than 1 is either prime or a product of primes. 
    First, 2 is prime. Then, by strong induction, assume this is true for all numbers greater 
    than 1 and less than n. If n is prime, there is nothing more to prove. Otherwise, there 
    are integers a and b, where n = a b, and 1 < a \<le> b < n. By the induction hypothesis, 
    a = p1 p2 \<cdot>\<cdot>\<cdot> pj and b = q1 q2 \<cdot>\<cdot>\<cdot> qk are products of primes. 
    But then n = a b = p1 p2 \<cdot>\<cdot>\<cdot> pj q1 q2 \<cdot>\<cdot>\<cdot> qk is a product of primes.
    
    Uniqueness
    Suppose, to the contrary, there is an integer that has two distinct prime factorizations. 
    Let n be the least such integer and write n = p1 p2 ... pj = q1 q2 ... qk, 
    where each pi and qi is prime. We see that p1 divides q1 q2 ... qk, so p1 divides some qi 
    by Euclid's lemma. Without loss of generality, say p1 divides q1. 
    Since p1 and q1 are both prime, it follows that p1 = q1. 
    Returning to our factorizations of n, we may cancel these two factors to conclude 
    that p2 ... pj = q2 ... qk. We now have two distinct prime factorizations of some integer 
    strictly smaller than n, which contradicts the minimality of n.  
  \<close>
  

  subsection \<open>a) formalize the above existence proof in Isar! (15p)\<close>

  text \<open>
    NOTE: Be careful what lemmas sledgehammer uses to prove your goals!
      It might find the fundamental theorem of arithmetic or derived theorems in the library,
      and use them to discharge your current subgoal! These 'shortcuts' are obviously not allowed. 
      Please follow the given proof outline, and make sure that the proofs of each step do not use
      weird detours, in particular the ones found by sledgehammer!
  \<close>

  (* Excluding a few 'obvious' shortcut lemmas from sledgehammer's view. *)
  lemmas [no_atp] = 
    prime_factorization_prod_mset_primes prime_factorization_eqI_strong
    prod_mset_prime_factorization_nat
    prime_factorization_exists_nat
  
  

  thm less_induct (* That's what the proof refers to as "strong induction". 
    Can be used as (induction n rule: less_induct). *)

  text \<open>
    HINTS:
    Use multisets, instead of enumerating elements! 
    With multisets, you go slightly beyond the 'comfort zone' of auto, but sledgehammer will
    usually find the relevant proofs! If not: insert intermediate steps, and use find_theorems!
  
    Be careful with types: the constant prime is defined over a more generic type than nat,
    but some of the lemmas you need only hold for nat. Check your types and add annotations when
    you run into unexpectedly unprovable statements.
  \<close>
  
  lemma exists_prime_factorization: "1 < n \<Longrightarrow> \<exists>s. n = \<Prod>\<^sub># s \<and> (\<forall>x\<in>#s. prime x)" for n :: nat
    oops


  section \<open>Bonus: Uniqueness of prime factorizations (5 points)\<close>

  text \<open>WARNING: There is a an easy extra bonus exercise after this one, 
    which only requires basic reasoning about lists.  
  
    This exercise is very difficult though! You may want to try the next one first!
  \<close>

  text \<open>
    Complete the proof of the Fundamental Theorem of Arithmetic, 
    by proving uniqueness. Follow the proof sketch from above (taken from Wikipedia).
  
    Hint: the Wikipedia proof sketch skips about quite some details here, 
    that you will have to amend! You are welcome to make slight changes to the proof structure,
    if that helps! (e.g., in our sample solution, we used the number of prime factors of n, 
    rather than the number n itself, for the "get-a-smaller" argument)
  \<close>
  
  lemma prime_factorization_unique:
    fixes s s' :: "nat multiset"
    assumes "1 < \<Prod>\<^sub># s" "\<Prod>\<^sub># s = \<Prod>\<^sub># s'" "\<forall>x\<in>#s. prime x" "\<forall>x\<in>#s'. prime x"
    shows "s=s'"
    oops
  
  lemma fundamental_thm_arith: "n>1 \<Longrightarrow> \<exists>!s. n=\<Prod>\<^sub># s \<and> (\<forall>x\<in>#s. prime x)" for n :: nat
    oops



  section \<open>Extra Bonus: Stable quicksort (5 points)\<close>

  text \<open>This is an additional bonus exercise to catch up with some points. It's easier than the
    previous one, and easier than some of the past regular homeworks. 
    Try to give it a go!
  \<close>

  text \<open>
    Recall that usual in-place implementations of quicksort are not stable.
    \<^url>\<open>https://en.wikipedia.org/wiki/Quicksort\<close>
    
    This is because of the partitioning algorithm.
    When we implement quicksort functionally, it's not in place, and can easily 
    be implemented in a stable way.
    
    Hint:
    This exercise can be solved with simple definitions and simple proofs!
    When your definitions (or proofs) get too complicated, you may be on the wrong track.
    
    It only depends on very basic concepts that have been introduced in the first weeks of the lecture.
    If you think you are getting too confused with the typeclass \<open>'b::linorder\<close>, you are welcome to
    restrict your key function to \<open>'a \<Rightarrow> nat\<close> for -1 point. 
    (Don't forget to at least try to change it back after you have done all the proofs with nat!)
  \<close>

  subsection \<open>a) Specify sorted_key (1p)\<close>
  
  text \<open>
    Given a function that maps the elements into a linearly ordered type,
    we can define when a list of keys is sorted wrt. their value. This means that we don't sort the 
    list itself but we sort the entries by the value of the mapping function. E.g. suppose we have 
    a function \<open>f: a \<mapsto> 2, b \<mapsto>5, c \<mapsto> 1\<close>, then \<open>sorted_key f [c,a,b]\<close> holds.

    Specify sorted key according to this specification. As a sanity check, lemma sorted_key_wrt
    should hold.
  \<close>

  fun sorted_key :: "('a \<Rightarrow> 'b::linorder) \<Rightarrow> 'a list \<Rightarrow> bool" where
    "sorted_key _ _  = undefined"

  
  text \<open>Prove an alternative characterization of the form: sorted_key k xs <--> sorted_wrt XXX xs\<close>  
  lemma sorted_key_wrt: "sorted_key k xs \<longleftrightarrow> sorted_wrt insertSomethingMeaningfulHere xs"
    oops

    
  subsection \<open>b) Quicksort Implementation (1p)\<close>      
  
  text \<open>There is a function @{term partition} which, given a predicate and a list, computes a pair of
        lists such that one of them has the members of the given list satisfying the given predicate,
        and the second has members which do not.\<close>
  
  thm partition.simps
  
  text \<open>Implement quicksort using @{term partition}\<close>
  
  fun quicksort :: "('a \<Rightarrow> nat) \<Rightarrow> 'a list \<Rightarrow> 'a list" where
    "quicksort _ _ = undefined"


  subsection \<open>c) Element preservation (1p)\<close>

  text \<open>Prove that quicksort preserves the elements of the list\<close>
      
  lemma quicksort_preserves_mset: "mset (quicksort k xs) = mset xs"
    oops



  subsection \<open>d) Sortedness (1p)\<close>
  text \<open>Prove that quicksort sorts\<close>
    
  
  lemma quicksort_sorts: "sorted_key k (quicksort k xs)"
    oops

  subsection \<open>e) Stability (1p)\<close>
  
  text \<open>
    We are going to prove that the implementation here is stable. A sorting algorithm is stable if
    elements with the same value occur in the same order as they occur in the original list.
    For example, given \<open>g: a \<mapsto> 2, b \<mapsto>5, c \<mapsto> 1, d \<mapsto> 2\<close>, we observe that \<open>a\<close> and \<open>d\<close> map to the 
    same value. If \<open>a\<close> occurs before \<open>d\<close> in the original list, then \<open>a\<close> occurs before \<open>d\<close> in the 
    sorted list, e.g. \<open>quicksort g [a,b,c,d] = [c,a,d,b]\<close>
    However, if \<open>d\<close> would occur before \<open>a\<close> in the original list, then \<open>d\<close> occurs before \<open>a\<close> in the
    sorted list as well, e.g. \<open>quicksort g [d,c,b,a] = [c,d,a,b]\<close>

    \<^url>\<open>https://en.wikipedia.org/wiki/Sorting_algorithm#Stability\<close>
  
    Specify stability formally in Isabelle, 
    *explain* why your specification makes sense in a few sentences,
    and prove that your quicksort algorithm is stable.

    Warning/Hint: there are many overly complicated ways to specify stability in HOL, 
      but at least one very simple way that comfortably fits into a single line without 
      being unreadable.
  \<close>

  lemma specify_your_own_lemma
    oops
end
