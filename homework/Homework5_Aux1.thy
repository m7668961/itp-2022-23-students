theory Homework5_Aux1
  imports Main "HOL-Computational_Algebra.Primes" "HOL-Data_Structures.Leftist_Heap"
begin


  text \<open>You can completely ignore this file, all important information is in the main homework file.\<close>
    
  text \<open>
    In the following, we establish the relevant correctness lemmas over a datatype that encapsulates 
    the invariant.
    
    Ignore the details. We explain the necessary things in the text boxes.
  \<close>
  lifting_forget multiset.lifting

  typedef (overloaded) 'a::linorder lheap = "{t::('a\<times>nat) tree. heap t \<and> ltree t}"
    using lheap.invar_empty by auto
  
  setup_lifting type_definition_lheap


  text \<open>The following definitions contain the abstraction operation \<open>lh_mset\<close> and the heap
    operations.\<close>

  lift_definition lh_mset :: "'a::linorder lheap \<Rightarrow> 'a multiset" is mset_tree .
  
  lift_definition lh_empty :: "'a::linorder lheap" is "Leftist_Heap.empty :: ('a::linorder \<times> nat) tree"
    using lheap.invar_empty by auto

  lift_definition lh_is_empty :: "'a::linorder lheap \<Rightarrow> bool" is "\<lambda>t. t=Leaf" .
      
  lift_definition lh_insert :: "'a::linorder \<Rightarrow> 'a lheap \<Rightarrow> 'a lheap" is "Leftist_Heap.insert"
    using lheap.invar_insert by auto
  
  lift_definition lh_get_min :: "'a::linorder lheap \<Rightarrow> 'a" is "Leftist_Heap.get_min" .
  
  lift_definition lh_del_min :: "'a::linorder lheap \<Rightarrow> 'a lheap" is "Leftist_Heap.del_min" 
    by (simp add: heap_del_min ltree_del_min)

  text \<open>The correctness of the refinement operations. \<close>

  lemma lh_empty_refine: "lh_mset lh_empty = {#}" 
    apply transfer
    by (simp add: lheap.mset_empty)   

  lemma lh_is_empty_refine: "lh_is_empty h \<longleftrightarrow> lh_mset h = {#}"  
    apply transfer
    by (simp add: mset_tree_empty)
    
  lemma lh_insert_refine: "lh_mset (lh_insert x h) = {#x#} + lh_mset h"
    apply transfer by (simp add: mset_insert)
  
  lemma lh_get_min_refine: "lh_mset h \<noteq> {#} \<Longrightarrow> lh_get_min h = Min_mset (lh_mset h)"
    apply transfer
    by (simp add: lheap.mset_get_min)
    
  lemma lh_del_min_refine: "lh_mset h \<noteq> {#} \<Longrightarrow> lh_mset (lh_del_min h) = lh_mset h - {#Min_mset (lh_mset h)#}"
    apply transfer
    by (simp add: lheap.mset_del_min lheap.mset_get_min)
  
  lifting_forget lheap.lifting 
  (* END: Ignore the details ;) *)    


  (* We instantiate size, such that the function package can handle termination proofs
    via the size of an lheap *)
  instantiation lheap :: (type) size
  begin
    definition "size_lheap h \<equiv> size (lh_mset h)"
    
    instance ..
  end

  lemma lh_del_min_size_decreases[termination_simp]: "\<not> lh_is_empty h \<Longrightarrow> size (lh_del_min h) < size h"
    unfolding size_lheap_def
    by (simp add: lh_is_empty_refine lh_del_min_refine size_Diff1_less)

  lemma mset_del_min_size_decreases[termination_simp]: "m\<noteq>{#} \<Longrightarrow> size (m - {# Min_mset m #}) < size m"  
    by (simp add: size_Diff1_less)
      
  
end
