chapter \<open>Homework 1\<close>
theory Homework1
imports Main
begin
  
text \<open>
  This file is intended to be viewed within the Isabelle/jEdit IDE.
  In a standard text-editor, it is pretty unreadable!


  HOMEWORK #1
  RELEASED: Tue, Nov 15 2022
  DUE:      Wed, Nov 23, 2022, 23:59

  To be submitted via email to p.lammich@utwente.nl.
  Include [ITP-Homework] in the subject line, and make sure to
  use your utwente email address, and/or include your name, 
  such that we can identify the sender.
\<close>
  

section \<open>General Hints\<close>  

text \<open>
  The best way to work on this homework is to fill in the missing gaps in this file.

  All solutions are a few lines only, and do, unless indicated, not require 
  to define any auxiliary functions. So if you end up with 
  lengthy and complicated function definitions, you are probably just 
  missing an easier solution.

  Do not hesitate to show me your problems with your solutions, 
  eg, if Isabelle throws some cryptic error messages at you 
    that you cannot decipher ...
\<close>



section \<open>Ex 1: Squaring using sum. (12 points)\<close>

text \<open>One can ofcourse calculate the n-th square integer by calculating n * n. However, it is also
  possible by adding the first n odd numbers together, i.e. \<open>n * n = \<Sigma>\<^sub>0\<^sub>\<le>\<^sub>i\<^sub><\<^sub>n (2 * n + 1)\<close>.
  In other words, it holds that 3*3 = 1 + 3 + 5 and 5*5 = 1 + 3 + 5 + 7 + 9\<close>


subsection \<open>a: Specify a function that calculates the square using the sum listed above.
  Don't use any auxiliary functions like nth_odd but specify the nth odd number as \<open>2 * n + 1\<close> (4p)\<close>

  fun "square" where 
    "square _ = undefined"


subsection \<open>b: Prove the correctness of your function. (4p)\<close>

  lemma "square n = n * n"
    sorry



text \<open>Isabelle already has a sum function \<open>sum_list\<close> built in which takes a list of 
  natural numbers and sums them. This means that you have to create a list with n odd numbers.
  There are multiple ways to do this. We suggest that you take a look at the \<open>map f xs\<close> function. 
  You can use \<open>[0..<n]\<close> to create a list with n values (you have seen this in the lecture).
  You can also use a lambda function \<open>(\<lambda> n. 2n + 1)\<close> which is a function that maps 2 to the nth odd 
  number. Combine these pieces to define \<open>n_odds\<close> which should return a list with the first n odd 
  numbers. E.g. \<open>n_odds 4 = [1,3,5,7]\<close>.\<close>

subsection \<open>c: Specify \<open>n_odds\<close> according to the specification given above and prove that the sum 
  of this list squares n. (4p)\<close>


text \<open>HINT: Unlike \<open>fun\<close>, a \<open>definition\<close> do not support pattern matching. You don't need pattern 
  matching if you follow the specification that we have given. If you do want to use pattern 
  matching, you are allowed to use \<open>fun\<close> instead.\<close>

  definition n_odds :: "nat \<Rightarrow> nat list" where
    "n_odds n = undefined"

  lemma "sum_list (n_odds n) = n * n"
    unfolding n_odds_def (* Unfolds the definition of n_odds, check the proof state to see the effect*)
    sorry


section \<open>Ex 2: Explode and Flatten. (14 points)\<close>


text\<open>Next up, we define aflatten function, which takes a list of lists and appends them all together.
  For example, flatten [[1,3],[2,6,5],[]] = [1,3,2,6,5]\<close>

subsection \<open>a: Specify a function \<open>flatten\<close> that fits the despription given above. (3p)\<close>

  fun flatten :: "'a list list \<Rightarrow> 'a list" where
    "flatten _ = undefined"


text \<open>Applying the map function to each separate list and flattening it is the same as 
  applying the map function to the flattened list. For example, let f x = x + 1, then 
  flatten (map (map f) [[1,3],[2,6,5],[]]) = flatten [[2,4],[3,7,6],[]] = [2,4,3,7,6] = 
    map f (flatten [[1,3],[2,6,5],[]]).\<close>

subsection \<open>b: Show that this holds for each function \<open>f\<close> and list \<open>xs\<close>. (4p)\<close>

  lemma "flatten (map (map f) xs) = map f (flatten xs)"
    sorry


subsection \<open>c: Specify a function \<open>explode\<close> which turns a list into a list of lists of length 1.
  For example "explode [1,3,6] = [[1],[3],[6]]" (3p)\<close>

  fun explode where
    "explode _ = undefined"
  

subsection \<open>d: Prove that flattening an exploded list is an identity function. (4p)\<close>

  lemma "flatten (explode xs) = xs"
    sorry
  

section \<open>Ex 3: \<exists> and \<forall> for lists. (14 points)\<close>


text \<open>Given a predicate \<open>P\<close>, the formula \<open>\<exists>x\<in>A. P x\<close> states that there exists a value \<open>x\<close> in \<open>A\<close> 
  such that \<open>P x\<close> is true. We can do the same for lists by defining a function \<open>any\<close> where \<open>any P xs\<close>
  holds if and only if \<open>xs\<close> contains an element such that \<open>P x\<close> holds.
  For example, let \<open>P x \<longleftrightarrow> x > 5\<close>, then \<open>any P [2,4,5]\<close> is False but \<open>any P [2,4,5,6]\<close> is True.\<close>

subsection \<open>a: Specify function \<open>any\<close> (3p)\<close>

text \<open>Hint: Note that \<open>any P []\<close> is False\<close>

  fun any :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> bool" where
    "any _ _ = undefined"


text \<open>Isabelle/HOL has a built-in filter function which returns a list of values that satisfy a 
  given predicate. For examplelet \<open>P x \<longleftrightarrow> x > 5\<close>  \<open>filter P [2,4,5,6] = [6]\<close> \<close>

subsection \<open>b: Show that \<open>filter\<close> will not return the empty list if and only if \<open>any\<close> holds. (4p)\<close>

  lemma "any P xs \<longleftrightarrow> filter P xs \<noteq> []"
    sorry
  

text \<open>Similarly, we can define a \<open>all\<close> function that represents \<open>\<forall>x\<in>A. P x\<close>.
  For example, let \<open>P x \<longleftrightarrow> x > 5\<close>, then \<open>all P [5,6,7]\<close> is False but \<open>all P [6,7]\<close> is True.\<close>


subsection \<open>c: Specify function \<open>all\<close> (3p)\<close>

text \<open>Hint: Note that \<open>all P []\<close> is True\<close>

  fun all :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> bool" where
    "all _ _ = undefined"


text \<open>The quantifiers \<open>\<forall>\<close> and \<open>\<exists>\<close> are dual, i.e. \<open>\<not>\<exists>x\<in>A. P x \<longleftrightarrow> \<forall>x\<in>A. \<not>P x\<close>. Please reason about
  why this is true.\<close>

subsection \<open>d: Show that \<open>any\<close> and \<open>all\<close> are dual as well. (4p)\<close>
    
  lemma "\<not>any P xs \<longleftrightarrow> all (Not o P) xs"
    sorry


section \<open>Bonus: Tree traversal (5 Bonus Points)\<close>


text \<open>During the lecture, you have been given a tree datatype which stores data, but only in the 
  nodes. Additionally, you have been given a mirror function that flips a tree.\<close>

  datatype 'a tree = Leaf | Node "'a tree" 'a "'a tree"
  
  fun mirror :: "'a tree \<Rightarrow> 'a tree" where
    "mirror Leaf = Leaf" |
    "mirror (Node l x r) = Node (mirror r) x (mirror l)"

text \<open>There are different ways to retrieve data from a tree. Traversal means that you start at the
  root node and visit the value and the children of the node. The order in which we do this can 
  differ. The inorder traversal first retrieves all of the values of the left child (also inorder), 
  then retrieves  the value of the current node and then that of the right child (inorder).
  For example \<open>inorder (Node (Node Leaf 2 Leaf) 1 (Node Leaf 3 Leaf)) = [2,1,3]\<close>\<close>

subsection \<open>a: Specify function \<open>inorder\<close>. (1 bonus)\<close>

  fun inorder :: "'a tree \<Rightarrow> 'a list" where
    "inorder _ = undefined"


text \<open>Similarly, we can define preorder traversal. This means that we preprend the value of the node 
  to the values of the left child and then the right child, both are retrieved in preorder. 
  For example \<open>preorder (Node (Node Leaf 2 Leaf) 1 (Node Leaf 3 Leaf)) = [1,2,3]\<close>\<close>

subsection \<open>b: Specify function \<open>preorder\<close>. (1 bonus)\<close>

  fun preorder :: "'a tree \<Rightarrow> 'a list" where
    "preorder _ = undefined"


text \<open>Lastly, we can define postorder traversal. This means that we first collect the values of the 
  left child and then the right child, both in postorder. After that we append the value of the 
  current node.
  For example \<open>postorder (Node (Node Leaf 2 Leaf) 1 (Node Leaf 3 Leaf)) = [2,3,1]\<close>\<close>

subsection \<open>c: Specify function \<open>postorder\<close>. (1 bonus)\<close>

  fun postorder :: "'a tree \<Rightarrow> 'a list" where
    "postorder _ = undefined"


text \<open>If we mirror a tree, the inorder traversal will produce a different list compared to the 
  inorder traversal of the original tree. In fact, the produced list will be reversed.\<close>

subsection \<open>d: Specify a lemma that states that the reverse list of an inorder tree traversal is
  exactly the same as the inorder traversal of the mirrored tree. Then prove this lemma. (1 bonus)\<close>

  lemma specify_your_own_lemma
    sorry


text \<open>Sadly, the same does not hold for \<open>postorder\<close> and \<open>preorder\<close>. However, they do share some dual
  property. The reverse list of a postorder traversal matches exactly to the preorder 
  traversal of the mirrored tree.\<close>

subsection \<open>e: Specify a lemma that states that the reverse list of an postorder tree traversal is
  exactly the same as the preorder traversal of the mirrored tree. Then prove this lemma. (1 bonus)\<close>

  lemma specify_your_own_lemma
    sorry
end
