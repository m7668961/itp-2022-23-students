theory Homework2sol_RLE_nth_dep_invar
imports Main
begin

type_synonym 'a rl = "(nat \<times> 'a) list"
  
fun rl_\<alpha> :: "'a rl \<Rightarrow> 'a list" where
  "rl_\<alpha> [] = []"
| "rl_\<alpha> ((n,x) # xs) = replicate n x @ rl_\<alpha> xs"  

  
fun rl_invar :: "'a rl \<Rightarrow> bool" where
  "rl_invar [] = True"
| "rl_invar [(n,x)] \<longleftrightarrow> n\<noteq>0"
| "rl_invar ((n\<^sub>1,x\<^sub>1)#(n\<^sub>2,x\<^sub>2)#xs) \<longleftrightarrow> n\<^sub>1\<noteq>0 \<and> x\<^sub>1\<noteq>x\<^sub>2 \<and> rl_invar ((n\<^sub>2,x\<^sub>2)#xs)"


fun rl_nth :: "'a rl \<Rightarrow> nat \<Rightarrow> 'a" where
  "rl_nth [] _  = undefined" |
  "rl_nth ((a,b)#xs) 0 = b" | (** Note how this explicitly relies on a>0 *)
  "rl_nth ((a,b)#xs) (Suc m) = (case a of Suc 0 \<Rightarrow> rl_nth xs m | Suc n \<Rightarrow> rl_nth ((n,b)#xs) m)"

  
(** When you tried to prove the unaugmented lemma, quickcheck found a counterexample.

  I highly recommend you to enable auto-quickcheck, this will save you spending time on such unprovable goals.
*)  
lemma rl_nth_refine: "i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i" 
  quickcheck (** Note: enable auto-quickcheck to not try to prove invalid lemmas! *)
  oops
  
  
(** Here's my annotated attempt at proving the lemma with the invariant *)  
  
(** aux lemmas added during attempt. *)
lemma aux1: "\<not>rl_invar ((0,v)#xs)" by (cases xs) auto

lemma aux2: "rl_invar ((Suc (Suc x2), b) # xs) \<Longrightarrow> rl_invar ((Suc x2, b) # xs)"
  apply (cases xs) by auto

lemma aux3: "rl_invar ((n,v)#xs) \<Longrightarrow> rl_invar xs"  
  by (cases xs) auto
  
(** Note: we have add the invariant *)
lemma rl_nth_refine: "rl_invar xs \<Longrightarrow> i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i" 
  (**                 ^^^ required for lemma to be correct. But will make proof more complicated! *)
  apply (induction xs i rule: rl_nth.induct) (** 
    picking the most complicated recursion as induction scheme. This is a good heuristics, 
    as we get the most complicated function unfolded for free. The auxiliary lemmas are required 
    to align the other involved functions (\<alpha>,invar) *)
  apply (auto)
  (** we see the term (replicate a b @ rl_\<alpha> xs) ! 0, and think that should be simplified *)
  find_theorems "(_@_)!_" (** Let's check what we have in the library *)
  apply (auto simp: nth_append)
  (** rl_invar ((0, b) # xs) \<longrightarrow> I don't believe in that, so created aux1 *)
  apply (auto simp: aux1)
  (** I see nat case, so splitting that  *)
  apply (auto split: nat.splits  simp: aux1 nth_append)
  (** In order to apply the IH, we need  rl_invar ((Suc x2, b) # xs)
    but we only got: rl_invar ((Suc (Suc x2), b) # xs)
    \<longrightarrow> aux2
    *)
  apply (auto simp: aux2) (** (b # replicate x2 b @ rl_\<alpha> xs) ! m = b should be simplified! *) 
  find_theorems "(_#_)!_" (** Again, the library helps us. *)
  
  apply (auto simp: nth_append nth_Cons' split: if_splits)
  (**
    rl_invar xs \<Longrightarrow> False; rl_invar ((Suc 0, b) # xs)
    
    this looks very suspicious! \<longrightarrow> aux3
  *)

  apply (auto simp: aux3)
  done  
  
  (** After that exploration, I normally would clean up the proof! *)
lemma "rl_invar xs \<Longrightarrow> i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i" 
  apply (induction xs i rule: rl_nth.induct)
  (** Note aux2 and aux3 together will loop in the simplifier! I'll use them as dest rules instead! *)
  apply (auto simp: nth_append nth_Cons' aux1 dest: aux2 aux3 split: if_splits nat.splits)
  done
  
  


end
