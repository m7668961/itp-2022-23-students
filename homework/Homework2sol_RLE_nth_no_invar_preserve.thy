theory Homework2sol_RLE_nth_no_invar_preserve
imports Main
begin

type_synonym 'a rl = "(nat \<times> 'a) list"
  
fun rl_\<alpha> :: "'a rl \<Rightarrow> 'a list" where
  "rl_\<alpha> [] = []"
| "rl_\<alpha> ((n,x) # xs) = replicate n x @ rl_\<alpha> xs"  

  
fun rl_invar :: "'a rl \<Rightarrow> bool" where
  "rl_invar [] = True"
| "rl_invar [(n,x)] \<longleftrightarrow> n\<noteq>0"
| "rl_invar ((n\<^sub>1,x\<^sub>1)#(n\<^sub>2,x\<^sub>2)#xs) \<longleftrightarrow> n\<^sub>1\<noteq>0 \<and> x\<^sub>1\<noteq>x\<^sub>2 \<and> rl_invar ((n\<^sub>2,x\<^sub>2)#xs)"


(** Note that this rl_nth does not preserve the invariant during recursion.
  The invariant does not hold for ((0, x)#xs).
*)
fun rl_nth :: "'a rl \<Rightarrow> nat \<Rightarrow> 'a" where
  "rl_nth [] _ = undefined" |
  "rl_nth ((0, x)#xs) y = rl_nth xs y" |
  "rl_nth ((Suc n, x)#xs) y = (case y of 0 \<Rightarrow> x | Suc k \<Rightarrow> rl_nth ((n, x)#xs) k)"

(**
  Still, it is correct: 
    if you 'extend' the \<alpha> function to also work for 0s (ours does), you can prove it without 
    requiring an invariant.

*)  

lemma "i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i"
  apply (induction xs i rule: rl_nth.induct)
  apply (auto split: nat.splits)
  done
  
(**
  Should you require the invariant, though, it becomes part of your lemma, 
  and thus of your IH \<dots> you can only apply the IH for the recursive case, 
  if the invariant holds in this case (which is doesn't, so you're stuck)!

  Note that this is an excellent example of generalization for induction: 
  the lemma without the invariant (and maybe an extended \<alpha>, if your definition happens to not work for 0)
  is provable, while the (more special) lemma with the invariant cannot be proved by induction directly.
  
  Here's an annotated attempt how this case can be discovered:
*)

(** Many of you proved (or would have required) this aux lemma: *)
lemma aux: "\<not>rl_invar ((0, x) # xs)" by (cases xs) auto

  
lemma "rl_invar xs \<Longrightarrow> i<length (rl_\<alpha> xs) \<Longrightarrow> rl_nth xs i = rl_\<alpha> xs ! i"
  (**  ^^^ this is going to cause trouble **)
  apply (induction xs i rule: rl_nth.induct)
  apply (auto)
  apply (auto simp: aux)
  apply (auto split: nat.splits)
  subgoal for n x xs i
    (** 
      At this point, we can see that the IH has not yet been applied.
      To apply it, we'd need: \<open>rl_invar ((n, x) # xs)\<close>
      However, we've only got: \<open>rl_invar ((Suc n, x) # xs)\<close>
      
      And there's no way we can prove n\<noteq>0, thus this proof attempt get's stuck here
    *)
  
  oops  
    
  
  
  


end