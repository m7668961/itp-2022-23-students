theory "Tutorial6"
imports "../Demos/Graph_Lib" "../Demos/RBTS_Lib" "../Demos/While_Lib"
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  (*
    You have been given this DFS algorithm in the lecture. We are going to adapt it a bit.
    Our algorithm should set a flag to True if it finds a cycle through a DFS search. If a cycle
    is identified, the while loop is immediately broken.
    We have given some important definitions for the algorithm below.
  *)

  section \<open>Prerequisites\<close>

  subsection \<open>Overview of Used ADTs\<close>

  (**
    Sets (by red-black trees)
  *)

  term rbts_\<alpha>
  
  term rbts_empty thm rbts_empty_correct

  term rbts_is_empty thm rbts_is_empty_correct
  
  term rbts_member thm rbts_member_correct
    
  term rbts_insert thm rbts_insert_correct
  
  term rbts_delete thm rbts_delete_correct
  
  term rbts_from_list thm rbts_from_list_correct
  
  term rbts_diff_list thm rbts_diff_list_correct

  term rbts_to_list thm rbts_to_list_sorted rbts_to_list_set
    

  (**
    Graphs (as adjacency list, by red-black trees)
  *)

  term rg_\<alpha>
  
  term rg_empty thm rg_empty_correct
  
  term rg_add_edge thm rg_add_edge_correct

  term rg_succs thm rg_succs_correct


  subsection \<open>Overview of While-Loop Rules\<close>
  
  term while_option
  thm while_option_rule'


  subsection \<open>Requirements for loop-finding algorithm\<close>

  (* 
    Loop reachable from v\<^sub>0: v\<^sub>0 \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v 
    g\<^sup>* means reachable in g in 0 or more steps, g\<^sup>+ means reachable in g with 1 or more steps
    The definition below defines a 'lasso'. A state v\<^sub>0 that has a path that leads to a state v
    such that v can reach itself in 1 or more transitions.
  *)
  definition "has_loop g v\<^sub>0 \<equiv> \<exists>v. (v\<^sub>0, v) \<in> g\<^sup>* \<and> (v, v) \<in> g\<^sup>+"
  
  (*
    If there is a loop reachable from v, there is also a successor of v from which we can reach the loop.
    
    Intuition (\<Longrightarrow>):
      Either loop not yet reached: v\<^sub>0 \<rightarrow> v' \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v. Obviously, loop also reachable from successor v'.
      Or v\<^sub>0 is already part of loop: v\<^sub>0\<rightarrow>\<^sup>+v\<^sub>0. Just 'rotate' loop by one, to start and end at successor of v\<^sub>0.
  
    (\<Longleftarrow>): obvious
  *)
  lemma has_loop_iff_succ: "has_loop g u \<longleftrightarrow> (\<exists>v. (u,v)\<in>g \<and> has_loop g v)"
    apply (auto simp: has_loop_def)
    subgoal by (meson rtrancl_trancl_trancl tranclD)
    subgoal by (meson converse_rtrancl_into_rtrancl)
    done
  
  (* Dest-version of this lemma, as it is not a good simp lemma! *)  
  lemmas has_loop_succD = has_loop_iff_succ[THEN iffD1]
  
  section \<open>Algorithm\<close>
  
  

  text \<open>We implicitly fix a graph, and a start node. 
    This is just to make things more readable.
  \<close>
  context
    fixes gi and g :: "'v::linorder graph" and v\<^sub>0 :: 'v
    assumes [simp]: "rg_\<alpha> gi = g"

  begin
    
    (*
      We give the function \<open>stack_reachable\<close> which states that the nodes on the stack form a path.
      As a matter of fact, any two entries on the stack will form a path.
      This is obvious, as we only ever push a successor of the current top node.
    *)
    definition "stack_reachable w = (\<forall>ws p v ws'. w = ws @ (p,v) # ws' \<longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v})"
    
    lemma stack_reachableD: assumes "stack_reachable w" "w = ws @ (p,v) # ws'" shows "snd ` (set ws) \<subseteq> g\<^sup>+``{v}"
      using assms
      unfolding stack_reachable_def
      by blast
    
    lemma stack_reachableI: assumes "\<And>ws p v ws'. w = ws @ (p,v) # ws' \<Longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v}" shows "stack_reachable w"
      using assms
      unfolding stack_reachable_def
      by blast
    
    lemma stack_reachableI':
      assumes "\<And>w\<^sub>1 w\<^sub>2 w\<^sub>3 p\<^sub>1 p\<^sub>2 u\<^sub>1 u\<^sub>2. w = w\<^sub>1@(p\<^sub>1,u\<^sub>1)#w\<^sub>2@(p\<^sub>2,u\<^sub>2)#w\<^sub>3 \<Longrightarrow> (u\<^sub>2,u\<^sub>1)\<in>g\<^sup>+"
      shows "stack_reachable w"
      using assms unfolding stack_reachable_def
      by (auto simp: in_set_conv_decomp)


    (*
      EXERCISE:
      You are now given the DFS algorithm from the lecture. Try to adapt it such that it will set a
      flag to True if there is a loop in the graph, and to False otherwise. If the flag is set to
      True, the while loop should be immediately broken.
    *)
      
    definition "dfs_aux \<equiv> 
      while_option (\<lambda>(d,w). w\<noteq>[]) (\<lambda>(d,w). 
        let (v,w) = (hd w, tl w) in
        if rbts_member v d then (d,w)
        else 
          let d = rbts_insert v d in
          let w = rg_succs gi v @ w in
          (d,w)
      
      ) (rbts_empty,[v\<^sub>0]) 
    "
  
    definition "dfs \<equiv> fst (the (dfs_aux))"

    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" using rg_finite_ran[of gi] by simp
      finally (finite_subset) show ?thesis .
    qed        
      


    definition "dfs_invar \<equiv> \<lambda>(d,w). 
      v\<^sub>0\<in>rbts_\<alpha> d \<union> set w
    \<and> rbts_\<alpha> d \<union> set w \<subseteq> g\<^sup>*``{v\<^sub>0}
    \<and> g``rbts_\<alpha> d \<subseteq> rbts_\<alpha> d \<union> set w
    "
    
    
    lemma invar_init: "dfs_invar (rbts_empty,[v\<^sub>0])"
      unfolding dfs_invar_def
      by (simp)
    
    lemma invar_step1: "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<in>rbts_\<alpha> d \<Longrightarrow> dfs_invar (d,tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply auto
      done
      
    lemma invar_step2: "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<notin>rbts_\<alpha> d \<Longrightarrow> dfs_invar (rbts_insert (hd w) d, rg_succs gi (hd w) @ tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply (auto)
      done
      
    thm Image_closed_trancl
    
    lemma invar_final: "dfs_invar (d,[]) \<Longrightarrow> rbts_\<alpha> d = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_invar_def
      apply (auto)
      thm Image_closed_trancl
      by (metis Image_closed_trancl rev_ImageI)

 
    definition "dfs_wfrel \<equiv> 
      inv_image 
        (inv_image finite_psubset (\<lambda>d. g\<^sup>*``{v\<^sub>0} - d) <*lex*> measure length) 
        (apfst rbts_\<alpha>)"
      
    lemma dfs_wfrel_wf: "wf dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto
      
    lemma dfs_wfrel1: "w \<noteq> [] \<Longrightarrow> ((d, tl w), (d, w)) \<in> dfs_wfrel"  
      unfolding dfs_wfrel_def
      by auto

    lemma dfs_wfrel2: 
      assumes "dfs_invar (d, w)" "w \<noteq> []" "hd w \<notin> rbts_\<alpha> d"
      shows "((rbts_insert (hd w) d,w'),(d,w)) \<in> dfs_wfrel"
    proof -  
      
      have "hd w\<in>g\<^sup>*``{v\<^sub>0}" 
        using assms(1,2)
        unfolding dfs_invar_def
        by (auto simp: neq_Nil_conv)
      with \<open>hd w \<notin> rbts_\<alpha> d\<close> show ?thesis
        unfolding dfs_wfrel_def
        using finite_reachable
        by auto
        
    qed    
      
    lemma dfs_aux_correct: "\<exists>rbts. dfs_aux = Some (rbts,[]) \<and> rbts_\<alpha> rbts = g\<^sup>*``{v\<^sub>0}"
      unfolding dfs_aux_def
      apply (rule while_option_rule'[where P=dfs_invar and r=dfs_wfrel])
      subgoal by (auto simp: invar_init)
      subgoal by (auto simp: invar_step1 invar_step2)
      subgoal by (auto dest: invar_final)
      
      subgoal by (rule dfs_wfrel_wf)
      subgoal by (auto simp: dfs_wfrel1 dfs_wfrel2)
      done

    lemma dfs_correct: "rbts_\<alpha> (dfs) = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_def using dfs_aux_correct by force     
  
  end  
  
  
            
end
