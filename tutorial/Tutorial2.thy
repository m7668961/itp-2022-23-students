theory Tutorial2
  imports Main
begin


(*
We define a trie, which is a tree that stores boolean values. We use this to encode lists of booleans.
The left subtree represents a word whose first entry is False and the right subtree represents a word
whose first entry is True. Using a list of bools, we can traverse the tree. If we end up at a node
that contains true, then the list that we used to traverse the tree is in or encoded list.
If we end up at false or if we have to continue at at a Leaf, it's not. This means that out trie 
encodes a set of bool lists.
*)

datatype trie = is_Leaf: Leaf (this: bool) | Node (left: trie) (this: bool) (right: trie)

(*
This is the datastructure that we use. For example, \<open>Node (Node Leaf False (Node Leaf True Leaf)) True Leaf\<close>
contains the list \<open>[]\<close>, because the root node is set, and [False,True], because we end up at true
if we traverse through the left subtree (because the first entry is False) and then the right subtree
of that (because the second entry is True). Doing this means that we end up at a node that contains
True.
Define \<open>trie_set\<close> which does this.
*)
  
fun trie_set :: "trie \<Rightarrow> bool list set" where
  "trie_set _ = undefined"  


(*
Define an empty tree list and show correctness
*)

definition "trie_empty = undefined"
lemma trie_empty_correct[simp]: undefined oops


(*
Now we want to define membership of our trie. Similar to (\<in>) for regular sets.
*)
fun trie_member :: "bool list \<Rightarrow> trie \<Rightarrow> bool" where
  "trie_member _ _ = undefined"  


(*
  We now specify the correctness lemma of our function, showing that the member function that we
  have just defined corresponds to the basic abstract operation that it represents (here \<in>).
*)
    
lemma trie_member_correct[simp]: undefined  
  oops
    
(*
Now we do the same for insert
*)
fun set_this where
  "set_this (Leaf b) = Leaf True"
| "set_this (Node l b r) = Node l True r"  
  
lemma set_this_correct[simp]: undefined
  oops
    
fun trie_insert :: "bool list \<Rightarrow> trie \<Rightarrow> trie" where
  "trie_insert _ _ = undefined"
  
lemma trie_insert_correct: undefined
  oops

(*
And just like that, we can also delete.
*)

 fun reset_this where
  "reset_this _ = undefined"
  
lemma reset_this_correct[simp]: undefined oops
      
fun trie_delete :: "bool list \<Rightarrow> trie \<Rightarrow> trie" where
  "trie_delete _ _ = undefined"
    
lemma trie_delete_correct: undefined oops

(*
This should give some overview of how to work with abstraction. You define a concrete datatype and
a corresponding abstract datatype with according operations. Sometimes, not all representable
data will be valid. In this case you need an invariant to shrink down the possible values.
You'll play with this in the homework!
*)




(*
If time permits, we take a look at polynomials with one variable encoded as lists. 
[2,3,5] represents the polynomial 2 + 3x + 5x\<^sup>2. We define a tail-recursive function which
evaluates a polynomial at a given value. We then define a fold operation that does the same and show 
that they are equal.
*)
fun eval_polynomial_tr :: "int \<Rightarrow> int list \<Rightarrow> int \<Rightarrow> int" where
  "eval_polynomial_tr _ _ _ = undefined"


value "eval_polynomial_tr 0 [2,4,6] 10"


lemma "eval_polynomial_tr a xs y = fold (\<lambda> x a. a * y + x) xs a"
  oops


(*
We can also define a polynomial evaluation function that is not tail-recursive.
Define this and show that both versions are equal (technically they are not equal, but with some 
transformations they are).
*)
fun eval_polynomial :: "int list \<Rightarrow> int \<Rightarrow> int" where
  "eval_polynomial _ _ = undefined"


lemma "eval_polynomial_tr a xs y = eval_polynomial (rev xs) y + a * y ^ length xs"
  oops

(*
There are several ways to calculate the product of a polynomial. A rather convoluted way is called a
convolution. A convolution of two lists a and b is a list c in which entry i is the sum
product of each pair of entries of separate lists such that the sum of the indices in the original 
list add up to i.

For example the entry at index 2 of the convolution of [1,3,5,7] and [2,4,6] is 1*6 + 3*4 + 5*2.
Thats because 1 has index 0 in the first list and 6 has index 2 in the second array. This adds up 
to 2. Similarly 3 and 4 both have index 1 in their respective list, etc.

Define an function that does this inductively. 
*)


fun convolve :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
  "convolve _ _ = undefined"


(*
Show that this indeed corresponds to multiplication of polynomials.
*)

value "convolve [1,2,3] [4,5,6]"

lemma "eval_polynomial (convolve f g) x = eval_polynomial f x * eval_polynomial g x"
  oops

end