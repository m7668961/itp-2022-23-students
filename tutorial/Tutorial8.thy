theory Tutorial8
  imports Main "HOL-Library.Monad_Syntax" "HOL-Library.While_Combinator" "HOL-Library.Char_Ord"
begin


  (*
    You have seen the state error monad during last weeks tutorial. We are now going to look
    at a different application, the parser. The parser monad is a function that maps a string to
    an output and another string, just like a parser does.
  *)
  
  datatype ('a,'s) senM = COMP (run: "'s \<Rightarrow> ('a \<times> 's) option option")
  
  definition "return x = COMP (\<lambda>s. Some (Some (x,s)))"
  definition "nonterm = COMP (\<lambda>_. None)"
  definition "fail = COMP (\<lambda>_. Some None)"
  
  (*
    To recap, we implements the bind function
  *)
  definition "bind_senM m f = undefined"
  
  adhoc_overloading bind bind_senM


  (*
    When reading symbols, we want to have an option to read an alternative symbol when the 
    initial parsing attempt fails. \<open>then_else\<close> executes p1 and applies function f to the output 
    value. 
  *)
  definition then_else :: "('c, 'b) senM \<Rightarrow> ('c \<Rightarrow> ('a, 'b) senM) \<Rightarrow> ('a, 'b) senM \<Rightarrow> ('a, 'b) senM" where
    "then_else p1 f p2 = undefined"

  definition alternative (infixr "<|>" 60) where "alternative a b \<equiv> then_else a return b"

  definition "get = COMP (\<lambda>s. Some (Some (s,s)))"
  definition "put s = COMP (\<lambda>_. Some (Some ((),s)))"
  
          
  (*
    We define the weakest precondition a bit differently. Instead of returning False when a program
    does not terminate, we return lib. If lib set to True, we do not care whether a program 
    terminates. We also additionally have a function F which is the value that we return if the
    program terminates, but crashes. It can be seen as a way to deal with invalid programs.
  *)
  definition wp_gen :: "'a \<Rightarrow> ('b, 'c) senM \<Rightarrow> ('b \<Rightarrow> 'c \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> 'c \<Rightarrow> 'a" where
  "wp_gen lib m Q F s = undefined"

  abbreviation "wp\<equiv>wp_gen False"    
  abbreviation "wlp\<equiv>wp_gen True"
    
  
  (*
    We present some monad and wp rules. These are general rules that always follow a particular 
    pattern.
  *)

  context 
    notes [simp] = bind_senM_def return_def fun_eq_iff wp_gen_def nonterm_def fail_def then_else_def
                   get_def put_def alternative_def
    notes [split] = option.splits Option.bind_splits
  begin

    lemma nonterm_then_else[simp]: "then_else nonterm b c = undefined" oops
    lemma return_then_else[simp]: "then_else (return x) b c = undefined" oops
    lemma fail_then_else[simp]: "then_else fail b c = undefined" oops
    
    (* TODO: There must be assoc-laws for then-else! *)
    
    lemma then_else_is_bind[simp]: "then_else m f fail = undefined" oops

    lemma nonterm_bind[simp]: "do {x\<leftarrow>nonterm; f x} = undefined" oops
    lemma fail_bind[simp]: "do {x\<leftarrow>fail; f x} = undefined" oops
        
    lemma return_bind[simp]: "do {x\<leftarrow>return v; f x} = undefined" oops
    lemma bind_return[simp]: "do {x\<leftarrow>m; return x} = undefined" oops
    lemma bind_assoc[simp]: "do {y\<leftarrow>do{x\<leftarrow>m; f x}; g y} = undefined" for m :: "(_,_) senM" oops



    lemma wlp_trivial [simp]: "wlp m (\<lambda> _ _. True) True s"
      oops

    
    lemma wp_return[simp]: "wp_gen l (return x) Q F s \<longleftrightarrow> undefined" oops
    lemma wp_noterm[simp]: "wp_gen l nonterm Q F s \<longleftrightarrow> undefined" oops
    lemma wp_fail[simp]: "wp_gen l fail Q F s \<longleftrightarrow> undefined" oops

    lemma wp_then_else[simp]: "wp_gen l (then_else a b c) Q F s = undefined" oops

    lemma wp_alternative [simp]: "wp_gen l (a <|> b) Q F s \<longleftrightarrow> undefined" oops
        
    lemma wp_bind[simp]: "wp_gen l (do {x\<leftarrow>m; f x}) Q F s = undefined" oops

    lemma wp_get[simp]: "wp_gen l get Q F s = undefined" oops
    lemma wp_put[simp]: "wp_gen l (put s) Q F s' = undefined" oops
    
        
  end








  (*
    The following lemmas are setup for the rest of the exercise. We suggest that you ignore
    it for now as it is only used to handle non-terminating behaviour in partial functions.
    We will never actually need these lemmas in our proofs.
  *)
  
  find_theorems partial_function_definitions "_::_ option"
  abbreviation "option_lub \<equiv> flat_lub None"  

  definition "sen_ord = img_ord run (fun_ord option_ord)"
    
  definition "sen_lub = img_lub run COMP (fun_lub option_lub)"
  
  abbreviation "mono_sen \<equiv> monotone (fun_ord sen_ord) sen_ord"
  
  
  lemma sen_pfd: "partial_function_definitions sen_ord sen_lub"
    unfolding sen_ord_def sen_lub_def
    apply (rule partial_function_image)
    apply (rule partial_function_lift)
    apply (rule flat_interpretation)
    apply (rule senM.expand; simp)
    by simp

  lemma sen_lub_empty: "sen_lub {} = nonterm"
    unfolding sen_lub_def nonterm_def img_lub_def flat_lub_def fun_lub_def
    by (auto simp: fun_eq_iff)
    
  interpretation senM:
    partial_function_definitions sen_ord sen_lub
    rewrites "sen_lub {} \<equiv> nonterm"
    apply (rule sen_pfd)
    by (simp add: sen_lub_empty)

  lemma admissible_flat: "ccpo.admissible (flat_lub None) (flat_ord None) P"  
    apply (rule ccpo.admissibleI)
    by (metis equals0I flat_lub_in_chain flat_ord_def option.lub_upper)
    
    
  lemma wlp_admissible: 
    "senM.admissible (\<lambda>Uf. \<forall>x. All (wlp (Uf x) (P x) (E x)))"
    apply (rule admissible_fun[OF sen_pfd])
    unfolding sen_ord_def sen_lub_def
    apply (rule admissible_image)
    apply (rule partial_function_lift)
    apply (rule flat_interpretation)
    apply (simp add: comp_def wp_gen_def)
    
    apply (rule admissible_fun)
    apply (rule flat_interpretation)
    apply (rule admissible_flat)
    
    apply (rule senM.expand; simp)
    by simp
    
  lemma wlp_nonterm: "wlp nonterm P E s" by simp

  (* TODO: If we try to use this lemma for partial_function induction rule,
       the package fails, seemingly expecting variable at head position of goal.
       Is this behaviour expected/necessary?
  *)
  lemma fixp_induct_senM:
    fixes F :: "'c \<Rightarrow> 'c" and
      U :: "'c \<Rightarrow> 'b \<Rightarrow> ('a,'s) senM" and
      C :: "('b \<Rightarrow> ('a,'s) senM) \<Rightarrow> 'c" and
      P :: "'b \<Rightarrow> 'a \<Rightarrow> 's \<Rightarrow> bool"
    assumes mono: "\<And>x. mono_sen (\<lambda>f. U (F (C f)) x)"
    assumes eq: "f \<equiv> C (ccpo.fixp (fun_lub sen_lub) (fun_ord sen_ord) (\<lambda>f. U (F (C f))))"
    assumes inverse2: "\<And>f. U (C f) = f"
    assumes step: "\<And>f x s. \<lbrakk>\<And>x s. wlp (U f x) (P x) (E x) s\<rbrakk> \<Longrightarrow> wlp (U (F f) x) (P x) (E x) s"
    shows "wlp (U f x) (P x) (E x) s"
    apply (rule senM.fixp_induct_uc[of U F C, OF mono eq inverse2, where P="\<lambda>Uf. \<forall>x s. wlp (Uf x) (P x) (E x) s", rule_format])
    apply (rule wlp_admissible)
    apply (rule wlp_nonterm)
    subgoal using step .
    done
              
  declaration \<open>Partial_Function.init "senM" \<^term>\<open>senM.fixp_fun\<close>
    \<^term>\<open>senM.mono_body\<close> @{thm senM.fixp_rule_uc} @{thm senM.fixp_induct_uc}
    (NONE)\<close>
    
  
  lemma bind_senM_mono[partial_function_mono]:
    assumes mf: "mono_sen B" and mg: "\<And>y. mono_sen (\<lambda>f. C y f)"
    shows "mono_sen (\<lambda>f. bind_senM (B f) (\<lambda>y. C y f))"
    supply [[show_abbrevs=false]]
    apply (rule monotoneI)
    using assms
    apply (auto simp: fun_ord_def sen_ord_def img_ord_def flat_ord_def bind_senM_def)
    apply (auto split: option.splits simp: fun_ord_def monotone_def)
    apply (metis option.distinct(1))
    subgoal by (metis option.discI)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.sel prod.sel(1) prod.sel(2))
    done

    
  lemma then_else_mono[partial_function_mono]:
    assumes ma: "mono_sen A" and mb: "\<And>y. mono_sen (\<lambda>f. B y f)" and mc: "mono_sen C"
    shows "mono_sen (\<lambda>f. then_else (A f) (\<lambda>y. B y f) (C f))"
    supply [[show_abbrevs=false]]
    apply (rule monotoneI)
    using assms
    apply (auto simp: fun_ord_def sen_ord_def img_ord_def flat_ord_def then_else_def)
    apply (auto split: option.splits Option.bind_splits simp: fun_ord_def monotone_def)
    subgoal by (metis option.distinct(1))
    subgoal by (metis option.discI)
    subgoal by blast
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.sel prod.sel(1) prod.sel(2))
    done
  
  lemma alternative_mono[partial_function_mono]: 
    assumes "mono_sen A" "mono_sen B"
    shows "mono_sen (\<lambda>f. A f <|> B f)"
    unfolding alternative_def
    by (intro partial_function_mono assms)
        
  context
    notes [simp] = alternative_def
  begin  
    lemma nonterm_alt[simp]: "nonterm <|> b = nonterm" by simp
    lemma fail_alt[simp]: "fail <|> b = b" by simp
    lemma return_alt[simp]: "return x <|> b = return x" by simp
    
    lemma alt_fail[simp]: "m <|> fail = m" by simp
    
    lemma alt_assoc[simp]: "(a <|> b) <|> c = a <|> b <|> c"
      apply simp
      unfolding then_else_def
      by (auto simp: fun_eq_iff return_def split: Option.bind_splits option.splits)
  end    

  (*
    End of setup!
  *)

  definition "assert P = (if P then return () else fail)"
  
  lemma wp_assert[simp]: "wp_gen l (assert P) Q F s \<longleftrightarrow> undefined"
  
  
  
  (* Building a parser:
    An elementary operation is to read the next character on a string and return it as a value.
    The rest of the string is untouched and returned as the new state.
  *)
  
  definition "read \<equiv> undefined"
  
  (* Setup: Word around ML's 'value restriction': no polymorphic values, i.e., functions without args *)
  lemmas [code del] = read_def   
  definition "read_code (_::unit) \<equiv> read"
  lemma [code_unfold]: "read = read_code ()" by (simp add: read_code_def)
  lemmas [code] = read_code_def[unfolded read_def]

  lemma wp_read[simp]: "wp_gen l read Q F s \<longleftrightarrow> undefined" oops
    
  
  (*
    rng is a function that reads a symbol and checks whether the symbol is in a given set.
    If it is not, the parser will abort. We achieve this using an assert.
  *)

  definition "rng S \<equiv> undefined"

  lemma wp_rng [simp]: "wp_gen l (rng S) Q F s \<longleftrightarrow> undefined" oops
  
  
  (*
    Function many reads characters as long as they match the parsers requirement. E.g. many (rng S)
    keeps reading characters as long as they are in S. If they aren't, function many will terminate.
    This is why we define many as a partial function. It is not given that it will terminate.
    To make sure that we can use induction, we have to define the function in a way that it 
    understands what to "return" when the program does not terminate.
  *)
  partial_function (senM) many where
    [code]: "many p = undefined"
  
  (* Boilerplate required for induction lemma. There seems to be a limitation in partial_function induction lemma generation,
     leaving it unable to generate wlp lemmas.
  
     Maybe, work with REC-combinator directly!? 
  *)  
  lemmas many_wlp_induct = many.fixp_induct[OF wlp_admissible, rule_format, OF wlp_nonterm]
  
  value "run (many (rng {CHR ''a''..CHR ''z''})) ''abcdXX''"
 
  
  (*
    We define some programs now. \<open>char\<close> reads only the given character. \<open>digit\<close> only reads digits.
    \<open>alpha\<close> only reads lower and upper case symbols. \<open>alphanum\<close> reads both from digit and alpha.
    \<open>identifier\<close> reads words that start with an alpha and end with an arbitrary number of alphanum.
    Those are given as examples.
  *)
  definition "char c = rng {c}"

  abbreviation "digits \<equiv> {CHR ''0''..CHR ''9''}"
  abbreviation "lowercase_letters \<equiv> {CHR ''a''..CHR ''z''}"
  abbreviation "uppercase_letters \<equiv> {CHR ''A''..CHR ''Z''}"
  abbreviation "letters \<equiv> lowercase_letters \<union> uppercase_letters"
  
  definition "digit = rng (digits)"
  definition "alpha = rng lowercase_letters <|> rng uppercase_letters"
  definition "alphanum = digit <|> alpha"
  
  definition "identifier = do { c\<leftarrow>alpha; cs\<leftarrow>many alphanum; return (c#cs) }"
  

  (*
    Our task is now to parse a list of identifiers that is typed out as a string and convert it
    to an actual list using the parser.
  *)
  
  definition "list p = enclose (char CHR ''['') (char CHR '']'') (separate (char CHR '','') p)"

    

  lemma wp_identifier[simp] : "wp (identifier) Q F s \<longleftrightarrow> undefined"
    oops

  lemma wp_separate [simp]: "wp (separate psep p) Q F s \<longleftrightarrow> undefined"
    oops

  lemma wp_enclose [simp]: "wp (enclose pl pr p) Q F s \<longleftrightarrow> undefined"
    oops
  
  value "run (list identifier) ''[a,b,hd,xha89]''" 
  
  fun pretty_commas where
    "pretty_commas [] = ''''"
  | "pretty_commas [x] = [x]"
  | "pretty_commas (x#xs) = x # '','' @ pretty_commas xs"
  

  lemma "CHR '','' \<notin> set xs \<Longrightarrow> wp (separate (char CHR '','') (read)) (\<lambda> a b. a = xs \<and> b = []) False (pretty_commas xs)"
    oops


end