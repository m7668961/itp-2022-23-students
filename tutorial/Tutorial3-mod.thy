theory "Tutorial3-mod"
  imports Main
begin


(*
We are going to do some backwards reasoning. Given below are some lemmas that simp can easily solve.
However, sometimes you will find yourself in a situation where simp and auto just won't find the 
solution. In that case you have to take over a bit. This part of the tutorial is meant to train
for these kinds of situations.
Try to solve these exercises purely using backwards reasoning (rule, drule, erule, assumption)
So try0 and sledgehammer and everything that they recommend are not allowed!
*)

find_theorems "insert _ _ ≠ {}"

lemma "{1,2} ≠ {}" 
  apply(rule insert_not_empty)
  done

find_theorems "?m < ?n ⟹ ?m < Suc ?n"

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) ⟹ x < Suc (Suc (Suc (Suc (Suc (Suc 0)))))"
  apply(rule less_SucI)
  apply assumption
  done

find_theorems "?x < ?y ⟹ ?y < ?z ⟹ ?x < ?z"
find_theorems "?a < ?b ⟹ Suc ?a < Suc ?b"
find_theorems "0 < Suc _"

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) ⟹ x < Suc (Suc (Suc (Suc (Suc (Suc (Suc 0))))))"
  apply(erule less_trans) 
  apply(rule Suc_mono)+
  apply(rule zero_less_Suc)
  done

find_theorems "?n < Suc ?y"
thm lessI

lemma "x < Suc (Suc (Suc (Suc (Suc y)))) ⟹ x < Suc (Suc (Suc (Suc (Suc (Suc (Suc y))))))"
  apply(erule less_trans) 
  apply(rule Suc_mono)+
  apply(rule less_SucI)
  apply(rule lessI)
  done


thm less_SucI[of _ "Suc _"]

lemma "x < Suc (Suc (Suc (Suc (Suc y)))) ⟹ x < Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc y)))))))"
  apply(erule less_trans) 
  apply(rule Suc_mono)+
  apply(rule less_SucI[of _ "Suc _"])+
  apply(rule lessI)
  done
  
find_theorems "last (?x # ?xs)" 
find_theorems "[]" Cons

lemma "last [2,3,4] = last [3,4]"
  apply(rule last_ConsR)
  apply(rule list.simps(3))
  done




(** This is the reason why this lemma is such a bad example for the tutorial **)

lemma aux3: "a ≠ b ⟹ a ∉ B ⟹ a ∉ insert b B"
  unfolding insert_iff de_Morgan_disj
  apply(rule conjI)
   apply assumption
  apply assumption
  done
  

  
lemma distinct_specific: "distinct [4::nat,2,3]"
  unfolding distinct.simps 
  unfolding set_simps 
  unfolding empty_iff 
  unfolding singleton_iff 

  apply(rule conjI)
   apply(rule aux3)
  unfolding numeral_nat 
    apply(rule less_not_refl2)
   apply(rule Suc_mono)+
    apply(rule zero_less_Suc) 

  unfolding singleton_iff 
    apply(rule less_not_refl2)
   apply(rule Suc_mono)+
    apply(rule zero_less_Suc) 

  apply (rule conjI) 
   
   apply(rule less_not_refl3)  
   apply(rule Suc_mono)+
   apply(rule zero_less_Suc) 

  apply rule+
   apply assumption
  apply rule
  done



lemma ConsI: "x = y ⟹ xs = ys ⟹ x#xs = y#ys" 
  apply(erule arg_cong2[of _ _ _ _ Cons]) 
  apply assumption
  done

lemma aux2: "[4, 2, 3, 4] = [4, 2, 3] @ [4]"
  unfolding list.inject[of "4::nat" _ 4]
  apply(rule ConsI[of 4 4 "[2,3,4]" "[2, 3] @ [4]", folded append_Cons])
   apply rule
  apply(rule ConsI[of 2 2 "[3,4]" "[3] @ [4]", folded append_Cons])
   apply(rule)
  unfolding append_Cons append_Nil
  apply rule
  done


lemma "¬distinct [4::nat,2,3,4]"
  unfolding not_distinct_conv_prefix
  apply(rule exI[where ?x = "[4,2,3]"])
  apply(rule exI[where ?x = "4"])
  apply(rule exI[where ?x = "[]"])
  apply(rule conjI)
   apply(rule list.set_intros(1))
  apply(rule conjI)
  apply(rule distinct_specific)
  apply(rule aux2)
  done

(** But this is all irrelevant for the exams, it's more important to see different proof techniques 
  that you can employ when you are stuck. It's often more of a hassle to actually do entire proofs
  like this, but you may sometimes need to perform certain steps by yourself.*)




lemma "∃ x. D x ⟶ (∀ x. D x)"
  apply(cases "(∀ x. D x)")
   apply(rule exI)
  apply(rule impI)
  apply assumption
  apply(rule exI[where ?x = "SOME x. ¬D x"])
  unfolding not_all
  apply(rule impI)
  unfolding SMT.verit_sko_ex
  apply(rule ccontr)
  apply(erule cnf.clause2raw_notE)
  unfolding not_not
  apply assumption
  done
  

(*
Previous restrictions are lifted, you can continue with these proofs as usual.

First of all, we take a look at labelled automata. A labelled automaton is a state machine in which 
each edge is labelled with a symbol. By following a path on the automaton, we can build a word out
of the symbols that we passed over. For example, we may have an automaton like the one below.
Automaton δ:

S⇩1 ―c→ S⇩2 ―b → S⇩3
         |        |
         a        a
         ↓        ↓
         S⇩4 ―c→ S⇩5

These edges are defined as a set of triples (p,a,q) where p is the source state
We can then define a function ‹word› where ‹word δ S⇩1 ws S⇩5› is True for ‹ws = [c,b,a]› and
‹ws = [c,a,c]› and False for everything else. In other words, function ‹word› checks whether a given
word matches a path in between two given nodes."

Define ‹word›
*)

fun word :: "('q × 'a × 'q) set ⇒ 'q ⇒ 'a list ⇒ 'q ⇒ bool" where
  "word δ p [] q ⟷ p = q" |
  "word δ p (w#ws) q ⟷ (∃r. (p,w,r) ∈ δ ∧ word δ r ws q)"

(*
We can also build the product of two automata. A state in this automaton is a tuple of a state of
both automata. An edge still only has a single symbol, but both automata transition to a next state.
If a tuple contains at least one state that doesn't have a transition with a given symbol, then 
neither does the automaton.

For example: if δ⇩1 contains the edge 
*)
definition δx :: "('q × 'a × 'q) set ⇒ ('q × 'a × 'q) set ⇒ (('q × 'q) × 'a × ('q × 'q)) set" where 
  "δx δ⇩1 δ⇩2 = {((p1,q1),w,(p2,q2)) | p1 q1 w p2 q2. (p1,w,p2) ∈ δ⇩1 ∧ (q1,w,q2) ∈ δ⇩2}"


(*
Show correctness of this definition
*)
lemma "word δ⇩1 p⇩1 as q⇩1 ∧ word δ⇩2 p⇩2 as q⇩2 ⟷ word (δx δ⇩1 δ⇩2) (p⇩1,p⇩2) as (q⇩1,q⇩2)"
  apply(induction as arbitrary: p⇩1 p⇩2)
  apply(auto simp: δx_def)
  done


(*
An automaton is deterministic if each state has only outgoing edges with distinct symbols.
In other words, if an edge goes from state p via symbol a to state q1 and an edge goes from
state p via symbol a to state q1 then q1 = q2.
*)
definition det :: "('q × 'a × 'q) set ⇒ bool" where 
  "det δ ≡ ∀q a q1 q2. (q, a, q1) ∈ δ ∧ (q, a, q2) ∈ δ ⟶ q1=q2"

(*
This carries over to our definition of words (if defined correctly)
*)
lemma "det δ ⟹ word δ q w q1 ⟹ word δ q w q2 ⟹ q1=q2"
  unfolding δx_def det_def
  apply(induction w arbitrary: q)
  apply auto
  by blast

(*
This carries over to product construction as well.
*)
lemma "det δ⇩1 ⟹ det δ⇩2 ⟹ det (δx δ⇩1 δ⇩2)"
  unfolding δx_def det_def
  apply auto
  done
