theory "Tutorial6-mod"
imports "../Demos/Graph_Lib" "../Demos/RBTS_Lib" "../Demos/While_Lib"
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  (*
    You have been given this DFS algorithm in the lecture. We are going to adapt it a bit.
    Our algorithm should set a flag to True if it finds a cycle through a DFS search. If a cycle
    is identified, the while loop is immediately broken.
    We have given some important definitions for the algorithm below.
  *)

  section \<open>Prerequisites\<close>

  subsection \<open>Overview of Used ADTs\<close>

  (**
    Sets (by red-black trees)
  *)

  term rbts_\<alpha>
  
  term rbts_empty thm rbts_empty_correct

  term rbts_is_empty thm rbts_is_empty_correct
  
  term rbts_member thm rbts_member_correct
    
  term rbts_insert thm rbts_insert_correct
  
  term rbts_delete thm rbts_delete_correct
  
  term rbts_from_list thm rbts_from_list_correct
  
  term rbts_diff_list thm rbts_diff_list_correct

  term rbts_to_list thm rbts_to_list_sorted rbts_to_list_set
    

  (**
    Graphs (as adjacency list, by red-black trees)
  *)

  term rg_\<alpha>
  
  term rg_empty thm rg_empty_correct
  
  term rg_add_edge thm rg_add_edge_correct

  term rg_succs thm rg_succs_correct


  subsection \<open>Overview of While-Loop Rules\<close>
  
  term while_option
  thm while_option_rule'


  subsection \<open>Requirements for loop-finding algorithm\<close>

  (* 
    Loop reachable from v\<^sub>0: v\<^sub>0 \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v 
    g\<^sup>* means reachable in g in 0 or more steps, g\<^sup>+ means reachable in g with 1 or more steps
    The definition below defines a 'lasso'. A state v\<^sub>0 that has a path that leads to a state v
    such that v can reach itself in 1 or more transitions.
  *)
  definition "has_loop g v\<^sub>0 \<equiv> \<exists>v. (v\<^sub>0, v) \<in> g\<^sup>* \<and> (v, v) \<in> g\<^sup>+"
  
  (*
    If there is a loop reachable from v, there is also a successor of v from which we can reach the loop.
    
    Intuition (\<Longrightarrow>):
      Either loop not yet reached: v\<^sub>0 \<rightarrow> v' \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v. Obviously, loop also reachable from successor v'.
      Or v\<^sub>0 is already part of loop: v\<^sub>0\<rightarrow>\<^sup>+v\<^sub>0. Just 'rotate' loop by one, to start and end at successor of v\<^sub>0.
  
    (\<Longleftarrow>): obvious
  *)
  lemma has_loop_iff_succ: "has_loop g u \<longleftrightarrow> (\<exists>v. (u,v)\<in>g \<and> has_loop g v)"
    apply (auto simp: has_loop_def)
    subgoal by (meson rtrancl_trancl_trancl tranclD)
    subgoal by (meson converse_rtrancl_into_rtrancl)
    done
  
  (* Dest-version of this lemma, as it is not a good simp lemma! *)  
  lemmas has_loop_succD = has_loop_iff_succ[THEN iffD1]
  
  section \<open>Algorithm\<close>
  
  

  text \<open>We implicitly fix a graph, and a start node. 
    This is just to make things more readable.
  \<close>
  context
    fixes gi and g :: "'v::linorder graph" and v\<^sub>0 :: 'v
    assumes [simp]: "rg_\<alpha> gi = g"

  begin
    
    (*
      We give the function \<open>stack_reachable\<close> which states that the nodes on the stack form a path.
      As a matter of fact, any two entries on the stack will form a path.
      This is obvious, as we only ever push a successor of the current top node.
    *)
    definition "stack_reachable w = (\<forall>ws p v ws'. w = ws @ (p,v) # ws' \<longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v})"
    
    lemma stack_reachableD: assumes "stack_reachable w" "w = ws @ (p,v) # ws'" shows "snd ` (set ws) \<subseteq> g\<^sup>+``{v}"
      using assms
      unfolding stack_reachable_def
      by blast
    
    lemma stack_reachableI: assumes "\<And>ws p v ws'. w = ws @ (p,v) # ws' \<Longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v}" shows "stack_reachable w"
      using assms
      unfolding stack_reachable_def
      by blast
    
    lemma stack_reachableI':
      assumes "\<And>w\<^sub>1 w\<^sub>2 w\<^sub>3 p\<^sub>1 p\<^sub>2 u\<^sub>1 u\<^sub>2. w = w\<^sub>1@(p\<^sub>1,u\<^sub>1)#w\<^sub>2@(p\<^sub>2,u\<^sub>2)#w\<^sub>3 \<Longrightarrow> (u\<^sub>2,u\<^sub>1)\<in>g\<^sup>+"
      shows "stack_reachable w"
      using assms unfolding stack_reachable_def
      by (auto simp: in_set_conv_decomp)


    (*
      EXERCISE:
      You are now given the DFS algorithm from the lecture. Try to adapt it such that it will set a
      flag to True if there is a loop in the graph, and to False otherwise. If the flag is set to
      True, the while loop should be immediately broken.
    *)

(*
  Due to time constraints, we only tackled soundness of the algorithm. The completeness proof
  necessary to show correctness is uploaded separately.
*)
      
    definition "dfs_aux \<equiv> 
      while_option (\<lambda>(c,d,w). w\<noteq>[] \<and> \<not>c) (\<lambda>(c,d,w). 
        let ((p,v),w) = (hd w, tl w) in
        case p of 
          [] \<Rightarrow> (False, rbts_insert v d, w)
        | u # p \<Rightarrow> (
          if rbts_member u d then (False,d, (p,v) # w)
          else if u \<in> (snd ` set w) then (True, d, w)
          else
            let w = (rg_succs gi u, u) # (p,v) # w in
            (False,d,w)
      
      )) (False,rbts_empty,[(rg_succs gi v\<^sub>0, v\<^sub>0)]) 
    "
  
    definition "dfs \<equiv> fst (the (dfs_aux))"

    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" using rg_finite_ran[of gi] by simp
      finally (finite_subset) show ?thesis .
    qed        
      
(*
  This is an invariant that can prove correctness of our loop algorithm.
  Remember: You can always make your invariant stronger as long as the additions are actually 
  preserved over iterations. This may make the proof that the goal follows from the invariant 
  (step 3) easer in some cases, but it will definitely make the invariant preservation step (step 2)
  harder as you need to do more work.

  An invariant that is too weak however, will deny you to prove that the goal follows from the 
  invariant. As we saw in the lecture: if we use \<open>dfs_find_loop_invar_sound\<close> instead of 
  \<open>dfs_find_loop_invar\<close>, we suddenly cannot prove that there is a loop if c is set to False.
  Try it our yourself, it should become easily visible why the invariant cannot prove the statement
  (HINT: it assumes that c is True, what does this say about the value that \<open>has_loop\<close> should have?
  This will usually become clear in the proof that the goal follows from the invariant (step 3).
*)
    definition "dfs_find_loop_invar_sound = (\<lambda> (c,d,w). 
      rbts_\<alpha> d \<union> snd ` set w \<subseteq> g\<^sup>*``{v\<^sub>0}
    \<and> (\<forall>(p,v) \<in> set w. set p \<subseteq> g `` {v})
    \<and> stack_reachable w)"


    definition "dfs_find_loop_invar = (\<lambda> (c,d,w). if c then has_loop g v\<^sub>0 else dfs_find_loop_invar_sound (c,d,w))"

    thm Cons_eq_append_conv

    lemma invar_init: "dfs_find_loop_invar_sound (False, rbts_empty, [(rg_succs gi v\<^sub>0, v\<^sub>0)])"
      unfolding dfs_find_loop_invar_sound_def
      apply (auto simp: stack_reachable_def Cons_eq_append_conv)
      done

(*
  Note that we were able to leave out some assumptions here like \<open>s_ = (False, d, ([], v) # w)\<close> and
  \<open>\<not> x_\<close> because they are not used anywhere else in the statement. Because of this, the following
  lemma suffices to prove the first subgoal for invariant preservation.
*)
    lemma invar_step1: "dfs_find_loop_invar (False, d, ([], v) # w) \<Longrightarrow> dfs_find_loop_invar (False, rbts_insert v d, w)"
      sorry

 
    definition "dfs_wfrel \<equiv> 
      inv_image 
        ({(True,False)} <*lex*> finite_psubset <*lex*> less_than <*lex*> less_than) 
        (\<lambda> (c,d,w). (c, g\<^sup>*``{v\<^sub>0} - rbts_\<alpha> d, length w, length (fst (hd w))))"

    find_theorems "_ \<noteq> []" "_ # _"
      
    lemma dfs_aux_sound: "\<exists>d w c. dfs_aux = Some (c,d,w) \<and> (c \<longrightarrow> has_loop g v\<^sub>0)"
      unfolding dfs_aux_def
      apply (rule while_option_rule'[where P=dfs_find_loop_invar and r=dfs_wfrel])
      subgoal (*step 1: invariant holds initially*) 
        by(simp add: invar_init dfs_find_loop_invar_def)
      subgoal (*step 2: invariant is preserved after iteration*) 
        apply(auto simp: neq_Nil_conv split!: if_splits list.splits)
        subgoal for _ d v w by(simp add: invar_step1)
        sorry
      subgoal (*step 3: goal follows from invariant*) 
        apply(auto simp: dfs_find_loop_invar_def dfs_find_loop_invar_sound_def)
        done
      subgoal (*step 4: variant relation is wellfounded*) 
        by(auto simp add: dfs_wfrel_def)
      subgoal (*step 5: the operations on the variant bring us closer to termination *) sorry
      done

    lemma dfs_sound: "dfs \<longrightarrow> has_loop g v\<^sub>0"  
      unfolding dfs_def using dfs_aux_sound by force     
  
  end  
  
  
            
end
