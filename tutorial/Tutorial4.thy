theory Tutorial4
  imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin



(*
  To warm up for today, let's prove that the square root of 2 is irrational!

  HINT: The Isabelle proof can be found online, please try it yourself first!
*)

lemma sqrt_2_irrational: "sqrt 2 \<notin> \<rat>"
  sorry




(*
  Next up, we will define a language inductively. The alphabet contains merely parentheses and the 
  grammar matches every parenthesis open (LPAR) with a parenthesis close (RPAR)
*)

datatype par = LPAR | RPAR


(*
  Define the grammar S in an inductive way, the context-free grammar looks as follows:
  S \<rightarrow> LPAR S LPAR | S S | LPAR RPAR
*)
inductive S :: "par list \<Rightarrow> bool" where
  "S undefined"



(*
  Let's first prove some simple lemmas.
*)

lemma S_not_empty [simp]: "\<not>S []"
  oops


lemma S_not_LPAR[simp]: "\<not>S [LPAR]"
  oops


lemma "S [LPAR, LPAR, LPAR, RPAR, RPAR, x] \<Longrightarrow> x = RPAR"
  oops


(*
  We can also use a function to determine whether all parentheses match. \<open>match_parantheses\<close> accepts
  a list of symbols \<open>ws\<close> and a natural number \<open>n\<close> by incrementing n if it reads LPAR and decrementing
  n when it reads RPAR. If we try to decrement 0, we return False, as we have an unmatched RPAR then.
  If n does not equal 0 when we reach the end of the list, we have an unmatched LPAR and also return
  False.

  We assume you know how to define this by now. There's nothing new here.
*)
  

fun match_parentheses :: "par list \<Rightarrow> nat \<Rightarrow> bool" where 
  "match_parentheses [] n \<longleftrightarrow> n = 0" |
  "match_parentheses (w # ws) n = (case w of 
    LPAR \<Rightarrow> match_parentheses ws (Suc n) | 
    RPAR \<Rightarrow> (case n of 
      0 \<Rightarrow> False | 
      Suc m \<Rightarrow> match_parentheses ws m
    )
  )"


(*
  This function does exactly the same as the inductive definition, unless the input list is empty. 
  Prove this!

  WARNING: This is tricky!
*)


lemma S_to_match_parentheses: "S xs \<Longrightarrow> match_parentheses xs 0"
  oops

lemma match_parentheses_to_S: "match_parentheses xs 0 \<Longrightarrow> xs = [] \<or> S xs"
  oops

lemma "match_parentheses xs 0 \<longleftrightarrow> xs = [] \<or> S xs"
  oops


end


(*
  Basic Isar
  induction with isar
  cases with isar
*)