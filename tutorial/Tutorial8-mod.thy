theory "Tutorial8-mod"
  imports Main "HOL-Library.Monad_Syntax" "HOL-Library.While_Combinator" "HOL-Library.Char_ord"
begin


  (*
    You have seen the state error monad during last weeks tutorial. We are now going to look
    at a different application, the parser. The parser monad is a function that maps a string to
    an output and another string, just like a parser does.
  *)
  
  datatype ('a,'s) senM = COMP (run: "'s \<Rightarrow> ('a \<times> 's) option option")
  
  definition "return x = COMP (\<lambda>s. Some (Some (x,s)))"
  definition "nonterm = COMP (\<lambda>_. None)"
  definition "fail = COMP (\<lambda>_. Some None)"
  
  (*
    To recap, we implements the bind function
  *)
  definition "bind_senM m f = COMP (\<lambda> s. case run m s of
    None \<Rightarrow> None
  | Some None \<Rightarrow> Some None
  | Some (Some (x,s')) \<Rightarrow> run (f x) s')"
  
  adhoc_overloading bind bind_senM


  (*
    When reading symbols, we want to have an option to read an alternative symbol when the 
    initial parsing attempt fails. \<open>then_else\<close> executes p1 and applies function f to the output 
    value. 
  *)
  definition then_else :: "('c, 'b) senM \<Rightarrow> ('c \<Rightarrow> ('a, 'b) senM) \<Rightarrow> ('a, 'b) senM \<Rightarrow> ('a, 'b) senM" where
    "then_else p1 f p2 = COMP (\<lambda>s. do {
      ra \<leftarrow> run p1 s;
      case ra of
        None \<Rightarrow> (run p2 s)
      | Some (x,s) => run (f x) s
    })"

  definition alternative (infixr "<|>" 60) where "alternative a b \<equiv> then_else a return b"

  definition "get = COMP (\<lambda>s. Some (Some (s,s)))"
  definition "put s = COMP (\<lambda>_. Some (Some ((),s)))"
  
          
  (*
    We define the weakest precondition a bit differently. Instead of returning False when a program
    does not terminate, we return lib. If lib set to True, we do not care whether a program 
    terminates. We also additionally have a function F which is the value that we return if the
    program terminates, but crashes. It can be seen as a way to deal with invalid programs.
  *)
  definition wp_gen :: "bool \<Rightarrow> ('b, 's) senM \<Rightarrow> ('b \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> 's \<Rightarrow> bool" where
  "wp_gen lib m Q F s = (case run m s of
    None \<Rightarrow> lib
  | Some None \<Rightarrow> F
  | Some (Some (x,s)) \<Rightarrow> Q x s)"

  abbreviation "wp\<equiv>wp_gen False"    
  abbreviation "wlp\<equiv>wp_gen True"
    
  
  (*
    We present some monad and wp rules. These are general rules that always follow a particular 
    pattern.
  *)

  context 
    notes [simp] = bind_senM_def return_def fun_eq_iff wp_gen_def nonterm_def fail_def then_else_def
                   get_def put_def alternative_def
    notes [split] = option.splits Option.bind_splits
  begin

    lemma nonterm_then_else[simp]: "then_else nonterm b c = nonterm" by simp
    lemma return_then_else[simp]: "then_else (return x) b c = b x" by simp
    lemma fail_then_else[simp]: "then_else fail b c = c" by simp
    
    (* TODO: There must be assoc-laws for then-else! *)
    
    lemma then_else_is_bind[simp]: "then_else m f fail = m \<bind> f" by simp

    lemma nonterm_bind[simp]: "do {x\<leftarrow>nonterm; f x} = nonterm" by simp
    lemma fail_bind[simp]: "do {x\<leftarrow>fail; f x} = fail" by simp
        
    lemma return_bind[simp]: "do {x\<leftarrow>return v; f x} = f v" by simp
    lemma bind_return[simp]: "do {x\<leftarrow>m; return x} = m" apply(cases m) by (simp)
    lemma bind_assoc[simp]: "do {y\<leftarrow>do{x\<leftarrow>m; f x}; g y} = do {x \<leftarrow> m; y \<leftarrow> f x; g y}" for m :: "(_,_) senM" by simp


    lemma wlp_trivial [simp]: "wlp m (\<lambda> _ _. True) True s"
      by simp

    
    lemma wp_return[simp]: "wp_gen l (return x) Q F s \<longleftrightarrow> Q x s" by simp
    lemma wp_noterm[simp]: "wp_gen l nonterm Q F s \<longleftrightarrow> l" by simp
    lemma wp_fail[simp]: "wp_gen l fail Q F s \<longleftrightarrow> F" by simp

    lemma wp_then_else[simp]: "wp_gen l (then_else a b c) Q F s = wp_gen l a (\<lambda> x s. wp_gen l (b x) Q F s) (wp_gen l c Q F s) s"
      by auto

    lemma wp_alternative [simp]: "wp_gen l (a <|> b) Q F s \<longleftrightarrow> wp_gen l a Q (wp_gen l b Q F s) s" by simp
        
    lemma wp_bind[simp]: "wp_gen l (do {x\<leftarrow>m; f x}) Q F s = wp_gen l m (\<lambda> x s. wp_gen l (f x) Q F s) F s" by simp

    lemma wp_get[simp]: "wp_gen l get Q F s = Q s s" by simp
    lemma wp_put[simp]: "wp_gen l (put s) Q F s' = Q () s" by simp
    
        
  end








  (*
    The following lemmas are setup for the rest of the exercise. We suggest that you ignore
    it for now as it is only used to handle non-terminating behaviour in partial functions.
    We will never actually need these lemmas in our proofs.
  *)
  
  find_theorems partial_function_definitions "_::_ option"
  abbreviation "option_lub \<equiv> flat_lub None"  

  definition "sen_ord = img_ord run (fun_ord option_ord)"
    
  definition "sen_lub = img_lub run COMP (fun_lub option_lub)"
  
  abbreviation "mono_sen \<equiv> monotone (fun_ord sen_ord) sen_ord"
  
  
  lemma sen_pfd: "partial_function_definitions sen_ord sen_lub"
    unfolding sen_ord_def sen_lub_def
    apply (rule partial_function_image)
    apply (rule partial_function_lift)
    apply (rule flat_interpretation)
    apply (rule senM.expand; simp)
    by simp

  lemma sen_lub_empty: "sen_lub {} = nonterm"
    unfolding sen_lub_def nonterm_def img_lub_def flat_lub_def fun_lub_def
    by (auto simp: fun_eq_iff)
    
  interpretation senM:
    partial_function_definitions sen_ord sen_lub
    rewrites "sen_lub {} \<equiv> nonterm"
    apply (rule sen_pfd)
    by (simp add: sen_lub_empty)

  lemma admissible_flat: "ccpo.admissible (flat_lub None) (flat_ord None) P"  
    apply (rule ccpo.admissibleI)
    by (metis equals0I flat_lub_in_chain flat_ord_def option.lub_upper)
    
    
  lemma wlp_admissible: 
    "senM.admissible (\<lambda>Uf. \<forall>x. All (wlp (Uf x) (P x) (E x)))"
    apply (rule admissible_fun[OF sen_pfd])
    unfolding sen_ord_def sen_lub_def
    apply (rule admissible_image)
    apply (rule partial_function_lift)
    apply (rule flat_interpretation)
    apply (simp add: comp_def wp_gen_def)
    
    apply (rule admissible_fun)
    apply (rule flat_interpretation)
    apply (rule admissible_flat)
    
    apply (rule senM.expand; simp)
    by simp
    
  lemma wlp_nonterm: "wlp nonterm P E s" by simp

  (* TODO: If we try to use this lemma for partial_function induction rule,
       the package fails, seemingly expecting variable at head position of goal.
       Is this behaviour expected/necessary?
  *)
  lemma fixp_induct_senM:
    fixes F :: "'c \<Rightarrow> 'c" and
      U :: "'c \<Rightarrow> 'b \<Rightarrow> ('a,'s) senM" and
      C :: "('b \<Rightarrow> ('a,'s) senM) \<Rightarrow> 'c" and
      P :: "'b \<Rightarrow> 'a \<Rightarrow> 's \<Rightarrow> bool"
    assumes mono: "\<And>x. mono_sen (\<lambda>f. U (F (C f)) x)"
    assumes eq: "f \<equiv> C (ccpo.fixp (fun_lub sen_lub) (fun_ord sen_ord) (\<lambda>f. U (F (C f))))"
    assumes inverse2: "\<And>f. U (C f) = f"
    assumes step: "\<And>f x s. \<lbrakk>\<And>x s. wlp (U f x) (P x) (E x) s\<rbrakk> \<Longrightarrow> wlp (U (F f) x) (P x) (E x) s"
    shows "wlp (U f x) (P x) (E x) s"
    apply (rule senM.fixp_induct_uc[of U F C, OF mono eq inverse2, where P="\<lambda>Uf. \<forall>x s. wlp (Uf x) (P x) (E x) s", rule_format])
    apply (rule wlp_admissible)
    apply (rule wlp_nonterm)
    subgoal using step .
    done
              
  declaration \<open>Partial_Function.init "senM" \<^term>\<open>senM.fixp_fun\<close>
    \<^term>\<open>senM.mono_body\<close> @{thm senM.fixp_rule_uc} @{thm senM.fixp_induct_uc}
    (NONE)\<close>
    
  
  lemma bind_senM_mono[partial_function_mono]:
    assumes mf: "mono_sen B" and mg: "\<And>y. mono_sen (\<lambda>f. C y f)"
    shows "mono_sen (\<lambda>f. bind_senM (B f) (\<lambda>y. C y f))"
    supply [[show_abbrevs=false]]
    apply (rule monotoneI)
    using assms
    apply (auto simp: fun_ord_def sen_ord_def img_ord_def flat_ord_def bind_senM_def)
    apply (auto split: option.splits simp: fun_ord_def monotone_def)
    apply (metis option.distinct(1))
    subgoal by (metis option.discI)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.sel prod.sel(1) prod.sel(2))
    done

    
  lemma then_else_mono[partial_function_mono]:
    assumes ma: "mono_sen A" and mb: "\<And>y. mono_sen (\<lambda>f. B y f)" and mc: "mono_sen C"
    shows "mono_sen (\<lambda>f. then_else (A f) (\<lambda>y. B y f) (C f))"
    supply [[show_abbrevs=false]]
    apply (rule monotoneI)
    using assms
    apply (auto simp: fun_ord_def sen_ord_def img_ord_def flat_ord_def then_else_def)
    apply (auto split: option.splits Option.bind_splits simp: fun_ord_def monotone_def)
    subgoal by (metis option.distinct(1))
    subgoal by (metis option.discI)
    subgoal by blast
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.inject)
    subgoal by (metis option.discI option.sel prod.sel(1) prod.sel(2))
    done
  
  lemma alternative_mono[partial_function_mono]: 
    assumes "mono_sen A" "mono_sen B"
    shows "mono_sen (\<lambda>f. A f <|> B f)"
    unfolding alternative_def
    by (intro partial_function_mono assms)
        
  context
    notes [simp] = alternative_def
  begin  
    lemma nonterm_alt[simp]: "nonterm <|> b = nonterm" by simp
    lemma fail_alt[simp]: "fail <|> b = b" by simp
    lemma return_alt[simp]: "return x <|> b = return x" by simp
    
    lemma alt_fail[simp]: "m <|> fail = m" by simp
    
    lemma alt_assoc[simp]: "(a <|> b) <|> c = a <|> b <|> c"
      apply simp
      unfolding then_else_def
      by (auto simp: fun_eq_iff return_def split: Option.bind_splits option.splits)
  end    

  (*
    End of setup!
  *)

  definition "assert P = (if P then return () else fail)"
  
  lemma wp_assert[simp]: "wp_gen l (assert P) Q F s \<longleftrightarrow> (P \<longrightarrow> Q () s) \<and> (\<not>P \<longrightarrow> F)"
    unfolding assert_def by simp
  
  
  
  (* Building a parser:
    An elementary operation is to read the next character on a string and return it as a value.
    The rest of the string is untouched and returned as the new state.
  *)
  
  definition "read \<equiv> do {
    xs \<leftarrow> get;
    assert (xs \<noteq> []);
    put (tl xs);
    return (hd xs)
  }"
  
  (* Setup: Word around ML's 'value restriction': no polymorphic values, i.e., functions without args *)
  lemmas [code del] = read_def   
  definition "read_code (_::unit) \<equiv> read"
  lemma [code_unfold]: "read = read_code ()" by (simp add: read_code_def)
  lemmas [code] = read_code_def[unfolded read_def]

  lemma wp_read[simp]: "wp_gen l read Q F s \<longleftrightarrow> (case s of [] \<Rightarrow> F | x#xs \<Rightarrow> Q x xs)" 
    unfolding read_def
    apply (auto split: list.splits)
    done
    
  
  (*
    rng is a function that reads a symbol and checks whether the symbol is in a given set.
    If it is not, the parser will abort. We achieve this using an assert.
  *)

  definition "rng S \<equiv> do{
    x \<leftarrow> read;
    assert (x \<in> S);
    return x
  }"

  lemma wp_rng [simp]: "wp_gen l (rng S) Q F s \<longleftrightarrow> (case s of [] \<Rightarrow> F | x#xs \<Rightarrow> if x\<in>S then Q x xs else F)"
    apply (auto simp: rng_def split: list.splits)
    done
  
  (*
    Function many reads characters as long as they match the parsers requirement. E.g. many (rng S)
    keeps reading characters as long as they are in S. If they aren't, function many will terminate.
    This is why we define many as a partial function. It is not given that it will terminate.
    To make sure that we can use induction, we have to define the function in a way that it 
    understands what to "return" when the program does not terminate.
  *)
  partial_function (senM) many where
    [code]: "many p = do {x \<leftarrow> p; xs \<leftarrow> many p; return (x#xs)} <|> return []"



  (* UNFINISHED IN TUTORIAL
    A program that satisfies this condition will never fail. It may never terminate (due to wlp).
  *)
  abbreviation "cannot_fail p \<equiv> 
    (\<forall>s. wlp p (\<lambda> _ _. True) False s)"

  (* UNFINISHED IN TUTORIAL
    \<open>many\<close> can never fail (as soon as it fails, it returns an empty string
  *)
  lemma many_no_fail [simp]: "cannot_fail (many p)"
    apply(subst many.simps)
    by auto

  (* UNFINISHED IN TUTORIAL
    A general lemma about then_else. We can use this to convert the proof goal after the first apply 
    in lemma \<open>many_unfold\<close> to a trivial proof.
  *)
  lemma then_else_eq_bind: "\<forall>x. cannot_fail (b x) \<Longrightarrow> 
    then_else a b c = bind a b <|> c"
    unfolding then_else_def alternative_def return_def 
      bind_senM_def wp_gen_def
    apply (auto simp: fun_eq_iff 
      split: option.splits Option.bind_splits)
    done

  lemma many_unfold: "many p = then_else p 
    (\<lambda> x. do {xs \<leftarrow> many p; return (x#xs)}) (return [])"
    apply(subst many.simps)
    apply (auto simp: then_else_eq_bind)
    done
  
  (* Boilerplate required for induction lemma. There seems to be a limitation in partial_function induction lemma generation,
     leaving it unable to generate wlp lemmas.
  
     Maybe, work with REC-combinator directly!? 
  *)  
  lemmas many_wlp_induct = many.fixp_induct[OF wlp_admissible, rule_format, OF wlp_nonterm]
  
  value "run (many (rng {CHR ''a''..CHR ''z''})) ''abcdXX''"
 
  
  (*
    We define some programs now. \<open>char\<close> reads only the given character. \<open>digit\<close> only reads digits.
    \<open>alpha\<close> only reads lower and upper case symbols. \<open>alphanum\<close> reads both from digit and alpha.
    \<open>identifier\<close> reads words that start with an alpha and end with an arbitrary number of alphanum.
    Those are given as examples.
  *)
  definition "char c = rng {c}"

  abbreviation "digits \<equiv> {CHR ''0''..CHR ''9''}"
  abbreviation "lowercase_letters \<equiv> {CHR ''a''..CHR ''z''}"
  abbreviation "uppercase_letters \<equiv> {CHR ''A''..CHR ''Z''}"
  abbreviation "letters \<equiv> lowercase_letters \<union> uppercase_letters"
  
  definition "digit = rng (digits)"
  definition "alpha = rng lowercase_letters <|> rng uppercase_letters"
  definition "alphanum = digit <|> alpha"
  
  definition "identifier = do { c\<leftarrow>alpha; cs\<leftarrow>many alphanum; return (c#cs) }"
  

  (*
    Our task is now to parse a list of identifiers that is typed out as a string and convert it
    to an actual list using the parser.
  *)
  definition "enclose pleft pright p = do {pleft; x \<leftarrow> p; pright; return x}"

  definition "separate c p = do {x \<leftarrow> p; xs \<leftarrow> many (c \<then> p); return (x # xs)} <|> return []"
  
  definition "list p = enclose (char CHR ''['') (char CHR '']'') (separate (char CHR '','') p)"


  lemma wp_char[simp]: "wp (char c) Q F s \<longleftrightarrow> 
      (case s of [] \<Rightarrow> F 
    | x # xs \<Rightarrow> (if x = c then Q x xs else F))"
    unfolding char_def
    apply (auto split: list.splits)
    done
    
  lemma wp_alpha [simp]: "wp alpha Q F s \<longleftrightarrow> (case s of [] \<Rightarrow> F | x # xs \<Rightarrow> (if x \<in> letters then Q x xs else F))"
    unfolding alpha_def
    apply (auto split: list.splits)
    done


  lemma wp_identifier[simp] : "wp (identifier) Q F s \<longleftrightarrow> wp alpha (\<lambda>c. wp (many alphanum) (\<lambda>cs. Q (c # cs)) F) F s"
    unfolding identifier_def
    apply simp
    done

  lemma wp_separate [simp]: "wp (separate psep p) Q F s \<longleftrightarrow> wp p (\<lambda>x. wp (many (psep \<bind> (\<lambda>_. p))) (\<lambda>xs. Q (x # xs)) (Q [] s)) (Q [] s) s"
    unfolding separate_def
    apply simp
    done

  lemma wp_enclose [simp]: "wp (enclose pl pr p) Q F s \<longleftrightarrow> wp pl (\<lambda>_. wp p (\<lambda>x. wp pr (\<lambda>_. Q x) F) F) F s"
    unfolding enclose_def
    apply simp
    done
  
  value "run (list identifier) ''[a,b,hd,xha89]''" 
  
  fun pretty_commas where
    "pretty_commas [] = ''''"
  | "pretty_commas [x] = [x]"
  | "pretty_commas (x#xs) = x # '','' @ pretty_commas xs"
  

  (*
    We finished the proof. It's possible to finish the proof with merely simp and many_unfold due to
    the way we set up the wp in the simp set. This demonstrates the power of the wp and monads.
    We have proven that parsing a pretty printed list of characters yields that same list of 
    characters.
  *)
  lemma "CHR '','' \<notin> set xs \<Longrightarrow> wp (separate (char CHR '','') (read)) (\<lambda> a b. a = xs \<and> b = []) False (pretty_commas xs)"
    apply(induction xs rule: pretty_commas.induct)
    apply simp
    apply simp
    apply(subst many_unfold)
    apply simp
    apply (simp split: list.splits)
    apply(subst many_unfold)
    apply simp
    done



  (*
    As an added bonus, we did the same proof for arbitrary strings. We now need wp_cons to split up
    wp lemmas. This is caused by the fact that we have to deal with lemmas in the form of:
    lemma a: \<open>assumption1 \<Longrightarrow> wp m Q1 F1 s\<close>

    For example: if we have a proof obligation wp m (\<lambda> x s. wp n Q1 F1 s) F s we can apply wp_cons
    and use lemma a to obtain a proof goal that contains \<open>assumption1\<close> which may be easier to proof.
    We show this in an example below.
  *)

  lemma wp_gen_cons:
    assumes "wp_gen l m Q F s"
    assumes "l \<Longrightarrow> l'"
    assumes "\<And>x s. Q x s \<Longrightarrow> Q' x s"
    assumes "F \<Longrightarrow> F'"
    shows "wp_gen l' m Q' F' s"
    using assms
    apply(cases m)
    apply (auto simp: wp_gen_def split: option.splits)
    done

  lemma wp_cons:
    assumes "wp m Q F s"
    assumes "\<And>x s. Q x s \<Longrightarrow> Q' x s"
    assumes "F \<Longrightarrow> F'"
    shows "wp m Q' F' s"
    using wp_gen_cons[of False m Q F s False Q' F']
    using assms
    apply auto
    done

  lemma wlp_cons:
    assumes "wlp m Q F s"
    assumes "\<And>x s. Q x s \<Longrightarrow> Q' x s"
    assumes "F \<Longrightarrow> F'"
    shows "wlp m Q' F' s"
    using wp_gen_cons[of True m Q F s True Q' F']
    using assms
    apply auto
    done

  (*
    Defining a general wp lemma for \<open>many\<close> may be possible, but since p can be anything, a lot of 
    edge cases may pop up. It's easier to work with if we have a hint of what p can be. If it is 
    \<open>rng\<close> we can be sure that it terminates with certain behaviour.
  *)
  lemma wp_many: "wp (many (rng S)) Q F s \<longleftrightarrow> 
    Q (takeWhile (\<lambda> s. s \<in> S) s) (dropWhile (\<lambda> s. s \<in> S) s)"
    apply(induction s arbitrary: Q F)
    apply(simp)
    apply(subst many_unfold)
    apply simp
    apply(subst many_unfold)
    by clarsimp 

  (*
    Some introduction lemmas that we found through trial and error.
  *)
  lemma wp_many_rngI1: "set xs \<subseteq> S \<Longrightarrow> 
    wp (many (rng S)) (\<lambda>xs' ys'. xs'=xs \<and> ys'=[]) False xs"
    by (auto simp: wp_many)
  
  lemma wp_many_rngI2: "zs=xs@t#ys \<Longrightarrow> set xs \<subseteq> S \<Longrightarrow> t\<notin>S \<Longrightarrow> 
    wp (many (rng S)) (\<lambda>xs' ys'. xs'=xs \<and> ys'=t#ys) False zs"
    by (auto simp add: wp_many subset_code)

  lemma wp_many_emptyI: "wp p (\<lambda>_ _. False) True [] \<Longrightarrow> 
    wp (many p) (\<lambda>r s. r=[] \<and> s=[]) False []"
    apply (subst many_unfold)
    apply simp
    apply (erule wp_cons)
    by auto


  (*
    We adapted pretty_commas to pretty print lists of strings rather than characters.
  *)
  fun pretty_commas' where
    "pretty_commas' [] = ''''"
  | "pretty_commas' [x] = x"
  | "pretty_commas' (x#xs) = 
    x @ '','' @ pretty_commas' xs"

 value "pretty_commas' []"
 value "run (separate (char CHR '','') (many (rng letters))) (pretty_commas' [])"



  lemma "\<forall>x\<in>set xs. x\<noteq>'''' \<and> set x \<subseteq> letters \<Longrightarrow> xs\<noteq>[] \<Longrightarrow> 
    wp (separate (char CHR '','') (many (rng letters))) 
    (\<lambda> a b. a = xs \<and> b = []) False (pretty_commas' xs)"
    apply(induction xs rule: pretty_commas'.induct)
    apply simp
    apply (simp split: list.splits) (*We have an embedded wp here, but the only wp lemmas on many are introduction rules*)
    apply (rule wp_cons) (*We can use wp_cons to help us out. Now we need to find a Q' and F' that can prove our lemma*)
    apply (rule wp_many_rngI1) (* Lemma \<open>wp_many_rngI1\<close> has one assumption that we know to be true, so it's easy to prove. *)
    apply auto[2] (* Now, we use the \<open>Q=(\<lambda>xs' ys'. xs'=xs \<and> ys'=[])\<close> of lemma \<open>wp_many_rngI1\<close> in the wp, making it easier to use.*)
    apply (rule wp_many_emptyI)
    apply simp
    apply simp

    apply clarsimp
    apply (rule wp_cons)
    apply (rule wp_many_rngI2[OF refl])
    apply simp
    apply simp

    apply clarsimp
    apply (subst many_unfold)
    apply simp

    apply simp
    done


end