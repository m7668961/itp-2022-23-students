section "Arithmetic and Boolean Expressions"

theory "Tutorial5" imports Main "HOL-Library.RBT"
begin


(* 
  ANNOUNCEMENT: Next week's homework (sheet 6) will be a 'Come up with your own proof' exercise 
  where you can apply   everything you've learned so far to something that interests you.
  Think of something from a hackathon, Youtube video or a course at the UT. We will also give you
  some hints on next week's sheet.
  It's good to be ambitious, but also be realistic, a proof will always take longer than expected.
*)


(*
  We have seen this graph definition before, we define a graph as a set of edges.
*)

type_synonym 'v graph = "('v\<times>'v) set"

(*
  We have seen this definition for paths in an LTS in Homework 4. An extended version with symbols
  is available (named \<open>word_dir\<close>) in Homework 3.
*)

fun path :: "('a \<times> 'a) set \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> 'a \<Rightarrow> bool" where
  "path \<delta> p [] q \<longleftrightarrow> p=q"
| "path \<delta> p (p'#ps) q \<longleftrightarrow> ((p,p')\<in>\<delta> \<and> path \<delta> p' ps q)"


(*
  Additionally, we can define weights on our paths like in the function \<open>cost c p ps\<close>. It takes a
  function that maps edges to costs.
*)

fun cost :: "('a \<times> 'a \<Rightarrow> nat) \<Rightarrow> 'a \<Rightarrow> 'a list \<Rightarrow> nat" where
  "cost _ _ [] = 0"
| "cost c p (p'#ps) = (c (p,p') + cost c p ps)"


(*
  Since all edges have nonnegative costs, there must be a path with with minimal costs. This is
  exactly what we prove with this lemma. If there is a path between two nodes p and q, then there 
  is a path whose costs are as low or lower than the costs of any other path between those nodes.

  Try to prove this lemma, you may find something odd about this prove, that's totally possible.
  Try to figure out what it is.
*)

lemma 
  assumes EXP: "\<exists> xs. path \<delta> p xs q"
  shows "\<exists>xs. path \<delta> p xs q \<and> (\<forall> ys. path \<delta> p ys q \<longrightarrow> cost c p xs \<le> cost c p ys)"
  oops


(*
  We already hinted at something strange that is going on in the previous lemma. Try to do the
  proof again, maybe we can generalize something here. Use the general lemma to prove the
  following statement.
*)


lemma 
  assumes EXP: "\<exists> xs. path \<delta> p xs q"
  shows "\<exists>xs. path \<delta> p xs q \<and> (\<forall> ys. path \<delta> p ys q \<longrightarrow> cost c p xs \<le> cost c p ys)"
  oops





(*
  Next up, we want to show how to refine our graph to a more concrete implementation.
  We can implement a graph as a list of edges, but also as an RBT, where the keys are states. The
  values are lists of states. The lists contain the successor states of the key.

  First of all, we define some abstract operations of the graph.
*)

definition empty_graph :: "'v graph" where 
  "empty_graph \<equiv> {}"


definition add_edge :: "('v\<times>'v) \<Rightarrow> 'v graph \<Rightarrow> 'v graph" where
  "add_edge e E \<equiv> E \<union> {e}"  


text \<open>The refinement to a list is analogous. We also define the according refined operations.\<close>

type_synonym 'v list_graph = "('v\<times>'v) list"

definition lg_\<alpha> :: "'v list_graph \<Rightarrow> 'v graph" where
  "lg_\<alpha> xs = undefined"


definition lg_empty :: "'v list_graph" where 
  "lg_empty \<equiv> undefined"


definition lg_add_edge :: "('v\<times>'v) \<Rightarrow> 'v list_graph \<Rightarrow> 'v list_graph" where 
  "lg_add_edge \<equiv> undefined"



(*
  Ofcourse, we also need to show that this is actually a valid refinement.
*)
context
  notes [simp] = lg_\<alpha>_def empty_graph_def lg_empty_def lg_add_edge_def add_edge_def
begin

lemma lg_empty_correct [simp]: "lg_\<alpha> lg_empty = empty_graph"
  by(auto)

lemma lg_add_edge_correct [simp]: "lg_\<alpha> (lg_add_edge e E) = add_edge e (lg_\<alpha> E)"
  by(auto)

end


(*  
  The list data structure allows us to efficiently iterate over the edges, but if we want to
  find adjacent vertices we have to iterate through the entire set of edges. This is very 
  inefficient. Alternatively, we can use a different data structure that maps each node it a list 
  of adjacent nodes.
*)

type_synonym 'v rbt_graph = "('v,'v list) rbt"

definition rg_\<alpha> :: "'v::linorder rbt_graph \<Rightarrow> 'v graph"  where 
  "rg_\<alpha> t \<equiv> undefined"


definition rg_empty :: "'v::linorder rbt_graph" where 
  "rg_empty \<equiv> undefined"


definition rg_add_edge :: "('v::linorder\<times>'v) \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v rbt_graph" where
 "rg_add_edge \<equiv> undefined"



context
  notes [simp] = rg_\<alpha>_def rg_empty_def rg_add_edge_def add_edge_def
begin

lemma [simp]: "rg_\<alpha> rg_empty = {}"
  oops

lemma rg_add_edge_\<alpha> [simp]: "rg_\<alpha> (rg_add_edge x xt) = add_edge x (rg_\<alpha> xt)"
  oops

end



(*
  We expect the rbt graph and the list graph to behave equivalently. We can test this by 'parsing'
  the list graph into an rbt graph and checking that they are equal.
  This parsing function is \<open>read_lst\<close> which reads edges from the list and adds them to the rbt
  graph.
*)

definition read_lst :: \<open>'v::linorder list_graph \<Rightarrow> 'v rbt_graph\<close> where 
(*<*)
  "read_lst xs \<equiv> fold rg_add_edge xs rg_empty"
(*>*)

  
(*
  We now show that \<open>read_lst\<close> preserves the abstract graph by showing that the abstraction of the
  list graph is the same as the abstraction of the rbt graph that is parsed from that same list
  graph. We have visualized this procedure below:

  lg - read \<rightarrow> rg
  |            |
 lg_\<alpha>         rg_\<alpha>
  |            |
  g     =      g

*)
  


lemma \<open>rg_\<alpha> (read_lst xs) = lg_\<alpha> xs\<close> 
  oops








(* If time permits, we move on to sets implemented as RBTs. You have already seen this in the 
  lecture. We define the set as a \<open>('a,unit) rbt\<close>, which means that the RBT contains no data, only
  keys. We test for membership by looking whether the key is available in the RBT. For insert/delete
  we add/remove the key from the RBT. *)


(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_set t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
lemma rbts_insert_correct:
  "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
lemma rbts_delete_correct:
  "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)


(* We added a function to test whether an rbt set is empty *)
definition "rbts_is_empty t \<equiv> RBT.is_empty t"

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_set t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_set_def)
qed  
  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_set_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct


(*
  Once again we define a graph. In this tutorial, we are going to focus on abstraction. Often
  you don't want Isabelle/HOL to automatically simplify basic operations, as basic lemmas
  you have proven on these operations may not be applicable anymore. This will make you proof a
  lot more difficult.
*)

  
(* Abstract concept of nodes in a graph *)
definition V :: "'v graph \<Rightarrow> 'v set" where 
  "V E = undefined"


(* Abstract concept of dead end *)
definition has_dead_end :: "'v graph \<Rightarrow> bool" where
  "has_dead_end g \<equiv> undefined"


(* Alternative abstract representation, that suggests implementation by set difference *)
lemma has_dead_end1: "has_dead_end g \<longleftrightarrow> snd`g - fst`g \<noteq> {}"
  oops

(* Concrete implementation (naive) *)
definition lg_has_dead_end :: "'v list_graph \<Rightarrow> bool" where 
  "lg_has_dead_end xs \<equiv> undefined"


lemma lg_has_dead_end_correct: "lg_has_dead_end xs \<longleftrightarrow> has_dead_end (lg_\<alpha> xs)"
  oops


(*
  Lemma \<open>has_dead_end1\<close> shows that subtracting all destination nodes from all source nodes yields
  the empty set if and only if there is a dead end. To implement this for the rbts, we need to be
  able to subtract nodes from an rbts. For simplicity, we choose to subtract a list of with values
  from a set using function \<open>rbts_diff_list\<close>.

  We also define a function that constructs an rbts out of a list. This is necessary to have a 
  starting set to subtract the destination nodes from.
*)

definition "rbts_from_list xs = undefined"

lemma [simp]: "rbts_set (rbts_from_list xs) = set xs"  
  oops

definition "rbts_diff_list s xs = undefined"

lemma [simp]: "rbts_set (rbts_diff_list s xs) = rbts_set s - set xs"
  oops

(* More efficient implementation with two passes, and red-black-tree *)
definition lg_has_dead_end_rbt :: "'v::linorder list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end_rbt xs \<equiv> undefined"
(*>*)

lemma "lg_has_dead_end_rbt xs = has_dead_end (lg_\<alpha> xs)"
(*<*)
proof -
  (* The correctness proof is done by stepwise refinement: 
  
    we show that lg_has_dead_end_rbt implements lg_has_dead_end, 
      which we have already shown to implement the specification.
      
      
    Note that this is similar to the transitivity proof we did for lg_has_dead_end_correct,
    but that the intermediate implementation was defined as its own constant, rather than
    just appearing on the RHS of a lemma.
  *)
  show ?thesis sorry
qed  
(*>*)


end
