theory Tutorial7
  imports Main "HOL-Library.Monad_Syntax"
begin


(*
  You have seen the weakest precondition in the lecture a couple of times.
*)

fun owp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "owp None Q = False"
  | "owp (Some v) Q = Q v" 

lemma owp_bind[simp]: 
  "owp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> owp m (\<lambda>x. owp (f x) Q)"
  by (cases m; auto)

(*
  Consequence rule for the option monad
*)

lemma owp_cons: 
  assumes "owp c Q'"
  assumes "\<And>r. Q' r \<Longrightarrow> Q r"
  shows "owp c Q"
  using assms by (cases c) auto


(*
  We now introduce the state monad. The state monad maps a state to an output of type 'a and a new
  state. Find more information here \<^url>\<open>https://wiki.haskell.org/State_Monad\<close>
*)

datatype ('a, 's) s_monad = SCOMP (srun: "('s \<Rightarrow> 'a \<times> 's)")

definition sreturn :: "'a \<Rightarrow> ('a, 's) s_monad" where 
  "sreturn x = undefined"

definition sbind :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) s_monad) \<Rightarrow> ('b, 's) s_monad" where 
  "sbind m f = undefined"


(*
  The state monad is accompanied by a get and put function. Get returns a monad whose value equals
  the state and put returns a monad whose value is the unit value.
*)

definition sget :: "('s, 's) s_monad" where "sget = undefined"

definition sput :: "'s \<Rightarrow> (unit, 's) s_monad" where "sput s = undefined"


(*
  Then we also define the weakest precondition for the state monad
*)

definition swp :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "swp m Q s = undefined"


(*
  And the monad laws and other relevant lemmas should hold for our definitions
*)

lemma "undefined" oops


(*
  Next up, we define the state error monad which is a combination of the state monad and the option
  monad. If an "error" is encountered, a state will map to None, which is carried over through the 
  program.
*)

datatype ('a, 's) se_monad = COMP (run: "('s \<Rightarrow> ('a \<times> 's) option)")

definition return :: "'a \<Rightarrow> ('a, 's) se_monad" where 
  "return x = undefined"
            
definition se_bind :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) se_monad) \<Rightarrow> ('b, 's) se_monad" where 
  "se_bind m f = undefined"


(* 
  Adhoc overloading allows us to use the bind notation for monads.
*)
adhoc_overloading bind se_bind

(*
  Including the fail, get and put function
*)
definition fail :: "('a, 's) se_monad" where "fail = undefined"

definition get :: "('s, 's) se_monad" where "get = undefined"

definition put :: "'s \<Rightarrow> (unit, 's) se_monad" where "put s = undefined"


(*
  Now we define the weakest precondition for the state error monad.
*)

definition wp :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "wp m Q s = undefined"


(*
  Now define the monad laws and other rules, we have provided them already, as you have already 
  seen this.
*)
lemma se_return_bind[simp]: "se_bind (return x) f = f x"
  unfolding se_bind_def return_def oops

lemma se_bind_return[simp]: "se_bind m return = m"
  unfolding se_bind_def return_def oops

lemma se_bind_assoc[simp]: "se_bind (se_bind m f) g = se_bind m (\<lambda> x. se_bind (f x) g)"
  unfolding se_bind_def 
  oops

lemma bind_fail: "se_bind m (\<lambda> _. fail) = fail" (*This lemma does not hold for all monads*)
  unfolding se_bind_def fail_def oops

lemma fail_bind[simp]: "se_bind fail f = fail"
  unfolding se_bind_def fail_def oops

lemma se_wp_return: "wp (return x) Q s \<longleftrightarrow> Q x s"
  unfolding wp_def return_def oops

lemma se_wp_bind: "wp (se_bind m f) Q s = wp m (\<lambda> x s. wp (f x) Q s) s"
  unfolding wp_def se_bind_def 
  oops

lemma se_wp_fail: "wp fail Q s = False"
  unfolding wp_def fail_def
  oops

lemma se_wp_get: "wp (get) Q s = Q s s"
  unfolding get_def wp_def oops

lemma se_wp_put: "wp (put s') Q s = Q () s'"
  unfolding put_def wp_def oops
  
(*
lemmas wp_simps = se_wp_return se_wp_bind se_wp_fail se_wp_get se_wp_put

lemmas wp_basic_rls = wp_simps[THEN iffD2]
*)

lemma wp_cons: 
  assumes "wp c Q' s"
  assumes "\<And>r s. Q' r s \<Longrightarrow> Q r s"
  shows "wp c Q s"
  using assms unfolding wp_def
  by (auto intro: owp_cons)


(*
  We can also define an assert similarly to how we did it for the error monad
*)

definition "assert P \<equiv> undefined"
  
lemma assert_simps[simp]:
  "assert False = undefined"
  "assert True = undefined"
  oops

lemma wp_assert[simp]: "wp undefined undefined undefined"
  oops

(*
  We are now given this bit of proof from the warm up of last week, make this work with the state
  error monad!


type_synonym fs = "string \<Rightarrow> (string \<times> bool) option"

definition lookup :: "('k \<rightharpoonup> 'v) \<Rightarrow> 'k \<Rightarrow> 'v option"
  where "lookup fs n \<equiv> fs n"

lemma wp_lookup[simp]:
  "wp (lookup fs n) Q s \<longleftrightarrow> n\<in>dom fs \<and> (\<forall>v. fs n = Some v \<longrightarrow> Q v s)"
  unfolding lookup_def
  apply (cases "fs n") 
  by auto

definition "create n fs \<equiv> do {
  assert (n\<notin>dom fs);
  Some (fs(n\<mapsto>([],False)))
}"
  
definition "open n fs \<equiv> do {
  (d,op) \<leftarrow> lookup fs n;
  assert (\<not>op);
  Some (fs(n\<mapsto>(d,True)))
}"

definition "close n fs \<equiv> do {
  (d,op) \<leftarrow> lookup fs n;
  assert (op);
  Some (fs(n\<mapsto>(d,False)))
}"

definition "read n fs \<equiv> do {
  (d,op) \<leftarrow> lookup fs n;
  assert (op);
  Some d
}"

definition "write n d fs \<equiv> do {
  (_,op) \<leftarrow> lookup fs n;
  assert (op);
  Some (fs(n\<mapsto>(d,op)))
}"
  

definition "open_files fs = {n. \<exists>d. fs n = Some (d,True) }"
definition "data fs = map_option fst o fs"


lemma create_rl: "n \<notin> dom (data fs) \<Longrightarrow> wp 
  (create n fs) 
  (\<lambda>fs'. 
    data fs' = data fs(n\<mapsto>[])
  \<and> open_files fs' = open_files fs  
  )"
  unfolding create_def data_def open_files_def
  by auto
  
lemma open_rl: "\<lbrakk> n \<in> dom (data fs); n\<notin> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (open n fs) (\<lambda>fs'. 
    data fs' = data fs
  \<and> open_files fs' = insert n (open_files fs)
  )"
  unfolding open_def data_def open_files_def
  by auto
  
lemma close_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> \<Longrightarrow>
  wp (close n fs) (\<lambda>fs'.
    data fs' = data fs
  \<and> open_files fs' = open_files fs - {n}
  )
"  
  unfolding close_def data_def open_files_def
  by auto
  
lemma read_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (read n fs) (\<lambda>d. data fs n = Some d)"
  unfolding read_def data_def open_files_def
  by auto
  
lemma write_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (write n d fs) (\<lambda>fs'. 
    data fs' = (data fs)(n\<mapsto>d)
  \<and> open_files fs' = open_files fs  
)"
  unfolding write_def data_def open_files_def
  by auto

  
(* Derived operations *)  
  
(* Read, open/close if not yet open *)
definition "read' n fs \<equiv> 
  if n\<in>open_files fs then do {
    d \<leftarrow> read n fs;
    Some (d,fs)
  } else do {
    fs \<leftarrow> open n fs;
    d \<leftarrow> read n fs;
    fs \<leftarrow> close n fs;
    Some (d,fs)
  }
"

lemma read'_rl: "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
  \<Longrightarrow> wp (read' n fs) (\<lambda>(d,fs'). 
    data fs n = Some d \<and>
    open_files fs' = open_files fs \<and>
    data fs' = data fs
  )"
  unfolding read'_def
  apply auto
  apply (rule wp_cons)
  apply (rule read_rl)
  apply simp
  apply simp
  apply (rule wp_cons, rule open_rl)
  apply auto []
  apply simp
  apply (rule wp_cons, rule read_rl)
  apply simp
  apply (rule wp_cons, rule close_rl)
  apply simp
  apply simp
  done
  

(* A bit more automation *)    
lemma "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
  \<Longrightarrow> wp (read' n fs) (\<lambda>(d,fs'). 
    data fs n = Some d \<and>
    open_files fs' = open_files fs \<and>
    data fs' = data fs
  )"
  unfolding read'_def
  by (
    rule wp_cons, rule open_rl read_rl close_rl 
  | (auto)[] )+
*)

end