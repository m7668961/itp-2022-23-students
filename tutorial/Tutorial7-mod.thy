theory "Tutorial7-mod"
  imports Main "HOL-Library.Monad_Syntax"
begin


(*
  You have seen the weakest precondition in the lecture a couple of times.
*)

fun owp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
    "owp None Q = False"
  | "owp (Some v) Q = Q v" 

lemma owp_bind[simp]: 
  "owp (do {x\<leftarrow>m; f x}) Q \<longleftrightarrow> owp m (\<lambda>x. owp (f x) Q)"
  by (cases m; auto)

(*
  Consequence rule for the option monad
*)

lemma owp_cons: 
  assumes "owp c Q'"
  assumes "\<And>r. Q' r \<Longrightarrow> Q r"
  shows "owp c Q"
  using assms by (cases c) auto


(*
  We now introduce the state monad. The state monad maps a state to an output of type 'a and a new
  state. Find more information here \<^url>\<open>https://wiki.haskell.org/State_Monad\<close>
*)

datatype ('a, 's) s_monad = SCOMP (srun: "('s \<Rightarrow> 'a \<times> 's)")

definition sreturn :: "'a \<Rightarrow> ('a, 's) s_monad" where 
  "sreturn x = SCOMP (\<lambda> s. (x,s))"

definition sbind :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) s_monad) \<Rightarrow> ('b, 's) s_monad" where 
  "sbind m f = SCOMP (\<lambda> s. let (a,s') = (srun m) s in srun (f a) s')"


(*
  The state monad is accompanied by a get and put function. Get returns a monad whose value equals
  the state and put returns a monad whose value is the unit value.
*)

definition sget :: "('s, 's) s_monad" where "sget = SCOMP (\<lambda> s. (s,s))"

definition sput :: "'s \<Rightarrow> (unit, 's) s_monad" where "sput s = SCOMP (\<lambda> _. ((),s))"


(*
  Then we also define the weakest precondition for the state monad
*)

definition swp :: "('a, 's) s_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "swp m Q s = (let (a,s') = (srun m) s in Q a s')"


(*
  And the monad laws and other relevant lemmas should hold for our definitions
*)

lemma "sbind (sreturn a) f = f a"
  unfolding sbind_def sreturn_def by simp

lemma "sbind m sreturn = m"
  unfolding sbind_def sreturn_def by simp

lemma "sbind (sbind m f) g = sbind m (\<lambda> x. sbind (f x) g)"
  unfolding sbind_def by(auto split: prod.split)
  
lemma "swp (sreturn x) Q s \<longleftrightarrow> Q x s"
  unfolding swp_def sreturn_def by simp

lemma "swp (sbind m f) Q s \<longleftrightarrow> swp m (\<lambda> x s. swp (f x) Q s) s"
  unfolding swp_def sbind_def by auto

lemma "swp sget Q s = Q s s"
  unfolding swp_def sget_def by simp

lemma "swp (sput s') Q s = Q () s'"
  unfolding swp_def sput_def by simp

(*
  Next up, we define the state error mnad which is a combination of the state monad and the option
  monad. If an "error" is encountered, a state will map to None, which is carried over through the 
  program.
*)

datatype ('a, 's) se_monad = COMP (run: "('s \<Rightarrow> ('a \<times> 's) option)")

definition return :: "'a \<Rightarrow> ('a, 's) se_monad" where 
  "return x = COMP (\<lambda> s. Some (x,s))"
            
definition se_bind :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> ('b, 's) se_monad) \<Rightarrow> ('b, 's) se_monad" where 
  "se_bind m f = COMP (\<lambda> s. do{ (a,s') \<leftarrow> (run m) s; run (f a) s'})"

(* 
  Adhoc overloading allows us to use the bind notation for monads.
*)
adhoc_overloading bind se_bind

(*
  Including the fail, get and put function
*)
definition fail :: "('a, 's) se_monad" where "fail = COMP (\<lambda> _. None)"

definition get :: "('s, 's) se_monad" where "get = COMP (\<lambda> s. Some (s,s))"

definition put :: "'s \<Rightarrow> (unit, 's) se_monad" where "put s = COMP (\<lambda> _. Some ((), s))"


(*
  Now we define the weakest precondition for the state error monad.
*)

definition wp :: "('a, 's) se_monad \<Rightarrow> ('a \<Rightarrow> 's \<Rightarrow> bool) \<Rightarrow> 's \<Rightarrow> bool" where 
  "wp m Q s = (owp (run m s) (\<lambda> (a, s). Q a s))"


(*
  Now define the monad laws and other rules, we have provided them already, as you have already 
  seen this.
*)
lemma se_return_bind[simp]: "se_bind (return x) f = f x"
  unfolding se_bind_def return_def
  by auto

lemma se_bind_return[simp]: "se_bind m return = m"
  unfolding se_bind_def return_def 
  by auto

lemma se_bind_assoc[simp]: "se_bind (se_bind m f) g = se_bind m (\<lambda> x. se_bind (f x) g)"
  unfolding se_bind_def 
  by (auto split: Option.bind_splits)

lemma bind_fail: "se_bind m (\<lambda> _. fail) = fail" (*This lemma does not hold for all monads*)
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

lemma fail_bind[simp]: "se_bind fail f = fail"
  unfolding se_bind_def fail_def by (auto split: Option.bind_splits)

lemma se_wp_return: "wp (return x) Q s \<longleftrightarrow> Q x s"
  unfolding wp_def return_def by auto

lemma se_wp_bind: "wp (se_bind m f) Q s = wp m (\<lambda> x s. wp (f x) Q s) s"
  unfolding wp_def se_bind_def
  by (simp add: case_prod_unfold)

lemma se_wp_fail: "wp fail Q s = False"
  unfolding wp_def fail_def
  by auto

lemma se_wp_get: "wp (get) Q s = Q s s"
  unfolding get_def wp_def
  by auto

lemma se_wp_put: "wp (put s') Q s = Q () s'"
  unfolding put_def wp_def 
  by auto
  

lemmas wp_simps = se_wp_return se_wp_bind se_wp_fail se_wp_get se_wp_put

lemmas wp_basic_rls = wp_simps[THEN iffD2]


lemma wp_cons: 
  assumes "wp c Q' s"
  assumes "\<And>r s. Q' r s \<Longrightarrow> Q r s"
  shows "wp c Q s"
  using assms unfolding wp_def
  by (auto intro: owp_cons)


(*
  We can also define an assert similarly to how we did it for the error monad
*)

definition "assert P \<equiv> if P then return () else fail"
  
lemma assert_simps[simp]:
  "assert False = fail"
  "assert True = return ()"
  unfolding assert_def by auto

lemma wp_assert[simp]: "wp (assert P) Q s \<longleftrightarrow> P \<and> Q () s"
  by(cases P; auto simp: wp_simps)

(*
  We are now given this bit of proof from the warm up of last week, make this work with the state
  error monad!
*)

type_synonym fs = "string \<Rightarrow> (string \<times> bool) option"

type_synonym 'a fsM = "('a, fs) se_monad"

definition lookup :: "('k \<rightharpoonup> 'v) \<Rightarrow> 'k \<Rightarrow> ('v,'s) se_monad"
  where "lookup fs n \<equiv> (case fs n of None \<Rightarrow> fail | Some v \<Rightarrow> return v)"

lemma wp_lookup[simp]:
  "wp (lookup fs n) Q s \<longleftrightarrow> n\<in>dom fs \<and> (\<forall>v. fs n = Some v \<longrightarrow> Q v s)"
  unfolding lookup_def
  apply (cases "fs n") 
  by (auto simp: wp_simps)

definition "create n \<equiv> 
do {
  fs \<leftarrow> get;
  assert (n\<notin>dom fs);
  put (fs(n\<mapsto>([],False)))
}"

term "do {
  fs \<leftarrow> get;
  assert (n\<notin>dom fs);
  put (fs(n\<mapsto>([],False)))
}"
  
definition "open n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert (\<not>op);
  put (fs(n\<mapsto>(d,True)))
}"

definition "close n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert (op);
  put (fs(n\<mapsto>(d,False)))
}"



definition "read n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert (op);
  return d
}"

definition "write n d \<equiv> do {
  fs \<leftarrow> get;
  (_,op) \<leftarrow> lookup fs n;
  assert (op);
  put (fs(n\<mapsto>(d,op)))
}"

term "do {
  fs \<leftarrow> get;
  (_,op) \<leftarrow> lookup fs n;
  assert (op);
  put (fs(n\<mapsto>(d,op)))
}"
  
(*
  This is where we had to end the tutorial, everyhting after this 
*)
definition delete :: "string \<Rightarrow> unit fsM" where 
"delete n \<equiv> do {
  fs \<leftarrow> get;
  (d,op) \<leftarrow> lookup fs n;
  assert (\<not>op);
  put (fs(n:=None))
}"

definition "file_exists" :: "string \<Rightarrow> bool fsM" where 
"file_exists n \<equiv> do {
  fs \<leftarrow> get;
  return (n \<in> dom fs)
}"

definition "is_open" :: "string \<Rightarrow> bool fsM" where 
"is_open n \<equiv> do {
  fs \<leftarrow> get;
  (_,op) \<leftarrow> lookup fs n;
  return op
}"

definition "open_files fs = {n. \<exists>d. fs n = Some (d,True) }"
definition "data fs = map_option fst o fs"


lemma create_rl: "n \<notin> dom (data fs) \<Longrightarrow> wp 
  (create n) 
  (\<lambda> _ fs'. 
    data fs' = data fs(n\<mapsto>[])
  \<and> open_files fs' = open_files fs  
  ) fs"
  unfolding create_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma delete_rl: "\<lbrakk> n \<in> dom (data fs); n\<notin> open_files fs \<rbrakk>  
  \<Longrightarrow> wp (delete n) (\<lambda> _ fs'. data fs' = (data fs)(n:=None) 
    \<and> open_files fs' = open_files fs) fs"
  unfolding delete_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma open_rl: "\<lbrakk> n \<in> dom (data fs); n\<notin> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (open n) (\<lambda> _ fs'. 
    data fs' = data fs
  \<and> open_files fs' = insert n (open_files fs)
  ) fs"
  unfolding open_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma close_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> \<Longrightarrow>
  wp (close n) (\<lambda> _ fs'.
    data fs' = data fs
  \<and> open_files fs' = open_files fs - {n}
  ) fs
"  
  unfolding close_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma read_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (read n) (\<lambda>d fs'. data fs n = Some d 
    \<and> fs' = fs) fs"
  unfolding read_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma write_rl: "\<lbrakk> n \<in> open_files fs \<rbrakk> 
  \<Longrightarrow> wp (write n d) (\<lambda> _ fs'. 
    data fs' = (data fs)(n\<mapsto>d)
  \<and> open_files fs' = open_files fs  
) fs"
  unfolding write_def data_def open_files_def
  by (auto simp: wp_simps)
  
lemma file_exists_rl: "wp (file_exists n) 
  (\<lambda> r fs'. r = (n \<in> dom (data fs)) 
    \<and> fs' = fs) fs"
  unfolding file_exists_def data_def
  by (auto simp: wp_simps)
  
lemma is_open_rl: "\<lbrakk> n \<in> dom (data fs) \<rbrakk> \<Longrightarrow>
  wp (is_open n) (\<lambda> r fs'. r = (n \<in> open_files fs) 
    \<and> fs' = fs) fs"
  unfolding is_open_def open_files_def data_def
  by (auto simp: wp_simps)
                                                            
  
(* Derived operations *)  
  
(* Read, open/close if not yet open *)
definition read' :: "string \<Rightarrow> string fsM" where 
"read' n \<equiv> do {
    op \<leftarrow> is_open n;
    if op then do {
      d \<leftarrow> read n;
      return d
    } else do {
      open n;
      d \<leftarrow> read n;
      close n;
      return d
    }
  }
"

lemma read'_rl: "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
  \<Longrightarrow> wp (read' n) (\<lambda> d fs'. 
    data fs n = Some d \<and> 
    data fs' = data fs \<and>
    open_files fs' = open_files fs
  ) fs"
  unfolding read'_def
  supply [simp] = wp_simps
  apply auto
  apply (rule wp_cons)
  apply (rule is_open_rl)
  apply auto[]
  apply auto
  apply (rule wp_cons)
  apply (rule read_rl)
  apply simp
  apply simp
  
  apply (rule wp_cons, rule open_rl)
  apply auto []
  apply simp
  apply (rule wp_cons, rule read_rl)
  apply simp
  apply (rule wp_cons, rule close_rl)
  apply simp
  apply simp 
  done
  
  
lemma if_rl: 
  assumes "b \<Longrightarrow> wp t Q s" 
  assumes "\<not>b \<Longrightarrow> wp e Q s" 
  shows "wp (if b then t else e) Q s"
  using assms by simp

lemmas fs_rules = file_exists_rl read'_rl create_rl is_open_rl open_rl read_rl close_rl delete_rl write_rl


(* A bit more automation *)    
lemma "\<lbrakk> n\<in>dom (data fs) \<rbrakk> 
  \<Longrightarrow> wp (read' n) (\<lambda> d fs'. 
    data fs n = Some d \<and>
    open_files fs' = open_files fs \<and>
    data fs' = data fs
  ) fs"
  unfolding read'_def
  by (rule if_rl wp_basic_rls 
    | rule wp_cons, rule fs_rules  
    | (auto)[] )+
  
  
definition copy_file :: "string \<Rightarrow> string \<Rightarrow> unit fsM" where 
"copy_file src tgt \<equiv> do {
  d \<leftarrow> read' src;
  ex \<leftarrow> file_exists tgt;
  (if ex then delete tgt else return ());
  create tgt;
  open tgt;
  write tgt d;
  close tgt
}"
  

lemma "\<lbrakk>data fs src = Some d; 
  tgt \<notin> open_files fs\<rbrakk> 
  \<Longrightarrow> wp (copy_file src tgt) 
  (\<lambda> _ fs'. data fs' = data fs(tgt\<mapsto>d) 
    \<and> open_files fs' = open_files fs) fs"
  unfolding copy_file_def
  by (rule if_rl wp_basic_rls 
    | rule wp_cons, rule fs_rules  
    | (auto)[] )+



end