theory "Tutorial4-mod"
  imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin



(*
  To warm up for today, let's prove that the square root of 2 is irrational!

  HINT: The Isabelle proof can be found online, please try it yourself first!
*)

find_theorems \<rat> "_ / _"
term sqrt
find_theorems "?a ^ 2"
thm field_class.power_divide
thm prime_dvd_power
thm Rats_abs_nat_div_natE

lemma sqrt_2_irrational: "sqrt 2 \<notin> \<rat>"
proof
  assume "sqrt 2 \<in> \<rat>"
  then obtain m n::nat where "\<bar>sqrt 2\<bar> = m / n" and lowest_terms: "coprime m n"
    by(rule Rats_abs_nat_div_natE)
  hence "m ^ 2 = (sqrt 2) ^ 2 * n ^ 2" by (auto simp: field_class.power_divide)
  hence eq: "m ^ 2 = 2 * n ^ 2" 
    using of_nat_eq_iff by fastforce
  hence "2 dvd m ^ 2" by simp
  hence m_ev: "2 dvd m" by simp
  moreover have "2 dvd n"
  proof -
    from m_ev obtain k where "m = 2 * k" by blast
    with eq have "2 * n ^ 2 = 2 ^ 2 * k ^ 2" by auto
    hence "2 dvd n ^ 2" by simp
    thus ?thesis by simp
  qed
  ultimately have "\<not>coprime m n" by fastforce
  thus False using lowest_terms by blast
qed
  




(*
  Next up, we will define a language inductively. The alphabet contains merely parentheses and the 
  grammar matches every parenthesis open (LPAR) with a parenthesis close (RPAR)
*)

datatype par = LPAR | RPAR


(*
  Define the grammar S in an inductive way, the context-free grammar looks as follows:
  S \<rightarrow> LPAR S RPAR | S S | LPAR RPAR
*)
inductive S :: "par list \<Rightarrow> bool" where
  par: "S e \<Longrightarrow> S (LPAR # e @ [RPAR])" |
  dup: "S e1 \<Longrightarrow> S e2 \<Longrightarrow> S (e1 @ e2)" |
  fin: "S ([LPAR, RPAR])"



(*
  Let's first prove some simple lemmas.
*)

thm S.cases

lemma S_not_empty_aux: "S e \<Longrightarrow> e \<noteq> []"
  apply(induction e rule: S.induct)
    apply auto
  done

lemma S_not_empty [simp]: "\<not>S []"
  using S_not_empty_aux by blast
  

find_theorems "_ # _ = _ @ _"
thm S.cases[of "[LPAR]" False]

lemma S_not_LPAR[simp]: "\<not>S [LPAR]"
  apply(rule notI)
  apply(erule S.cases)
    apply (auto simp: Cons_eq_append_conv)
  done

lemma "S [LPAR, LPAR, LPAR, RPAR, RPAR, x] \<Longrightarrow> x = RPAR"
  apply(erule S.cases; auto simp: Cons_eq_append_conv)+
  done

(*
  We can also use a function to determine whether all parentheses match. \<open>match_parantheses\<close> accepts
  a list of symbols \<open>ws\<close> and a natural number \<open>n\<close> by incrementing n if it reads LPAR and decrementing
  n when it reads RPAR. If we try to decrement 0, we return False, as we have an unmatched RPAR then.
  If n does not equal 0 when we reach the end of the list, we have an unmatched LPAR and also return
  False.

  We assume you know how to define this by now. There's nothing new here.
*)
  

fun match_parentheses :: "par list \<Rightarrow> nat \<Rightarrow> bool" where 
  "match_parentheses [] n \<longleftrightarrow> n = 0" |
  "match_parentheses (w # ws) n = (case w of 
    LPAR \<Rightarrow> match_parentheses ws (Suc n) | 
    RPAR \<Rightarrow> (case n of 
      0 \<Rightarrow> False | 
      Suc m \<Rightarrow> match_parentheses ws m
    )
  )"


(*
  This function does exactly the same as the inductive definition, unless the input list is empty. 
  Prove this!

  WARNING: This is tricky!
*)

lemma match_parentheses_append: "match_parentheses e1 n \<Longrightarrow> match_parentheses e2 m \<Longrightarrow> match_parentheses (e1 @ e2) (n + m)"
  apply (induction e1 n rule: match_parentheses.induct)
   apply (auto split: par.split nat.splits)
  done

thm match_parentheses_append[of _ 0 "[RPAR]" "Suc 0", simplified]

lemma S_to_match_parentheses: "S xs \<Longrightarrow> match_parentheses xs 0"
  apply(induction xs rule: S.induct)
    apply(auto simp: match_parentheses_append[of _ 0 "[RPAR]" "Suc 0", simplified] match_parentheses_append[of _ 0 _ 0, simplified])
  done


lemma S_insert:  "S (xs @ ys) \<Longrightarrow> S zs \<Longrightarrow> S (xs @ zs @ ys)"
  proof (induction "(xs @ ys)" arbitrary: xs ys rule: S.induct)
    case (par w)
    then show ?case 
      proof (cases "xs = [] \<or> ys = []")
        case True
        then show ?thesis 
          apply(cases "xs = []")
          using par.hyps 
          by (auto intro!: dup par.prems S.par)
      next
        case False
        obtain xs' ys' where XSD: "xs = LPAR # xs'" and YSD: "ys = ys' @ [RPAR]" using par.hyps(3) False 
          by (metis Cons_eq_append_conv False last_appendR snoc_eq_iff_butlast)
        hence WDEF: "w = xs' @ ys'" using par.hyps(3) by simp
        have "S (xs' @ zs @ ys')" 
          using par(2)[OF WDEF par.prems] .
        from this[THEN S.par] show ?thesis 
          by(auto simp: XSD YSD)
      qed
  next
    case (dup v w)
    then show ?case
      apply(auto simp: append_eq_append_conv2[of v w xs ys] intro!: S.dup[of v]) 
      apply(fold append_assoc) 
      apply(rule S.dup[of _ w]) 
      by blast+
  next
    case fin
    then show ?case      
      apply(auto simp: Cons_eq_append_conv intro!: par)
       apply(auto simp: S.fin intro!: dup)
      using dup S.fin by fastforce
  qed



(*
  There was some confusion on why we would use match_parentheses.induct here instead of just doing
  induction over xs with arbitrary n. I stated that it would be the same, which is not true for this
  case as match_parentheses.induct already takes into account the case distinction in the IH.
  This gives us more information during the proof which allows us to ignore the \<open>xs = []\<close> case in 
  the IH, something that is very difficult if we just induct over xs.
  
  The adapted version with induction over just xs is given below.
*)
lemma match_parentheses_n_rep_S: "match_parentheses xs n \<Longrightarrow> xs = [] \<or> S (replicate n LPAR @ xs)"
  apply(induction xs n rule: match_parentheses.induct)
   apply(simp add: )
  apply (auto 
      split: par.splits nat.splits 
      simp flip: replicate_append_same 
      simp: fin
      intro: S_insert[OF _ fin, simplified]
      )
  done

lemma match_parentheses_to_S: "match_parentheses xs 0 \<Longrightarrow> xs = [] \<or> S xs"
  using match_parentheses_n_rep_S 
  by force

lemma "match_parentheses xs 0 \<longleftrightarrow> xs = [] \<or> S xs"
  using S_to_match_parentheses match_parentheses_to_S
  by auto


lemma match_parentheses_Suc_not_empty: "match_parentheses xs (Suc n) \<Longrightarrow> xs \<noteq> []" 
  apply auto
  done






(*
  The alternative proof: due to the choice of not inducting over match_parentheses.induct our proof
  (which is not optimized) suddenly required a lot more reasoning. This is caused by the fact that 
  the IH has a lot less information. Therefore, we have to reason ourselved about why xs = [] can
  or cannot happen, something that follows from the IH in the other case. This is partially trial
  and error. In this case induction over the functions induct rule was easier, but in another case
  it may be easier to induct over the data structure itself. The only way to find out is by 
  analysing your problem and reasoning about which way seems easier for you to prove.

  Luckily, there are multiple options, as we show here, we can prove it both ways (although this 
  one does not have my preference):
*)
lemma match_parentheses_n_rep_S_alt: "match_parentheses xs n \<Longrightarrow> xs = [] \<or> S (replicate n LPAR @ xs)"
  apply(induction xs arbitrary: n)
   apply simp
  subgoal for a xs n
    apply(cases a)
  apply (auto split: par.splits nat.splits if_splits  intro!: S_insert simp: split_ifs match_parentheses_Suc_not_empty simp flip: replicate_append_same ) 
     apply (metis append_Cons replicate_Suc replicate_app_Cons_same)
    apply(cases xs)
    apply (simp add: fin)
    apply(rule S_insert[OF _ fin, simplified])
    apply auto
    done
  done

end


(*
  Basic Isar
  induction with isar
  cases with isar
*)