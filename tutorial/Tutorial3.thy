theory Tutorial3
  imports Main
begin


(*
We are going to do some backwards reasoning. Given below are some lemmas that simp can easily solve.
However, sometimes you will find yourself in a situation where simp and auto just won't find the 
solution. In that case you have to take over a bit. This part of the tutorial is meant to train
for these kinds of situations.
Try to solve these exercises purely using backwards reasoning (rule, drule, erule, assumption)
So try0 and sledgehammer and everything that they recommend are not allowed!
*)

lemma "{1,2} \<noteq> {}" 
  oops

lemma "x < Suc (Suc (Suc (Suc (Suc 0)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc 0)))))"
  oops

lemma "x < Suc (Suc (Suc (Suc (Suc y)))) \<Longrightarrow> x < Suc (Suc (Suc (Suc (Suc (Suc (Suc y))))))"
  oops


lemma "last [2,3,4] = last [3,4]"
  oops
  

lemma "\<not>distinct [4::nat,2,3,4]"
  oops

lemma "\<exists> x. D x \<longrightarrow> (\<forall> x. D x)"
  oops



(*
Previous restrictions are lifted, you can continue with these proofs as usual.

First of all, we take a look at labelled automata. A labelled automaton is a state machine in which 
each edge is labelled with a symbol. By following a path on the automaton, we can build a word out
of the symbols that we passed over. For example, we may have an automaton like the one below.
Automaton \<delta>:

S\<^sub>1 \<comment>c\<rightarrow> S\<^sub>2 \<comment>b \<rightarrow> S\<^sub>3
         |        |
         a        a
         \<down>        \<down>
         S\<^sub>4 \<comment>c\<rightarrow> S\<^sub>5

These edges are defined as a set of triples (p,a,q) where p is the source state
We can then define a function \<open>word\<close> where \<open>word \<delta> S\<^sub>1 ws S\<^sub>5\<close> is True for \<open>ws = [c,b,a]\<close> and
\<open>ws = [c,a,c]\<close> and False for everything else. In other words, function \<open>word\<close> checks whether a given
word matches a path in between two given nodes."

Define \<open>word\<close>
*)

fun word :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> 'q \<Rightarrow> 'a list \<Rightarrow> 'q \<Rightarrow> bool" where
  "word _ _ _ _ = undefined"

(*
We can also build the product of two automata. A state in this automaton is a tuple of a state of
both automata. An edge still only has a single symbol, but both automata transition to a next state.
If a tuple contains at least one state that doesn't have a transition with a given symbol, then 
neither does the automaton.

For example: if \<delta>\<^sub>1 contains the edge 
*)
definition \<delta>x :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> ('q \<times> 'a \<times> 'q) set \<Rightarrow> (('q \<times> 'q) \<times> 'a \<times> ('q \<times> 'q)) set" where 
  "\<delta>x = undefined"


(*
Show correctness of this definition
*)
lemma "word \<delta>\<^sub>1 p\<^sub>1 as q\<^sub>1 \<and> word \<delta>\<^sub>2 p\<^sub>2 as q\<^sub>2 \<longleftrightarrow> word (\<delta>x \<delta>\<^sub>1 \<delta>\<^sub>2) (p\<^sub>1,p\<^sub>2) as (q\<^sub>1,q\<^sub>2)"
  oops


(*
An automaton is deterministic if each state has only outgoing edges with distinct symbols.
In other words, if an edge goes from state p via symbol a to state q1 and an edge goes from
state p via symbol a to state q1 then q1 = q2.
*)
definition det :: "('q \<times> 'a \<times> 'q) set \<Rightarrow> bool" where 
  "det \<delta> \<equiv> \<forall>q a q1 q2. (q, a, q1) \<in> \<delta> \<and> (q, a, q2) \<in> \<delta> \<longrightarrow> q1=q2"

(*
This carries over to our definition of words (if defined correctly)
*)
lemma "det \<delta> \<Longrightarrow> word \<delta> q w q1 \<Longrightarrow> word \<delta> q w q2 \<Longrightarrow> q1=q2"
  oops

(*
This carries over to product construction as well.
*)
lemma "det \<delta>\<^sub>1 \<Longrightarrow> det \<delta>\<^sub>2 \<Longrightarrow> det (\<delta>x \<delta>\<^sub>1 \<delta>\<^sub>2)"
  oops

