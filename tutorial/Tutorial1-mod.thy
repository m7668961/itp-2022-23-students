theory "Tutorial1-mod"
imports Main
begin

(*
  This is the first tutorial sheet for the ITP course. We will discuss the content in the
  practical session on the 17th of November between 10:45 and 12:30.
*)


(*
Specify a function which takes a natural number n as a parameter and calculates the sum of all
natural numbers up to n
*) 

fun sum_upto :: "nat \<Rightarrow> nat" where
  "sum_upto 0 = 0" |
  "sum_upto (Suc n) = Suc n + sum_upto n"


(*
  We now have a classical example for induction. The sum that we have just defined is equal to 
  n * (n + 1) / 2. 
*)

lemma "sum_upto n = n * (n + 1) div 2"
  apply(induction n)
   apply auto
  done


lemma "sum_list [0..<n+1] = n * (n + 1) div 2"
  apply(induction n)
   apply auto
  done


lemma "fold (\<lambda> a n. a + n) [0..<n+1] a = n * (n+1) div 2 + a"
  apply(induction n)
  apply auto
  done

(*
  In the lecture we have taken a look at the tree datatype which stores data in the nodes.
  We now look at a tree that stores data in the leafs. Define the datatype here:
*)

datatype 'a tree = Leaf 'a | Node "'a tree" "'a tree"


(*
  We can convert a tree to a list by adding the data from left to right to a list.
*)
fun to_list :: "'a tree \<Rightarrow> 'a list" where
  "to_list (Leaf x) = [x]" |
  "to_list (Node lt rt) = to_list lt @ to_list rt"


(*
  We made a stupid design choice. Since we don't store data in the nodes we essentially waste 
  memory. Let's quantify how much memory is wasted by counting the nodes.
*)
fun count_nodes :: "'a tree \<Rightarrow> nat" where
  "count_nodes (Leaf _) = 0" |
  "count_nodes (Node lt rt) = 1 + count_nodes lt + count_nodes rt"


(*
  Ofcourse, this number does not say much if we don't compare it to the amount of data we are 
  actually storing. Try to come up with a relation between the amount of data stored 
  (length of to_list) and the number of nodes (count nodes).
*)
lemma aux3: "length (to_list t) = count_nodes t + 1"
  apply(induction t)
   apply auto
  done


lemma aux1:"length (to_list t) > 0"
  apply(induction t)
   apply auto
  done

lemma aux2: "n > 0 \<Longrightarrow> (n - Suc 0 = m) \<longleftrightarrow> (n = Suc m)"
  by auto

thm aux2[OF aux1, of _ 2]

lemma "length (to_list t) - 1 = count_nodes t"
  apply(simp add: aux2[OF aux1] aux3)
  done

find_theorems "Suc (_ - Suc _)"
find_theorems "Suc _ + _"
find_theorems "Suc (_ - Suc _)"

lemma "length (to_list t) - 1 = count_nodes t"
  apply(induction t)
  apply simp
  subgoal premises prems for t1 t2
    apply (simp add: prems[symmetric])
    apply (subst aux2)
    using aux1 apply auto[] 
    unfolding add_Suc[symmetric] 
    apply (subst Suc_pred)
    using aux1 apply auto[]
    apply (unfold add_Suc_shift)  
    apply (subst Suc_pred)
    using aux1 apply auto
    done

(*
  A very important function in functional programming is the map function. \<open>map f xs\<close> applies a
  function f to each element in the list xs.
*)

value "[0..<10]"
value "map (\<lambda> n. 3 * n) [0..<10]"


(*
  We can define a similar function for trees. We can define a function \<open>map_on_tree f t\<close> which
  returns a tree where each leaf was mapped using f. 
*)
fun map_on_tree :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a tree \<Rightarrow> 'b tree" where
  "map_on_tree f (Leaf x) = Leaf (f x)" |
  "map_on_tree f (Node lt rt) = Node (map_on_tree f lt) (map_on_tree f rt)"


value "Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3)"
value "map_on_tree (\<lambda> n. 3 * n) (Node (Node (Leaf (1::nat)) (Leaf 2)) (Leaf 3))"


(*
  If we did this correctly, the map function applied to \<open>to_list t\<close> is the same as the to_list 
  function applied to map_on_tree.
*)
lemma "map f (to_list t) = to_list (map_on_tree f t)"
  apply(induction t)
  by auto


(*
  We define a function \<open>alternate a xs\<close> which returns a list in which each entry in \<open>xs\<close> is folowed
  by \<open>a\<close>. For example \<open>alternate 8 [2,4,6] = [2,8,4,8,6,8]\<close>
*)
fun alternate :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "alternate _ [] = []" |
  "alternate x (y#ys) = y#x#alternate x ys"

value "alternate (8::nat) [2,4,6] = [2,8,4,8,6,8]"


(*
  We cannot directly do this for trees. But we can replace each \<open>Leaf x\<close> with 
  \<open>Node (Leaf x) (Leaf a)\<close>.
*) 
fun alternate_tree :: "'a \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "alternate_tree x (Leaf a) = Node (Leaf a) (Leaf x)" |
  "alternate_tree x (Node lt rt) = Node (alternate_tree x lt) (alternate_tree x rt)"


(*
  If we convert an alternate_tree to a list, it's the same as converting the tree to a list 
  directly and then applying alternate on it.
  WARNING, you may need some additional steps here
*)

value "alternate (8::nat) [2,4,6]"

value "alternate (8::nat) [2,4] @ alternate (8::nat) [6]"

lemma alternate_append[simp]: "alternate a (xs @ ys) = alternate a xs @ alternate a ys"
  apply(induction xs)
   apply auto
  done

lemma "alternate a (to_list t) = to_list (alternate_tree a t)" 
  apply(induction t)
   apply auto
  done


value "foldl (+) 0 [0..<8]"

fun fold_tree :: "('b \<Rightarrow> 'a \<Rightarrow> 'b) \<Rightarrow> 'b  \<Rightarrow> 'a tree \<Rightarrow> 'b" where
  "fold_tree f a (Leaf x) = f a x" |
  "fold_tree f a (Node lt rt) = fold_tree f (fold_tree f a lt) rt"

lemma "fold_tree f a t = foldl f a (to_list t)"
  apply(induction t arbitrary: a)
   apply auto
  done


value "fold_tree (+) 0 (Node (Node (Leaf (1::nat)) (Leaf 2)) (Node (Leaf (4::nat)) (Leaf 2)))"






