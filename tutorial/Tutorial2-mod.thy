theory "Tutorial2-mod"
  imports Main
begin


(*
We define a trie, which is a tree that stores boolean values. We use this to encode lists of booleans.
The left subtree represents a word whose first entry is False and the right subtree represents a word
whose first entry is True. Using a list of bools, we can traverse the tree. If we end up at a node
that contains true, then the list that we used to traverse the tree is in or encoded list.
If we end up at false or if we have to continue at at a Leaf, it's not. This means that out trie 
encodes a set of bool lists.
*)

datatype trie = is_Leaf: Leaf (this: bool) | Node (left: trie) (this: bool) (right: trie)

(*
This is the datastructure that we use. For example, 
\<open>Node (Node (Leaf False) False (Node (Leaf False) True (Leaf False))) True (Leaf False)\<close>
contains the list \<open>[]\<close>, because the root node is set, and [False,True], because we end up at true
if we traverse through the left subtree (because the first entry is False) and then the right subtree
of that (because the second entry is True). Doing this means that we end up at a node that contains
True.
Define \<open>trie_set\<close> which does this.
*)

value "Node (Node (Leaf False) False (Node (Leaf False) True (Leaf False))) True (Leaf False)"
    
fun trie_set :: "trie \<Rightarrow> bool list set" where
  "trie_set (Leaf b) = (if b then {[]} else {})" |
  "trie_set (Node l b r) = (if b then {[]} else {}) 
    \<union> {False # xs | xs. xs \<in> trie_set l} 
    \<union> {True # xs | xs. xs \<in> trie_set r}"


(*
Define an empty tree list and show correctness
*)

definition "trie_empty = Leaf False"
lemma trie_empty_correct[simp]: "trie_set trie_empty = {}"
  unfolding trie_empty_def
  by auto


(*
Now we want to define membership of our trie. Similar to (\<in>) for regular sets.
*)
fun trie_member :: "bool list \<Rightarrow> trie \<Rightarrow> bool" where
  "trie_member [] t = this t" |
  "trie_member (b#bs) t = (if is_Leaf t then False 
    else trie_member bs (if b then (right t) else (left t)))"


(*
  We now specify the correctness lemma of our function, showing that the member function that we
  have just defined corresponds to the basic abstract operation that it represents (here \<in>).
*)

lemma this_trie_set[simp]: "this t \<longleftrightarrow> [] \<in> trie_set t"
  apply(cases t)
   apply auto
  done 


lemma True_prepend_right_subtree: "\<not> is_Leaf t \<Longrightarrow> True # bs \<in> trie_set t \<longleftrightarrow> bs \<in> trie_set (right t)"
  apply(cases t) apply auto
  done

lemma False_prepend_left_subtree: "\<not> is_Leaf t \<Longrightarrow> False # bs \<in> trie_set t \<longleftrightarrow> bs \<in> trie_set (left t)"
  apply(cases t) apply auto
  done

lemma trie_member_correct_aux1: "trie_member bs t \<Longrightarrow> bs \<in> trie_set t"
  apply(induction bs arbitrary: t)
   apply (auto split: if_splits simp: True_prepend_right_subtree False_prepend_left_subtree)
  done

lemma trie_member_correct_aux2: "bs \<in> trie_set t \<Longrightarrow> trie_member bs t"
  apply(induction bs arbitrary: t)
   apply simp
  apply (auto split: simp: True_prepend_right_subtree False_prepend_left_subtree)
  subgoal for a bs t apply(cases t) apply (auto split: if_splits) done
  subgoal for a bs t apply(cases t) apply (auto split: if_splits) done
  done


lemma trie_member_correct[simp]: "trie_member bs t \<longleftrightarrow> bs \<in> trie_set t"
  using trie_member_correct_aux1 trie_member_correct_aux2 
  by blast
    
(*
Now we do the same for insert
*)
fun set_this where
  "set_this (Leaf b) = Leaf True"
| "set_this (Node l b r) = Node l True r"  
  
lemma set_this_correct[simp]: "trie_set (set_this t) = insert [] (trie_set t)"
  apply(cases t) 
   apply auto
  done
    
fun trie_insert :: "bool list \<Rightarrow> trie \<Rightarrow> trie" where
  "trie_insert [] t = set_this t" |
  "trie_insert (b#bs) t = (case t of 
    Leaf b' \<Rightarrow> Node (if b then Leaf False else trie_insert bs (Leaf False)) b' (if b then trie_insert bs (Leaf False) else Leaf False) | 
    Node l b' r \<Rightarrow> (if b then Node l b' (trie_insert bs r) else Node (trie_insert bs l) b' r)
  )"
  
lemma trie_insert_correct: "trie_set (trie_insert bs t) = insert bs (trie_set t)"
  apply(induction bs arbitrary: t)
   apply simp
  apply (auto split: trie.split)
  done

(*
And just like that, we can also delete.
*)

 fun reset_this where
  "reset_this (Leaf b) = Leaf False" |
  "reset_this (Node l b r) = Node l False r"
  
lemma reset_this_correct[simp]: "trie_set (reset_this t) = trie_set t - {[]}"
  apply(cases t)
   apply auto
  done
      
fun trie_delete :: "bool list \<Rightarrow> trie \<Rightarrow> trie" where
  "trie_delete [] t = reset_this t" |
  "trie_delete (b#bs) t = (case t of 
    Leaf b' \<Rightarrow> Leaf b' | 
    Node l b' r \<Rightarrow> (if b then Node l b' (trie_delete bs r) else Node (trie_delete bs l) b' r)
  )"
    
lemma trie_delete_correct: "trie_set (trie_delete bs t) = trie_set t - {bs}"
  apply(induction bs arbitrary: t)
   apply (auto split: trie.split)
  done
  

(*
This should give some overview of how to work with abstraction. You define a concrete datatype and
a corresponding abstract datatype with according operations. Sometimes, not all representable
data will be valid. In this case you need an invariant to shrink down the possible values.
You'll play with this in the homework!
*)


end