(*******************
  FINAL EXAM: Interactive Theorem Proving, Year 2022/23, Q2
  
  This theory will work in Isabelle2022.
  
  This theory depends on the theory Exam_Lib, which is included in the archive this exam was distributed in.

  CONTENTS OF ARCHIVE
    \<^item> This file and Exam_Lib.thy
    \<^item> A copy of the course's gitlab repo, with all homeworks and solutions, demos and warmups, and lecture slides
  
  GENERAL RULES:
    \<^item> You have three hours to work on this exam.
    \<^item> Before starting to work on this exam, make sure your internet connection is disabled (we will check that!)
    \<^item> When time is up, we will make an announcement. You can then switch on your internet, and send the exam to
      \<open>p.lammich@utwente.nl\<close>, using [ITP-EXAM] in the subject
    \<^item> Afterwards, please wait until we have confirmed that we received your solution.
    \<^item> If you want to submit earlier, raise your hand and ask for early submission

    \<^item> The questions are independent of each other! 
      The parts of a single question are largely independent, i.e., typically you can solve later parts
      without correctly solving previous parts. You should make sure, however, that the relevant lemmas are 'proved' with sorry.
    
    \<^item> Questions are annotated with a difficulty level (easy,easy+,medium,medium+,hard,hard+). 
      This is our subjective estimation of the difficulty, and is meant to help you to decide 
      in which order you process the questions, whether to skip a question, and if you are on the right track.
      For example, an easy question is unlikely to require a complicated 20-line proof.
        
    \<^item> You can use the sidekick-panel to get a quick overview of the contents of this exam.
        
  RULES FOR DEFINITIONS and PROOFS:
    \<^item> You should replace all terms of the form \<open>\<llangle>replace with your solution\<rrangle>\<close> by meaningful terms,
      and all \<open>sorry\<close>s and \<open>oops\<close>es by valid proofs.
  
    \<^item> Close your proofs (using qed/done or oops/sorry if need be). 
      Ideally, your submission contains no Isabelle errors!
      
    \<^item> Sledgehammer is allowed unless explicitly forbidden in the question. 
      However, make sure that sledgehammer does not pick up statements that you \<open>sorry\<close>ed in other 
      parts of the exam!
      In general, all the questions can be solved independently, and sledgehammer is actually only 
      needed where explicitly indicated in the question!
    \<^item> You can freely choose your proof style (apply, isar), unless explicitly restricted in the question
    \<^item> If you cannot fully prove a theorem, use auxiliary lemmas/sorry/comments to clearly explain us what part 
      you could not prove, and why you think it is provable. 
    \<^item> If we ask you to prove a lemma, you can convert it from short-goal to long-goal format (fixes/assumes,shows) if you like to.
      However, not that the given lemma format fits our recommended way of proving the lemma.
    \<^item> If we ask you to define a function, you can change the given definition style (definition/fun/inductive) if you like to. 
      However, note that the given definition style is how we recommend to define the function.
    
  
    

  
  
  
*******************)



theory (*< Exam*)Exam_Sol(*>*)
imports Exam_Lib
begin

(*<*)

abbreviation (input) hide_undefined :: "'a::{} \<Rightarrow> 'a::{}" ("\<llangle>undef _\<rrangle>") where "hide_undefined x \<equiv> x"
abbreviation (input) hide_undefined_prop :: "prop \<Rightarrow> prop" ("\<llangle>undefp _\<rrangle>") where "hide_undefined_prop x \<equiv> x"
  

(*>*)

section \<open>Functional Programming and Basic Induction (12 Points)\<close>

subsection \<open>Sum of spliced list (easy+) (2p)\<close>
(*
  In Isabelle's list library, there is a function "splice", 
  that splices two lists by repeatedly taking one element from each list.
*)
term splice
thm splice.simps

(*
  Use find_theorems, value, etc to make sure you understand what this function does!
*)

(*
  Show that the sum of a spliced list is the sum of the sums of the two original lists!
*)
lemma sum_list_splice: "sum_list (splice xs ys) = sum_list xs + sum_list ys" for xs ys :: "nat list"
(*
  Hint: note the interesting recursion pattern of splice, swapping the arguments in each recursion,
    and choose an appropriate induction method!
*)
(*< oops *)
  apply (induction xs ys rule: splice.induct)
  by auto
(*>*)  

subsection \<open>seriesUp (medium) (2p)\<close>

(* 
  Have a look at the Isabelle functions:
    concat
    map
    [l..<h]  (List.upt)
    
  If in doubt, use value, find_theorems, etc to understand what they do!  
*)

term concat
term map
term "[l..<h]" term "List.upt l h"


(*
  Use these functions to define a function 
    \<open>seriesUp :: nat \<Rightarrow> nat list\<close>, such that
  \<open>seriesUp n = [ 1,  1,2,  1,2,3,  \<dots>, 1,2,\<dots>,n ]\<close>

*)
definition seriesUp :: "nat \<Rightarrow> nat list" where 
  "seriesUp n = \<llangle>undef concat (map (\<lambda>i. [1..<i+1]) [1..<n+1])\<rrangle>"

(* Here are a few validity checks! *)
value "seriesUp 0 = []"
value "seriesUp 1 = [1]"
value "seriesUp 5 = [1,  1,2,  1,2,3,  1,2,3,4,  1,2,3,4,5]"

(* Prove! *)

lemma "length (seriesUp n) = (n+1)*n div 2"
  (*< oops*)
  apply (induction n)
  by (auto simp: seriesUp_def)
  (*>*)  
 
  
subsection \<open>map-filter via fold (medium) (3p)\<close>
(* Prove the following lemma, that expresses mapping and filtering by a fold! *)  
lemma "map f (filter P xs) = fold (\<lambda>x acc. if P x then acc@[f x] else acc) xs []"
(* Hint: recall that fold usually requires generalization for induction proofs! *)
(*< oops *)
proof -
  have "fold (\<lambda>x acc. if P x then acc@[f x] else acc) xs acc = acc@map f (filter P xs)" for acc
    apply (induction xs arbitrary: acc)
    by auto
  thus ?thesis by simp
qed
(*>*)
  
subsection \<open>Reduce (easy) (2p)\<close>

(*
  Consider the reduce operation on lists 
    reduce :: "('a\<Rightarrow>'a\<Rightarrow>'a) \<Rightarrow> 'a list \<Rightarrow> 'a"
   
  For an associative binary operator (\<cdot>), and a non-empty list xs=[x\<^sub>1,\<dots>,x\<^sub>n], we have
  reduce (\<cdot>) xs = x\<^sub>1 \<cdot> \<dots> \<cdot> x\<^sub>n

  For an empty list, reduce is undefined.
*)

(* Let's fix an associative binary operator f: *)  
context
  fixes f :: "'a\<Rightarrow>'a\<Rightarrow>'a"
  assumes assoc: "f (f a b) c = f a (f b c)"
begin
  (* A straightforward implementation of reduce is the following: *)
  fun reduce where
    "reduce [] = undefined"
  | "reduce [x] = x"
  | "reduce (x#xs) = f x (reduce xs)"

  (* Prove that we could also use foldr! *)
  lemma "xs\<noteq>[] \<Longrightarrow> reduce xs = foldr f (butlast xs) (last xs)"
  (*< oops*)
    apply (induction xs rule: reduce.induct)
    apply auto
    done
  (*>*)

subsection \<open>Parallel Reduce (hard) (3p)\<close>
      
  (* 
    The above version of reduce cannot be parallelised. 
    However, if we arrange the reductions in a tree structure, 
    we can execute each level of the tree in parallel.
  
    The following function works by splitting the list in half, reducing both halfs, 
    and then combining the results. 
    The reduction of both halfs could (in theory) be executed in parallel.
  *)    

  fun reduce' where
    "reduce' [] = undefined"
  | "reduce' [x] = x"
  | "reduce' xs = (let 
      m=length xs div 2 in 
      f (reduce' (take m xs)) (reduce' (drop m xs))
    )"
  
  (* Show that both versions of reduce are equal!
  
    Hints: 
      * don't forget that f is associative
      * you'll need an auxiliary lemma about \<open>reduce (xs@ys)\<close>
      * if (auto simp: \<dots>) does not succeed in using your auxiliary lemma, use Isar
  
  *)    
  thm assoc

  (*<*)    
  lemma reduce_append: "\<lbrakk> xs\<noteq>[]; ys\<noteq>[] \<rbrakk> \<Longrightarrow> reduce (xs@ys) = f (reduce xs) (reduce ys)"
    apply (induction xs rule: reduce.induct)
    apply (auto simp: assoc neq_Nil_conv)
    done
  (*>*)
    
  lemma "xs\<noteq>[] \<Longrightarrow> reduce' xs = reduce xs"
  (*< oops *)
    apply (induction xs rule: reduce'.induct)
    apply (auto simp: Let_def)
    apply (auto simp: Let_def simp flip: reduce_append)
    done
  (*>*)

  
end

section \<open>Arithmetic Sequence (12 Points)\<close>
(*<*)
(* Skills: Isar
*)
(*>*)

(* Recall the Gauss sum, and it's closed form:

  0 + 1 + \<dots> + (n-1) = (n*(n-1)) div 2
*)

lemma gauss_sum: "(\<Sum>i=0..<n. i) = (n*(n-1)) div 2" for n :: nat
  by (simp add: Sum_Ico_nat)


(*
  An arithmetic sequence is a sequence where the distance of each two elements is constant.
  It's determined by a start value a, and a distance d. The ith element is:
    aseq\<^sub>i = a + i*d

  In Isabelle:  
*)

context
  fixes a d :: nat
begin

  definition "aseq i = a + i*d"

  (*
    Note that the Gauss-sequence is an arithmetic sequence with a=0 and d=1!
  
    To determine a closed form for the sum of an arbitrary arithmetic sequence, we can use the 
    already known sum of the Gauss sequence, as follows:
    
      \<Sum>i=0..<n. a + i*d
    = a*n + (\<Sum>i=0..<n. i)  * d   -- split sum, sum of constants, and factor out d
            | = n*(n-1)/2 |       -- Use formula for Gauss-sum
    = a*n + ((n*(n-1)) div 2) * d -- Assemble everything
  
  *)
  
  (* Formalize the above proof sketch in Isar. Try to keep the level of detail close to the sketch!
  
    Hint: use find_theorems/sledgehammer to find the relevant lemmas about \<Sum>. 
      Alternatively, prove them yourself as auxiliary lemmas.
      
    DO NOT USE SLEDGEHAMMER TO SHORTCUT THE GIVEN PROOF SKETCH!  
  *)
  
  subsection \<open>Isar Proof (easy) (12p)\<close>

  lemma "(\<Sum>i=0..<n. aseq i) = a*n + ((n*(n-1)) div 2) * d"
  proof -
  (*< oops*)
    have "(\<Sum>i=0..<n. a + i * d) = a*n + (\<Sum>i=0..<n. i)*d" 
      by (simp add: sum.distrib sum_distrib_right)
    also have "(\<Sum>i=0..<n. i) = (n*(n-1)) div 2" 
      by (simp add: gauss_sum)
    finally show ?thesis  
      unfolding aseq_def .
  qed     
  (*>*)
  
end

  

section \<open>Finite State Machines (12 Points)\<close>
(*<*)
(*
  Skills: 
    induction, computation induction
    graphs, paths
    
*)
(*>*)

(* Consider the transition relation of a finite state machine, i.e. a labeled directed graph. 
  
  We set the states to be int, and the alphabet to be char:
*)
type_synonym state = int
type_synonym fsm_trans = "(state \<times> char \<times> state) set"

(* And fix a transition relation \<delta> *)
context
  fixes \<delta> :: fsm_trans
begin

(* We can define a word between two states as follows: *)
fun word where
  "word q\<^sub>1 [] q\<^sub>2 \<longleftrightarrow> q\<^sub>1=q\<^sub>2"
| "word q\<^sub>1 (a#w) q\<^sub>3 \<longleftrightarrow> (\<exists>q\<^sub>2. (q\<^sub>1,a,q\<^sub>2)\<in>\<delta> \<and> word q\<^sub>2 w q\<^sub>3)"

  
subsection \<open>Alternative Definition of word (medium+) (3p)\<close>

(* Alternatively, we could have used an inductive definition: *)
inductive word' where
  emp: "word' q [] q"
| app: "\<lbrakk> word' q\<^sub>1 w q\<^sub>2; (q\<^sub>2,a,q\<^sub>3)\<in>\<delta> \<rbrakk> \<Longrightarrow> word' q\<^sub>1 (w@[a]) q\<^sub>3"  

(*<*)
lemma [simp]: "word q\<^sub>1 (w\<^sub>1@w\<^sub>2) q\<^sub>3 \<longleftrightarrow> (\<exists>q\<^sub>2. word q\<^sub>1 w\<^sub>1 q\<^sub>2 \<and> word q\<^sub>2 w\<^sub>2 q\<^sub>3)"
  by (induction w\<^sub>1 arbitrary: q\<^sub>1) auto 
 
lemma word'_imp_word: "word' q w q' \<Longrightarrow> word q w q'"
  apply (induction rule: word'.induct)
  by auto

lemma word_imp_word': "word q w q' \<Longrightarrow> word' q w q'"
  apply (induction w arbitrary: q' rule: rev_induct)
  apply (auto intro: word'.intros)
  done

(*>*)

(* prove that both definitions are equal!

  Hint: there's a twist here: 
    word is defined to append characters to the word, while word' prepends characters.
    This makes the proof slightly more complicated. Here are a few hints how to proceed:
    * Prove both directions separately
    * Prove an append-lemma for word', i.e., something of the form \<open>word' _ (_@_) _ \<longleftrightarrow> _\<close>
    * Wisely choose your induction method. E.g., the rule \<open>rev_induct\<close> does induction over a list, 
      but appending instead of prepending elements.

*)

thm rev_induct

lemma "word q w q' = word' q w q'"
  (*< oops *)
  using word_imp_word' word'_imp_word ..
  (*>*)  

end

subsection \<open>Renaming States (easy+) (3p)\<close>

(*
  Now, we want to rename states with an injective function. 
  We restrict ourselves to (injective) functions of the form \<open>\<lambda>x. x+ofs\<close> (recall that states are ints)
*)

definition "rename_trans ofs \<delta> = { (q+ofs,a,q'+ofs) | q a q'. (q,a,q')\<in>\<delta> }"

(*
  Show that renaming does not change the accepted words!
  (Hint: the proof works best if you convert the statement to 
    word' first, using what you have proved (or "sorry"ed) in the previous part!
  )
*)

(** ERROR: the hint was misleading! (we had changed word/word', and forgot to remove the hint) *)

lemma rename_word: "word (rename_trans ofs \<delta>) (q+ofs) w (q'+ofs) \<longleftrightarrow> word \<delta> q w q'"
  (*< sorry*)
  apply (induction w arbitrary: q)
  apply (auto simp: rename_trans_def)
  done
  (*>*)

subsection \<open>Language of FSM (easy+) (3p)\<close>
  
(*
  A finite state machine (FSM) is a transition relation together with a start state and a set of final states.
*)  
type_synonym fsm = "fsm_trans \<times> state \<times> state set"

(*
  The language of a finite state machine is the set of words between the start state and a final state.

  Define that formally!
*)  
definition L :: "fsm \<Rightarrow> string set" where "L \<equiv> \<lambda>(\<delta>,s,F). \<llangle>undef { w. \<exists>q\<in>F. word \<delta> s w q }\<rrangle>"

subsection \<open>Renaming FSM (medium) (3p)\<close>
(*
  Define a function rename, that renames a finite state machine, and prove that renaming does
  not change the language.
*)

definition rename :: "int \<Rightarrow> fsm \<Rightarrow> fsm" where 
  "rename ofs \<equiv> \<lambda>(\<delta>,s,F). \<llangle>undef (rename_trans ofs \<delta>,s+ofs,(\<lambda>q. q+ofs)`F)\<rrangle>"

(* Hint: depending on your exact definition of rename, the proof may be slightly beyond the capabilities of auto.
  Try sledgehammer, or do an Isar proof in that case!
*)
lemma "L (rename ofs fsm) = L fsm"
(*< oops *)
  unfolding L_def
  by (auto simp: rename_def rename_word)
(*>*)  






section \<open>ADTs (12 Points)\<close>
(*<*)
(*
  Skills: 
    ADTs (specifying correctness lemmas for impl)
    Induction+generalization
    While-Loops: Finding invariant
    Finding non-straightforward proof (a bit of Isar or manual fine-tuning)
*)
(*>*)

subsection \<open>Implementing a FIFO queue by two stacks (medium) (6p)\<close>

(*
  A queue is an abstract data type based on a list, that supports the operations:
  
    empty = []
    is_empty xs = (xs=[])
    enqueue a xs = xs@[a]
    dequeue xs = (hd xs, tl xs)
  
  That is, we append elements to the end, and take them from the front.  
    
  This data type is also called FIFO queue (first-in first out), as we always dequeue the 'oldest' element in the list.
      

  To provide an efficient functional FIFO queue implementation, we use two stacks l and r: 
    enqueue enqueues it's element to r, while dequeue takes its elements from l.
    If l is empty, the content of r is copied over to l (reversing it in the process).
   
  Side Remark: this implementation ensures that enqueue always takes constant time,
    and dequeue only takes non-constant time if it has to copy the stacks.
    When regarding a sequence of enqueue and dequeue operations, each of them takes constant time on average.
    
*)

(* In Isabelle, we define this data structure as follows: *)
type_synonym 'a fifo = "'a list \<times> 'a list"

(* The abstraction function maps a fifo to the list it represents *)
definition "fifo_\<alpha> \<equiv> \<lambda>(l,r). l @ rev r"

(* The operations on the queue are implemented as follows *)  
definition "fifo_empty = ([],[])"  
definition "fifo_is_empty \<equiv> \<lambda>(l,r). l=[] \<and> r=[]"
definition "fifo_enqueue x \<equiv> \<lambda>(l,r). (l,x#r)"
definition "fifo_dequeue \<equiv> \<lambda>(l,r). 
  if l=[] then (last r, (rev (butlast r),[])) 
  else (hd l, (tl l,r))"

(*
  Specify and prove the correctness theorems for the above four operations!

  (Use \<open>fifo_\<alpha>\<close> to relate the concrete queue with the abstract list it represents)
*)

(* easy *)
lemma fifo_empty_correct: "\<llangle>undef fifo_\<alpha> fifo_empty = []\<rrangle>"
(*< sorry *)
  by (auto simp: fifo_\<alpha>_def fifo_empty_def)
(*>*)  
  
(* easy *)
lemma fifo_is_empty_correct: "\<llangle>undef fifo_is_empty ds \<longleftrightarrow> fifo_\<alpha> ds = []\<rrangle>"
(*< sorry *)
  by (auto simp: fifo_\<alpha>_def fifo_is_empty_def)
(*>*)  
  
(* easy *)
lemma fifo_enqueue_correct: "\<llangle>undef fifo_\<alpha> (fifo_enqueue x ds) = fifo_\<alpha> ds @ [x]\<rrangle>"
(*< sorry *)
  by (auto simp: fifo_\<alpha>_def fifo_enqueue_def split: prod.splits)
(*>*)  


(* medium+ *)
lemma fifo_dequeue_correct: "\<llangle>undefp fifo_dequeue ds = (x,ds') \<Longrightarrow> fifo_\<alpha> ds \<noteq> [] \<Longrightarrow> x = hd (fifo_\<alpha> ds) \<and> fifo_\<alpha> ds' = tl (fifo_\<alpha> ds)\<rrangle>"
(*< sorry *)
  unfolding fifo_\<alpha>_def fifo_dequeue_def
  apply (auto split: prod.splits if_splits simp: hd_rev )
  by (metis butlast_rev rev_swap)
(*>*)  

subsection \<open>List data structure from set (medium+) (6p)\<close>  

(* Now consider the following set data structure: *)
typ "'a ls" term ls_\<alpha>

(* It can represent finite sets only: *)
thm ls_\<alpha>_finite

(* and has the following operations: *)
term ls_empty thm ls_empty_correct (* Empty set *)
term ls_is_empty thm ls_is_empty_correct (* Check for emptiness *)
term ls_insert thm ls_insert_correct (* Insert element *)
term ls_delete thm ls_delete_correct (* Delete element *)
term ls_some thm ls_some_correct (* Pick some element from non-empty set *)

(* NOTE: to solve this question, you do not need to look into the details of the implementation,
    which we have defined to be more complicated than necessary on purpose!
  The constants and lemmas given above are enough.
*)

(*
  The following function uses a while-loop to iteratively remove the elements from
  an ls data structure, and add them to a list.
*)
definition "ls_to_list ls = (fst (the (while_option 
  (\<lambda>(acc,ls). \<not>ls_is_empty ls) 
  (\<lambda>(acc,ls). (ls_some ls#acc, ls_delete (ls_some ls) ls))
  ([],ls))))
  "

(*
  Your task is to show that:
    \<^item> the resulting list contains the correct elements (medium)
    \<^item> the resulting list is distinct (medium+)
    \<^item> the loop terminates (medium+)

  We have set up the proof structure for you already. 
  You only need to find a suitable invariant and termination relation, and prove
  the auxiliary lemmas below.

  Recommended approach:
    \<^item> First, try to find an invariant that guarantees the correct elements, but not necessarily distinctness,
      and prove all auxiliary lemmas but \<open>ls_to_list_invar_final2\<close>.

    \<^item> Save your solution somewhere, and make sure you can quickly go back to it in case you get stuck in the next step! 
  
    \<^item> Amend your invariant such that it guarantees distinctness, and adapt the proofs of the auxiliary lemmas.
  
  
*)  
  
definition "ls_to_list_invar ls\<^sub>0 \<equiv> \<lambda>(acc,ls). \<llangle>undef ls_\<alpha> ls\<^sub>0 = set acc \<union> ls_\<alpha> ls \<and> distinct acc \<and> set acc \<inter> ls_\<alpha> ls = {}\<rrangle>"

definition "ls_to_list_trel \<equiv> measure \<llangle>undef (\<lambda>(acc,ls). card (ls_\<alpha> ls))\<rrangle>"
(*
  Hints: 
    \<^item> you can find a relation that does not depend on the initial data structure.
      Of course, you are welcome to add an ls\<^sub>0 parameter here if you want to!
    
    \<^item> the number of elements in a finite set is returned by the card function. 
      Many of its lemmas are in the simpset, but you can always use find_theorems etc. 
      to discover more useful lemmas
    
*)

term card

find_theorems "card (_ - {_})"

(* Easy *)  
lemma ls_to_list_invar_initial: "ls_to_list_invar ls ([], ls)"
(*< sorry *)
  by (auto simp: ls_to_list_invar_def)
(*>*)  

(* Medium+ *)  
lemma ls_to_list_invar_step: "\<lbrakk>ls_to_list_invar ls\<^sub>0 (acc, ls); \<not> ls_is_empty ls\<rbrakk> 
  \<Longrightarrow> ls_to_list_invar ls\<^sub>0 (ls_some ls # acc, ls_delete (ls_some ls) ls)"  
(*< sorry *)
  using ls_some_correct
  by (force simp: ls_is_empty_correct ls_to_list_invar_def ls_delete_correct)
(*>*)  
  
(* Easy *)  
lemma ls_to_list_invar_final1: "\<And>acc ls. \<lbrakk>ls_to_list_invar ls\<^sub>0 (acc, ls); ls_is_empty ls\<rbrakk> \<Longrightarrow> set acc = ls_\<alpha> ls\<^sub>0"
(*< sorry *)
  unfolding ls_to_list_invar_def
  by (simp add: ls_is_empty_correct)
(*>*)  

(* Easy (provided you found a good invariant for distinctness!).
  
  Skip the proof of this lemma until you have found an invariant that guarantees distinctness
*)
lemma ls_to_list_invar_final2: "\<And>acc ls. \<lbrakk>ls_to_list_invar ls\<^sub>0 (acc, ls); ls_is_empty ls\<rbrakk> \<Longrightarrow> distinct acc"
(*< sorry *)
  unfolding ls_to_list_invar_def
  by simp
(*>*)  
  
(* easy *)    
lemma ls_to_list_trel_wf: "wf ls_to_list_trel" 
(*< sorry *)
  by (simp add: ls_to_list_trel_def)
(*>*)  
  

(* medium+ 

  use find_theorems to get useful lemmas about card, and, if necessary, Isar/sledgehammer
*)
lemma ls_to_list_trel_decr: "\<lbrakk>ls_to_list_invar ls\<^sub>0 (acc, ls); \<not> ls_is_empty ls\<rbrakk> 
  \<Longrightarrow> ((ls_some ls # acc, ls_delete (ls_some ls) ls), acc, ls) \<in> ls_to_list_trel"
(*< sorry *)
  unfolding ls_to_list_trel_def
  using ls_some_correct
  by (force simp: ls_is_empty_correct ls_delete_correct ls_\<alpha>_finite intro!: card_Diff1_less)
(*>*)  
  

(* Do not modify this proof! (Except for adding an ls\<^sub>0 parameter to ls_to_list_trel if you want to) *)
lemma ls_to_list_correct: "set (ls_to_list ls\<^sub>0) = ls_\<alpha> ls\<^sub>0 \<and> distinct (ls_to_list ls\<^sub>0)"
  unfolding ls_to_list_def
  apply (rule while_option_rule'[where P="ls_to_list_invar ls\<^sub>0" and r="ls_to_list_trel"])
  apply (clarsimp 
    simp: ls_to_list_invar_initial ls_to_list_invar_step 
    simp: ls_to_list_invar_final1 ls_to_list_invar_final2 
    simp: ls_to_list_trel_wf ls_to_list_trel_decr)+
  done



    
section \<open>Handling Errors in the Option Monad (12 Points)\<close>
(*<*)
(* Skills: monads
*)
(*>*)

(*
  Recall the option monad: errors are propagated, and cannot be recovered from.
*)

subsection \<open>Implementing Handle (easy+) (2p)\<close>
(*
  Implement a function 
    \<open>handle :: 'a option \<Rightarrow> 'a option \<Rightarrow> 'a option\<close>
  such that \<open>handle m h\<close> executes m, and, should that fail, executes h. 
*)

fun handle :: "'a option \<Rightarrow> 'a option \<Rightarrow> 'a option" where
(*< "handle _ _ = undefined" *)
  "handle (Some a) _ = Some a"
| "handle None h = h"  
(*>*)


subsection \<open>Laws for handle (medium) (3p)\<close>
(*
  Analogously to the monad laws that connect bind and return,
  we can define laws that connect handle and fail.
  Complete and prove the following lemmas!
*)

(* easy *)
lemma "handle None h = \<llangle>undef h\<rrangle>" 
  (*< oops*)
  by simp
  (*>*)

(* easy *)
lemma "handle m None = \<llangle>undef m\<rrangle>" 
  (*< oops*)
  by (cases m) auto
  (*>*)

(* medium *)
lemma "handle (handle m h\<^sub>1) h\<^sub>2 = handle m (\<llangle>undef handle h\<^sub>1 h\<^sub>2\<rrangle>)"
  (*< oops*)
  by (cases m) auto
  (*>*)
    
subsection \<open>Weakest Precondition (medium+) (3p)\<close>
(*
  As failure can now be recovered from, our weakest precondition predicate 
  needs to have post-conditions for both, a normal result and failure!
*)

fun wp :: "'a option \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool" where
  "wp None Q F \<longleftrightarrow> F"
| "wp (Some x) Q F \<longleftrightarrow> Q x"

(*
  Thus, in \<open>wp m Q F\<close>, \<open>Q\<close> is the post-condition in case of a normal result,
  and \<open>F\<close> is the post-condition in case of failure. 
  As there is no result associated with a failure, F is merely a Boolean flag that indicates
  if failure is allowed or not.
*)

(* Complete and prove the following lemmas for wp! *)

lemma wp_bind: "wp (bind m f) Q F \<longleftrightarrow> wp m \<llangle>undef (\<lambda>x. wp (f x) Q F)\<rrangle> \<llangle>undef F\<rrangle>"
  (*< sorry *)
  by (cases m) auto
  (*>*)

lemma wp_handle: "wp (handle m h) Q F \<longleftrightarrow> wp m \<llangle>undef Q\<rrangle> \<llangle>undef (wp h Q F)\<rrangle>"
  (*< sorry *)
  by (cases m) auto
  (*>*)

(* We also have a consequence rule for wp. Add the missing assumptions and prove! *)  
lemma wp_cons:
  assumes "wp m Q' F'"
  (* assumes \<dots>  \<leftarrow> some assumptions are missing here! *)
  (*<*)
  assumes "\<And>x. Q' x \<Longrightarrow> Q x"
  assumes "F' \<Longrightarrow> F"
  (*>*)
  shows "wp m Q F"
  (*< sorry*)
  using assms by (cases m) auto
  (*>*)

(* At this point, you won't need the definition of bind and handle any more, 
  as you should use the wp_bind and wp_handle lemmas!
  
  The following setup tries to ensure that you (or sledgehammer) does not accidentally use them.
*)  

lemmas [simp del, no_atp] = Option.bind.simps
lemmas [simp del, no_atp] = handle.simps
  
  
subsection \<open>Monadic Fold (hard) (2p)\<close>

(*
  Recall Isabelle's fold function
*)
term fold
thm fold_simps

(* 
  Define a fold function that folds a list using a function that goes into the option monad.
    f :: 'a \<Rightarrow> 's \<Rightarrow> 's option.
    
  If f fails for any element, the whole fold fails:
*)
  
fun mfold :: "('a \<Rightarrow> 'b \<Rightarrow> 'b option) \<Rightarrow> 'a list \<Rightarrow> 'b \<Rightarrow> 'b option" where
  (*< "mfold _ _ _ = undefined" *)
  "mfold f [] s = Some s"
| "mfold f (x#xs) s = do { s \<leftarrow> f x s; mfold f xs s }"
  (*>*)

(*
  In the lecture, we discussed an invariant rule for fold (cf. Numbering_Demo.thy). 
  The invariant only kept track of the set of elements already iterated over.
  
  We now want to prove a stronger rule, where the invariant keeps track of the
  exact position in the iteration:
  
    \<open>I xs\<^sub>1 xs\<^sub>2\<close> is the invariant when we have iterated over the elements \<open>xs\<^sub>1\<close>,
      and still need to iterate over \<open>xs\<^sub>2\<close>

  Prove the following fold rule!
  
    Hint: requires the right generalization for the induction to go through.
      You might get inspired by the proof of the fold-rule in Numbering_Demo!
  
*)
lemma wp_mfold:
  assumes "I [] xs s\<^sub>0"
  assumes "\<And>xs\<^sub>1 x xs\<^sub>2 s. \<lbrakk> xs=xs\<^sub>1@x#xs\<^sub>2; I xs\<^sub>1 (x#xs\<^sub>2) s \<rbrakk> 
    \<Longrightarrow> wp (f x s) (I (xs\<^sub>1@[x]) xs\<^sub>2) False"
  assumes "\<And>s. I xs [] s \<Longrightarrow> Q s"
  shows "wp (mfold f xs s\<^sub>0) Q False"
  (*< sorry *)  
proof -

  have "\<lbrakk>I xs\<^sub>1 xs\<^sub>2 s; xs=xs\<^sub>1@xs\<^sub>2\<rbrakk> \<Longrightarrow> wp (mfold f xs\<^sub>2 s) Q False" for xs\<^sub>1 xs\<^sub>2 s
    apply (induction xs\<^sub>2 arbitrary: xs\<^sub>1 s)
    apply (auto intro: assms(3)) []
    apply (clarsimp simp: wp_bind)
    apply (rule wp_cons, erule assms(2))
    apply auto
    done
    
  from this[OF assms(1)] show ?thesis by simp
qed
(*>*)

  
subsection \<open>Summing Up (medium+) (2p)\<close>

(*
  If you could not solve the previous part,
  make sure to at least have some constant mfold and the wp_mfold lemma.
  
  If in doubt, use "mfold = undefined", and sorry for the wp_mfold lemma!
*)

(* The following function looks up a key in a key-value map, and fails if the key is not contained *)
definition lookup :: "('k \<Rightarrow> 'v option) \<Rightarrow> 'k \<Rightarrow> 'v option" where "lookup m k = m k"

lemma wp_lookup: "wp (lookup m k) (\<lambda>v. m k = Some v) (k\<notin>dom m)"
  unfolding lookup_def
  apply (cases "m k")
  by auto


(*
  Given a key-value map, we can use mfold to sum up
  all values for a given list of keys, failing if a key does not exist.
*)

definition sum_up :: "('k \<Rightarrow> nat option) \<Rightarrow> 'k list \<Rightarrow> nat option"
  where "sum_up db ks \<equiv> mfold (\<lambda>k s. do { v\<leftarrow>lookup db k; Some (v+s) }) ks 0"

(*
  As an example, think of, e.g., a map from items to prices, and
  computing the cost of a list of items by summing up all prices.
*)  
value "
  let 
    prices_db = [
      ''Salami Pizza'' \<mapsto> 8, 
      ''Funghi Pizza'' \<mapsto> 7,
      ''Seafood Pizza'' \<mapsto> 9,
      ''Coke'' \<mapsto> 3,
      ''Beer'' \<mapsto> 4
    ];
    order1 = [''Salami Pizza'',''Coke'',''Beer'',''Beer'',''Beer'',''Beer''];
    order2 = [''Seafood Pizza'',''Wine'']
  in (sum_up prices_db order1, sum_up prices_db order2)
"  
(*
  should yield: 
    "(Some 27, None)" :: "nat option \<times> nat option"

  however, if you haven't defined mfold in the above question, 
  the value command won't work, and you'll get something along the lines:
  
    exception Fail raised (line 126 of "generated code"): undefined
    
*)  
  
  
  
(* Find an invariant and prove the following lemma! *)
definition sum_up_invar :: "('k \<Rightarrow> nat option) \<Rightarrow> 'k list \<Rightarrow> 'k list \<Rightarrow> 'k list \<Rightarrow> nat \<Rightarrow> bool"
  where "sum_up_invar db ks xs\<^sub>1 xs\<^sub>2 s \<longleftrightarrow> \<llangle>undef s = sum_list (map (\<lambda>k. the (db k)) xs\<^sub>1)\<rrangle>"

lemma "set ks \<subseteq> dom db 
  \<Longrightarrow> wp (sum_up db ks) (\<lambda>r. r = sum_list (map (\<lambda>k. the (db k)) ks)) False"
  unfolding sum_up_def
  apply (rule wp_mfold[where I="sum_up_invar db ks"])
  (*< oops*)  
  unfolding sum_up_invar_def
  apply (auto simp: wp_bind)
  apply (rule wp_cons, rule wp_lookup)
  apply auto
  done
  (*>*)  



      
(*<*)
    
  (*
    Topics/Skills:
  
      functional programming
    
      function/inductive/definition

      specification: translating intuitive \<rightleftharpoons> formal
            
      induction. 
        structural/computation/rule
        generalization
        
      Isar
        formalizing mathematical proofs
      
      ADTs  
        interface
        using ADTs in bigger algorithms
      
      While-Loops
        invariant/variant  
        well-founded relations

      Monads+vcg:
        monads + monad laws
        wp
        syntactically driven vcg
      
                
      Graph Algorithms
        Representing a graph, path, etc
        
      Isabelle handling:
        find-theorems/consts  
        
      structuring proofs. modularization
  
  *)

(*>*)


end

