theory Refine
imports Hoare_Logic
begin

  subsection \<open>Miscellaneous Setup\<close>
  lemma rel_compp_id[simp]:
    "R OO (=) = R" 
    "(=) OO R = R" 
    by auto


  subsection \<open>Refinement\<close>  
  
  text \<open>
    A concrete program \<open>m\<close> refines an abstract program \<open>m'\<close> with (data) refinement relation \<open>R\<close>,
    iff either \<open>m'\<close> fails, or, \<open>m\<close> does not fail, and every result in \<open>m\<close> 
    is related to some result in \<open>m'\<close> via relation \<open>R\<close>.
    
    Intuitively, each result of the concrete program must be justified by a 
    corresponding abstract result. Moreover, we will ultimately prove that the abstract program does not fail.
    Thus, for refinement, we can assume that the abstract program does not fail, which means, that the failing
    abstract program is trivially refined by everything.  
  \<close>
  
  definition [pw_init]: "refines R m m' 
    \<longleftrightarrow> is_fail m' \<or> (\<not>is_fail m \<and> (\<forall>x. is_res m x \<longrightarrow> (\<exists>x'. R x x' \<and> is_res m' x')))"
  
  text \<open>Refinement is reflexive and transitive\<close>  
  lemma refines_refl[simp]: "refines (=) m m"  
    and refines_trans[trans]: "refines R\<^sub>1 m\<^sub>1 m\<^sub>2 \<Longrightarrow> refines R\<^sub>2 m\<^sub>2 m\<^sub>3 \<Longrightarrow> refines (R\<^sub>1 OO R\<^sub>2) m\<^sub>1 m\<^sub>3"
    apply pw
    apply (pw; blast)
    done

  text \<open>Pattern-matching style definition\<close>
  lemma 
    "refines R m FAIL \<longleftrightarrow> True"  
    "refines R (SPEC P) (SPEC Q) \<longleftrightarrow> (\<forall>x. P x \<longrightarrow> (\<exists>y. Q y \<and> R x y))"
    "refines R FAIL (SPEC Q) \<longleftrightarrow> False"
    by pw+
    
    
  text \<open>Transitivity can also be applied with a weakest 
    precondition, as @{lemma \<open>wp m Q = refines (=) m (SPEC Q)\<close> by pw}.\<close>  
  lemma refines_wp_trans[trans]:
    "refines R\<^sub>1 m\<^sub>1 m\<^sub>2 \<Longrightarrow> wp m\<^sub>2 Q \<Longrightarrow> refines R\<^sub>1 m\<^sub>1 (SPEC Q)"
    by pw 
    
  text \<open>Once transitivity has been used to show that 
    a concrete implementation refines the abstract 
    specification, a Hoare triple for the concrete 
    implementation can be derived.\<close>  
  lemma refines_SPEC_wpI:
    assumes "refines R m (SPEC Q)"
    assumes "\<And>r r'. Q r' \<Longrightarrow> R r r' \<Longrightarrow> P r"
    shows "wp m P"
    using assms by pw
    
    
    
  subsection \<open>Automation\<close>
  text \<open>Whenever a syntactic rule introduces a fresh refinement relation, we wrap it
    into a \<open>rel\<close> constant. This is a purely syntactic marker to allow us to infer a suitable 
    refinement relation in a controlled way.
  \<close>
  definition rel:: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> bool" where "rel R \<equiv> R"

  named_theorems rel_rule \<open>Theorems to instantiate refinement relations\<close>

  text \<open>Method to instantiate relations, and then unfold the \<^const>\<open>rel\<close> tagging constant.\<close>
  method rel = (((changed \<open>all \<open>(changed \<open>rule rel_rule | (rule asm_rl[of "rel _ _ _"], rprems)\<close>)?\<close>\<close>)+)?, (unfold rel_def)?)?
    
  lemma rel_prod[rel_rule]: "rel A a a' \<Longrightarrow> rel B b b' \<Longrightarrow> rel (rel_prod A B) (a,b) (a',b')"
    unfolding rel_def by simp

  text \<open>Use this lemma to declare refinement relations\<close>  
  lemma rel: "rel R a b \<Longrightarrow> rel R a b" .

  text \<open>For example, always use identity if possible 
    (if concrete and abstract type are equal)\<close>
  lemmas [rel_rule] = rel[of "(=)"]
  
      
  subsection \<open>Syntactic Rules\<close>  
  text \<open>We prove syntactic rules to prove refinement goals\<close>

 
  lemma refines_FAIL[wp_rule]: "refines R m FAIL" by pw     
    
  lemma refines_RETURN[wp_rule]: "rel R x x' \<Longrightarrow> refines R (RETURN x) (RETURN x')"
    unfolding rel_def by pw

  lemma refines_SPEC[wp_rule]: "\<lbrakk>\<And>x. P x \<Longrightarrow> \<exists>x'. R x x' \<and> P' x'  \<rbrakk> \<Longrightarrow> refines R (SPEC P) (SPEC P')"
    by pw

  text \<open>Special case of refining a specification by an implementation\<close>  
  lemma refines_SPEC_wp[wp_rule]: "wp m P \<Longrightarrow> refines (=) m (SPEC P)"
    by pw
    
    
    
  text \<open>The following rules treat assertions specially, 
    in particular, they do not require a corresponding assertion 
    on the other side of the refinement.\<close>
    
  text \<open>As we have defined refinement to trivially hold for the failing abstract program,
    we can assume that an abstract assertion does not fail: \<close>  
  lemma refines_assert_bind_right[wp_rule]: 
    assumes "P' \<Longrightarrow> refines R m m'"
    shows "refines R m (do { assert P'; m' })"  
    using assms by pw
  
  text \<open>Symmetrically, we have to prove that concrete assertions do not fail:\<close>  
  lemma refines_assert_bind_left[wp_rule]: 
    assumes "\<not>is_fail m' \<Longrightarrow> P"
    assumes "refines R m m'"
    shows "refines R (do { assert P; m }) (m')"  
    using assms by pw
    
  text \<open>The rules for the assertions are defined \<^emph>\<open>before\<close> the general bind rule. 
    Thus, the @{method wp} method tries them first.
    \<close>
  lemma refines_BIND[wp_rule]:
    assumes "refines Rh m m'"
    assumes "\<And>x x'. \<lbrakk> Rh x x'; is_res m x; is_res m' x' \<rbrakk> \<Longrightarrow> refines R (f x) (f' x')"
    shows "refines R (BIND m (\<lambda>x. f x)) (BIND m' (\<lambda>x'. f' x'))"
    using assms
    apply pw
    by metis
    
    
    
  lemma refines_if[wp_rule]: 
    assumes "rel (=) b b'"
    assumes "b' \<Longrightarrow> refines R m\<^sub>1 m\<^sub>1'"
    assumes "\<not>b' \<Longrightarrow> refines R m\<^sub>2 m\<^sub>2'"
    shows "refines R (if b then m\<^sub>1 else m\<^sub>2) (if b' then m\<^sub>1' else m\<^sub>2')"  
    using assms unfolding rel_def by simp

  lemma refines_let[wp_rule]:
    assumes "\<And>x x'. x=v \<Longrightarrow> x'=v' \<Longrightarrow> refines R (f x) (f' x')"
    shows "refines R (let x=v in f x) (let x'=v' in f' x')"
    using assms unfolding Let_def by simp  
    
  lemma refines_case_prod[wp_rule]: 
    assumes "\<And>a b a' b'. \<lbrakk> p=(a,b); p'=(a',b') \<rbrakk> \<Longrightarrow> refines R (f a b) (f' a' b')"
    shows "refines R (case p of (a,b) \<Rightarrow> f a b) (case p' of (a',b') \<Rightarrow> f' a' b')"  
    using assms by (auto split: prod.split)  
  
  text \<open>The rule for recursion is easily proved by fixed-point induction. \<close>  
  lemma refines_rec[wp_rule]: 
    assumes M: "nres_mono F"
    assumes R: "rel R x x'"
    assumes S: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> refines S (D x) (D' x') \<rbrakk> \<Longrightarrow> refines S (F D x) (F' D' x')"
    shows "refines S (REC F x) (REC F' x')"
  proof -
  
    have "\<forall>x. R x x' \<longrightarrow> refines S (REC F x) (REC F' x')"
      apply (rule REC_pointwise_induct[where F=F'])
      apply pw
      apply (subst REC_unfold[OF M])
      apply (blast intro: S[unfolded rel_def])
      done
    thus ?thesis using R unfolding rel_def by blast
  qed

  text \<open>Tell the @{method wp} method to discharge refinement goals from the premises. 
    Such premises are generated by the refinement rules for recursion \<close>  
  lemmas [wp_recursion_rule] = asm_rl[of "refines _ _ _"]
    
  
  
  text \<open>We also derive rules for annotated recursions\<close>
  lemma refines_recI[wp_rule]: 
    assumes M[THEN nres_monoD, mono]: "nres_mono F"
    assumes R[rel_rule]: "rel R x x'"
    assumes S[wp_rule]: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> refines S (D x) (D' x') \<rbrakk> \<Longrightarrow> refines S (F D x) (F' D' x')"
    assumes P: "\<And>x x'. R x x' \<Longrightarrow> P' x' \<Longrightarrow> P x"
    shows "refines S (RECI V P F x) (RECI V' P' F' x')"
    unfolding RECI_def
    apply wp
    apply rel
    using P M
    by simp_all
  
  lemma refines_rec_recI[wp_rule]: 
    assumes M[THEN nres_monoD, mono]: "nres_mono F"
    assumes R[rel_rule]: "rel R x x'"
    assumes S[wp_rule]: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> refines S (D x) (D' x') \<rbrakk> \<Longrightarrow> refines S (F D x) (F' D' x')"
    shows "refines S (REC F x) (RECI V' P' F' x')"
    unfolding RECI_def
    apply wp
    apply rel
    by simp_all

      
  lemma refines_WHILE[wp_rule]: 
    assumes [rel_rule]: "rel R s s'"
    assumes [wp_rule]: "\<And>s s'. R s s' \<Longrightarrow> refines (=) (b s) (b' s')"
    assumes [wp_rule]: "\<And>s s'. \<lbrakk>R s s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> refines R (f s) (f' s')"
    shows "refines R (WHILE b f s) (WHILE b' f' s')"
    unfolding WHILE_def
    apply wp
    apply rel
    apply simp_all
    done
  

  lemma refines_WHILEI[wp_rule]: 
    assumes [rel_rule]: "rel R s s'"
    assumes [wp_rule]: "\<And>s s'. R s s' \<Longrightarrow> refines (=) (b s) (b' s')"
    assumes I: "\<And>s s'. R s s' \<Longrightarrow> I' s' \<Longrightarrow> I s"
    assumes [wp_rule]: "\<And>s s'. \<lbrakk>R s s'; I' s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> refines R (f s) (f' s')"
    shows "refines R (WHILEI V I b f s) (WHILEI V' I' b' f' s')"
    unfolding WHILEI_def
    apply wp
    apply rel
    apply (auto simp: pw_simp I)
    done

  lemma refines_WHILE_WHILEI[wp_rule]: 
    assumes [rel_rule]: "rel R s s'"
    assumes [wp_rule]: "\<And>s s'. R s s' \<Longrightarrow> refines (=) (b s) (b' s')"
    assumes [wp_rule]: "\<And>s s'. \<lbrakk>R s s'; I' s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> refines R (f s) (f' s')"
    shows "refines R (WHILE b f s) (WHILEI V' I' b' f' s')"
    unfolding WHILEI_def
    apply wp
    apply rel
    apply (auto simp: pw_simp)
    done

    
  subsubsection \<open>Fallback rules\<close>  
  text \<open>Rules for non-perfect matches of combinators\<close>
    
  lemma refines_return_spec[wp_rule]: "P x \<Longrightarrow> refines (=) (return x) (SPEC P)"
    by pw
    
  lemma refines_return_spec'[wp_rule]: "\<exists>x'. R x x' \<and> P x' \<Longrightarrow> refines R (return x) (SPEC P)"
    by pw
    
    
  lemma refines_let_bind[wp_rule]: 
    assumes "refines Rh (return v) m'"
    assumes "\<And>x x'. \<lbrakk>Rh x x'; x=v; is_res m' x'\<rbrakk> \<Longrightarrow> refines R (f x) (f' x')"
    shows "refines R (let x=v in f x) (do {x'\<leftarrow>m'; f' x'})"
    using assms
    by pw' metis
  
    

end
