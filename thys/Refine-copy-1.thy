theory Refine
imports Hoare_Logic
begin

  subsection \<open>Refinement\<close>  
  definition "refines m m' \<equiv> case (m,m') of 
      (_,FAIL) \<Rightarrow> True
    | (SPEC P,SPEC Q) \<Rightarrow> (\<forall>x. P x \<longrightarrow> Q x)  
    | _ \<Rightarrow> False
  "
        
  lemma refines_refl[simp]: "refines m m"
    and refines_antisym[sym]: "refines m m' \<Longrightarrow> refines m' m \<Longrightarrow> m=m'"
    and refines_trans[trans]: "refines m m' \<Longrightarrow> refines m' m'' \<Longrightarrow> refines m m''"
    unfolding refines_def
    apply (cases m; auto) []
    apply (cases m; cases m'; auto) []
    apply (cases m; cases m'; cases m''; auto) []
    done
    
  lemma pw_refines[pw_init]: "refines m m' \<longleftrightarrow> is_fail m' \<or> (\<not>is_fail m \<and> (\<forall>x. is_res m x \<longrightarrow> is_res m' x))"  
    unfolding refines_def
    apply (simp split: nres.splits)
    apply pw
    done
    
  lemma refines_return[wp_rule]: "x=y \<Longrightarrow> refines (RETURN x) (RETURN y)"  
    by pw
    
    
  lemma refines_assert_bind_right[wp_rule]: 
    assumes "P' \<Longrightarrow> refines m m'"
    shows "refines m (do { assert P'; m' })"  
    using assms by pw
  
  lemma refines_assert_bind_left[wp_rule]: 
    assumes "\<not>is_fail m' \<Longrightarrow> P"
    assumes "refines m m'"
    shows "refines (do { assert P; m }) (m')"  
    using assms by pw
    
  lemma refines_bind[wp_rule]:
    "\<lbrakk> refines m m'; 
      \<And>x. \<lbrakk>is_res m x; is_res m' x\<rbrakk> \<Longrightarrow> refines (f x) (f' x)
     \<rbrakk> \<Longrightarrow> refines (BIND m (\<lambda>x. f x)) (BIND m' (\<lambda>x. f' x))"
    by pw
    
  lemma refines_if[wp_rule]: 
    assumes "b \<longleftrightarrow> b'"
    assumes "b' \<Longrightarrow> refines m\<^sub>1 m\<^sub>1'"
    assumes "\<not>b' \<Longrightarrow> refines m\<^sub>2 m\<^sub>2'"
    shows "refines (if b then m\<^sub>1 else m\<^sub>2) (if b' then m\<^sub>1' else m\<^sub>2')"
    using assms by auto
  
  lemma refines_spec_iff: "refines m (SPEC Q) \<longleftrightarrow> wp m Q"
    by pw
  
  lemmas [wp_rule] = refines_spec_iff[THEN iffD2]  
    
  
  lemma refines_rec[wp_rule]: 
    assumes M: "nres_mono F"
    assumes S: "\<And>D D' x. \<lbrakk> \<And>x. refines (D x) (D' x) \<rbrakk> \<Longrightarrow> refines (F D x) (F' D' x)"
    shows "refines (REC F x) (REC F' x)"
    apply (rule REC_pointwise_induct[where F=F'])
    apply pw
    apply (subst REC_unfold[OF M])
    apply (rule S)
    .

  lemmas [wp_recursion_rule] = asm_rl[of "refines _ _"]
      
  lemma refines_WHILE[wp_rule]: 
    assumes "\<And>x. refines (b x) (b' x)"
    assumes "\<And>x. \<lbrakk>is_res (b x) True; is_res (b' x) True\<rbrakk> \<Longrightarrow> refines (f x) (f' x)"
    shows "refines (WHILE b f s) (WHILE b' f' s)"
    unfolding WHILE_def
    apply (intro wp_rule mono assms)
    apply simp_all
    done
    
    
    
  subsection \<open>Data Refinement\<close>
  
  definition rel:: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> bool" where "rel R \<equiv> R"
  
  definition "conc R m \<equiv> case m of FAIL \<Rightarrow> FAIL | SPEC P \<Rightarrow> SPEC (\<lambda>x'. \<exists>x. R x' x \<and> P x)"

  lemma pw_conc[pw_simp]:
    "conc R m = FAIL \<longleftrightarrow> m = FAIL"
    "is_res (conc R m) x' \<longleftrightarrow> (\<exists>x. R x' x \<and> is_res m x)"
    unfolding conc_def
    apply (cases m; simp)
    apply (cases m; simp add: pw_simp)
    done
  
  lemma conc_Id[simp]: "conc (=) = id"
    by rule pw
    
    
    
  definition "drefines R m m' \<equiv> refines m (conc R m')"
  
  lemma drefines_pw[pw_init]: "drefines R m m' 
    \<longleftrightarrow> is_fail m' \<or> (\<not>is_fail m \<and> (\<forall>x. is_res m x \<longrightarrow> (\<exists>x'. R x x' \<and> is_res m' x')))"
    unfolding drefines_def
    by pw
  
  lemma drefines_refines: "drefines (=) = refines" 
    unfolding drefines_def by simp
  
  lemma drefines_refl[simp]: "drefines (=) m m"  
    and drefines_trans[trans]: "drefines R\<^sub>1 m\<^sub>1 m\<^sub>2 \<Longrightarrow> drefines R\<^sub>2 m\<^sub>2 m\<^sub>3 \<Longrightarrow> drefines (R\<^sub>1 OO R\<^sub>2) m\<^sub>1 m\<^sub>3"
    apply pw
    apply (pw; blast)
    done
    
 
  lemma drefines_FAIL[wp_rule]: "drefines R m FAIL" by pw     
    
  lemma drefines_RETURN[wp_rule]: "rel R x x' \<Longrightarrow> drefines R (RETURN x) (RETURN x')"
    unfolding rel_def by pw

  lemma drefines_SPEC[wp_rule]: "\<lbrakk>\<And>x. P x \<Longrightarrow> \<exists>x'. R x x' \<and> P' x'  \<rbrakk> \<Longrightarrow> drefines R (SPEC P) (SPEC P')"  
    by pw

  lemma drefines_assert_bind_right[wp_rule]: 
    assumes "P' \<Longrightarrow> drefines R m m'"
    shows "drefines R m (do { assert P'; m' })"  
    using assms by pw
  
  lemma drefines_assert_bind_left[wp_rule]: 
    assumes "\<not>is_fail m' \<Longrightarrow> P"
    assumes "drefines R m m'"
    shows "drefines R (do { assert P; m }) (m')"  
    using assms by pw
    
      
  lemma drefines_BIND[wp_rule]:
    assumes "drefines Rh m m'"
    assumes "\<And>x x'. \<lbrakk> Rh x x'; is_res m x; is_res m' x' \<rbrakk> \<Longrightarrow> drefines R (f x) (f' x')"
    shows "drefines R (BIND m (\<lambda>x. f x)) (BIND m' (\<lambda>x'. f' x'))"
    using assms
    apply pw
    by metis
    
    
    
  lemma drefines_if[wp_rule]: 
    assumes "b = b'"
    assumes "b' \<Longrightarrow> drefines R m\<^sub>1 m\<^sub>1'"
    assumes "\<not>b' \<Longrightarrow> drefines R m\<^sub>2 m\<^sub>2'"
    shows "drefines R (if b then m\<^sub>1 else m\<^sub>2) (if b' then m\<^sub>1' else m\<^sub>2')"  
    using assms by simp

  lemma drefines_let[wp_rule]:
    assumes "\<And>x x'. x=v \<Longrightarrow> x'=v' \<Longrightarrow> drefines R (f x) (f' x')"
    shows "drefines R (let x=v in f x) (let x'=v' in f' x')"
    using assms unfolding Let_def by simp  
    
  lemma drefines_case_prod[wp_rule]: 
    assumes "\<And>a b a' b'. \<lbrakk> p=(a,b); p'=(a',b') \<rbrakk> \<Longrightarrow> drefines R (f a b) (f' a' b')"
    shows "drefines R (case p of (a,b) \<Rightarrow> f a b) (case p' of (a',b') \<Rightarrow> f' a' b')"  
    using assms by (auto split: prod.split)  
  
  lemma drefines_rec[wp_rule]: 
    assumes M: "nres_mono F"
    assumes R: "rel R x x'"
    assumes S: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> drefines S (D x) (D' x') \<rbrakk> \<Longrightarrow> drefines S (F D x) (F' D' x')"
    shows "drefines S (REC F x) (REC F' x')"
  proof -
  
    have "\<forall>x. R x x' \<longrightarrow> drefines S (REC F x) (REC F' x')"
      apply (rule REC_pointwise_induct[where F=F'])
      apply pw
      apply (subst REC_unfold[OF M])
      apply (blast intro: S[unfolded rel_def])
      done
    thus ?thesis using R unfolding rel_def by blast
  qed

  lemma drefines_recI[wp_rule]: 
    assumes M: "nres_mono F"
    assumes R: "rel R x x'"
    assumes S: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> drefines S (D x) (D' x') \<rbrakk> \<Longrightarrow> drefines S (F D x) (F' D' x')"
    assumes P: "\<And>x x'. R x x' \<Longrightarrow> P' x' \<Longrightarrow> P x"
    shows "drefines S (RECI V P F x) (RECI V' P' F' x')"
    unfolding RECI_def
    apply (wp_step+; (rule R)?)
    subgoal using P by blast
    subgoal using S by blast
    subgoal using M[THEN nres_monoD] .
    .
  
  lemma drefines_rec_recI[wp_rule]: 
    assumes M: "nres_mono F"
    assumes R: "rel R x x'"
    assumes S: "\<And>D D' x x'. \<lbrakk> R x x'; \<And>x x'. rel R x x' \<Longrightarrow> drefines S (D x) (D' x') \<rbrakk> \<Longrightarrow> drefines S (F D x) (F' D' x')"
    shows "drefines S (REC F x) (RECI V' P' F' x')"
    unfolding RECI_def
    apply (wp_step+; (rule R)?)
    subgoal using S by blast
    subgoal using M[THEN nres_monoD] .
    .

  lemmas [wp_recursion_rule] = asm_rl[of "drefines _ _ _"]
    
      
  lemma drefines_WHILE[wp_rule]: 
    assumes "rel R s s'"
    assumes "\<And>s s'. R s s' \<Longrightarrow> drefines (=) (b s) (b' s')"
    assumes "\<And>s s'. \<lbrakk>R s s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> drefines R (f s) (f' s')"
    shows "drefines R (WHILE b f s) (WHILE b' f' s')"
    unfolding WHILE_def
    apply (rule wp_rule mono assms | fact | assumption | simp add: rel_def)+
    done
  

  lemma drefines_WHILEI[wp_rule]: 
    assumes "rel R s s'"
    assumes "\<And>s s'. R s s' \<Longrightarrow> drefines (=) (b s) (b' s')"
    assumes "\<And>s s'. R s s' \<Longrightarrow> I' s' \<Longrightarrow> I s"
    assumes "\<And>s s'. \<lbrakk>R s s'; I' s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> drefines R (f s) (f' s')"
    shows "drefines R (WHILEI V I b f s) (WHILEI V' I' b' f' s')"
    unfolding WHILEI_def
    apply wp_step+
    apply (auto simp: pw_simp assms)
    done

  lemma drefines_WHILE_WHILEI[wp_rule]: 
    assumes "rel R s s'"
    assumes "\<And>s s'. R s s' \<Longrightarrow> drefines (=) (b s) (b' s')"
    assumes "\<And>s s'. \<lbrakk>R s s'; I' s'; is_res (b s) True; is_res (b' s') True\<rbrakk> \<Longrightarrow> drefines R (f s) (f' s')"
    shows "drefines R (WHILE b f s) (WHILEI V' I' b' f' s')"
    unfolding WHILEI_def
    apply wp_step+
    apply (auto simp: pw_simp assms)
    done


end
