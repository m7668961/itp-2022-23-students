section \<open>Nondeterministic Result Monad\<close>
theory NRes
imports Essential_Lib Flat_CCPO
begin
  subsection \<open>Type Definition\<close>
  text \<open>
    The result of a nondeterministic computation
  \<close>
  datatype 'a nres = 
    SPEC (the_spec: "'a \<Rightarrow> bool") \<comment> \<open>May return any value that satisfies a predicate\<close>
  | FAIL  \<comment> \<open>May fail\<close>

  subsection \<open>Pointwise Reasoning\<close>
  text \<open>Many theorems about the result monad can easily be 
    solved by transforming them to pointwise proposition over
    possible results.\<close>
  
  definition "is_fail m \<longleftrightarrow> m=FAIL"
  definition "is_res m x \<longleftrightarrow> (m\<noteq>FAIL \<and> the_spec m x)"
    
  lemma pw_basic:
    "is_res (SPEC P) x \<longleftrightarrow> P x"
    "\<not>is_res FAIL x"
    "is_fail m \<longleftrightarrow> m=FAIL"
    unfolding is_res_def is_fail_def by auto
  
  lemma pw_eq_iff: "m=m' \<longleftrightarrow> ((is_fail m \<longleftrightarrow> is_fail m') \<and> (\<forall>x. is_res m x \<longleftrightarrow> is_res m' x))"
    by (cases m; cases m'; auto simp add: pw_basic)
    

  subsubsection \<open>Automation\<close>
  text \<open>We first rewrite with initialization rules, that convert
    a property to a pointwise property, and then with 
    simplification rules, which syntactically decompose pointwise 
    properties.
  \<close>

  named_theorems pw_init \<open>Pointwise reasoning: initialization\<close>
  named_theorems pw_simp \<open>Pointwise reasoning: simplification\<close>

  lemmas [pw_simp] = pw_basic
  lemmas [pw_init] = pw_eq_iff
  
  method pw = ((simp only: pw_init)?; auto simp: pw_simp)
  method pw' = ((simp only: pw_init)?; simp add: pw_simp)
      
  subsection \<open>Basic Combinators\<close>

  subsubsection \<open>Return and Bind\<close>
  text \<open>Deterministically return a result\<close>
  definition "RETURN x \<equiv> SPEC (\<lambda>r. r=x)"
  
  text \<open>Sequential composition: apply \<open>f\<close> to every possible result of \<open>m\<close>.\<close>
  definition "BIND m f \<equiv> case m of 
    SPEC P \<Rightarrow> (if \<exists>x. P x \<and> f x = FAIL then FAIL
              else SPEC (\<lambda>r. \<exists>x Q. P x \<and> f x = SPEC Q \<and> Q r))
  | FAIL \<Rightarrow> FAIL"

  lemma BIND_simps:
    "BIND FAIL f = FAIL"
    "BIND (SPEC P) f = (
      if \<exists>x. P x \<and> f x = FAIL then FAIL
      else SPEC (\<lambda>r. \<exists>x Q. P x \<and> f x = SPEC Q \<and> Q r)
    )"  
    unfolding BIND_def by auto
  
  
  lemma pw_RETURN[pw_simp]:
    "RETURN x \<noteq> FAIL"
    "is_res (RETURN x) y \<longleftrightarrow> x=y"
    unfolding RETURN_def by pw+

  lemma pw_BIND[pw_simp]:
    "BIND m f = FAIL \<longleftrightarrow> m=FAIL \<or> (\<exists>y. is_res m y \<and> f y = FAIL)"
    "is_res (BIND m f) x \<longleftrightarrow> (\<forall>y. is_res m y \<longrightarrow> \<not>is_fail (f y)) \<and> (\<exists>y. is_res m y \<and> is_res (f y) x)"
    unfolding BIND_def
    apply (auto simp: pw_simp is_fail_def split: nres.split)
    by (meson is_res_def nres.exhaust_sel)


  text \<open>\<^const>\<open>RETURN\<close> and \<^const>\<open>BIND\<close> satisfy the monad laws:\<close>  
  lemma return_bind[simp]: "BIND (RETURN x) f = f x"
    by pw
  
  lemma bind_return[simp]: "BIND m RETURN = m"  
    by pw

  lemma bind_bind[simp]: "BIND (BIND m f) g = BIND m (\<lambda>x. BIND (f x) g)"
    by pw

  text \<open>There are also laws for the interaction of \<^const>\<open>BIND\<close> and \<^const>\<open>FAIL\<close>\<close>
  lemma fail_bind[simp]: "BIND FAIL f = FAIL" by pw
  
  text \<open>Note that \<^term>\<open>SPEC (\<lambda>_. False)\<close> is special, in that it specifies a program
    with no possible results. It's the dual to \<^const>\<open>FAIL\<close>. We call it \<open>EMPTY\<close>: \<close>
  abbreviation "EMPTY \<equiv> SPEC (\<lambda>_. False)"  
    
  text \<open>\<^const>\<open>EMPTY\<close> can be an un-intuitive corner-case for some rules, 
    like the following, that only holds for \<^term>\<open>m\<noteq>EMPTY\<close>:\<close>  
  lemma bind_fail: "BIND m (\<lambda>_. FAIL) = FAIL \<longleftrightarrow> m\<noteq>EMPTY" by pw

  lemma bind_empty[simp]: "BIND EMPTY f = EMPTY" by pw
      

    
  subsubsection \<open>Syntax\<close>  

  text \<open>We establish some syntax\<close>
    
  (*abbreviation (input) bind_doI where "bind_doI m (\<lambda>x. f x) \<equiv> BIND m (\<lambda>x. f x)"*)
  abbreviation then_doI where "then_doI m f \<equiv> BIND m (\<lambda>_. f)"

  nonterminal doI_binds and doI_bind
  syntax
    "_doI_block" :: "doI_binds \<Rightarrow> 'a" ("do {//(2  _)//}" [12] 62)
    "_doI_bind"  :: "[pttrn, 'a] \<Rightarrow> doI_bind" ("(2_ \<leftarrow>/ _)" 13)
    "_doI_let" :: "[pttrn, 'a] \<Rightarrow> doI_bind" ("(2let _ =/ _)" [1000, 13] 13)
    "_doI_then" :: "'a \<Rightarrow> doI_bind" ("_" [14] 13)
    "_doI_final" :: "'a \<Rightarrow> doI_binds" ("_")
    "_doI_cons" :: "[doI_bind, doI_binds] \<Rightarrow> doI_binds" ("_;//_" [13, 12] 12)
    (*"_thenM" :: "['a, 'b] \<Rightarrow> 'c" (infixr "\<then>" 54)*)

  syntax (ASCII)
    "_doI_bind" :: "[pttrn, 'a] \<Rightarrow> doI_bind" ("(2_ <-/ _)" 13)
    (*"_thenM" :: "['a, 'b] \<Rightarrow> 'c" (infixr ">>" 54)*)

  translations
    "_doI_block (_doI_cons (_doI_then t) (_doI_final e))"
      \<rightleftharpoons> "CONST then_doI t e"

    "_doI_block (_doI_cons (_doI_bind p t) (_doI_final e))"
      \<rightleftharpoons> "CONST BIND t (\<lambda>p. e)"

    "_doI_block (_doI_cons (_doI_let p t) bs)"
      \<rightleftharpoons> "let p = t in _doI_block bs"

    "_doI_block (_doI_cons b (_doI_cons c cs))"
      \<rightleftharpoons> "_doI_block (_doI_cons b (_doI_final (_doI_block (_doI_cons c cs))))"

    "_doI_cons (_doI_let p t) (_doI_final s)"
      \<rightleftharpoons> "_doI_final (let p = t in s)"

    "_doI_block (_doI_final e)" \<rightharpoonup> "e"
  
    "(CONST then_doI m n)" \<rightharpoonup> "(CONST BIND m (\<lambda>_. n))"
    
   
  notation RETURN ("return _" 20)
  notation SPEC (binder "spec " 10)
  
      
  subsubsection \<open>Assert\<close>    
  text \<open>An assertion returns unit if the predicate holds, and fails otherwise.
    Note that returning unit is the functional way of having a function with no return value (e.g. void in C/C++).
  \<close>
  definition "ASSERT P \<equiv> if P then return () else FAIL"
  
  lemma pw_ASSERT[pw_simp]: 
    "ASSERT P = FAIL \<longleftrightarrow> \<not>P"  
    "is_res (ASSERT P) x \<longleftrightarrow> P"
    unfolding ASSERT_def
    apply pw+
    done
      
  notation ASSERT ("assert _" 20)
    
  subsubsection \<open>Recursion\<close>  
  text \<open>For recursion, we define a fixed-point combinator 
    utilizing a chain-complete partial order (CCPO).
    While CCPO's are advanced material, we try to capture the intuition
    below.
  \<close>
  

  
  context
  begin
    interpretation FR: flat_rec FAIL .
  
    text \<open>The recursion combinator models recursive functions. 
      The recursive call is the first parameter to the function body, and
      the argument is the second parameter.
    \<close>
    definition REC :: "(('a \<Rightarrow> 'b nres) \<Rightarrow> 'a \<Rightarrow> 'b nres) \<Rightarrow> 'a \<Rightarrow> 'b nres" where "REC \<equiv> FR.REC"
    
    text \<open>The function body must be monotone. Intuitively, this means that 
      it terminates at less arguments if the recursive call terminates at less arguments.
    \<close>
    definition nres_mono :: "(('a \<Rightarrow> 'b nres) \<Rightarrow> 'a \<Rightarrow> 'b nres) \<Rightarrow> bool" where "nres_mono = FR.mono"
    
    text \<open>A recursive function can be unfolded, i.e., the body is inlined once.\<close>
    lemma REC_unfold: "nres_mono F \<Longrightarrow> REC F = F (REC F)"
      unfolding REC_def nres_mono_def using FR.REC_unfold .

    text \<open>Pointwise properties of recursive functions can be proved by well-founded induction 
      on the arguments.\<close>
    lemma REC_wf_induct: 
      assumes WF: "wf V"
      assumes MONO: "nres_mono F"
      assumes STEP: "\<And>x D. \<lbrakk>\<And>y. \<lbrakk>(y,x)\<in>V\<rbrakk> \<Longrightarrow> P y (D y) \<rbrakk> \<Longrightarrow> P x (F D x)"
      shows "P x (REC F x)"
      using assms unfolding nres_mono_def REC_def 
      using FR.REC_wf_induct by metis

    text \<open>For pointwise properties, which hold at non-terminating arguments, we
      can use the following induction scheme, which does not require a well-founded ordering.\<close>
    lemma REC_pointwise_induct:  
      assumes BOT: "\<And>x. P x FAIL"
      assumes STEP: "\<And>D x. (\<And>y. P y (D y)) \<Longrightarrow> P x (F D x)"
      shows "P x (REC F x)"
      using assms REC_def
      using FR.REC_pointwise_induct by metis
      
  
    subsubsection \<open>Monotonicity\<close>  
    text \<open>Function bodies have to be monotone, i.e., when invoked with a recursive call 
      that terminates at less arguments, it must terminate at less arguments.
      
      This property can be established syntactically for the standard combinators
      programs are build from.
    \<close>
    
    
    named_theorems mono \<open>Monotonicity theorems for nres\<close> 
    
    definition "flat_le \<equiv> FR.le"
    
    lemma pw_flat_le[pw_init]: "flat_le m m' \<longleftrightarrow> is_fail m \<or> (\<not>is_fail m' \<and> (\<forall>x. is_res m x \<longleftrightarrow> is_res m' x))"  
      apply (cases m; cases m'; auto simp: pw_simp)
      apply (auto simp: flat_le_def FR.le_def)
      done
    
    lemma mono_alt: "nres_mono F = (\<forall>x y. fun_ord flat_le x y \<longrightarrow> fun_ord flat_le (F x) (F y))"
      unfolding nres_mono_def monotone_def FR.le_def flat_le_def ..
    
    definition nres_mono' :: "(('a \<Rightarrow> 'b nres) \<Rightarrow> 'c nres) \<Rightarrow> bool"
      where "nres_mono' F \<equiv> \<forall>D D'. fun_ord flat_le D D' \<longrightarrow> flat_le (F D) (F D')"
      
    lemma nres_monoI[mono]: 
      assumes "\<And>x. nres_mono' (\<lambda>D. F D x)"
      shows "nres_mono F"
      using assms unfolding mono_alt nres_mono'_def fun_ord_def
      by simp
    
    lemma nres_monoD: "nres_mono F \<Longrightarrow> nres_mono' (\<lambda>D. F D x)"
      unfolding mono_alt nres_mono'_def fun_ord_def
      by simp
      
    lemma mono_const[mono]: 
      "nres_mono' (\<lambda>_. c)"  
      "nres_mono' (\<lambda>D. D y)"
      unfolding nres_mono'_def fun_ord_def
      apply pw+
      done
      
    lemma mono_If[mono]: "
      nres_mono' (\<lambda>D. F D) \<Longrightarrow> nres_mono' (\<lambda>D. G D) \<Longrightarrow>
      nres_mono' (\<lambda>D. if b then F D else G D)"  
      unfolding nres_mono'_def fun_ord_def
      apply pw
      done
      
    lemma mono_BIND[mono]: "
      nres_mono' (\<lambda>D. F D) \<Longrightarrow> 
      (\<And>y. nres_mono' (\<lambda>D. G D y)) \<Longrightarrow> 
      nres_mono' (\<lambda>D. BIND (F D) (G D))"  
      unfolding nres_mono'_def fun_ord_def
      apply pw
      apply blast
      apply (smt (verit, ccfv_threshold))
      apply (smt (verit, ccfv_threshold))
      apply (smt (verit, ccfv_threshold))
      done
      

    lemma mono_REC[mono]: 
      assumes "\<And>D. nres_mono (F D)"
      assumes "\<And>DD x. nres_mono' (\<lambda>D. F D DD x)"
      shows "nres_mono' (\<lambda>D. REC (F D) x)"  
      using assms
      unfolding nres_mono'_def flat_le_def REC_def
      apply clarsimp
      subgoal for D D'
        apply (rule FR.REC_mono[of F D D', unfolded fun_ord_def, rule_format])
        subgoal by (simp add: fun_ord_def nres_mono_def monotone_def)
        subgoal by blast
        done
      done
      
    lemma mono_case_prod[mono]:
      assumes "\<And>a b. nres_mono' (\<lambda>D. F D a b)"
      shows "nres_mono' (\<lambda>D. case p of (a,b) \<Rightarrow> F D a b)"
      using assms
      unfolding nres_mono'_def fun_ord_def
      apply (cases p) by simp
    
    lemma mono_Let[mono]:
      assumes "\<And>x. nres_mono' (\<lambda>D. F D x)"
      shows "nres_mono' (\<lambda>D. let x=v in F D x)"
      using assms
      unfolding nres_mono'_def fun_ord_def
      by simp
      
      
      
            
  end
    
    
  subsubsection \<open>While Loop\<close>
  text \<open>The while loop derived from recursion\<close>
  
  definition "WHILE b f s \<equiv> REC (\<lambda>W s. do {r\<leftarrow>b s; if r then do {s\<leftarrow>f s; W s} else return s}) s"
  
  lemma WHILE_unfold: "WHILE b f s = do {r\<leftarrow>b s; if r then do {s\<leftarrow>f s; WHILE b f s} else return s}"
    unfolding WHILE_def
    apply (subst REC_unfold)
    apply (intro mono)
    ..
  
  lemma mono_WHILE[mono]: 
    assumes "\<And>DD x. nres_mono' (\<lambda>D. b D x)"
    assumes "\<And>DD x y. nres_mono' (\<lambda>D. f D x)"
    shows "nres_mono' (\<lambda>D. WHILE (b D) (f D) s)"
    using assms
    unfolding WHILE_def
    by (intro mono)
    

  subsection \<open>Miscellaneous\<close>
  experiment
  begin
    text \<open>Illustration why more simple-minded definition of recursion does not work as expected.
      For example, we could try to define recursion as finite unfoldings, along the lines
      \<^term>\<open>(B ^^ n x) bot x\<close> for a large enough number \<open>n x\<close>, which depends on the argument.
      
      While this works for deterministic programs, we get some unexpected results on 
      nondeterministic programs. The following function takes two arguments: a Boolean flag and a number.
      If the flag is true, it recursively calls itself with false and an arbitrary, non-deterministically 
      choosen number. If the flag is false, it recursively counts down the number argument and terminates.
    \<close>
    
    definition ubrec_body :: "(bool\<times>nat \<Rightarrow> unit nres) \<Rightarrow> bool\<times>nat \<Rightarrow> unit nres"
    where "ubrec_body \<equiv> \<lambda>ubrec (b,n). 
      if b then do {
        n \<leftarrow> spec _. True;
        ubrec (False,n)
      } else if n=0 then return ()
      else ubrec (False,n-1)"
    
    definition ubrec :: "(bool\<times>nat) \<Rightarrow> unit nres" where 
    "ubrec \<equiv> REC ubrec_body"
  
    
    
    text \<open>Clearly, the program terminates as we can find a well-founded ordering:\<close>
    definition "ubrec_ord \<equiv> (measure (\<lambda>b. if b then 1 else 0) <*lex*> less_than)"

    lemma "ubrec (b,n) = RETURN ()"
      unfolding ubrec_def ubrec_body_def
      apply (rule REC_wf_induct[where V=ubrec_ord and P="\<lambda>_ m. m=RETURN ()"])
      apply (simp add: ubrec_ord_def; blast) 
      apply (intro mono)
      apply (clarsimp simp: ubrec_ord_def)
      apply pw
      done
      
    text \<open>However, if the flag is true, we won't find any number of unfoldings that 
      would be sufficient to show termination:\<close>
      
    lemma "\<nexists>n. (ubrec_body ^^ n) (\<lambda>_. FAIL) (True,nn) = RETURN ()"  
    proof -
      text \<open>For argument \<open>(False,n)\<close> we fail for \<open>\<le>n\<close> unfoldings, in particular for \<open>n\<close> unfoldings:\<close>
      have "(ubrec_body^^n) (\<lambda>_. FAIL) (False,n) = FAIL" for n 
        apply (induction n)
        apply (auto simp: ubrec_body_def)
        done
      text \<open>Thus, if the flag is true, we can always choose a number for which our remaining unfoldings will fail:\<close>
      then have "(ubrec_body ^^ n) (\<lambda>_. FAIL) (True,nn) = FAIL" for n
        apply (cases n; simp)
        apply (subst ubrec_body_def)
        by pw
      thus ?thesis by pw
    qed  

    
        
  end    

  
end
